<form id="formscout" method="POST" action="{{ route('register.scout') }}">
        @csrf

        <input class="currentTab" name="currentTab" type="hidden" value="{{ old('currentTab') }}">
                       
        {{-- {{ $errors }} --}}
        <div class="col-12 row px-0">
            {{-- FirstName--}}
            <div class="col-12 col-sm-6 px-0 pr-sm-1 form-group">
                    <label for="firstname" class="col-12 ">{{ __('auth.firstname') }}</label>
                    <div class="col-12">
                        <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}">

                        @if ($errors->has('firstname'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>

            {{-- Lastname --}}
            <div class="col-12 col-sm-6 px-0 pl-sm-1 form-group">
                    <label for="lastname" class="col-12 ">{{ __('auth.lastname') }}</label>
                    <div class="col-12">
                        <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}">

                        @if ($errors->has('lastname'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-12 ">
                {{ __('auth.email') }} 
                {{-- <span class="text-gray-700"> ({{ __('auth.corporative-email') }}) </span>  --}}
            </label>

            <div class="col-12">
                <input id="email" data-toggle="tooltip" title="{{ trans('auth.tooltipEmail') }}" type="email" class="tooltipFocus form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        {{-- Phone --}}
        <div class="form-group">
            <label class="col-12 ">{{ __('auth.phone') }}</label>
            <div class="col-12">
                <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                    <div style="height:2.4rem; margin-left: .35rem" class="absolute text-lg text-gray-700 left-0 flex items-center">
                        <span> + </span>
                    </div>
                    <input id="phone" type="text" data-toggle="tooltip" title="{{ trans('auth.tooltipPhone') }}" autocomplete="off" maxlength="30" placeholder="" class="pl-4 tooltipFocus form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>  

        <div class="col-12 row px-0">
            <label for="entity_id" class="col-12 ">{{ __('auth.entity') }}</label>
            {{-- Type Entity --}}
            <div class="col-12 col-sm-6 col-lg-5 form-group">
                <div class="col-12 px-0">    
                    <select name="entity_type_id" id="entity_type_id" class="{{ $errors->has('entity_type_id') ? 'form-control is-invalid' : 'form-control' }}" placeholder="{{ trans('auth.typeEntity') }}">
                        {{-- <option value=""> {{ trans('auth.typeEntity') }} </option> --}}
                        @foreach ($entitiestype as $type)
                            <option value="{{ $type['id'] }}" {{ old('entity_type_id') == $type['id'] ? 'selected' : '' }} > {{ trans('fields.entitiesType.'. $type['name']) }} </option>
                        @endforeach
                    </select>
                    {{-- {!! Form::select('entity_type_id',$entitiestype, null, array('class' => $errors->has('entity_type_id') ? 'form-control is-invalid' : 'form-control', 'placeholder' => trans('auth.typeEntity') )) !!} --}}
                    @if ($errors->has('entity_type_id'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('entity_type_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-7 form-group">    
                <div class="col-12 px-0">
                    <select name="entity_id" id="entity_id" class="{{ $errors->has('entity_id') ? 'form-control is-invalid' : 'form-control' }}" data-placeholder="Club">
                    </select>
                    {{-- {!! Form::select('entity_id',$entities, null, array('id' => 'entity_id', 'class' => "form-control {{ $errors->has('entity_id') ? ' is-invalid' : '' }}", 'placeholder' => 'Club' )) !!} --}}
                    @if ($errors->has('entity_id'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('entity_id') }}</strong>
                    </span>
                    @endif
                </div>
                
            </div>

            </div>
            <div class="otherClubWrapper hidden pt-2 row col-12">
                    
                    {{-- New Entity --}}
                    <div class="col-12 px-0 form-group">

                        <input type="text" class="form-control{{ $errors->has('otherClub') ? ' is-invalid' : '' }}" name="otherClub" value="{{ old('otherClub') }}" placeholder="{{ __('profile.athlete.nameOtherClub') }}">
                        @if ($errors->has('otherClub'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('otherClub') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

        <div class="form-group">
            <label for="password" class="col-12 ">{{ __('auth.password') }}</label>

            <div class="col-12">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-12 ">{{ __('auth.confirmPassword') }}</label>

            <div class="col-12">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            </div>
        </div>

        <div class="form-group">
            <label class="col-12 flex items-center font-normal">
                <input name="tyc" class="mr-2 form-checkbox" type="checkbox" {{ old('tyc') ? ' checked' : '' }}>
                <span class="">
                    {!! trans('auth.acceptTerms') !!}
                </span>
            </label>
            @if ($errors->has('tyc'))
            <div class="col-12"> 
                <span class="block invalid-feedback">
                    <strong>{{ $errors->first('tyc') }}</strong>
                </span>
            </div>
            @endif
        </div>

        <div class="form-group mt-4 mb-0">
            <div class="col-12">
                <button type="submit" class="btn btn-primary hover:shadow-lg rounded">
                    {{ __('auth.registerBtn') }}
                </button>
            </div>
        </div>

    </form>