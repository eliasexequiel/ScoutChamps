<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {!! trans('titles.app') !!} </title>

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">

</head>

<body class="overflow-x-scroll">

    @include('partials.nav-welcome')
    <div id="app" class="section-wrapper" style="min-height: calc(100vh - 160px);">
        <div class="w-full py-2 flex justify-center">
            <div class="w-full max-w-xs">
                <div class="card-p">
                    
                    <div class="w-full">
                        <h3 class="mb-4 font-semibold">{{ trans('auth.resetPassword') }}</h3>
                    
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email">{{ trans('auth.email') }}</label>

                            <div class="col-12 px-0">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row pt-2">
                            <div class="col-12 px-0">
                                <button type="submit" class="btn btn-primary rounded">
                                    {{ __('auth.sendResetLink') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@include('partials.footer')
<script src="{{ mix('/js/app.js') }}"></script>
</body>

</html>