@extends('layouts.app')

@section('template_title')
    Dashboard
@endsection

@section('template_fastload_css')
@endsection

@section('content')

    <div class="w-full px-6">
        <div class="mt-2 mx-auto w-11/12 sm:w-4/5 md:w-1/2">
            <div class="card">
                
                <div class="card-body">
                    <h4 class="mb-4 font-bold">{{ trans('auth.adviceVerifyEmailTitle') }}</h4>

                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ trans('auth.adviceVerifyEmailLinkSent') }}
                        </div>
                    @endif

                    {{ trans('auth.adviceVerifyEmail') }}
                    {{ trans('auth.adviceVerifyEmailText2') }}, <a href="{{ route('verification.resend') }}">{{ trans('auth.adviceVerifyEmailLink') }}</a>.
                </div>
            </div>
        </div>
    </div>


@endsection

@section('footer_scripts')
@endsection