<form id="formatleta" method="POST" action="{{ route('register.athlete') }}">
        @csrf

        <input class="currentTab" name="currentTab" type="hidden" value="{{ old('currentTab') }}">
                       
        <div class="col-12 row px-0">
        {{-- FirstName--}}
        <div class="col-12 col-sm-6 px-0 pr-sm-1 form-group">
                <label for="firstname" class="col-12 ">{{ __('auth.firstname') }}</label>
                <div class="col-12">
                    <input id="firstname" type="text" autocomplete="off" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}">

                    @if ($errors->has('firstname'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                </div>
        </div>

        {{-- Lastname --}}
        <div class="col-12 col-sm-6 px-0 pl-sm-1 form-group">
                <label for="lastname" class="col-12 ">{{ __('auth.lastname') }}</label>
                <div class="col-12">
                    <input id="lastname" type="text" autocomplete="off" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}">

                    @if ($errors->has('lastname'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif
                </div>
        </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-12 ">{{ __('auth.email') }}</label>

            <div class="col-12">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-12 ">{{ __('auth.password') }}</label>

            <div class="col-12">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-12 ">{{ __('auth.confirmPassword') }}</label>

            <div class="col-12">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
            </div>
        </div>


        <div class="col-12 row px-0">
        {{-- Phone --}}
        <div class="col-12 col-sm-6 px-0 pr-sm-1 form-group">
            <label for="password" class="col-12 ">{{ __('auth.phone') }}</label>
            <div class="col-12">
                <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                    <div style="height:2.4rem; margin-left: .35rem" class="absolute text-lg text-gray-700 left-0 flex items-center">
                        <span> + </span>
                    </div>
                    <input id="phone" type="text" autocomplete="off" maxlength="30" placeholder="" class="pl-4 form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}">
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        {{-- Data of birth --}}
        <div class="col-12 col-sm-6 px-0 pl-sm-1 form-group">
            <label for="password" class="col-12 ">{{ __('auth.dob') }}</label>
            <div class="col-12">
                <input id="dob" type="text" data-position="top left" autocomplete="off" placeholder="{{ app()->getLocale() == "en" ? 'mm/dd/yyyy' : 'dd/mm/yyyy' }}" maxlength="10" class="datepicker-custom form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" value="{{ old('date_of_birth') }}">

                @if ($errors->has('date_of_birth'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('date_of_birth') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        </div>

        <div class="blockTutor hidden">

        <input id="age" name="age" type="hidden">    

        <div class="col-12 row px-0">
            {{-- Firstname Tutor --}}
            <div class="col-12 col-sm-6 px-0 pr-sm-1 form-group">
                    <label for="password" class="col-12 ">{{ __('auth.firstnameTutor') }}</label>
                    <div class="col-12">
                        <input id="firstnameTutor" type="text" autocomplete="off" class="form-control{{ $errors->has('firstnameTutor') ? ' is-invalid' : '' }}" name="firstnameTutor" value="{{ old('firstnameTutor') }}">

                        @if ($errors->has('firstnameTutor'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('firstnameTutor') }}</strong>
                            </span>
                        @endif
                </div>
            </div>

            {{-- lastnameTutor --}}
            <div class="col-12 col-sm-6 px-0 pl-sm-1 form-group">
                    <label for="password" class="col-12 ">{{ __('auth.lastnameTutor') }}</label>
                    <div class="col-12">
                        <input id="lastnameTutor" type="text" autocomplete="off" class="form-control{{ $errors->has('lastnameTutor') ? ' is-invalid' : '' }}" name="lastnameTutor" value="{{ old('lastnameTutor') }}">

                        @if ($errors->has('lastnameTutor'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('lastnameTutor') }}</strong>
                            </span>
                        @endif
                </div>
            </div>
        </div>

        {{-- Email Tutor --}}
        <div class="form-group">
                <label for="password" class="col-12 ">{{ __('auth.emailTutor') }}</label>
                <div class="col-12">
                    <input id="emailTutor" type="text" autocomplete="off" class="form-control{{ $errors->has('emailTutor') ? ' is-invalid' : '' }}" name="emailTutor" value="{{ old('emailTutor') }}">

                    @if ($errors->has('emailTutor'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('emailTutor') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

        {{-- phoneTutor --}}
        <div class="form-group">
                <label for="password" class="col-12 ">{{ __('auth.phoneTutor') }}</label>
                <div class="col-12">
                        <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                            <div style="height:2.4rem; margin-left: .35rem" class="absolute text-lg text-gray-700 left-0 flex items-center">
                                <span> + </span>
                            </div>
                            <input id="phoneTutor" type="text" maxlength="30" placeholder="" autocomplete="off" class="pl-4 form-control {{ $errors->has('phoneTutor') ? ' is-invalid' : '' }}" name="phoneTutor" value="{{ old('phoneTutor') }}">
                            @if ($errors->has('phoneTutor'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phoneTutor') }}</strong>
                                </span>
                            @endif
                        </div>
            </div>
        </div>

    </div>
    <div class="form-group">
        <label class="col-12 flex items-center font-normal">
            <input name="tyc" class="mr-2 form-checkbox" type="checkbox" {{ old('tyc') ? ' checked' : '' }}>
            <span class="">
                {!! trans('auth.acceptTerms') !!}
            </span>
        </label> 
        @if ($errors->has('tyc'))
        <div class="col-12"> 
            <span class="block invalid-feedback">
                <strong>{{ $errors->first('tyc') }}</strong>
            </span>
        </div>
        @endif
    </div>



        <div class="form-group mb-0">
            <div class="col-12">
                <button type="submit" class="btn btn-primary hover:shadow-lg rounded">
                    {{ __('auth.registerBtn') }}
                </button>
            </div>
        </div>

    </form>