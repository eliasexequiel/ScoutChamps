<div class="mt-4 sm:mt-0 card-p w-full overflow-hidden">

    <h5 class="font-bold mb-2"> {{ trans('profile.trayectory') }} </h5>
    {{-- CLUBS ATHLETE / SCOUT --}}
    <div class="w-full pt-2" style="max-height: 24rem;overflow-y: scroll;">
            @forelse ($user->profile->entitiesReverse as $id => $c)
                @include('public.users.partial-item-club')
            @empty
                {{ trans('profile.noTrayectory') }}
            @endforelse
    </div>

</div>