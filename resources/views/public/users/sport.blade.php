<!-- SPORT Athlete -->
@if($user->athlete)
                @if($user->athlete->sport)
                {{-- <div class="w-full"> --}}
                        <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.sport') }} </div>
                                <div class="text-gray-700 text-right"> {{ $user->athlete->sport ? trans('sportfields.titles.'.$user->athlete->sport->name) : '-' }} </div>
                        </div>
                        @foreach ($user->athlete->fieldsSports->filter(function($item) { return !$item->is_secondary; }) as $item)
                            @if($item->sport_id == $user->athlete->sport_id && $item->value)
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ $item->sportfield->name }} </div>
                                        
                                    @if($item->sportfield->fieldtype ==  'select')
                                        <div class="text-gray-700 text-right"> {{ app()->getLocale() == 'en' ? json_decode($item->value)->en : json_decode($item->value)->es }} </div>
                                    @else
                                        <div class="text-gray-700 text-right"> {{ $item->value }} </div>
                                    @endif    
                                </div>
                            @endif
                        @endforeach
                        @if($user->athlete->sporting_goal)
                        <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.sportingGoal') }} </div>
                                <div class="text-gray-700 text-right"> {{ trans('fields.sportingGoal.'.$user->athlete->sporting_goal) ?? '-' }} </div>
                        </div>
                        @endif
                        
                {{-- </div> --}}
                @endif
                {{-- SECONDARY SPORT --}}
                @if($user->athlete->sportSecondary)
                {{-- <div class=""> --}}
                    {{-- <div class="w-full"> --}}
                        <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                <div class="font-semibold"> {{ trans('profile.athlete.secondarySport') }} </div>
                                <div class="text-gray-700 text-right"> {{ $user->athlete->sportSecondary ? trans('sportfields.titles.'.$user->athlete->sportSecondary->name) : '-' }} </div>
                        </div>
                        @foreach ($user->athlete->fieldsSports->filter(function($item) { return $item->is_secondary; }) as $item)
                            @if($item->sport_id == $user->athlete->secondary_sport_id && $item->value)
                                <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
                                    <div class="font-semibold"> {{ $item->sportfield->name }} </div>
                                        
                                    @if($item->sportfield->fieldtype ==  'select')
                                        <div class="text-gray-700 text-right"> {{ app()->getLocale() == 'en' ? json_decode($item->value)->en : json_decode($item->value)->es }} </div>
                                    @else
                                        <div class="text-gray-700 text-right"> {{ $item->value }} </div>
                                    @endif    
                                </div>
                            @endif
                        @endforeach
                    {{-- </div> --}}
                {{-- </div> --}}
                @endif
@endif

{{-- SPORT Scout --}}
@if($user->scout && $user->scout->sport)
{{-- <div class="w-full"> --}}
    <div class="py-1 flex justify-between w-full space-odd-item xl:w-1/2">
        <div class="font-semibold"> {{ trans('profile.athlete.sport') }} </div>
        <div class="text-gray-700 text-right"> {{ $user->scout->sport ? trans('sportfields.titles.'.$user->scout->sport->name) : '-' }} </div>
    </div>
{{-- </div> --}}
@endif
                