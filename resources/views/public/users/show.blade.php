@extends('layouts.app')

@section('template_title')
    {!! trans('profile.showProfileTitle',['username' => $user->fullName]) !!}
@endsection

@section('meta_tags')
    <meta property="og:title" content="ScoutChamps - Perfil {{ $user->fullName }}">
    <meta property="og:description" content="ScoutChamps - Perfil {{ $user->fullName }}">
    <meta property="og:image" content="{{ $user->avatar }}">
    <meta property="og:url" content="{{ $user->publicLink }}">
@endsection

@section('template_fastload_css')
<style>
.aboutme-short {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    height: 2.8rem;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    
}
.aboutme-short > *,
.aboutme-large > * {
    line-height: 1.4rem;
}

.space-odd-item > div:nth-child(2) {
    text-align: right;
}

@media(min-width: 1280px) {
    .space-odd-item:nth-child(odd) {
        padding-right: .5rem;
    }
}

</style>
@endsection

@section('content')
    <div class="w-full px-6 py-4">
        {{-- Profile --}}
        <div class="flex flex-wrap">
        
            <div class="w-full sm:w-1/2 md:w-1/2">
                @include('public.users.main',['user' => $user,'followed' => $followed])

                @can('view', $user) <!-- Policy -->
                
                <p-user-images class="mt-4" :user="{{ $user }}"></p-user-images>
                <p-user-videos class="mt-4" :user="{{ $user }}"></p-user-videos>
                
                @endcan
                
            </div>
            <div class="sm:pl-4 w-full sm:w-1/2 md:w-1/2">
                {{-- PUBLIC RECRUITMENTS --}}
                {{-- @if(Auth::user()->hasRole('athlete') && $user->scout)
                    @include('public.users.recruitmentsScout',['user' => $user])
                @endif --}}
                
                @if($user->scout)
                    @can('view', $user)
                        @include('public.users.recruitmentsScout',['user' => $user])
                    @endcan
                @endif

                @can('view', $user) <!-- Policy -->
                @if($user->athlete || $user->scout)
                    <div class="w-full">
                        @include('public.users.clubs',['user' => $user])
                        @if($user->hasPermission('publish.posts'))
                            @include('public.users.posts',['user' => $user])
                        @endif
                    </div>
                @endif
                @endcan
            </div>
    </div>
</div>

<!-- Si es scout y acaba de agregar como favorito o seguir a un atleta, 
     mostramos el modal para enviar invitaciones de RECLUTAMIENTOS -->

@if(Auth::user()->hasRole('scout') && $user->athlete)
    @include('modals.modal-recruitment-invitation')
@endif

@include('modals.modal-remove-follow')
@include('modals.modal-remove-favorite')

@include('modals.modal-advice-follow-user-to-send-message')
@include('modals.modal-advice-user-premium-to-send-message')

<!-- Si es Admin le permitimos ver followers, following, visits y scouts visits -->
@role('admin')
    @include('modals.modal-users',['id' => 'showModalScoutVisitedProfile','title' => trans('profile.athlete.scoutVisits'),'followersUsers' => $visitedScout])
    @include('modals.modal-users',['id' => 'showModalVisitsProfile','title' => trans('profile.athlete.visits'),'followersUsers' => $visits])
    @include('modals.modal-users',['id' => 'showModalFollowersProfile','title' => trans('profile.athlete.followers'),'followersUsers' => $followersUsers])
    @include('modals.modal-users',['id' => 'showModalFollowingProfile','title' => trans('profile.athlete.following'),'followersUsers' => $followingUsers])
@endrole

@endsection


@section('footer_scripts')
<script>
    $('.btn-readmore').on('click',function() {
        let isShortHide =  $('.aboutme-short').hasClass('hidden');
        if(isShortHide) {
            $('.aboutme-large').addClass('hidden');
            $('.aboutme-short').removeClass('hidden');
            $(this).text("@lang('profile.readmore')");
        } else {
            $('.aboutme-short').addClass('hidden');
            $('.aboutme-large').removeClass('hidden');
            $(this).text("@lang('profile.readless')");
        }
    })
</script>
@append