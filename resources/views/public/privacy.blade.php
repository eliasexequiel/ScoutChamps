@extends('layouts.app-guest')

@section('template_title')
    {!! trans('titles.privacy') !!}
@endsection

@section('content')
<div class="w-full px-6 pt-4">
        <div class="row justify-content-center">
                <div class="w-11/12">
                    <div class="card-p">
                            <div class="flex pb-2 border-b mb-2">
                                <h4 class="font-bold">  {!! trans('config.privacy') !!} </h4>
                            </div>
                            <div class="h-128 overflow-y-scroll">
                                {!! $privacy !!}
                            </div>
                    </div>
                </div>
        </div>
</div>
@endsection