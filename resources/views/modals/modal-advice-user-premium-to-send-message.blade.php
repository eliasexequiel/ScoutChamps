<div class="modal fade modal-danger" id="adviceUserPremiumToSendMessage" role="dialog" aria-labelledby="label" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <p> {!! __('profile.adviceUserPremiumToSendMessage') !!} </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn pull-left btn-gray" data-dismiss="modal"> {{ __('profile.btnAccept') }} </button>
        </div>
      </div>
    </div>
  </div>
  