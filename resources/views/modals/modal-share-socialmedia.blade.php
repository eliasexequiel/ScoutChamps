@php
    $socials = [
      ['name' => 'Facebook', 'img' => asset('img/social/fb.png'), 'url' => Auth::user()->getShareUrl('facebook')],
      ['name' => 'Twitter',  'img' => asset('img/social/tw.png'), 'url' => Auth::user()->getShareUrl('twitter')],
      ['name' => 'Whatsapp', 'img' => asset('img/social/wp.png'), 'url' => Auth::user()->getShareUrl('whatsapp')]
    ]
@endphp
<div class="modal fade" id="modalShareSocialMedia" role="dialog" aria-labelledby="modalShareSocialMedia" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">

            <div class="modal-body">
                <div class="pb-2 border-b flex w-full justify-between">
                  <h4> {{ trans('sidebar.shareSocialMedia') }} </h4>
                  <button type="button" data-dismiss="modal" class="close-icon"></button>
                </div>
              <div>
                    <div class="flex w-full">
                      @foreach ($socials as $s)
                          <div class="w-1/3 py-2">
                            <a target="_blank" href="{{ $s['url'] }}"> 
                              <img class="w-20 mx-auto" src="{{ $s['img'] }}" alt="{{ $s['name'] }}">
                            </a>
                          </div>
                      @endforeach
                    </div>
              </div>
            </div>
          </div>
        </div>
</div>
    