<div class="modal fade" id="sendInvitationsRecruitments" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            {!! Form::open(array('route' => 'recruitments.invitations.send', 'method' => 'POST', 'role' => 'form','id' => 'recruitmentInvitationForm', 'class' => 'form')) !!}
            {!! csrf_field() !!}

            <div class="modal-body">
              <h4 class="pb-2 border-b"> {{ trans('recruitments.invitation.title',[ 'name' => $user->fullName ]) }} </h4>
              <div>
                  {{-- UUID --}}
                  <input id="uuid" name="uuid" value="{{ $user->uuid }}" type="hidden">

                    <div class="w-full">
                      @foreach ($recruitments as $r)
                          <div class="py-2 border-b flex w-full">
                            <label class="col-12 flex items-center">
                              <input name="recruitments[]" class="mr-2 form-checkbox" value="{{ $r->id }}" type="checkbox">
                              <div class="flex items-center">
                                <div class="w-4/5">
                                  <div class="font-thin"> {{ $r->place }} </div>
                                  <div class="text-gray-900"> {{$r->day_start}} {{ $r->day_end ? ' - '.$r->day_end : '' }} </div>
                                  <div class="text-gray-700"> {{ $r->hour }} </div> 
                                </div>
                                <div class="pl-2">
                                  @if($r->is_public)
                                      <span class="badge badge-info rounded-lg py-1 px-2"> {{ trans('recruitments.forms.public') }} </span>
                                  @else
                                      <span class="badge badge-success rounded-lg py-1 px-2"> {{ trans('recruitments.forms.private') }} </span> 
                                  @endif
                                </div>
                            </div>
                          </label>
                          </div>
                      @endforeach
                      @if(count($recruitments) == 0)
                        <div class="text-lg mt-2">
                          {{ trans('recruitments.noResults') }}
                        </div>
                        <div class="w-full">
                            <a class="mt-2 btn btn-gray" href="/recruitments/create">
                              {!! trans('recruitments.buttons.create-new') !!}
                            </a>
                        </div>
                      @endif
                    </div>
              </div>
            </div>
            <div class="modal-footer">
              {!! Form::button(trans('modals.form_modal_cancel'), array('class' => 'btn btn-outline pull-left btn-light', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
              @if(count($recruitments) > 0)
                {!! Form::button(trans('modals.form_modal_send'), array('class' => 'btn btn-success rounded-full pull-right', 'type' => 'submit' )) !!}
              @endif
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    
      @if(session('openModalRecruitment'))
        <div id="openModalRecruitment" class="hidden"></div>
      @endif

@section('footer_scripts')

<script type="text/javascript">
  $(window).on('load',function(){
    if($('#openModalRecruitment').length > 0) {
      // SHOW MODAL
      $('#sendInvitationsRecruitments').modal('show');
    }
  });
	// $('#recruitmentInvitationForm').on('submit', function(){
    // $(this).data('form').submit();
	// });

</script>

@append