<div class="modal fade modal-danger" id="confirmUnfollow" role="dialog" aria-labelledby="confirmUnfollowLabel" aria-hidden="true" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            
            <div class="modal-body">
              <p></p>
            </div>
            <div class="modal-footer">
              {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-light', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
              {!! Form::button(trans('profile.unfollow'), array('class' => 'btn btn-danger pull-right', 'type' => 'button', 'id' => 'confirm' )) !!}
            </div>
          </div>
        </div>
      </div>
    

@section('footer_scripts')
  
<script type="text/javascript">
$(window).on('load',function(){
        // CONFIRMATION UNFOLLOW USER MODAL
        $('#confirmUnfollow').on('show.bs.modal', function (e) {
            // e.preventDefault();
            var message = $(e.relatedTarget).attr('data-message');
            var title = $(e.relatedTarget).attr('data-title');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-body p').html(message);
            $(this).find('.modal-title').text(title);
            $(this).find('.modal-footer #confirm').data('form', form);
        });

        $('#confirmUnfollow').find('.modal-footer #confirm').on('click', function(){
              $(this).data('form').submit();
        });
});
</script>
    

@append