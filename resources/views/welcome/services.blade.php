<section id="welcomeServices" class="w-full bg-white py-12 relative">
        <div class="w-11/12 sm:w-5/6 lg:w-3/5 mx-auto text-center">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.ourServices') }} </div>
            </div>
            <div class="text-base">
                {{ trans('welcome.text-2') }}
            </div>
        </div>
</section>