<section id="planes" class="mt-12 md:mt-20 mb-20 w-11/12 lg:w-5/6 mx-auto">
        <div class="w-full sm:w-2/3 md:w-1/3 px-2 mx-auto">
            <div class="shadow-lg rounded-tl-lg rounded-tr-lg plan-scout">
            <div class="h-32 pt-20 relative text-center rounded-tl-lg rounded-tr-lg bg-primary">
                <div class="-mt-12 w-full absolute top-0">
                    <img class="mx-auto h-32 w-auto" src="{{ asset('img/home/plans/Free.png') }}" alt="{{ $planScout->name }}">
                </div>
                <div class="mt-2 text-white leading-none font-semibold text-3xl">
                    {{ $planScout->name }}
                </div>
                
            </div>
            {{-- Items --}}
            <div class="pb-6 px-2">
                @foreach ($planScout->items as $item)
                    @if($item != '')
                        <div class="text-sm border-b last:border-b-0 border-primary py-1">
                            <div class="px-2">
                                @if($item == 'Pictures' || $item == 'Fotos')
                                    {{ $imagesScoutFree }} {{ $item }}
                                @else
                                {{ $item }}
                                @endif
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            {{-- Choose --}}
            @guest
            <div class="pb-2 text-center mx-auto">
                <a class="btn btn-lg btn-primary rounded-lg" href="{{ url('register').'?tab=scout' }}"> {{ trans('welcome.choosePlan') }} </a>
            </div>
            @endguest
            </div>
        </div>
</section>