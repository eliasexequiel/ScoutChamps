<section class="w-full bg-gray-300 py-12 relative">
        <div class="w-full lg:w-11/12 mx-auto">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.howWorks.title') }} </div>
            </div>
            <div class="howworks-list w-full flex-wrap mx-auto">
                {{-- 1 --}}
                <div class="mb-6 lg:mb-10 px-2 w-full lg:w-1/2 flex">
                    <div class="w-16 sm:w-32 mr-2 md:mr-4">
                        <img class="w-full" src="{{ asset('img/home/funciona/1.png') }}" alt="Step-1">
                    </div>
                    <div class="w-5/6 text-sm sm:text-base text-monospace">
                        {{ trans('welcome.howWorks.steps.1') }}    
                    </div>    
                </div>
                {{-- 2 --}}
                <div class="mb-6 lg:mb-10 px-2 w-full lg:w-1/2 flex">
                    <div class="w-16 sm:w-32 mr-2 md:mr-4">
                        <img class="w-full" src="{{ asset('img/home/funciona/2.png') }}" alt="Step-2">
                    </div>
                    <div class="w-5/6 text-sm sm:text-base text-monospace">
                        {{ trans('welcome.howWorks.steps.2') }}    
                    </div>    
                </div>
                {{-- 3 --}}
                <div class="mb-6 lg:mb-10 px-2 w-full lg:w-1/2 flex">
                    <div class="w-16 sm:w-32 mr-2 md:mr-4">
                        <img class="w-full" src="{{ asset('img/home/funciona/3.png') }}" alt="Step-3">
                    </div>
                    <div class="w-5/6 text-sm sm:text-base text-monospace">
                        {{ trans('welcome.howWorks.steps.3') }}    
                    </div>    
                </div>
                {{-- 4 --}}
                <div class="mb-6 lg:mb-10 px-2 w-full lg:w-1/2 flex">
                    <div class="w-16 sm:w-32 mr-2 md:mr-4">
                        <img class="w-full" src="{{ asset('img/home/funciona/4.png') }}" alt="Step-4">
                    </div>
                    <div class="w-5/6 text-sm sm:text-base text-monospace">
                        {{ trans('welcome.howWorks.steps.4') }}    
                    </div>    
                </div>
            </div>
        </div>
    </section>