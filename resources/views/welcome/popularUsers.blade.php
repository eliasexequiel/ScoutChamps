<section class="mt-16 mb-16 w-11/12 md:w-11/12 lg:w-4/5 xl:w-3/4 mx-auto">
    <div class="flex flex-wrap">

        {{-- ATHLETES --}}
        @if(count($popularAthletes) > 0)
        <div class="w-full mb-8 md:mb-0 md:w-1/2 px-6">
            <div class="w-full text-center">
                <span class="text-2xl title-home w-auto"> {{ trans('titles.athletes') }} </span>
            </div>

            <div class="w-full mt-8 bg-gray-200 rounded px-4">
                    @foreach ($popularAthletes as $user)
                    <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-4 border-primary border-b last:border-b-0">
                            <div class="pr-2 w-auto">
                                @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16 sm:h-20 sm:w-20'])
                            </div>
                        <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center flex-1">
                            <div class="w-auto">  
                                @include('partials.user-card-information')
                            </div>
                        </div>
                        @php 
                            if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                            else $route = route('register');
                        @endphp
                        <div class="w-full sm:w-auto pt-4 sm:pt-1 sm:pl-1">
                            <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                <div class="pl-2 sm:pl-0 sm:mt-1">
                                    <a class="btn btn-sm text-info bg-white border border-info hover:bg-info hover:text-white rounded-lg" href="{{ $route }}">
                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                        {{ trans('profile.viewProfile') }} 
                                    </a>
                                </div>
                            </div>   
                        </div>
                    </div>
                    @endforeach
            </div>
        </div>
        @endif

        {{-- SCOUTS --}}
        @if(count($popularScouts) > 0)
        <div class="w-full md:w-1/2 px-6">
            <div class="w-full text-center">
                <span class="text-2xl title-home w-auto"> {{ trans('titles.scouts') }} </span>
            </div>

            <div class="w-full mt-8 bg-gray-200 rounded px-4">
                    @foreach ($popularScouts as $user)
                    <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-4 border-primary border-b last:border-b-0">
                        <div class="pr-2 w-auto">
                            @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16 sm:h-20 sm:w-20'])
                        </div>
                        <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center flex-1">
                            <div class="w-auto">  
                                @include('partials.user-card-information')
                            </div>
                        </div>
                        @php 
                            if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                            else $route = route('register');
                        @endphp
                        <div class="w-full sm:w-auto pt-4 sm:pt-1 sm:pl-1">
                            <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                <div class="pl-2 sm:pl-0 sm:mt-1">
                                    <a class="btn btn-sm text-info bg-white border border-info hover:bg-info hover:text-white rounded-lg" href="{{ $route }}">
                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                        {{ trans('profile.viewProfile') }} 
                                    </a>
                                </div>
                            </div>   
                        </div>
                    </div>
                    @endforeach
            </div>
        </div>
        @endif


    </div>
</section>