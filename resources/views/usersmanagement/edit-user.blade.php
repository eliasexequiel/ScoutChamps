@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.editing-user', ['name' => $user->fullName]) !!}
@endsection

@section('template_linked_css')
    <style type="text/css">
        .btn-save,
        .pw-change-container {
            display: none;
        }
    </style>
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11 col-md-9 col-lg-8">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold"> {!! trans('usersmanagement.editing-user', ['name' => $user->fullName]) !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-users') !!}
                                </a>
                                <a href="{{ url('/users/' . $user->id) }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-user') !!}
                                </a>
                            </div>
                    </div>
                    <div class="w-full">
                        {!! Form::open(array('route' => ['users.update', $user->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}

                            <div class="form-group has-feedback {{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                {!! Form::label('firstname', trans('forms.create_user_label_firstname'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                    {!! Form::text('firstname', $user->firstname, array('id' => 'firstname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}                                     
                                </div>
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                {!! Form::label('lastname', trans('forms.create_user_label_lastname'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                    {!! Form::text('lastname', $user->lastname, array('id' => 'lastname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}                                       
                                </div>
                            </div>

                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error ' : '' }}">
                                {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                    {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}                                        
                                </div>
                            </div>
{{-- 
                            <div class="form-group has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">

                                {!! Form::label('role', trans('forms.create_user_label_role'), array('class' => 'col-md-3 control-label')); !!}

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <select class="custom-select form-control" name="role" id="role">
                                            <option value="">{{ trans('forms.create_user_ph_role') }}</option>
                                            @if ($roles)
                                                @foreach($roles as $role)
                                                    <option value="{{ $role->id }}" {{ $currentRole->id == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="input-group-append">
                                            <label class="input-group-text" for="role">
                                                <i class="{{ trans('forms.create_user_icon_role') }}" aria-hidden="true"></i>
                                            </label>
                                        </div>
                                    </div>
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div> --}}

                            <div class="pw-change-container">
                                <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error ' : '' }}">

                                    {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-12 control-label')); !!}

                                    <div class="col-12">
                                        {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'))) !!}
                                    </div>
                                </div>
                                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">

                                    {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                        {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end my-2 px-2">
                                @if($user->athlete || $user->scout)
                                    <div class="w-auto mr-2">
                                        <a href="{{ route('user.edit.clubs',[$user->id]) }}" class="btn btn-info rounded-full" title="{{ trans('forms.edit-clubs')}} ">
                                            <i class="fa fa-fw fa-edit" aria-hidden="true"></i>
                                            <span></span> {!! trans('forms.edit-clubs') !!}
                                        </a>
                                    </div>
                                @endif
                                <div class="w-auto mr-2">
                                    <a href="#" class="btn btn-gray btn-change-pw rounded-full" title="{{ trans('forms.change-pw')}} ">
                                        <i class="fa fa-fw fa-lock" aria-hidden="true"></i>
                                        <span></span> {!! trans('forms.change-pw') !!}
                                    </a>
                                </div>
                                <div class="w-auto">
                                        {!! Form::button(trans('forms.save-changes'), array( 'class' => 'btn btn-success rounded-full','type' => 'submit')) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-save')
    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
  @include('scripts.save-modal-script')
  @include('scripts.check-changed')
@endsection
