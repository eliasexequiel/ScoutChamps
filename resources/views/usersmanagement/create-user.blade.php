@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.create-new-user') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold"> {!! trans('usersmanagement.create-new-user') !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-users') !!}
                                </a>
                            </div>
                        </div>

                    <div class="w-full">
                        {!! Form::open(array('route' => 'users.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}

                            <div class="col-12 row px-0">
                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                    {!! Form::label('firstname', trans('forms.create_user_label_firstname'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('firstname', NULL, array('id' => 'firstname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                                    </div>
                                </div>

                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                    {!! Form::label('lastname', trans('forms.create_user_label_lastname'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('lastname', NULL, array('id' => 'lastname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}                                        
                                    </div>
                                </div>
                            </div>

                            <div class="pt-3 col-12 row px-0">
                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('role') ? ' has-error ' : '' }}">
                                    {!! Form::label('role', trans('forms.create_user_label_role'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            <select class="custom-select form-control" name="role" id="role">
                                                <option value="">{{ trans('forms.create_user_ph_role') }}</option>
                                                @if ($roles)
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                    </div>
                                </div>

                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('email') ? ' has-error ' : '' }}">
                                    {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('email', NULL, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="pt-3 col-12 row px-0">
                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                    {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                        {!! Form::password('password', array('id' => 'password', 'class' => 'form-control ', 'placeholder' => trans('forms.create_user_ph_password'))) !!}
                                    </div>
                                </div>
                                <div class="form-group px-0 col-12 col-md-6 has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                    {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}                                        
                                    </div>
                                </div>
                            </div>

                            <div class="mt-4 w-full flex justify-end px-2">
                                {!! Form::button(trans('forms.create_user_button_text'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')
@endsection
