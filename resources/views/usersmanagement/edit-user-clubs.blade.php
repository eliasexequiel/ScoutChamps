@extends('layouts.app')

@section('template_title')
    {!! trans('usersmanagement.editing-user-clubs', ['name' => $user->fullName]) !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-11 col-md-10 col-lg-9">
            
            {{-- ALERTS  WITH JSON --}}
            <div id="edit-athlete-success" class="hidden alert alert-success alert-dismissable fade show" role="alert">
                    <a href="#" class="close" aria-label="close">&times;</a>
                    <span id="message"></span>
            </div>
            {{-- ERRORS --}}
            <div id="edit-athlete-error" class="hidden alert alert-danger alert-dismissable fade show" role="alert">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <h5>
                    <i class="icon fa fa-warning fa-fw" aria-hidden="true"></i>
                    {{ Lang::get('auth.someProblems') }}
                    </h5>
                    <ul class="pl-8 text-sm">
                    
                    </ul>
            </div>

            <div class="card-p">
                <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold"> {!! trans('usersmanagement.editing-user-clubs', ['name' => $user->fullName]) !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('users') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="top" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-users') !!}
                                </a>
                                <a href="{{ url('/users/' . $user->id . '/edit') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('usersmanagement.tooltips.back-users') }}">
                                    <i class="fa fa-fw fa-reply" aria-hidden="true"></i>
                                    {!! trans('usersmanagement.buttons.back-to-user') !!}
                                </a>
                            </div>
                </div>
                <div>
                    @if($user->athlete)
                        <admin-index-athlete-clubs :user-id="{{ $user->id }}"></admin-index-athlete-clubs>
                    @elseif($user->scout)
                        <admin-index-scout-clubs :user-id="{{ $user->id }}"></admin-index-scout-clubs>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

@endsection