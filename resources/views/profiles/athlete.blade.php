@extends('layouts.app')

@section('template_title')
    {{ trans('profile.templateTitle') }}
@endsection

@section('content')

        {{-- ALERTS  WITH JSON --}}
        <div id="edit-athlete-success" class="hidden mx-6 alert alert-success alert-dismissable fade show" role="alert">
                <a href="#" class="close" aria-label="close">&times;</a>
                <span id="message"></span>
        </div>
        {{-- ERRORS --}}
        <div id="edit-athlete-error" class="hidden mx-6 alert alert-danger alert-dismissable fade show" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <h5>
                  <i class="icon fa fa-warning fa-fw" aria-hidden="true"></i>
                   {{ Lang::get('auth.someProblems') }}
                </h5>
                <ul class="pl-8 text-sm">
                  
                </ul>
        </div>
        {{--  --}}
        
        <div class="w-full px-6 py-2">
            <div class="w-full">
                        @if ($user->profile)
                            @if (Auth::user()->id == $user->id)
                            <div class="">
                                <div class="row">
                                        <div class="col-12 col-sm-12 col-md-3 profile-sidebar text-white">
                                                <a class="btn btn-outline-primary rounded-lg bg-white flex justify-center items-center btn-block mb-4" href="{{ Auth::user()->publicLink }}">
                                                    <img class="mr-2" src="{{ asset('img/icons/my-profile.png') }}" alt="{{trans('profile.publicPofile')}}">
                                                    {{ trans('profile.publicPofile') }}
                                                </a>
                                                <div class="nav flex-row md:flex-col nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a class="nav-link @if(!session()->has('changeTabProfile')) active @endif" data-toggle="pill" href=".edit-profile-tab" role="tab" aria-controls="edit-profile-tab" aria-selected="true">
                                                        <img src="{{ asset('img/profile/accountSettings.png') }}" alt="Account Settings">
                                                        <span class="text"> {{ trans('profile.editProfileTitle') }} </span>
                                                        <span class="number">1</span>
                                                    </a>
                                                    {{-- <a class="nav-link " data-toggle="pill" href=".edit-settings-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                        {{ trans('profile.editAccountTitle') }}
                                                    </a> --}}
                                                    <a class="nav-link @if(session()->has('changeTabProfile') && session('changeTabProfile') == '1') active @endif" data-toggle="pill" href=".edit-account-tab" role="tab" aria-controls="edit-settings-tab" aria-selected="false">
                                                            <img src="{{ asset('img/profile/accountAdministration.png') }}" alt="Account Administration">
                                                            <span class="text"> {{ trans('profile.editAccountAdminTitle') }} </span>
                                                            <span class="number">2</span>
                                                    </a>
                                                    <a class="nav-link @if(session()->has('changeTabProfile') && session('changeTabProfile') == '2') active @endif" data-toggle="pill" href=".media-files-tab" role="tab" aria-controls="media-files-tab" aria-selected="false">
                                                            <img src="{{ asset('img/profile/multimedia.png') }}" alt="Multimedia">  
                                                            <span class="text"> {{ trans('profile.editMedia') }} </span>
                                                            <span class="number">3</span>
                                                    </a>
                                                    <a class="nav-link" data-toggle="pill" href=".sports-files-tab" role="tab" aria-controls="sports-files-tab" aria-selected="false">
                                                            <img src="{{ asset('img/profile/sport.png') }}" alt="Sports">  
                                                            <span class="text"> {{ trans('profile.sports') }} </span>
                                                            <span class="number">4</span>
                                                    </a>
                                                    <a class="nav-link" data-toggle="pill" href=".clubs-tab" role="tab" aria-controls="clubs-tab" aria-selected="false">
                                                            <img src="{{ asset('img/profile/clubs.png') }}" alt="Clubs">    
                                                            <span class="text"> {{ trans('profile.editAthleticCarrer') }} </span>
                                                            <span class="number">5</span>
                                                    </a>
                                                    {{-- @if(Auth::user()->isPremiun()) --}}
                                                    <a class="nav-link @if(session()->has('changeTabProfile') && session('changeTabProfile') == '6') active @endif" data-toggle="pill" href=".showcase-tab" role="tab" aria-controls="showcase-tab" aria-selected="false">
                                                            <img src="{{ asset('img/profile/showcase.png') }}" alt="Showcase">  
                                                            <span class="text"> {{ trans('profile.showcase') }} </span>
                                                            <span class="number">6</span>
                                                    </a>
                                                    {{-- @endif --}}
                                                </div>
                                            </div>
                                    <div class="mt-2 md:mt-0 col-12 col-sm-12 col-md-9 px-0 md:pl-4">
                                        <div class="mb-4 text-primary text-base font-semibold bg-white py-4 px-2 text-center rounded-lg shadow-lg">
                                            {{ trans('profile.completeAllSteps') }}
                                        </div>

                                        <div class="tab-content border-primary bg-white rounded-lg shadow-lg overflow-hidden" id="v-pills-tabContent">
                                            <!-- PROFILE -->
                                            <div class="py-4 tab-pane fade edit-profile-tab @if(!session()->has('changeTabProfile')) show active @endif" role="tabpanel" aria-labelledby="edit-profile-tab">
                                                    <div class="flex flex-wrap">
                                                        <div class="flex w-full md:w-full lg:w-1/3 justify-center mb-1">
                                                            <div class="w-auto">
                                                                <div id="avatar_container">
                                                                        <div class="card-body">
                                                                            <div class="dz-preview"></div>
                                                                            {!! Form::open(array('route' => 'avatar.upload', 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}
                                                                                <img id="user_selected_avatar" class="user-avatar" src="{{ $user->avatar }}" alt="{{ $user->fullName }}">
                                                                            {!! Form::close() !!}

                                                                            <button class="px-2 flex items-center text-primary text-sm py-1 mx-auto" id="remove-avatar"> 
                                                                                <img src="{{ asset('img/profile/trash.png') }}" alt="Delete">
                                                                                <span class="pl-1">{{ trans('forms.remove-file') }} </span>
                                                                            </button>
                                                                            <div class="photoProfileError hidden mt-1 px-2 py-1 alert alert-danger alert-dismissable text-xs sm:text-sm rounded flex justify-center">
                                                                            </div>
                                                                            <div class="mt-1 px-2 py-1 bg-gray-200 text-xs sm:text-sm border border-gray-500 rounded flex items-center">
                                                                                <i class="fa fa-info-circle"></i>
                                                                                <span class="pl-2">{{ trans('profile.athlete.uploadPhotoProfileMaxSize') }} </span>
                                                                            </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="w-full md:w-full lg:w-2/3">
                                                        {!! Form::model($user->profile, ['method' => 'PUT', 'route' => ['profile.update', $user->id], 'id' => 'user_profile_form', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
                                                            {{ csrf_field() }}
                                                            
                                                            <div class="row col-12 px-0">
                                                                <!-- FirstName -->
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('firstname') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('firstname', trans('forms.create_user_label_firstname'), array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::text('firstname', $user->firstname, array('id' => 'firstname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_firstname'))) !!}
                                                                    </div>
                                                                </div>

                                                                <!-- Lastname -->
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('lastname', trans('forms.create_user_label_lastname'), array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::text('lastname', $user->lastname, array('id' => 'lastname', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_lastname'))) !!}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{-- Email --}}
                                                            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error ' : '' }}">
                                                                {!! Form::label('email', trans('forms.create_user_label_email'), array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::text('email', $user->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => trans('forms.create_user_ph_email'))) !!}
                                                                </div>
                                                            </div>

                                                            <div class="row col-12 px-0">
                                                                
                                                                {{-- Phone --}}
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('phone') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('phone', trans('profile.label-phone') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                                                                            <div style="height:2.3rem" class="absolute text-lg text-gray-700 left-0 ml-2 flex items-center">
                                                                                <span> + </span>
                                                                            </div>
                                                                            {!! Form::text('phone', old('phone'), array('class' => 'pl-6 form-control','autocomplete' => 'off')) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {{-- Date Birth --}}
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback {{ $errors->has('date_of_birth') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('date_of_birth', trans('profile.athlete.dob') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::text('date_of_birth', old('date_of_birth'), array('class' => 'datepicker-custom form-control','autocomplete' => 'off','maxlength' => '10','data-position' => 'top left')) !!}
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- AboutMe -->
                                                            <div class="form-group has-feedback {{ $errors->has('lastname') ? ' has-error ' : '' }}">
                                                                {!! Form::label('about', trans('forms.aboutme'), array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::textarea('about', $user->about, array('id' => 'about', 'class' => 'form-control','rows' => 3, 'placeholder' => trans('forms.aboutme'))) !!}
                                                                </div>
                                                            </div>
                                                                
                                                            {{-- Residence City --}}
                                                            <div class="form-group has-feedback {{ $errors->has('location') ? ' has-error ' : '' }}">
                                                                {!! Form::label('residence_city', trans('profile.label-residence-city') , array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::text('residence_city', old('residence_city'), array('id' => 'location', 'class' => 'form-control','placeholder ' => '','autocomplete' => 'off')) !!}
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="row col-12 px-0">
                                                                {{-- Nationality --}}
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                                                        {!! Form::label('nationality', trans('profile.label-nationality') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::select('nationality_id',$countries, old('nationality'), array('id' => 'nationality', 'class' => 'countries form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                        </div>
                                                                </div>
                                                                {{-- Second Nationality --}}
                                                                <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                                                        {!! Form::label('second_nationality', trans('profile.label-second-nationality') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::select('second_nationality_id',$countries, old('second_nationality'), array('id' => 'second_nationality', 'class' => 'countries form-control','placeholder' => trans('profile.label-doesnotApply'))) !!}
                                                                        </div>
                                                                </div>
                                                            </div>

                                                            {{-- Country residence --}}
                                                            <div class="form-group has-feedback">
                                                                    {!! Form::label('residence_country', trans('profile.label-residence-country') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::select('residence_country_id',$countries, old('residence_country'), array('id' => 'residence_country', 'class' => 'countries form-control','placeholder' => trans('profile.label-select'))) !!}
                                                                    </div>
                                                            </div>

                                                            {{-- <div class="form-group has-feedback {{ $errors->has('bio') ? ' has-error ' : '' }}">
                                                                {!! Form::label('bio', trans('profile.label-bio') , array('class' => 'col-12 control-label')); !!}
                                                                <div class="col-12">
                                                                    {!! Form::textarea('bio', old('bio'), array('id' => 'bio', 'class' => 'form-control', 'placeholder' => trans('profile.ph-bio'))) !!}
                                                                    <span class="glyphicon glyphicon-pencil form-control-feedback" aria-hidden="true"></span>
                                                                    @if ($errors->has('bio'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('bio') }}</strong>
                                                                        </span>
                                                                    @endif
                                                                </div>
                                                            </div> --}}
                                                            <div class="row col-12 px-0">
                                                                {{-- Gender --}}
                                                                <div class="col-12 col-sm-4 col-md-4 px-0 form-group">
                                                                    {!! Form::label('gender', trans('profile.label-gender') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::select('gender', $genders, old('gender'), array('class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>
                                                                {{-- Sporting Goal --}}
                                                                <div class="col-12 col-sm-8 col-md-8 px-0 form-group has-feedback {{ $errors->has('sporting_goal') ? ' has-error ' : '' }}">
                                                                    {!! Form::label('sporting_goal', trans('profile.label-sporting-goal') , array('class' => 'col-12 control-label')); !!}
                                                                    <div class="col-12">
                                                                        {!! Form::select('sporting_goal',$sportingGoal,old('sporting_goal'), array('class' => 'form-control')) !!}
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="row col-12 px-0">
                                                                {{-- Height --}}
                                                                <div class="col-12 col-sm-4 col-md-4 px-0 form-group has-feedback {{ $errors->has('height') ? ' has-error ' : '' }}">
                                                                        {!! Form::label('height', trans('profile.label-height') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::text('height', old('height'), array('class' => 'form-control','maxlength' => 6,'autocomplete' => 'off')) !!}
                                                                        </div>
                                                                        <div class="col-12 mt-1 flex"> 
                                                                            <label class="w-1/2 px-2 flex items-center">
                                                                                <input name="height_unit" class="mr-2 form-radio form-radio-rounded" type="radio" value="cm" {{ app()->getLocale() == 'es' ? 'checked' : '' }}>
                                                                                <span class="text-xs">
                                                                                    {{ trans('profile.athlete.heightCm') }}
                                                                                </span>
                                                                            </label>   
                                                                            <label class="w-1/2 flex items-center">
                                                                                <input name="height_unit" class="mr-2 form-radio form-radio-rounded" type="radio" value="feets" {{ app()->getLocale() == 'en' ? 'checked' : '' }}>
                                                                                <span class="text-xs">
                                                                                    {{ trans('profile.athlete.heightFt') }}
                                                                                </span>
                                                                            </label>
                                                                        </div>
                                                                </div>
                                                                {{-- Professional --}}
                                                                <div class="col-12 col-sm-8 col-md-8 px-0 form-group has-feedback {{ $errors->has('status') ? ' has-error ' : '' }}">
                                                                        {!! Form::label('status', trans('profile.label-status') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::select('status',$status, old('status'), array('id'=>'status_professional','class' => 'form-control')) !!}
                                                                        </div>
                                                                </div>
                                                                {{-- Sport --}}
                                                                {{-- <div class="col-12 col-sm-8 col-md-8 px-0 form-group has-feedback {{ $errors->has('sport_id') ? ' has-error ' : '' }}">
                                                                        {!! Form::label('sport_id', trans('profile.label-sport') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::select('sport_id',$sports, old('sport_id'), array('class' => 'form-control')) !!}
                                                                            <span class="glyphicon glyphicon-pencil form-control-feedback" aria-hidden="true"></span>
                                                                            @if ($errors->has('sport_id'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('sport_id') }}</strong>
                                                                                </span>
                                                                            @endif
                                                                        </div>
                                                                </div> --}}
                                                            </div>

                                                            @if($user->athlete->age() < 18)
                                                                <div class="row col-12 px-0">
                                                                    <!-- FirstName Tutor -->
                                                                    <div class="col-12 col-md-6 px-0 form-group">
                                                                        {!! Form::label('firstnameTutor', trans('profile.athlete.tutorFirstName'), array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::text('firstnameTutor', old('firstnameTutor'), array('id' => 'firstnameTutor', 'class' => 'form-control', 'placeholder' => trans('profile.athlete.tutorFirstName'))) !!}
                                                                        </div>
                                                                    </div>
    
                                                                    <!-- Lastname Tutor -->
                                                                    <div class="col-12 col-md-6 px-0 form-group">
                                                                        {!! Form::label('lastnameTutor', trans('profile.athlete.tutorLastname'), array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::text('lastnameTutor', old('lastnameTutor'), array('id' => 'lastnameTutor', 'class' => 'form-control', 'placeholder' => trans('profile.athlete.tutorLastname'))) !!}
                                                                        </div>
                                                                    </div>
                                                                </div>        

                                                                    {{-- Email Tutor --}}
                                                                    <div class="form-group has-feedback">
                                                                        {!! Form::label('emailTutor', trans('profile.athlete.tutorEmail'), array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            {!! Form::text('emailTutor', old('emailTutor'), array('id' => 'emailTutor', 'class' => 'form-control', 'placeholder' => trans('profile.athlete.tutorEmail'))) !!}
                                                                        </div>
                                                                    </div>
        
                                                                    {{-- Phone Tutor --}}
                                                                    <div class="form-group has-feedback">
                                                                        {!! Form::label('phoneTutor', trans('profile.athlete.tutorPhone') , array('class' => 'col-12 control-label')); !!}
                                                                        <div class="col-12">
                                                                            <div class="flex flex-wrap items-stretch mb-4 relative w-full">
                                                                                <div style="height:2.3rem" class="absolute text-lg text-gray-700 left-0 ml-2 flex items-center">
                                                                                    <span> + </span>
                                                                                </div>
                                                                                {!! Form::text('phoneTutor', old('phoneTutor'), array('class' => 'pl-6 form-control','autocomplete' => 'off')) !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            

                                                                @endif

                                                            <div class="form-group m-0">
                                                                <div class="px-4 flex justify-end items-center">
                                                                        {!! Form::button('<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'), array( 'class' => 'mr-2 btn btn-success rounded-full','type' => 'submit','disabled' => false)) !!}
                                                                        <a class="text-primary" href="{{ route('profile.index') }}"> {{ trans('forms.cancel') }} </a>
                                                                    </div>
                                                            </div>
                                                        {!! Form::close() !!}
                                                        </div>
                                                </div>
                                            </div>
                                            
                                            <!-- Password Account -->
                                            @include('profiles.passwordAccount')

                                            <!-- Media -->
                                            <div class="py-4 tab-pane fade media-files-tab @if(session()->has('changeTabProfile') && session('changeTabProfile') == '2') show active @endif" role="tabpanel" aria-labelledby="media-files-tab">
                                                <div class="w-full px-4">
                                                    <user-images :user="{{ Auth::user() }}"></user-images>
                                                    <user-videos :user="{{ Auth::user() }}"></user-videos>
                                                </div>
                                            </div>

                                            <!-- SPORTS -->
                                            <div class="py-4 tab-pane fade sports-files-tab" role="tabpanel" aria-labelledby="sports-files-tab">
                                                <div class="flex flex-wrap w-full px-4">
                                                    <div class="px-0 pb-2 md:pb-0 sm:px-2 col-12 col-md-6 border-b sm:border-b-0">
                                                        <athlete-sports :user="{{ Auth::user() }}"></athlete-sports>
                                                    </div>
                                                    <div class="mt-4 sm:mt-0 px-0 sm:px-2 col-12 col-md-6">
                                                        <athlete-sports :secondary="{{ true }}" :user="{{ Auth::user() }}"></athlete-sports>   
                                                    </div> 
                                                </div> 
                                            </div>

                                            <!-- CLUBS -->
                                            <div class="tab-pane fade clubs-tab" role="tabpanel" aria-labelledby="clubs-tab">
                                                <div class="w-full">
                                                    <index-athlete-clubs></index-athlete-clubs>   
                                                </div> 
                                            </div>

                                            <!-- SHOWCASE -->
                                            <div class="py-4 tab-pane fade showcase-tab @if(session()->has('changeTabProfile') && session('changeTabProfile') == '6') show active @endif" role="tabpanel" aria-labelledby="showcase-tab">
                                                <div class="w-full">
                                                    @if(Auth::user()->isPremiun())
                                                        <index-athlete-showcase></index-athlete-showcase>   
                                                    @else
                                                        <div class="w-full text-center max-w-xl mx-auto px-2">
                                                            <div class="text-lg">{!! trans('showcase.adviceToAthleteFree') !!}</div>
                                                            <div class="pt-4 text-md">{!! trans('showcase.adviceToAthleteFreeText') !!}</div>
                                                        </div>
                                                    @endif
                                                </div> 
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            @else
                                <p>{{ trans('profile.notYourProfile') }}</p>
                            @endif
                        @else
                            <p>{{ trans('profile.noProfileYet') }}</p>
                        @endif

            </div>
               
        </div>
    

    @include('modals.modal-form')

@endsection

@section('footer_scripts')

    <link href="{{ asset('/assets/summernote/dist/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('/assets/summernote/dist/summernote-bs4.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/summernote/dist/summernote-bs4.min.js') }}"></script>

    @include('scripts.form-modal-script')
    {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZtkZggdWRXxTMsShH8gnhhyafkWlCx5k&libraries=places"></script> --}}


    @include('scripts.user-avatar-dz')

    <script type="text/javascript">

        $('#about').summernote({
            height: 100,
            toolbar: false
        });

        $('.dropdown-menu li a').click(function() {
            $('.dropdown-menu li').removeClass('active');
        });

        $('.profile-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-default');
        });

        $('.settings-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-info');
        });

        $('.admin-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
            $('.edit_account .nav-pills li, .edit_account .tab-pane').removeClass('active');
            $('#changepw')
                .addClass('active')
                .addClass('in');
            $('.change-pw').addClass('active');
        });

        $('.warning-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-warning');
        });

        $('.danger-pill-trigger').click(function() {
            $('.panel').alterClass('card-*', 'card-danger');
        });

        $('#user_basics_form').on('keyup change', 'input, select, textarea', function(){
            $('#account_save_trigger').attr('disabled', false).removeClass('disabled').show();
        });

        $('#user_profile_form').on('keyup change', 'input, select, textarea', function(){
            $('#confirmFormSave').attr('disabled', false).removeClass('disabled').show();
        });

        $("#password_confirmation").keyup(function() {
            checkPasswordMatch();
        });

        $("#password, #password_confirmation").keyup(function() {
            enableSubmitPWCheck();
        });

        $('#password, #password_confirmation').hidePassword(true);

        $('#password').password({
            shortPass: 'The password is too short',
            badPass: 'Weak - Try combining letters & numbers',
            goodPass: 'Medium - Try using special charecters',
            strongPass: 'Strong password',
            containsUsername: 'The password contains the username',
            enterPass: false,
            showPercent: false,
            showText: true,
            animate: true,
            animateSpeed: 50,
            username: false, // select the username field (selector or jQuery instance) for better password checks
            usernamePartialMatch: true,
            minimumLength: 6
        });

        function checkPasswordMatch() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            if (password != confirmPassword) {
                $("#pw_status").html("Passwords do not match!");
            }
            else {
                $("#pw_status").html("Passwords match.");
            }
        }

        function enableSubmitPWCheck() {
            var password = $("#password").val();
            var confirmPassword = $("#password_confirmation").val();
            var submitChange = $('#pw_save_trigger');
            if (password != confirmPassword) {
                submitChange.attr('disabled', true);
            }
            else {
                submitChange.attr('disabled', false);
            }
        }

        $('#edit-athlete-success a.close').on('click',function() {
            $('#edit-athlete-success').addClass('hidden');
        });

    </script>

<script>

    // if(document.getElementsByClassName('changeTabAfterUpdateProfile').length > 0) {
    //     console.log("ads");
    //     $('.nav-pills a[href=".edit-account-tab"]').tab('show')
    // }

    $(window).on("load", function () {

        let lang = $('html').attr('lang');
        $('.datepicker-custom').datepicker({
            language: lang,
            autoClose: true,
            dateFormat: 'dd/mm/yyyy',
            maxDate: new Date()
        }); 
    });

</script>
@endsection
