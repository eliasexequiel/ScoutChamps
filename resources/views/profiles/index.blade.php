@extends('layouts.app')

@section('template_title')
	My Profile
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="flex justify-center">
        <div class="w-11/12">
            <div class="card">
            <div class="card-body">
                <h4 class="font-bold"> Images </h4>
                <div class="flex flex-wrap">
                    <div id="list-images" class="w-full flex flex-wrap my-2">
                        @foreach ($images as $img)
                            <div style="max-height: 160px;" class="pr-0 sm:pr-2 mb-2 overflow-hidden rounded-lg w-full sm:w-1/2 md:1/4 lg:w-1/5">
                                <img class="w-full h-full object-cover rounded-lg" data-id="{{ $img->id }}" src="{{ $img->getFullUrl() }}" alt="{{ $img->name }}"> 
                            </div>
                        @endforeach
                    </div>
                    <div style="max-height: 160px;" class="pr-0 sm:pr-2 mb-2 overflow-hidden rounded-lg w-full sm:w-1/2 md:1/4 lg:w-1/5">
                        {!! Form::open(array('route' => 'profile.media.upload', 'method' => 'POST', 'name' => 'formDropzone','id' => 'formDropzone', 'class' => 'rounded-lg dropzone form', 'files' => true)) !!}
                        
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection


@section('footer_scripts')

<script src="{{{ config('settings.dropZoneJsCDN') }}}"></script>
<script type="text/javascript">

Dropzone.autoDiscover = false;

$(function() {
   Dropzone.options.formDropzone = {
        url: '{{ route('profile.media.upload') }}',
        paramName: 'file',
        maxFilesize: 10, // MB
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".png,.jpeg,.jpg",
        headers: {
            "Pragma": "no-cache"
        },
        init: function() {
            this.on("maxfilesexceeded", function(file) {
                //
            });
            this.on("maxfilesreached", function(file) {
                //
            });
            this.on("uploadprogress", function(file) {
                var html = '<div class="progress">';
                html += '<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">';
                html += '</div>';
                html += '</div>';
                $('.dz-message').html(html).show();
            });
            this.on("maxfilesreached", function(file) {});
            this.on("complete", function(file) {
                this.removeFile(file);
            });
            this.on("success", function(file, response) {
                // console.log(response);
                var html = '<div class="alert-success rounded" style="width:100%"> Upload Success </div>';
                $('.dz-message').html(html).show();
                $('.dz-file-preview').html('');
                $('#list-images').append(`<div style="max-height: 160px;" class="overflow-hidden rounded-lg w-full sm:w-1/2 md:1/4 lg:w-1/5">
                                            <img class="w-full h-full object-cover rounded-lg" data-id="${response.id}" src="${ response.url }" alt="Image"> 
                                            </div>`);
                setTimeout(function() {
                    $('.dz-message').text('Drop files here to upload').show();
                }, 2000);
            });
            this.on("error", function(file, res) {
                // console.log(res);
                var html = '<div class="alert-danger rounded" style="width:100%"> Upload Failed </div>';
                $('.dz-message').html(html).show();
                setTimeout(function() {
                    $('.dz-message').append('<div class="w-full"> Drop files here to upload </div>').show();
                }, 2000);
            });
        }
    };
    var formDropzone = new Dropzone("#formDropzone");
});

</script>
@endsection