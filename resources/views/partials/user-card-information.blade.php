@if(isset($link))
    <a class="{{ isset($sizefullname) ? $sizefullname : 'text-sm' }} leading-tight pr-2" href="{{ $user->publicLink }}"> {{ $user->fullName }} </a>
@else
    <div class="{{ isset($sizefullname) ? $sizefullname : 'text-sm' }} leading-tight pr-2"> {{ $user->fullName }} </div>
@endif                
{{-- Nationality --}}
@if($user->profile->nationality)
<div class="leading-none">    
    <span class="text-xs">{{ trans('profile.athlete.nationality') }}</span>: 
    <span class="text-xs text-gray-700"> {{ $user->profile->nationality->name }} </span>
</div>
@endif
{{-- Sport --}}
@if($user->profile->sport)
<div class="leading-none">    
    <span class="text-xs">{{ trans('profile.athlete.sport') }}</span>: 
    <span class="text-xs text-gray-700"> {{ trans('sportfields.titles.'.$user->profile->sport->name) }} </span>
</div>
@endif

{{-- Check if position exist --}}
@if($user->profile->sport && $user->profile->fieldsSports && $user->profile->fieldsSports->count() > 0)
    @php
        $position = $user->profile->fieldsSports->filter(function($s) use ($user) { 
            return ($s->sportfield->fieldtype == 'select' && !$s->is_secondary && $s->sport_id == $user->profile->sport->id &&  ($s->sportfield->name == 'Primary Position' || $s->sportfield->name == 'Posición primaria')); 
        })->first();

        $positionSecondary = $user->profile->fieldsSports->filter(function($s) use ($user) { 
            return ($s->sportfield->fieldtype == 'select' && !$s->is_secondary && $s->sport_id == $user->profile->sport->id &&  ($s->sportfield->name == 'Secondary Position' || $s->sportfield->name == 'Posición secundaria')); 
        })->first();
    @endphp
    @if($position && $position->value)
    <div class="leading-none">
        <span class="text-xs">{{ trans('sportfields.position') }}</span>: 
        <span class="text-xs text-gray-700"> {{ app()->getLocale() == 'en' ? json_decode($position->value)->en : json_decode($position->value)->es }} </span>
    </div>
    @endif

    {{-- Secondary Position --}}
    @if($positionSecondary && $positionSecondary->value)
    <div class="leading-none">
        <span class="text-xs">{{ trans('sportfields.positionSecondary') }}</span>: 
        <span class="text-xs text-gray-700"> {{ app()->getLocale() == 'en' ? json_decode($positionSecondary->value)->en : json_decode($positionSecondary->value)->es }} </span>
    </div>
    @endif

@endif

{{-- Secondary Sport --}}
@if($user->profile->sportSecondary)
    <div class="leading-none">    
        <span class="text-xs">{{ trans('profile.athlete.secondarySport') }}</span>: 
        <span class="text-xs text-gray-700"> {{ trans('sportfields.titles.'.$user->profile->sportSecondary->name) }} </span>
    </div>

    @if($user->profile->fieldsSports && $user->profile->fieldsSports->count() > 0)
        @php
            $positionSportSec = $user->profile->fieldsSports->filter(function($s) use ($user) { 
                return ($s->sportfield->fieldtype == 'select' && $s->is_secondary && $s->sport_id == $user->profile->sportSecondary->id &&  ($s->sportfield->name == 'Primary Position' || $s->sportfield->name == 'Posición primaria')); 
            })->first();

            $positionSecondarySportSec = $user->profile->fieldsSports->filter(function($s) use ($user) { 
                return ($s->sportfield->fieldtype == 'select' && $s->is_secondary && $s->sport_id == $user->profile->sportSecondary->id &&  ($s->sportfield->name == 'Secondary Position' || $s->sportfield->name == 'Posición secundaria')); 
            })->first();
        @endphp
        @if($positionSportSec && $positionSportSec->value)
        <div class="leading-none">
            <span class="text-xs">{{ trans('sportfields.position') }}</span>: 
            <span class="text-xs text-gray-700"> {{ app()->getLocale() == 'en' ? json_decode($positionSportSec->value)->en : json_decode($positionSportSec->value)->es }} </span>
        </div>
        @endif

        {{-- Secondary Position --}}
        @if($positionSecondarySportSec && $positionSecondarySportSec->value)
        <div class="leading-none">
            <span class="text-xs">{{ trans('sportfields.positionSecondary') }}</span>: 
            <span class="text-xs text-gray-700"> {{ app()->getLocale() == 'en' ? json_decode($positionSecondarySportSec->value)->en : json_decode($positionSecondarySportSec->value)->es }} </span>
        </div>
        @endif

    @endif

@endif

{{-- Current Club --}}
@if(count($user->profile->currentEntity) > 0)
@foreach ($user->profile->currentEntity as $c)
<div class="leading-none">    
    
    @if($c->entity->type)
        <span class="text-xs">{{ trans('clubes.types.'.$c->entity->type->name) }}</span>: 
    @else
        <span class="text-xs">{{ trans('clubes.types.Club') }}</span>:
    @endif    
    <span class="text-xs text-gray-700"> {{ $c->entity->name }} </span>
</div>
@endforeach
@endif