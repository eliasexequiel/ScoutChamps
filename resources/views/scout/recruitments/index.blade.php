@extends('layouts.app')

@section('template_title')
    {!! trans('recruitments.my-recruitments') !!}
@endsection

@section('template_linked_css')
    @if(config('recruitments.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('recruitments.datatablesCssCDN') }}">
    @endif
    <style type="text/css">
        table tbody td.buttons-actions-recruitments {
            min-width: 380px;
        }

        @media(max-width: 991px) {
            table tbody td.buttons-actions-recruitments {
                min-width: 180px;
            }
        }
    </style>
@endsection

@section('content')
            <div class="w-full px-6">
                <div class="card-p">

                        <div class="flex flex-wrap items-center mb-2 border-b pb-2">

                                <h4 class="font-bold pr-4"> {!! trans('recruitments.my-recruitments') !!} </h4>
                                
                                <div class="w-auto">
                                    <span class="badge badge-primary rounded-full"> 
                                        {{ count($recruitments) }} 
                                        {{ trans('recruitments.recruitmentsTotal') }}
                                    </span>    
                                </div>

                                <div class="mt-2 sm:mt-0 btn-group btn-group-xs sm:pl-4">
                                    {{-- <div class="dropdown-menu dropdown-menu-right"> --}}
                                        <a class="btn btn-info rounded-full" href="/recruitments/create">
                                            <i class="fa fa-plus fa-fw"></i>
                                            {!! trans('recruitments.buttons.create-new') !!}
                                        </a>
                                    {{-- </div> --}}
                                </div>
                    </div>

                    <div class="w-full px-2 lg:px-4">

                        {{-- @if(config('recruitments.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}

                        <div class="table-responsive users-table">
                            <table class="table primary table-sm data-table vertical-middle">
                                <thead>
                                    <tr>
                                        <th>{!! trans('recruitments.recruitments-table.date') !!}</th>
                                        <th>{!! trans('recruitments.recruitments-table.place') !!}</th>
                                        <th>{!! trans('recruitments.recruitments-table.address') !!}</th>
                                        <th>{!! trans('recruitments.recruitments-table.type') !!}</th>
                                        {{-- <th>{!! trans('recruitments.recruitments-table.created') !!}</th> --}}
                                        <th>{!! trans('recruitments.recruitments-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($recruitments as $c)
                                        <tr>
                                            <td>
                                                <span class="text-sm"> {{$c->day_start}} {{ $c->day_end ? ' - '.$c->day_end : '' }} </span>
                                                <span class="text-sm block text-gray-700"> {{ $c->hour }} </span>
                                            </td>
                                            <td>{{ $c->place }}</td>
                                            <td>{{ $c->addressComplete }} </td>
                                            <td>
                                                @if($c->is_public)
                                                    <span class="badge badge-success rounded-full py-1 px-2"> {{ trans('recruitments.forms.public') }} </span>
                                                @else
                                                   <span class="badge badge-primary rounded-full py-1 px-2"> {{ trans('recruitments.forms.private') }} </span> 
                                                @endif
                                            </td>
                                            {{-- <td>{{$c->created_at}}</td> --}}
                                            
                                            <td class="buttons-actions-recruitments">
                                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('recruitments.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('recruitments.buttons.show') !!}
                                                </a>
                                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('recruitments.edit',[$c->id]) }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('recruitments.buttons.edit') !!}
                                                </a>
                                                {!! Form::open(array('url' => 'recruitments/' . $c->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('recruitments.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Recruitment', 'data-message' => 'Are you sure you want to delete this recruitment ?')) !!}
                                                {!! Form::close() !!}

                                            {{-- <button type="button" data-toggle="modal" data-target="#invitationsOneRecruitment"  class="btn btn-gray btn-sm">
                                                    <i aria-hidden="true" class="fa fa-envelope-o fa-fw"></i> 
                                                    <span class="hidden-xs hidden-sm"> {{ trans('recruitments.invitation.send') }}  </span>
                                                </button> --}}
                                                @if(\Carbon\Carbon::createFromFormat('d/m/Y H:i',$c->day_start. $c->hour)->gte(\Carbon\Carbon::now()))
                                                <a class="btn btn-sm btn-gray rounded-full" href="{{ route('recruitments.invitations.show',['id' => $c->id]) }}"> 
                                                    <i class="fa fa-envelope fa-fw"></i>
                                                    <span class="hidden-xs hidden-sm"> {{ trans('recruitments.invitation.sendInvitations') }} </span>
                                                </a>
                                                @endif
                                                
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            {{-- @if(config('recruitments.enablePagination')) --}}
                                {{ $recruitments->links() }}
                            {{-- @endif --}}

                        </div>
                    </div>
            </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('recruitments.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
@endsection
