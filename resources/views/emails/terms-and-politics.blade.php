@component('mail::message')

<h1 align="center"> {{ trans('titles.termsConditions') }} </h1>
{!! $terms !!}

<br>
<br>

<h1 align="center"> {{ trans('titles.privacy') }} </h1>
{!! $politics !!}

@endcomponent