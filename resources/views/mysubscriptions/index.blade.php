@extends('layouts.app')

@section('template_title')
    {!! trans('subscriptions.mysubscriptions.title') !!}
@endsection

@section('content')
    <div class="container-fluid">
        @if(isset($currentSubscription))
        <div class="w-full flex justify-center">
            <div class="w-full sm:w-5/6 lg:w-5/6">
                <div class="card-p">

                        <div class="flex justify-between items-center pb-2 border-b mb-2">

                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold"> {!! trans('subscriptions.mysubscriptions.title') !!}</h4>
                            </div>
                            <div>
                                <a href="{{ route('mysubcriptions.invoices') }}" class="mr-2 btn btn-outline-dark btn-sm" title="{{ trans('subscriptions.mysubscriptions.invoices') }}">
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    {{ trans('subscriptions.mysubscriptions.invoices') }}
                                </a>

                                <a href="{{ route('mysubcriptions.paymentmethods') }}" class="btn btn-outline-dark btn-sm" title="{{ trans('subscriptions.mysubscriptions.paymentsMethods') }}">
                                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                                    {{ trans('subscriptions.mysubscriptions.paymentsMethods') }}
                                </a>
                            </div>
                        </div>

                    <div class="w-full">

                        <div class="w-full mb-2">
                        
                        <div class="w-full">

                            <div class="border flex flex-wrap items-center justify-between px-2 py-2 border-primary">
                                @php
                                    $subscription = \Stripe\Subscription::retrieve($currentSubscription->stripe_id);
                                    $planName =  $subscription->plan->nickname;
                                @endphp
                            <div>
                                <span class="pr-2 font-semibold"> {{ trans('subscriptions.mysubscriptions.currentPlan') }}:</span>
                                <span> {{ $planName }} </span>
                            </div>
                            {{-- <div>             
                                            
                                    @if($subscription->latest_invoice)
                                        @php  $invoice = \Stripe\Invoice::retrieve($subscription->latest_invoice); @endphp
                                        
                                        <span class="text-sm"> {{ $invoice->total / 100 }} {{ $invoice->currency }} </span>
                                        @if($invoice->discount)
                                            <span class="text-xs text-gray-600"> ({{ $invoice->discount->coupon->name }}) </span>
                                        @endif
                                    @endif
                            </div> --}}
                            <div>
                                <button class="btn btn-success btn-sm rounded-full showPlans"> {{ trans('subscriptions.mysubscriptions.modifyPlan') }} </button>
                            </div>
                                            
                            </div>

                            
                        </div>
                        
                        </div>
                        
                    </div>

                </div>
            </div>
        </div>
        @endif

        {{----------------- PLANS --------------------------}}
        <div id="planes" class="@if(!isset($currentSubscription)) card-p @else hidden mt-20 @endif w-11/12 mx-auto">
            @if(!isset($currentSubscription))
                <div class="pb-2 w-full border-b mb-4 md:mb-20">
                    <div class="flex flex-wrap items-center">
                        <h4 class="font-bold"> {!! trans('subscriptions.mysubscriptions.newPlan') !!}</h4>
                    </div>
                </div>
            @endif
            <div class="hidden md:flex flex-wrap">
            @foreach ($plans as $p)
                <div class="w-1/2 md:w-1/4 mb-16 md:mb-2 px-2">
                    <div class="shadow-lg rounded-tl-lg rounded-tr-lg plan">
                    <div class="h-48 lg:h-44 pt-20 relative text-center rounded-tl-lg rounded-tr-lg {{ $p->type == 'Premium' ? 'bg-primary' : 'border-primary border-2'}}">
                        <div class="-mt-12 w-full absolute top-0">
                            <img class="mx-auto h-32 w-auto" src="{{ $p->image }}" alt="{{ $p->name }}">
                        </div>
                        <div class="mt-2 leading-none text-white font-semibold text-xl xl:text-2xl">
                            @if(app()->getLocale() == 'en')
                                {{ $p->type == 'Premium' ? 'Premium' : '' }} {{ trans('titles.athlete') }}
                            @else
                                {{ trans('titles.athlete') }} {{ $p->type == 'Premium' ? 'Premium' : '' }}
                            @endif
                        </div>
                        @if($p->name != 'Free')
                        <div class="text-white leading-none font-semibold  text-xl xl:text-2xl">
                                {{ trans('titles.'.$p->name) }} {{ $p->price }} <span class="text-base"> USD </span>
                        </div>
                        @else
                        <div class="text-primary leading-none font-semibold text-xl xl:text-2xl">
                                {{-- {{ trans('titles.'.$p->name) }} {{ trans('titles.athlete') }} --}}
                                @if(app()->getLocale() == 'en') 
                                    {{ trans('titles.'.$p->name) }} {{ trans('titles.athlete') }}
                                @else
                                    {{ trans('titles.athlete') }} {{ trans('titles.'.$p->name) }}
                                @endif
                        </div>
                        @endif
                        @if($p->name == 'Semester')
                            <span class="text-white font-semibold text-base xl:text-lg"> {{ trans('welcome.savePlan') }} 10% </span>
                        @elseif($p->name == 'Anual')
                            <span class="text-white font-semibold text-base xl:text-lg"> {{ trans('welcome.savePlan') }} 20% </span>
                        @endif
                    </div>
                    {{-- Items --}}
                    <div class="px-2">
                        @foreach ($p->items as $item)
                            @if($item->value != '')
                                <div class="text-sm border-b last:border-b-0 border-primary py-1">
                                    <div class="px-2">
                                        @if(is_numeric($item->value) || $item->value == 'Unlimited')
                                            {{ $item->item->name }}:
                                            <span>
                                                @if($item->value == 'Unlimited') 
                                                    {{ trans('titles.unlimited') }} 
                                                @else 
                                                    {{ $item->value }} 
                                                @endif
                                            </span>
                                        @else
                                            {{ $item->item->name }}
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    {{-- Choose --}}
                    <div class="text-center select-button">
                        {{-- @if(!isset($currentSubscription)) --}}
                            <a class="btn btn-lg btn-success rounded-lg" href="{{ route('plan.show',[$p->stripe_plan]) }}"> {{ trans('welcome.choosePlan') }} </a>
                        {{-- @else
                            <a class="btn btn-lg btn-success rounded-lg" href="{{ route('plan.show',[$p->stripe_plan]) }}"> {{ trans('subscriptions.mysubscriptions.modifyPlan') }} </a>
                        @endif --}}
                    </div>
                    </div>
                </div>
            @endforeach
            </div>
            
            {{-- PLANS MOBILE --}}
            <div class="w-full sm:w-3/4 block mx-auto md:hidden mobile-plans">
                    <div class="plans-pills flex w-full border border-primary rounded-t-lg">
                            <div class="nav text-center w-full flex-row nav-pills" id="v-pills-tab" role="tablist" aria-orientation="horizontal">
                                @foreach ($plans as $p)
                                    <a class="p-1 border-primary border-b w-1/4 nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="pill" href="#{{ $p->name }}" role="tab" aria-controls="{{ $p->name }}" aria-selected="true">
                                        <div>
                                            <img class="mx-auto object-contain h-20 w-auto" src="{{ $p->image }}" alt="{{ $p->name }}">
                                        </div>
                                        @if($p->name != 'Free')
                                            <div class="leading-none">
                                                    @if(app()->getLocale() == 'en')
                                                        {{ $p->type == 'Premium' ? 'Premium' : '' }} {{ trans('titles.athlete') }}
                                                    @else
                                                        {{ trans('titles.athlete') }} {{ $p->type == 'Premium' ? 'Premium' : '' }}
                                                    @endif
                                            </div>
                                            <div class="leading-none">
                                                {{ $p->name }} 
                                                <span class="block price mt-1 text-base">{{ $p->price }} <span class="text-sm"> USD </span> </span>
                                            </div>
                                        @else
                                            <div class="mt-4 leading-none font-semibold">
                                                {{-- {{ $p->name }} --}}
                                                @if(app()->getLocale() == 'en') 
                                                    {{ trans('titles.'.$p->name) }} {{ trans('titles.athlete') }}
                                                @else
                                                    {{ trans('titles.athlete') }} {{ trans('titles.'.$p->name) }}
                                                @endif
                                            </div>
                                        @endif
                                        @if($p->name == 'Semester')
                                            <span class="plan-save font-semibold text-sm"> {{ trans('welcome.savePlan') }} 10% </span>
                                        @elseif($p->name == 'Anual')
                                            <span class="plan-save font-semibold text-sm"> {{ trans('welcome.savePlan') }} 20% </span>
                                        @endif
            
                                    </a>
                                @endforeach
                            </div>
                    </div>
                    <div class="pb-4 w-full">
                            <div class="tab-content" id="v-pills-tabContent">
                                    @foreach ($plans as $p)
                                    <div id="{{ $p->name }}" class="tab-pane fade {{ $loop->first ? 'show active' : '' }}" role="tabpanel" aria-labelledby="{{ $p->name }}">
                                        <div class="shadow-lg pt-4 rounded-bl-lg rounded-br-lg">
                                        {{-- Items --}}
                                        <div class="px-2">
                                            @foreach ($p->items as $item)
                                                @if($item->value != '')
                                                    <div class="text-sm border-b last:border-b-0 border-primary py-1">
                                                        <div class="px-2">
                                                            @if(is_numeric($item->value) || $item->value == 'Unlimited')
                                                                {{ $item->item->name }}:
                                                                <span>
                                                                    @if($item->value == 'Unlimited') 
                                                                        {{ trans('titles.unlimited') }} 
                                                                    @else 
                                                                        {{ $item->value }} 
                                                                    @endif
                                                                </span>
                                                            @else
                                                                {{ $item->item->name }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        {{-- Choose --}}
                                        <div class="text-center pb-2 pt-6">
                                            <a class="btn btn-lg btn-success rounded-lg" href="{{ route('plan.show',[$p->stripe_plan]) }}"> {{ trans('welcome.choosePlan') }} </a>
                                        </div>
                                        </div>
                                    </div>
                                @endforeach
            
                            </div>
                    </div>
            </div>
            
        </div>
        {{----------------- END PLANS -----------------}}

    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    <script>
        $('.showPlans').on('click',function() {
            $('#planes').removeClass('hidden');
        });
    </script>
@endsection
