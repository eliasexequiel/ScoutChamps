@extends('layouts.app')

@section('template_title')
{!! trans('subscriptions.mysubscriptions.paymentsMethods') !!}
@endsection

@section('content')
<div class="container-fluid">
    <div class="w-full flex justify-center">
        <div class="w-full lg:w-4/5">
            <div class="card-p">
                <div class="flex justify-between items-center pb-2 border-b mb-2">
                    <div class="flex flex-wrap items-center">
                        <h4 class="font-bold"> {!! trans('subscriptions.mysubscriptions.paymentsMethods') !!}</h4>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-sm btn-dark rounded-full" href="{{ route('mysubcriptions.paymentmethods.create') }}"> 
                            <i class="fa fa-plus"></i>
                            {{ trans('subscriptions.mysubscriptions.addPaymentMethod') }} 
                        </a>

                        <a href="{{ route('mysubcriptions.index') }}" class="btn btn-light btn-sm float-right"
                            title="{{ trans('subscriptions.mysubscriptions.title') }}">
                            <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                            {{ trans('subscriptions.mysubscriptions.title') }}
                        </a>
                    </div>
                </div>
                <div class="w-full">
                    @if(count($paymentsMethods->data) > 0)
                        @foreach ($paymentsMethods as $p)
                        <div class="w-full flex justify-between border-b py-1">
                            <div class="w-1/3">
                                <span class="font-semibold uppercase">{{ $p->card->brand }} </span>
                                <span class="pl-4 text-gray-600"> •••• {{ $p->card->last4 }} </span>
                            </div>
                            <div class="flex">
                                <div class="text-xs text-gray-600">  </div>
                                <div> 
                                    @if(strlen($p->card->exp_month) == 1)
                                        0{{$p->card->exp_month}} 
                                    @else
                                        {{$p->card->exp_month}}
                                    @endif 
                                </div>
                                <div class="px-1">  / </div>
                                <div> {{$p->card->exp_year}} </div>
                            </div>
                            <div class="w-1/2 flex items-center justify-end">
                                @if(Auth::user()->subscribed('main'))
                                    @if($cardDefault->fingerprint == $p->card->fingerprint)
                                        <div class="pr-2">    
                                            <span class="badge badge-secondary leading-snug text-xs text-white px-2 rounded-full"> {{ trans('subscriptions.mysubscriptions.labelDefaultPaymentMethod') }} </span>
                                        </div>
                                    @else
                                        <div class="pr-2">
                                            <a class="btn btn-sm btn-outline-dark rounded-full" href="{{ route('mysubcriptions.paymentmethods.defaultcard',[$p->id]) }}"> {{ trans('subscriptions.mysubscriptions.buttons.defaultCard') }} </a>
                                        </div>
                                    @endif
                                @endif
                                {{-- <a class="btn btn-sm btn-outline-primary rounded-full" href="{{ route('mysubcriptions.paymentmethods.destroy',[$p->id]) }}"> 
                                </a> --}}
                                {!! Form::open(array('url' => 'mySubscriptions/paymentsMethods/' . $p->id, 'class' => 'inline-flex', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button(trans('subscriptions.mysubscriptions.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Coupon', 'data-message' => 'Are you sure you want to delete this payment method?')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                        @endforeach
                    @else
                        {{ trans('subscriptions.mysubscriptions.noPaymentsMethods') }}
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@include('modals.modal-delete')

@endsection

@section('footer_scripts')
@include('scripts.delete-modal-script')
@endsection