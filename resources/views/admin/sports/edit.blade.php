@extends('layouts.app')

@section('template_title')
    {!! trans('sports.editing-sport',['name' => $sport->name]) !!}
@endsection

@section('template_linked_css')
<style>
#remove-field svg {
    width: 35px;
    height: 35px;
}

#remove-field {
    /* top: 60%; */
    /* transform: translateY(-50%); */
}

</style>
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold"> {!! trans('sports.editing-sport',['name' => $sport->name]) !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('sports.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('sports.back-sports') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('sports.buttons.back-to-sports') !!}
                                </a>
                            </div>
                        </div>

                    <div class="w-full">
                        {!! Form::model($sport,array('route' => ['sports.update', $sport->id], 'method' => 'PUT', 'role' => 'form','id' => 'sportForm', 'class' => 'needs-validation','files' => true, 'enctype' => 'multipart/form-data')) !!}
                            {!! csrf_field() !!}

                            {{-- <div class="form-group has-feedback">
                                {!! Form::label('name', trans('sports.forms.name'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                        {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                </div>
                            </div> --}}

                            <div class="form-group has-feedback">
                                    {!! Form::label('description', trans('sports.forms.description'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                        {!! Form::textarea('description', NULL, array('id' => 'description','rows' => 4, 'class' => 'form-control' )) !!}                                        
                                    </div>
                            </div>

                            {{-- Fields --}}
                            
                            <div class="col-12 py-4">
                                <h6 class="font-bold"> {{ trans('sports.forms.fields') }} </h6>
                            </div>
                            <div id="wrapper-options" class="row col-12 px-0">
                                @if(count($sport->fields) > 0)
                                    @foreach ($sport->fields as $i => $f)
                                    <div class="field-option row col-12 px-0 py-1 border-b">
                                            {{-- Save ids to delete then in the controller --}}
                                            <input type="hidden" name="fields[{{ $i }}][id]" value="{{ $f->id }}">
                                            
                                            <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                                                {!! Form::label('entity_type_id', trans('sports.forms.field-name'), array('class' => 'col-12 control-label')); !!}
                                                <div class="col-12 mb-2">
                                                    <input type="text" name="fields[{{ $i }}][name_en]" class="form-control" value="{{ json_decode($f)->name->en }}">
                                                </div>
                                                <div class="col-12">
                                                    <input type="text" name="fields[{{ $i }}][name_es]" class="form-control" value="{{ json_decode($f)->name->es }}">
                                                </div>
                                            </div>
                                            {{-- {{ var_dump($types) }} --}}
                                            <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                                                    {!! Form::label('email', trans('sports.forms.field-type'), array('class' => 'col-12 control-label')); !!}
                                                    <div class="col-12">
                                                        <select id="field-type" class="form-control" name="fields[{{ $i }}][type]" value="{{ $f->fieldtype }}" id="">
                                                            @foreach($types as $type)
                                                                <option value="{{ $type }}" {{ $f->fieldtype == $type ? ' selected' : ''  }}> {{ $type }} </option>
                                                            @endforeach
                                                        </select>
                                                            {{-- <input type="text" name="fields[{{ $f->id }}][name]" class="form-control" value="{{ $f->name }}"> --}}
                                                            {{-- {!! Form::select("'fields[' .{{$f->id}}. '][type][]'",$types, $f->fieldtype, array('class' => 'form-control', 'id' => 'field-type' )) !!}                                         --}}
                                                    </div>
                                            </div>
    
                                            <div id="options-select" class="@if($f->fieldtype != 'select') hidden @endif flex flex-wrap items-end w-full mb-4 px-0 col-sm-6 form-group has-feedback">
                                                    <div class="col-12">
                                                        {!! Form::label('email', trans('sports.forms.field-options'), array('class' => 'w-full control-label')); !!}
                                                        @foreach($f->options_select as $k => $op)
                                                            @php 
                                                                $option = \App\Models\SportFieldsOptionsSelect::find($op->id); 
                                                            @endphp
                                                            {{-- {{  }} --}}
                                                            <div class="option mb-1 w-full flex">
                                                                <input name="fields[{{ $i }}][options][{{ $k }}][id]" value="{{ $op->id }}" type="hidden">
                                                                <input name="fields[{{ $i }}][options][{{ $k }}][en]" value="{{ $op->name }}" type="text" placeholder="EN" class="w-5/12 mr-1 form-control form-control-sm">
                                                                <input name="fields[{{ $i }}][options][{{ $k }}][es]" value="{{ json_decode($option)->name->es }}" type="text" placeholder="ES" class="w-5/12 form-control form-control-sm">
                                                                
                                                                <button id="removeOption" type="button" class="ml-2 cursor-pointer">
                                                                    <img class="w-4 h-4" src="/img/close-icon.png">    
                                                                </button>    
                                                                {{-- {!! Form::text('fields[options][][]', NULL, array('class' => 'form-control form-control-sm','placeholder' => 'Options' )) !!}                                         --}}
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="col-12">
                                                        <button id="addOption" data-idfield="{{ $i }}" type="button" class="btn btn-sm btn-secondary"> Add </button>
                                                    </div>
                                            </div>
                                            <div class="absolute right-0 top-0 w-8 mt-6" id="remove-field"> 
                                                    <img class="w-6 h-6" src="/img/close-icon.png">  
                                            </div>
                                            
                                        </div>    
                                        
                                    @endforeach
                                @else
                                    {{-- <div class="field-option row col-12 px-0">
                                        <div class="col-12 px-0 col-sm-4 form-group has-feedback">
                                            {!! Form::label('entity_type_id', trans('sports.forms.field-name'), array('class' => 'col-12 control-label')); !!}
                                            <div class="col-12">
                                                    {!! Form::text('fields[name][]',NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                            </div>
                                        </div>
                                        <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                                                {!! Form::label('email', trans('sports.forms.field-type'), array('class' => 'col-12 control-label')); !!}
                                                <div class="col-12">
                                                        {!! Form::select('fields[type][]',$types, NULL, array('class' => 'form-control', 'id' => 'field-type' )) !!}                                        
                                                </div>
                                        </div>

                                        <div id="options-select" class="hidden flex items-end w-full mb-4 px-0 col-sm-4 form-group has-feedback">
                                                <div class="option col-8">
                                                    {!! Form::label('email', trans('sports.forms.field-options'), array('class' => 'w-full control-label')); !!}
                                                    <div class="mb-1 w-full flex">
                                                        <input name="fields[][options][][][en]" type="text" placeholder="EN" class="w-5/12 mr-1 form-control form-control-sm">
                                                        <input name="fields[][options][][][es]" type="text" placeholder="ES" class="w-5/12 form-control form-control-sm">
                                                        
                                                        <button id="removeOption" type="button" class="ml-2 cursor-pointer">
                                                            <img class="w-4 h-4" src="/img/close-icon.png">    
                                                        </button>    
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <button id="addOption" type="button" class="btn btn-sm btn-secondary"> Add </button>
                                                </div>
                                        </div>
                                        
                                    </div> --}}
                                @endif
                            </div>
                            

                            <div class="col-12 pt-2">
                                <button id="btn-add-field" class="btn btn-gray rounded-full" type="button" title="{{ trans('sports.forms.field-add') }}"> 
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i> 
                                    {{ trans('sports.forms.field-add') }} 
                                </button>
                            </div>
                            
                            

                            <div class="flex w-full justify-end mt-2 px-2">
                                {!! Form::button(trans('sports.forms.update'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

<script type="text/javascript">

$(function() {
    $('#btn-add-field').on('click',function() {
        let optionsLength = $('.field-option').length;
        console.log(optionsLength);
        let html = `<div class="field-option row col-12 px-0 py-1 border-b">
                        <input type="hidden" name="fields[${optionsLength}][id]" value="">
                        <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                            <label for="entity_type_id" class="col-12 control-label">Name</label> 
                            <div class="col-12 mb-2"><input name="fields[${optionsLength}][name_en]" type="text" class="form-control" placeholder="EN"></div>
                            <div class="col-12"><input name="fields[${optionsLength}][name_es]" type="text" class="form-control" placeholder="ES"></div>
                        </div> 
                        <div class="col-12 px-0 col-sm-3 form-group has-feedback">
                            <label for="email" class="col-12 control-label">Type</label> 
                            <div class="col-12">
                                <select id="field-type" name="fields[${optionsLength}][type]" class="form-control">
                                    <option value="text">text</option>
                                    <option value="number">number</option>
                                    <option value="select">select</option>
                                </select>
                            </div>
                        </div> 
                        <div id="options-select" class="hidden flex flex-wrap items-end w-full mb-4 px-0 col-sm-6 form-group has-feedback">
                            <div class="col-12">
                                <label for="email" class="w-full control-label">Options Select</label> 
                                <div class="option mb-1 w-full flex">
                                    <input name="fields[${optionsLength}][options][0][id]" value="" type="hidden">
                                    <input name="fields[${optionsLength}][options][0][en]" type="text" placeholder="EN" class="w-5/12 mr-1 form-control form-control-sm">
                                    <input name="fields[${optionsLength}][options][0][es]" type="text" placeholder="ES" class="w-5/12 form-control form-control-sm">
                                    <button id="removeOption" type="button" class="ml-2 cursor-pointer">
                                        <img class="w-4 h-4" src="/img/close-icon.png">    
                                    </button>
                                </div>
                            </div>
                            <div class="col-12">
                                <button id="addOption" data-idfield="${optionsLength}" type="button" class="btn btn-sm btn-secondary"> Add </button>
                            </div>
                        </div>
                        <div class="absolute right-0 top-0 w-8 mt-6" id="remove-field"> 
                            <img class="w-6 h-6" src="/img/close-icon.png">  
                        </div>
                    </div>`;

        $('#wrapper-options').append(html);
    })

    $(document).on("change",'#field-type',function() {
        let value = $(this).val();
        if(value == 'select') {
            $(this).parent().parent().siblings('#options-select').removeClass('hidden');
        }
        else {
            $(this).parent().parent().siblings('#options-select').addClass('hidden');
        }   
    });

    $(document).on('click','#options-select #addOption',function() {
        let idField = $(this).attr('data-idfield');
        let cantOptions = $(this).parent().siblings().first().children('.option').length;
        console.log(cantOptions);
        let html = `<div class="option mb-1 w-full flex">
                        <input name="fields[${idField}][options][${cantOptions}][id]" type="hidden">   
                        <input name="fields[${idField}][options][${cantOptions}][en]" type="text" placeholder="EN" class="w-5/12 mr-1 form-control form-control-sm">
                        <input name="fields[${idField}][options][${cantOptions}][es]" type="text" placeholder="ES" class="w-5/12 form-control form-control-sm">
                        <button id="removeOption" type="button" class="ml-2 cursor-pointer">
                            <img class="w-4 h-4" src="/img/close-icon.png">    
                        </button>
                    </div>`
        $(this).parent().siblings().first().append(html);
    })

    // Remove option in field
    $(document).on('click','#options-select #removeOption',function() {
        $(this).parent().remove();
    });

    $(document).on("click",'#remove-field',function() {
        $(this).parent().remove();
    });

});

</script>
@endsection