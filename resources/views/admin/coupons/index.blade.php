@extends('layouts.app')

@section('template_title')
    {!! trans('coupons.title') !!}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">

                        <div class="flex justify-between items-center pb-2 border-b mb-2">

                                <div class="flex flex-wrap items-center">
                                    <h4 class="font-bold"> {!! trans('coupons.title') !!}</h4>

                                    <div class="pl-4"> 
                                        <span class="badge badge-primary rounded-full text-white">{{ trans('coupons.couponTotal') }}: {{ count($coupons) }} </span>
                                    </div>
                                </div>
                            <div class="btn-group pull-right btn-group-xs">
                                    <a class="btn btn-gray rounded-full" href="/coupons/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('coupons.buttons.create-new') !!}
                                    </a>
                            </div>
                        </div>

                    <div class="w-full">

                        {{-- @if(config('coupons.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('coupons.coupons-table.name') !!}</th>
                                        <th>{!! trans('coupons.coupons-table.discount') !!}</th>
                                        <th>{!! trans('coupons.coupons-table.duration') !!}</th>
                                        <th>{!! trans('coupons.coupons-table.used') !!}</th>
                                        <th>{!! trans('coupons.coupons-table.created') !!}</th>
                                        {{-- <th>{!! trans('coupons.coupons-table.updated') !!}</th> --}}
                                        <th>{!! trans('coupons.coupons-table.actions') !!}</th>
                                        <th class="no-search no-sort"></th>
                                        <th class="no-search no-sort"></th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($coupons as $c)
                                        <tr>
                                            
                                            <td>
                                                <span class="font-semibold"> {{$c->name}} </span>
                                                <span class="block text-sm text-gray-600"> {{ $c->id }} </span>
                                            </td>
                                            <td>
                                                @if($c->percent_off)
                                                    {{ $c->percent_off }} % 
                                                @elseif($c->amount_off)
                                                    {{ $c->amount_off }} {{ config('services.currency') }}
                                                @endif
                                            </td>
                                            <td> 
                                                {{ $c->duration }}
                                                @if($c->duration == 'repeating')
                                                    <span class="block text-sm text-gray-600"> ({{ $c->duration_in_months }} {{ trans('coupons.forms.months') }}) </span>
                                                @endif 
                                            </td>
                                            <td>
                                                {{ $c->times_redeemed }}
                                            </td>
                                            <td>
                                                @if($c->created)
                                                    {{ \Carbon\Carbon::createFromTimestamp($c->created)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td>
                                            {{-- <td>
                                                @if($c->updated_at)
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->updated_at)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td> --}}
                                            
                                            <td style="min-width: 290px;">
                                                {{-- <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('coupons.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('coupons.buttons.show') !!}
                                                </a> --}}
                                                
                                                {!! Form::open(array('url' => 'coupons/' . $c->id, 'class' => 'inline-flex', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('coupons.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Coupon', 'data-message' => 'Are you sure you want to delete this coupon?')) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                            <div class="w-full mt-4">
                                <ul class="pagination">
                                    @if(request()->query('after'))
                                        <li class="page-item">
                                            <a class="page-link" rel="previous" title="Previous page" href="{{ url('/coupons') }}"> « </a> 
                                        </li>
                                    @endif
                                    @if($has_more)
                                        <li class="page-item">
                                            <a class="page-link" rel="next" title="Next page" href="{{ url('/coupons') }}?after={{ $coupons[count($coupons)-1]['id'] }}"> » </a> 
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($coupons) > config('coupons.datatablesJsStartCount')) && config('coupons.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('coupons.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    {{-- @if(config('coupons.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
