@extends('layouts.app')

@section('template_title')
    {!! trans('coupons.create-new-coupon') !!}
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
                <div class="col-11 col-md-8 col-lg-6">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold">  {!! trans('coupons.create-new-coupon') !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('coupons.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('coupons.back-coupons') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('coupons.buttons.back-to-coupons') !!}
                                </a>
                            </div>
                        
                        </div>

                    <div class="w-full">
                        {!! Form::open(array('route' => 'coupons.store', 'method' => 'POST', 'role' => 'form','id' => 'couponForm')) !!}

                            {!! csrf_field() !!}

                            <div class="row">
                            <div class="col-12 col-md-6 px-0 form-group has-feedback">
                                {!! Form::label('name', trans('coupons.forms.name'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                        {!! Form::text('name', old('name'), array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                </div>
                            </div>
                            <div class="col-12 col-md-6 px-0 form-group">
                                    {!! Form::label('code', trans('coupons.forms.code'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('code', old('code'), array('id' => 'code', 'class' => 'form-control' )) !!}                                        
                                    </div>
                            </div>
                            </div>
                            <div class="col-12 px-0">
                                    {!! Form::label('name', trans('coupons.forms.type'), array('class' => 'col-12 control-label')); !!}
                                    <div class="px-2 flex w-full">
                                        <label class="w-full lg:w-1/2 px-2 flex items-center mb-0">
                                            <input name="type" value="percent_off" class="mr-2 form-radio form-radio-rounded" type="radio" {{ request()->input('type') != 'amount_off' ? 'checked' : '' }}>
                                            <span class="text-sm"> {{ trans('coupons.forms.type_percent_off') }}</span>
                                        </label>
                                        <label class="w-full lg:w-1/2 px-2 flex items-center mb-0">
                                            <input name="type" value="amount_off" class="mr-2 form-radio form-radio-rounded" type="radio" {{ request()->input('type') == 'amount_off' ? 'checked' : '' }}>
                                            <span class="text-sm"> {{ trans('coupons.forms.type_amount_off') }}</span>
                                        </label>
                                    </div>
                                    <div class="mt-1 col-12 col-md-8 col-lg-6">
                                            <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                <input id="amount_discount" name="amount_discount" type="text" class="form-control">
                                                <div class="absolute text-lg text-gray-700 inset-y-0 right-0 mr-2 flex justify-end items-center">
                                                    <span id="logo-discount"> % </span>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                            <!-- Duracion -->
                            <div class="row col-12 px-0">
                                <div class="form-group col-12 col-md-6 px-0">
                                    {!! Form::label('duration', trans('coupons.forms.duration') , array('class' => 'col-12 control-label')) !!}
                                    <div class="col-12">
                                        {!! Form::select('duration',$duration, old('duration'), array('id' => 'duration', 'class' => 'form-control',' placeholder' => trans('coupons.forms.duration') )) !!}
                                    </div>
                                </div>
                                <div class="container-months hidden form-group col-12 col-md-6 px-0">
                                        {!! Form::label('country', trans('coupons.forms.durationMonths') , array('class' => 'col-12 control-label')) !!}
                                        <div class="col-12">
                                            
                                        </div>
                                </div>
                            </div>

                            <div class="flex w-full justify-end mt-2 px-2">
                                    {!! Form::button(trans('coupons.forms.create'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>

                            {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection 

@section('footer_scripts')
<script>
(function() {

    function checkTypeCoupon() {
        if($('input[type=radio][name=type]:checked').val() == 'percent_off') {
            $('#logo-discount').html('%');
        } else {
            $('#logo-discount').html('$');
        }
    }

    checkTypeCoupon();
    
    $('input[type=radio][name=type]').on('change',function() {
        checkTypeCoupon();
    });

    $('#duration').on('change',function() {
        if($(this).val() == 'repeating') {
            $('.container-months').removeClass('hidden');
            $('.container-months > div').append('<input name="duration_in_months" class="form-control" min="1" type="number">')
        } else {
            $('.container-months').addClass('hidden');
            $('.container-months > div').empty();
        }
    });

})();
</script>
@endsection