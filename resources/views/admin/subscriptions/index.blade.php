@extends('layouts.app')

@section('template_title')
    {!! trans('subscriptions.title') !!}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="w-full flex justify-center">
            <div class="w-full sm:w-5/6 lg:w-2/3">
                <div class="card-p">

                        <div class="flex justify-between items-center pb-2 border-b mb-2">

                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold"> {!! trans('subscriptions.title') !!}</h4>

                                <div class="pl-4"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('subscriptions.subscriptionsActiveTotal') }}: {{ $susbscriptionsActivesTotal }} </span>
                                </div>
                            </div>
                            {{-- <div class="btn-group pull-right btn-group-xs">
                                    <a class="btn btn-gray rounded-full" href="/subscriptions/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('subscriptions.buttons.create-new') !!}
                                    </a>
                            </div> --}}
                        </div>

                    <div class="w-full">

                        {{-- @if(config('subscriptions.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}
                        @foreach($users as $u)
                        <div class="w-full border-b mb-2">
                        <div class="mb-2 flex flex-wrap justify-between w-full"> 
                            <div class="text-lg font-semibold">{{ $u->fullName }} </div>
                            <div>
                                <a class="btn btn-sm btn-outline-dark rounded" href="{{ route('subscriptions.invoices',[$u->id]) }}"> 
                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                    {{ trans('subscriptions.buttons.invoices') }} 
                                </a> 
                            </div>
                        </div>
                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('subscriptions.subscriptions-table.name') !!}</th>
                                        <th>{!! trans('subscriptions.subscriptions-table.status') !!}</th>
                                        <th>{!! trans('subscriptions.subscriptions-table.latestInvoice') !!}</th>
                                        <th>{!! trans('subscriptions.subscriptions-table.created') !!}</th>
                                        {{-- <th>{!! trans('subscriptions.subscriptions-table.updated') !!}</th> --}}
                                        {{-- <th>{!! trans('subscriptions.subscriptions-table.actions') !!}</th> --}}
                                        {{-- <th class="no-search no-sort"></th> --}}
                                        {{-- <th class="no-search no-sort"></th> --}}
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($u->subscriptions as $sub)
                                        <tr>
                                            
                                            <td>
                                                @php
                                                    $subscription = \Stripe\Subscription::retrieve($sub->stripe_id);
                                                    $planName =  $subscription->plan->nickname;
                                                @endphp
                                                <span class="font-semibold"> {{ $planName }} </span>
                                            </td>
                                            <td>
                                                @if($sub->ended())
                                                    <span class="badge badge-danger rounded-full"> {{ trans('subscriptions.status.cancelled') }} </span>
                                                @else
                                                <span class="badge badge-success rounded-full"> {{ trans('subscriptions.status.active') }} </span>
                                                @endif
                                            </td>
                                            <td>
                                                @if($subscription->latest_invoice)
                                                    @php  $invoice = \Stripe\Invoice::retrieve($subscription->latest_invoice); @endphp
                                                    {{-- {{ $invoice }} --}}
                                                    <span class="text-sm"> {{ $invoice->total / 100 }} {{ $invoice->currency }} </span>
                                                    @if($invoice->discount)
                                                        <span class="text-xs text-gray-600"> ({{ $invoice->discount->coupon->name }}) </span>
                                                    @endif
                                                @endif
                                            </td>
                                            {{-- <td>  --}}
                                                {{-- {{ $sub->hasIncompletePayment() }} --}}
                                                {{-- @if(!$sub->ended() && $sub->hasIncompletePayment())
                                                    Yes
                                                @else
                                                    No
                                                @endif --}}
                                            {{-- </td> --}}
                                            <td>
                                                @if($sub->created_at)
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$sub->created_at)->format('d/m/Y H:i')  }} 
                                                @endif
                                            </td>
                                            
                                            {{-- <td style="min-width: 200px;"> --}}
                                                {{-- <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('subscriptions.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('subscriptions.buttons.show') !!}
                                                </a> --}}
                                                
                                                {{-- {!! Form::open(array('url' => 'subscriptions/' . $sub->id, 'class' => 'inline-flex', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('subscriptions.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Coupon', 'data-message' => 'Are you sure you want to delete this coupon?')) !!}
                                                {!! Form::close() !!} --}}
                                            {{-- </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        </div>
                        @endforeach
                        @if(config('usersmanagement.enablePagination'))
                            <div class="w-full mt-4">
                                {{ $users->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('subscriptions.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    {{-- @if(config('subscriptions.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
