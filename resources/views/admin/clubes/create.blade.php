@extends('layouts.app')

@section('template_title')
    {!! trans('clubes.create-new-club') !!}
@endsection

@section('template_linked_css')
<style>
.dz-image-preview {
    display: none;
}
</style>
@endsection

@section('content')

    <div id="errors-edit-club" class="hidden alert alert-danger alert-dismissable fade show mx-6" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h5>
        <i class="icon fa fa-warning fa-fw" aria-hidden="true"></i>
        {{ Lang::get('auth.someProblems') }}
        </h5>
        <ul class="pl-8 text-sm">
        
        </ul>
    </div>

    <div class="container">
        <div class="flex w-full justify-center">
            <div class="w-full sm:w-5/6 md:w-4/5">
                <div class="card-p">

                    <div class="flex justify-between items-center pb-2 mb-2 border-b">
                                <h4 class="font-bold"> {!! trans('clubes.create-new-club') !!} </h4>
                                <div class="pull-right">
                                    <a href="{{ route('clubes.index') }}" class="btn btn-light btn-sm" data-toggle="tooltip" data-placement="left" title="{{ trans('clubes.buttons.back-to-clubs') }}">
                                        <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                        {!! trans('clubes.buttons.back-to-clubs') !!}
                                    </a>
                                </div>
                    </div>
                    

                    <div class="w-full">
                        {!! Form::open(array('route' => 'clubes.store', 'method' => 'POST', 'role' => 'form','id' => 'clubForm', 'class' => 'needs-validation','files' => true, 'enctype' => 'multipart/form-data')) !!}

                            {!! csrf_field() !!}

                            <div id="club-logo-container" class="row justify-content-center col-12">
                                <div class="col-auto">
                                    <div style="min-height: 110px;width: 96px;" id="clubDropzone" class="relative p-2 border-primary border-2 flex flex-wrap justify-center items-center mx-auto mb-2">
                                        {{-- <div class="w-full hidden dropzone-previews dz-preview"></div> --}}
                                        <div class="w-full dropzone-previews dz-preview">
                                            <img src="/img/club-d.png" class=''>
                                        </div>
                                        
                                    </div>
                                        <div class="dz-default dz-message"> {{ trans('forms.upload-file') }} </div>

                                        <button id="close-icon" class="px-2 flex items-center text-primary text-sm py-1 mx-auto" id="remove-avatar"> 
                                            <img src="{{ asset('img/profile/trash.png') }}" alt="Delete">
                                            <span class="pl-1">{{ trans('forms.remove-file') }} </span>
                                        </button>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                {!! Form::label('name', trans('clubes.forms.name'), array('class' => 'col-12 control-label')); !!}
                                <div class="col-12">
                                        {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                        
                                </div>
                            </div>

                            <div class="row col-12 px-0">
                                <div class="col-12 px-0 col-md-6 form-group has-feedback">
                                    {!! Form::label('entity_type_id', trans('clubes.forms.type'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {{-- {!! Form::select('entity_type_id', $types,NULL, array('id' => 'name', 'class' => 'form-control' )) !!}                                         --}}
                                            <select name="entity_type_id" id="entity_type_id" class="form-control" placeholder="{{ trans('auth.typeEntity') }}">
                                                    <option value=""> {{ trans('auth.typeEntity') }} </option>
                                                    @foreach ($types as $type)
                                                        <option value="{{ $type['id'] }}" {{ old('entity_type_id') == $type['id'] ? 'selected' : '' }} > {{ trans('fields.entitiesType.'. $type['name']) }} </option>
                                                    @endforeach
                                            </select>
                                    </div>
                                </div>
                                <div class="col-12 px-0 col-md-6 form-group has-feedback">
                                        {!! Form::label('email', trans('clubes.forms.email'), array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                                {!! Form::text('email', NULL, array('id'=>'email', 'class' => 'form-control' )) !!}                                        
                                        </div>
                                </div>
                            </div>

                            <div class="row col-12 px-0">
                                <div class="col-12 px-0 col-md-6 form-group has-feedback">
                                    {!! Form::label('website', trans('clubes.forms.website'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('website', NULL, array('id' => 'website', 'class' => 'form-control' )) !!}                                        
                                    </div>
                                </div>
                                <div class="col-12 px-0 col-md-6 form-group has-feedback">
                                        {!! Form::label('phone', trans('clubes.forms.phone'), array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                                {!! Form::text('phone', NULL, array('id' => 'phone', 'class' => 'form-control' )) !!}                                        
                                        </div>
                                </div>
                            </div>

                            {{-- Country --}}
                            <div class="col-12 col-md-6 row px-0 form-group has-feedback">
                                    {!! Form::label('country', trans('clubes.forms.country') , array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                        {!! Form::select('country_id',$countries, old('country_id'), array('id' => 'country', 'class' => 'countries form-control',' placeholder' => trans('clubes.forms.country') )) !!}
                                    </div>
                            </div>

                            <div class="flex w-full justify-end mt-2 px-2">
                                {!! Form::button(trans('clubes.forms.create'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

<script src="{{{ config('settings.dropZoneJsCDN') }}}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

$(function() {
    
    let base64URL = '';
    Dropzone.options.clubForm = {
        url: '{{ route('clubes.store') }}',
        paramName: 'file',
        autoProcessQueue: false,
        // previewsContainer: '.dz-preview',
        previewsContainer: '#clubDropzone',
        clickable: '.dz-message',
        maxFilesize: 60, // MB
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".png,.jpeg,.jpg",
        headers: {
            "Pragma": "no-cache"
        },
        accept: function(file, done) {
        // $("div#dp"+id)..css({'display':'flex'}).fadeIn();
        var reader = new FileReader();
        // Closure to capture the file information.
        base64URL = "";
        reader.onload = function(e) {
            //get the base64 url
            base64URL = e.target.result;
            //print to console

            var display = "";
            if(file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/gif' || file.type == 'image/bmp' || file.type == 'image/jpeg') {
              display = "<img src='"+base64URL+"' class=''>"
            }
            $(".dz-preview").removeClass('hidden');
            $(".dz-preview").html('');
            $(".dz-preview").html(display);
            done();

        };
        base64URL = file;
        // Read in the image file as a data URL.
        reader.readAsDataURL(file);

      },
        init: function() {
            this.on("maxfilesexceeded", function(file) {
                //
            });
            this.on("uploadprogress", function(file) {
                $('.dz-message').text("@lang('forms.uploading-file')").show();
            });
            this.on("addedfile", function(file) {
                // console.log(file);
                if (formDropzone.fileTracker) {
                    this.removeFile(formDropzone.fileTracker);
                }
                formDropzone.fileTracker = file;
                // console.log(this.files);
                // var img = `<img src="${file.dataURL}" class="img-fluid" />`; 
                // $('.dz-preview').append(img);

                // this.removeFile(file);
            });
            this.on("success", function(file, response) {
                console.log(response);
                window.location.href = response.url + '?success=created';
                // $("form").submit();
                
            });
            this.on("error", function(file, res) {
                $.each(formDropzone.files, function(i, file) {
                    file.status = Dropzone.QUEUED
                });
                $('.dz-message').text("@lang('forms.upload-file')").show();
                let lista = JSON.parse(JSON.stringify(res.errors));
                let errors = '';
                lista.forEach(e => {
                    errors += `<li> ${e} </li>`;
                })
                $('#errors-edit-club').removeClass('hidden');
                $('#errors-edit-club ul').append(errors);
                // $('.dz-message').text("@lang('forms.upload-failed')").show();
                // setTimeout(function() {
                //     $('.dz-message').text("@lang('forms.upload-file')").show();
                // }, 2000);
            });
        }
    };
    var formDropzone = new Dropzone("#clubForm");

    $("form").submit(function(event) {
        event.preventDefault();    
        event.stopPropagation();
        $('#errors-edit-club ul').html('');
        if(formDropzone.files.length > 0) {
            formDropzone.processQueue(); //processes the queue
        } else {
            submitWithoutImage();
        }
        console.log(formDropzone.files);
        console.log("FORM");
    });

    $('#close-icon').on('click',function(event) {
        event.preventDefault();    
        event.stopPropagation();
        formDropzone.removeAllFiles();
        $(".dz-preview").html("<img src='/img/club-d.png' class=''>"); 
    });

    function submitWithoutImage() {
        let isError = false;
        $('#errors-edit-club ul').html('');
        
        // let file = formDropzone.files.length > 0 ? base64URL : null;
        let name = $('#name').val();
        let entity_type_id = $('#entity_type_id').val();
        let email = $('#email').val();
        let website = $('#website').val();
        let phone = $('#phone').val();
        let country_id = $('#country').val();
        $.ajax({
          type:'POST',
          url: `/clubes`,
          data: { name, entity_type_id, email, website, phone, country_id },
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          success: function(response) {
              console.log(response);
              window.location.href = response.url + '?success=created';
            //   return result;
          },
          error: function (response, status, error) {
            //   return error;
            let lista = JSON.parse(JSON.stringify(response.responseJSON.errors));
            let errors = '';
            lista.forEach(e => { errors += `<li> ${e} </li>`; })
            $('#errors-edit-club').removeClass('hidden');
            $('#errors-edit-club ul').append(errors);
          }
        });

        return isError;
    }


});

</script>
@endsection