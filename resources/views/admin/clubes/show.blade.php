@extends('layouts.app')

@section('template_title')
  {!! trans('clubes.showing-club', ['name' => $club->name]) !!}
@endsection

@section('content')

  <div class="container">
      <div class="flex w-full justify-center">
          <div class="w-full sm:w-4/5 md:w-3/5">

        <div class="card-p">

            <div class="flex justify-between items-center pb-2 mb-2 border-b">
              <h4 class="font-bold"> {!! $club->name !!} </h4>
              <div class="float-right">
                <a href="{{ route('clubes.index') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('clubes.buttons.back-to-clubs') !!}
                </a>
              </div>
          </div>

          <div class="w-full">

            <div class="row flex justify-start items-center">
              <div class="w-auto pr-4">
                <img src="{{ $club->avatar }}" alt="{{ $club->name }}" class="h-24 lg:h-32 mx-auto rounded-circle center-block mb-3 mt-4">
              </div>
              <div class="w-auto">
                <h4 class="mt-2">
                    {{ $club->name }}
                </h4>
                @if($club->type)
                  <h6 class="mt-2"> {{ trans('fields.entitiesType.'. $club->type->name) }} </h6>
                @endif
                  <div class="mt-2">
                    <a href="{{ route('clubes.edit',[$club->id]) }}" class="btn btn-sm btn-outline-info rounded-full" title="{{ trans('titles.edit') }}">
                      <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span> {{ trans('titles.edit') }} </span>
                    </a>
                    {!! Form::open(array('url' => 'clubes/' . $club->id, 'class' => 'form-inline', 'title' => trans('titles.delete'))) !!}
                      {!! Form::hidden('_method', 'DELETE') !!}
                      {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span>' . trans('titles.delete') . '</span>' , array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Club', 'data-message' => 'Are you sure you want to delete this club?')) !!}
                    {!! Form::close() !!}
                    @if( $verifyClubs && $verifyClubs->value ) <!-- Table config_site -->
                    {{-- Verify or Unverify CLUB --}}
                    @if(!$club->verified)
                          {!! Form::open(array('url' => 'clubes/' . $club->id. '/verify', 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Verify')) !!}
                              {!! Form::hidden('_method', 'POST') !!}
                              {!! Form::button(trans('clubes.buttons.verify'), array('class' => 'btn btn-success rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Verify')) !!}
                          {!! Form::close() !!}
                          @else
                          {!! Form::open(array('url' => 'clubes/' . $club->id. '/verify', 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Unverify')) !!}
                              {!! Form::hidden('_method', 'POST') !!}
                              {!! Form::button(trans('clubes.buttons.unverify'), array('class' => 'btn btn-primary rounded-full btn-sm','type' => 'submit', 'style' =>'width: 100%;' , 'title' => 'Unverify')) !!}
                          {!! Form::close() !!}
                          @endif
                    @endif

                  </div>
              </div>
            </div>

            {{-- Email --}}
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                <div class="mr-2"> <strong> {{ trans('clubes.forms.email') }}: </strong> </div>
                <div> {{ $club->email ?? '--' }} </div>
            </div>

            {{-- Website --}}
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                <div class="mr-2"> <strong> {{ trans('clubes.forms.website') }}: </strong> </div>
                <div> {{ $club->website ?? '--' }} </div>
            </div>

            {{-- Country --}}
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                <div class="mr-2"> <strong> {{ trans('clubes.forms.country') }}: </strong> </div>
                <div> {{ $club->country ? $club->country->name : '--' }} </div>
            </div>

            {{-- Phone --}}
            <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                <div class="mr-2"> <strong> {{ trans('clubes.forms.phone') }}: </strong> </div>
                <div> {{ $club->phone ?? '--' }} </div>
            </div>

            @if ($club->created_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('clubes.clubes-table.created') }}: </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$club->created_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif

            {{-- @if ($club->updated_at)
              <div class="w-full flex flex-wrap border-b mb-1 pb-2">
                  <div class="mr-2"> <strong> {{ trans('clubes.clubes-table.updated') }}: </strong> </div>
                  <div> {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$club->updated_at)->format(config('settings.formatDate')) }} </div>
              </div>
            @endif --}}


          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection
