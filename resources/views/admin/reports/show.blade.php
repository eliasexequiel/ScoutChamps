@extends('layouts.app')

@section('template_title')
  {!! trans('reports.showing-report') !!}
@endsection

@section('content')

<div class="flex justify-center">
      <div class="w-full sm:w-4/5 md:3/4 lg:w-3/5">

        <div class="card-p">

              <div class="flex justify-between items-center pb-2 border-b mb-2">
              <h4 class="font-bold"> {!! trans('reports.showing-report') !!} </h4>
              <div class="float-right">
                <a href="{{ route('admin.reports.index') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('reports.buttons.back-to-reports') !!}
                </a>
              </div>
          </div>

          <div class="w-full px-2">

            <div class="row flex justify-center items-center">
              <div class="w-full">
                <div class="w-full">
                  @if($report->created_at)
                  <div class="text-gray-700 text-sm">
                      {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$report->created_at)->format(config('settings.formatDate'))  }}
                  </div>
                  @endif
                </div>
                <div class="mt-1">
                    <span class="font-semibold"> {{ trans('reports.table.createdBy') }}: </span>
                    @include('partials.link-profile',['uuid' => $report->user->uuid,'name' => $report->user->fullName])
                </div>

                <div class="mt-1">
                  
                  <span class="font-semibold"> {{ trans('reports.types.'.$report->tipo) }}: </span>
                  <div class="pl-4"> 
                    @if($report->tipo == 'Post')
                      <div> 
                        <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                        @include('partials.link-profile',['uuid' => $report->reported->user->uuid,'name' => $report->reported->user->fullName])
                      </div>
                      <div> {{ $report->reported->body }} </div>
                    @endif
                    @if($report->tipo == 'PostComment')
                      <div> 
                        <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                        @include('partials.link-profile',['uuid' => $report->reported->user->uuid,'name' => $report->reported->user->fullName])
                      </div>
                      <div> {{ $report->reported->body }} </div>
                    @endif 
                    @if($report->tipo == 'Image')
                      <div> 
                        <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                        @if($report->reported->model) @include('partials.link-profile',['uuid' => $report->reported->model->uuid,'name' => $report->reported->model->fullName]) @endif
                        <img class="mt-2 w-44 h-44 object-cover rounded-lg cursor-pointer" src="{{ $report->reported->fullUrl }}" alt="Image">
                      </div>
                    @endif
                    @if($report->tipo == 'Video')
                      <div> 
                        <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                        @if($report->reported->model) @include('partials.link-profile',['uuid' => $report->reported->model->uuid,'name' => $report->reported->model->fullName]) @endif
                        <video controls class="cursor-pointer w-full h-full object-cover rounded-lg">
                            <source src="{{ $report->reported->fullUrl }}" type="{{ $report->reported->mime_type }}">
                        </video>
                      </div>
                    @endif
                    @if($report->tipo == 'MediaUrl')
                      <div> 
                          <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                          @if($report->reported->user) @include('partials.link-profile',['uuid' => $report->reported->user->uuid,'name' => $report->reported->user->fullName]) @endif
                          <div class="cursor-pointer w-full h-96 object-cover rounded-lg">
                            <iframe class="w-full h-full object-cover rounded-lg" src="{{ $report->reported->url }}?mode=opaque&rel=0&autohide=1&showinfo=0&wmode=transparent" frameborder="0"></iframe>
                          </div>
                      </div>
                    @endif
                    @if($report->tipo == 'Message')
                      <div> 
                          <span class="font-semibold"> {{ trans('reports.userOwner') }}: </span>
                          @if($report->reported->user) @include('partials.link-profile',['uuid' => $report->reported->user->uuid,'name' => $report->reported->user->fullName]) @endif
                          <div> {{ $report->reported->body }} </div>
                          @if(isset($report->reported->thread))
                            <div>
                                <span class="font-semibold"> {{ trans('reports.conversationWith') }}: </span>
                                @include('partials.link-profile',['uuid' => $report->user->uuid,'name' => $report->user->fullName])                              
                            </div>
                          @endif
                      </div>
                    @endif
                  </div>
                </div>

                @if($report->message != '')
                <div class="mt-2">
                  <span class="font-semibold"> {{ trans('reports.table.message') }}: </span>
                  <span> {{ $report->message }} </span>
                </div>
                @endif

                <div class="w-full mt-4">
                    @if($report->solved)
                      <span class="rounded text-sm mr-2 p-1 bg-success text-white">
                          {{ trans('reports.solved') }}
                      </span>
                    @else
                      {{-- Solve --}}
                      <a class="btn btn-gray-outline btn-sm rounded-full" href="{{ route('admin.reports.close',['id' => $report->id]) }}"> {{ trans('reports.buttons.close') }} </a>
                    @endif
                    @if($report->reported_blocked)
                        <span class="rounded text-sm ml-2 p-1 bg-danger text-white">
                            {{ trans('reports.types.'.$report->tipo) }} {{ trans('reports.deleted') }}
                        </span>
                    @else
                        {{-- Delete Reported --}}
                        <a class="btn btn-outline-danger btn-sm rounded-full" href="{{ route('admin.reports.deleteReported',['id' => $report->id]) }}"> {{ trans('reports.buttons.remove') .' '. trans('reports.types.'.$report->tipo) }} </a>
                    @endif
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
</div>
  

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection
