@extends('layouts.app')

@section('template_title')
    {!! trans('faqs.title') !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

<div class="flex justify-center">
        
    <div class="w-11/12 lg:w-4/5">

            <div class="card-p">
                <div class="flex border-b pb-2">
                    <h4 class="font-bold">  {!! trans('faqs.title') !!} </h4>
                </div>

                <div class="flex flex-wrap w-full">
                    <div class="w-full border rounded my-2">
                        <admin-faqs :type-user="'Scout'"></admin-faqs>
                    </div>
                    <div class="w-full border rounded">
                        <admin-faqs :type-user="'Athlete'"></admin-faqs>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection