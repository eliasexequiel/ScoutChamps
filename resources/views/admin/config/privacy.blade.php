@extends('layouts.app')

@section('template_title')
    {!! trans('config.title') !!}
@endsection

@section('template_linked_css')
<style>
.filters-terms button {
    padding: 0.4rem 1rem;
    color: black;
}
.filters-terms button.active {
    padding: 0.4rem 1rem;
    background-color: black;
    color: white;
}
</style>
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="w-11/12 lg:w-4/5">
                <div class="card-p">
                    <div class="flex justify-between items-center pb-2 border-b mb-2">
                        <h4 class="font-bold">  {!! trans('config.privacy') !!} </h4>
                        <div>
                            <a href="{{ route('config.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('config.back-config') }}">
                                <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                {!! trans('config.back-to-config') !!}
                            </a>
                        </div>
                    </div>

                    <div class="w-full pt-4">
                        {!! Form::open(array('route' => 'privacy.store', 'method' => 'POST', 'role' => 'form','class' => 'needs-validation')) !!}

                            {!! csrf_field() !!}
                            
                            <div class="mb-4 flex filters-terms">
                                    <button class="active english"> EN </button>
                                    <button class="spanish"> ES </button>
                                </div>

                            <div id="privacyEn" class="col-12 px-0 form-group">    
                                    {!! Form::textarea('privacy', $privacy, array('maxlength' => '50000','rows' => '14','class' => 'summernote form-control')) !!}  
                            </div>
                            <div id="privacyEs" class="hidden col-12 px-0 form-group">
                                    {!! Form::textarea('privacy_es', $privacy_es, array('maxlength' => '50000','rows' => '14','class' => 'summernote form-control')) !!}  
                            </div>

                            <div class="w-full flex justify-end">
                                {!! Form::button(trans('config.save'), array('class' => 'btn btn-success rounded-full px-6','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        
        
        </div>
    </div>

@endsection

@section('footer_scripts')
<link href="{{ asset('/assets/summernote/dist/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/summernote/dist/summernote-bs4.css') }}" rel="stylesheet">
<script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="{{ asset('assets/summernote/dist/summernote-bs4.min.js') }}"></script>
<script>
// $(document).ready(function() {
  $('.summernote').summernote();

  $('.filters-terms button').on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass('english') && !$(this).hasClass('active')) {
            $(this).addClass('active');
            $('button.spanish').removeClass('active');
            $('#privacyEs').addClass('hidden');
            $('#privacyEn').removeClass('hidden');

        } else if ($(this).hasClass('spanish') && !$(this).hasClass('active')) {
            $(this).addClass('active');
            $('button.english').removeClass('active');
            $('#privacyEn').addClass('hidden');
            $('#privacyEs').removeClass('hidden');
        }
    })
// });
</script>
@endsection