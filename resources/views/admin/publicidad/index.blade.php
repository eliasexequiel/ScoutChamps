@extends('layouts.app')

@section('template_title')
    {!! trans('config.title') !!}
@endsection

@section('template_linked_css')
@endsection

@section('content')

{{-- ALERTS  WITH JSON --}}
<div id="edit-athlete-success" class="hidden mx-6 alert alert-success alert-dismissable fade show" role="alert">
    <a href="#" class="close" aria-label="close">&times;</a>
    <span id="message"></span>
</div>

<div class="w-full flex justify-center px-6">
    <admin-publicidades class="w-full md:w-2/3"></admin-publicidades>
</div>
@endsection

@section('footer_scripts')
<script type="text/javascript">
    $('#edit-athlete-success a.close').on('click',function() {
        $('#edit-athlete-success').addClass('hidden');
    });
</script>
@endsection