@extends('layouts.app')

@section('template_title')
    {!! trans('planes.title') !!}
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-10 col-md-8 col-lg-6">
                <div class="card-p">

                        <div class="flex justify-between items-center pb-2 border-b mb-2">

                                <div class="flex flex-wrap items-center">
                                    <h4 class="font-bold"> {!! trans('planes.title') !!}</h4>

                                    <div class="pl-4"> 
                                        <span class="badge badge-primary rounded-full text-white">{{ trans('planes.admin.planTotal') }}: {{ count($plans) }} </span>
                                    </div>
                                </div>
                            {{-- <div class="btn-group pull-right btn-group-xs">
                                    <a class="btn btn-gray rounded-full" href="/coupons/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('coupons.buttons.create-new') !!}
                                    </a>
                            </div> --}}
                        </div>

                    <div class="w-full">

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('planes.admin.table.name') !!}</th>
                                        <th>{!! trans('planes.admin.table.price') !!}</th>
                                        {{-- <th>{!! trans('planes.admin.table.actions') !!}</th> --}}
                                        {{-- <th class="no-search no-sort"></th> --}}
                                        {{-- <th class="no-search no-sort"></th> --}}
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($plans as $c)
                                        <tr>
                                            
                                            <td>
                                                <span class="font-semibold"> {{$c->nickname}} </span>
                                                {{-- <span class="block text-sm text-gray-600"> {{ $c->id }} </span> --}}
                                            </td>
                                            <td>
                                                @if($c->amount_decimal)
                                                    <span class="font-semibold"> {{ $c->amount_decimal/100 }} </span> {{ $c->currency }} / 
                                                    @if($c->interval_count == 1)
                                                    {{ $c->interval }} 
                                                    @else
                                                        {{ $c->interval_count }}  {{ $c->interval }}s
                                                    @endif
                                                @endif
                                            </td>
                                            {{-- <td> 
                                                {{ $c->duration }}
                                                @if($c->duration == 'repeating')
                                                    <span class="block text-sm text-gray-600"> ({{ $c->duration_in_months }} {{ trans('coupons.forms.months') }}) </span>
                                                @endif 
                                            </td>  --}}
                                            
                                            {{-- <td style="min-width: 80px;">
                                                {!! Form::open(array('url' => 'admin/planes/' . $c->id, 'class' => 'inline-flex', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('planes.admin.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Plan', 'data-message' => trans('planes.admin.buttons.delete-plan-question'))) !!}
                                                {!! Form::close() !!}
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @include('scripts.delete-modal-script')
    {{-- @if(config('coupons.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
