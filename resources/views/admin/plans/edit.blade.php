@extends('layouts.app')

@section('template_title')
    {!! trans('planes.admin.editPricePlan') !!}
@endsection

@section('content')

    <div class="container">
        <div class="row justify-content-center">
                <div class="col-11 col-md-8 col-lg-6">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold">  {{ trans('planes.plan') }}: {{ $plan->nickname }} </h4>
                            <div class="pull-right">
                                <a href="{{ route('admin.planes.index') }}" class="btn btn-light btn-sm float-right" title="{{ trans('planes.admin.back-plans') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('planes.admin.buttons.back-to-plans') !!}
                                </a>
                            </div>
                        
                        </div>

                    <div class="w-full">
                        {!! Form::model($plan,array('route' => ['admin.planes.update', $plan->id], 'method' => 'PUT', 'role' => 'form','id' => 'planForm')) !!}

                            {!! csrf_field() !!}


                            <div class="col-12 px-0">
                                    {!! Form::label('name', trans('planes.admin.forms.price'), array('class' => 'col-12 control-label')) !!}
                                    <div class="mt-1 col-12 col-md-8 col-lg-6">
                                            <div class="flex flex-wrap items-stretch w-full mb-4 relative">
                                                <input name="price" value="{{ $plan->amount_decimal/100 }}" type="text" class="form-control">
                                                <div class="absolute text-lg text-gray-700 inset-y-0 right-0 mr-2 flex justify-end items-center">
                                                    <span> $ </span>
                                                </div>
                                            </div>
                                    </div>
                            </div>

                            <div class="flex w-full justify-end mt-2 px-2">
                                    {!! Form::button(trans('planes.admin.forms.update'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>

                            {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection 

@section('footer_scripts')
<script>
(function() {

})();
</script>
@endsection