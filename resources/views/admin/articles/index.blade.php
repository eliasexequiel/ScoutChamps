@extends('layouts.app')

@section('template_title')
    {!! trans('articles.showing-all-articles') !!}
@endsection

@section('template_linked_css')
    @if(config('articles.enabledDatatablesJs'))
        <link rel="stylesheet" type="text/css" href="{{ config('articles.datatablesCssCDN') }}">
    @endif
    <style type="text/css" media="screen">
        .users-table {
            border: 0;
        }
        .users-table tr td:first-child {
            padding-left: 15px;
        }
        .users-table tr td:last-child {
            padding-right: 15px;
        }
        .users-table.table-responsive,
        .users-table.table-responsive table {
            margin-bottom: 0;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card-p">

                        <div class="flex flex-wrap justify-between items-center pb-2 border-b mb-2">
                            <div class="flex flex-wrap items-center">
                                <h4 class="font-bold pr-4"> {!! trans('articles.showing-all-articles') !!}</h4>

                                <div class="w-auto"> 
                                    <span class="badge badge-primary rounded-full text-white">{{ trans('articles.articleTotal') }}: {{ $articles->total() }} </span>
                                </div>
                            </div>

                            <div class="mt-2 sm:mt-0 btn-group btn-group-xs">
                                {{-- <div class="dropdown-menu dropdown-menu-right"> --}}
                                    <a class="btn btn-gray rounded-full" href="/articles/create">
                                        <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                                        {!! trans('articles.buttons.create-new') !!}
                                    </a>
                                {{-- </div> --}}
                            </div>
                    </div>

                    <div class="w-full">

                        {{-- @if(config('articles.enableSearchUsers'))
                            @include('partials.search-users-form')
                        @endif --}}

                        <div class="table-responsive users-table">
                            <table class="table table-striped table-sm vertical-middle data-table primary">
                                
                                <thead class="thead">
                                    <tr>
                                        <th>{!! trans('articles.articles-table.title') !!}</th>
                                        <th>{!! trans('articles.articles-table.created') !!}</th>
                                        {{-- <th>{!! trans('articles.articles-table.updated') !!}</th> --}}
                                        <th>{!! trans('articles.articles-table.actions') !!}</th>
                                    </tr>
                                </thead>
                                <tbody id="users_table">
                                    @foreach($articles as $c)
                                        <tr>
                                            <td style="min-width: 350px;" class="flex items-center">
                                                @if($c->image) 
                                                  <img class="rounded-lg border border-gray-400 img-fluid h-16 w-20 object-cover" src="{{ $c->image }}" alt="{{ $c->title }}">
                                                @endif
                                                <span class="pl-2"> {{$c->title}} </span> 
                                            </td>
                                            <td>
                                                @if(isset($c->created_at))
                                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$c->created_at)->format(config('settings.formatDate'))  }}
                                                @endif
                                            </td>
                                            {{-- <td>{{$c->updated_at}}</td> --}}
                                            
                                            <td class="buttons-actions">
                                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('articles.show',[$c->id]) }}" data-toggle="tooltip" title="Show">
                                                    {!! trans('articles.buttons.show') !!}
                                                </a>
                                            
                                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('articles.edit',[$c->id]) }}" data-toggle="tooltip" title="Edit">
                                                    {!! trans('articles.buttons.edit') !!}
                                                </a>
                                                {!! Form::open(array('url' => 'articles/' . $c->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                                    {!! Form::hidden('_method', 'DELETE') !!}
                                                    {!! Form::button(trans('articles.buttons.delete'), array('class' => 'btn btn-outline-danger btn-sm rounded-full','type' => 'button','data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Article', 'data-message' => 'Are you sure you want to delete this article ?')) !!}
                                                {!! Form::close() !!}

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                            
                        </div>
                        @if(config('usersmanagement.enablePagination'))
                            <div class="w-full mt-4">
                                {{ $articles->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @if ((count($articles) > config('articles.datatablesJsStartCount')) && config('articles.enabledDatatablesJs'))
        @include('scripts.datatables')
    @endif
    @include('scripts.delete-modal-script')
    @include('scripts.save-modal-script')
    @if(config('articles.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif
    {{-- @if(config('articles.enableSearchUsers'))
        @include('scripts.search-users')
    @endif --}}
@endsection
