@extends('layouts.app')

@section('template_title')
  {!! trans('articles.showing-article') !!}
@endsection

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">

        <div class="card-p">

              <div class="flex justify-between items-center pb-2 border-b mb-2">
              <h4 class="font-bold"> {!! trans('articles.showing-article') !!} </h4>
              <div class="float-right">
                <a href="{{ route('articles.index') }}" class="btn btn-light btn-sm float-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  {!! trans('articles.buttons.back-to-articles') !!}
                </a>
              </div>
          </div>

          <div class="card-body">

            <div class="row flex justify-center items-center">
              <div class="w-full">
                  <div>
                      <a href="{{ route('articles.edit',[$article->id]) }}" class="btn btn-sm btn-outline-info rounded-full" title="{{ trans('titles.edit') }}">
                        <i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span> {{ trans('titles.edit') }} </span>
                      </a>
                      {!! Form::open(array('url' => 'articles/' . $article->id, 'class' => 'form-inline', 'title' => trans('titles.delete'))) !!}
                        {!! Form::hidden('_method', 'DELETE') !!}
                        {!! Form::button('<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i> <span>' . trans('titles.delete') . '</span>' , array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Club', 'data-message' => 'Are you sure you want to delete this article?')) !!}
                      {!! Form::close() !!}
                    </div>
              </div>
              <div class="w-full">
                <h3 class="mt-2">
                    {{ $article->title }}
                </h3>
                <h6 class="mt-2">
                    {{ $article->resume ?? '' }}
                </h6>

                @if(count($article->getMedia('article')) > 0)
                  <div class="col-12 px-0">
                    <img src="{{ $article->image }}" alt="{{ $article->title }}" class="rounded center-block mb-3 mt-4">
                  </div>
                @endif
                  
              </div>
            </div>

            <div class="row col-12 px-0">
              @if($article->created_at)
              <div class="text-gray-600 text-sm col-12 px-0">
                  {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$article->created_at)->format(config('settings.formatDate'))  }}
              </div>
              @endif
              <div class="col-12 pt-2 px-0">
                 {{ $article->body }} 
                </div>
            </div>

          </div>

        </div>
      </div>
    </div>
  </div>

  @include('modals.modal-delete')

@endsection

@section('footer_scripts')
  @include('scripts.delete-modal-script')
@endsection
