@extends('layouts.app')

@section('template_title')
    {!! trans('articles.editing-article') !!}
@endsection

@section('template_linked_css')
<style>
.dz-image-preview {
    display: none;
}

.items-lang > div.active {
    color: white;
    background-color: black;
}
</style>
@endsection

@section('content')

<div class="w-full px-6">
    <div id="errors-edit-club" class="hidden alert alert-danger alert-dismissable fade show" role="alert">
        <h5>
          <i class="icon fa fa-warning fa-fw" aria-hidden="true"></i>
           {{ Lang::get('auth.someProblems') }}
        </h5>
        <ul class="pl-8 text-sm">
          
        </ul>
    </div>

        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="card-p">
                        <div class="flex justify-between items-center pb-2 border-b mb-2">
                            <h4 class="font-bold"> {!! trans('articles.editing-article') !!} </h4>
                            <div class="pull-right">
                                <a href="{{ route('articles.index') }}" class="btn btn-light btn-sm float-right" data-toggle="tooltip" data-placement="left" title="{{ trans('articles.back-articles') }}">
                                    <i class="fa fa-fw fa-reply-all" aria-hidden="true"></i>
                                    {!! trans('articles.buttons.back-to-articles') !!}
                                </a>
                            </div>
                    </div>

                    <div class="w-full">
                        {!! Form::model($article,array('route' => ['articles.update', $article->id], 'method' => 'PUT', 'role' => 'form','id' => 'articleForm', 'class' => 'needs-validation','files' => true, 'enctype' => 'multipart/form-data')) !!}
                            {!! csrf_field() !!}


                            <div class="items-lang flex my-2 border-b">
                                <div class="item-l active px-2 py-1 cursor-pointer">EN</div>
                                <div class="item-l px-2 py-1 cursor-pointer">ES</div>
                            </div>

                            {{-- ENGLISH --}}
                            <div class="w-full form-en">
                                <div class="form-group has-feedback">
                                    {!! Form::label('title', trans('articles.forms.title_en'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('title_en', $article->getTranslations('title')['en'], array('id' => 'title_en', 'class' => 'title_en form-control' )) !!}                                        
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    {!! Form::label('resume', trans('articles.forms.resume_en'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('resume_en', $article->getTranslation('resume','en',false), array('id' => 'resume_en','class' => 'form-control' )) !!}                                        
                                    </div>
                                </div>

                                {{-- Body --}}
                                <div class="form-group has-feedback">
                                        {!! Form::label('body', trans('articles.forms.body_en') , array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                            {!! Form::textarea('body_en', $article->getTranslations('body') ? $article->getTranslations('body')['en'] : '', array('id' => 'body_en','maxlength' => '10000', 'class' => 'body_en form-control' )) !!}
                                        </div>
                                </div>
                            </div>

                            {{-- SPANISH --}}
                            <div class="w-full form-es hidden">
                                <div class="form-group has-feedback">
                                    {!! Form::label('title', trans('articles.forms.title_es'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('title_es', $article->getTranslations('title') ? $article->getTranslations('title')['es'] : '', array('id' => 'title_es', 'class' => 'title_es form-control' )) !!}                                        
                                    </div>
                                </div>

                                <div class="form-group has-feedback">
                                    {!! Form::label('resume', trans('articles.forms.resume_es'), array('class' => 'col-12 control-label')); !!}
                                    <div class="col-12">
                                            {!! Form::text('resume_es', $article->getTranslation('resume','es',false), array('id' => 'resume_es','class' => 'form-control' )) !!}                                        
                                    </div>
                                    {{-- {{ $article->getTranslation('resume','es',false) }} --}}
                                </div>

                                {{-- Body --}}
                                <div class="form-group has-feedback">
                                        {!! Form::label('body', trans('articles.forms.body_es') , array('class' => 'col-12 control-label')); !!}
                                        <div class="col-12">
                                            {!! Form::textarea('body_es', $article->getTranslations('body') ? $article->getTranslations('body')['es'] : '', array('id' => 'body_es','maxlength' => '10000', 'class' => 'body_es form-control' )) !!}
                                        </div>
                                </div>
                            </div>

                            <div class="row justify-content-center col-12">
                                <div class="col-12 col-sm-9 col-md-8 col-lg-7 px-0">
                                    <div id="articleDropzone" class="relative p-2 border-gray rounded border flex flex-wrap justify-center items-center mx-auto mb-2">
                                        <div class="w-full dropzone-previews dz-preview">
                                            <img src="@if(count($article->getMedia('article')) == 0) /img/article.png  @else {{ $article->image }} @endif" class=''>
                                        </div>
                                            <span id="close-icon" class="@if(count($article->getMedia('article')) == 0 ) hidden @endif delete-media close-icon">
                                                @include('partials.close-icon')
                                            </span>
                                    </div>
                                    <div class="mx-auto max-w-xl cursor-pointer text-center rounded-full border border-gray-900 py-1 dz-default dz-message"> {{ trans('forms.upload-file') }} </div>
                                </div>
                            </div>

                            {{-- To verify if image is deleted --}}
                            <input id="imageDeleted" name="imageDeleted" type="hidden">

                            {{-- Save id to send via Ajax --}}
                            <input id="articleId" name="articleId" type="hidden" value={{ $article->id }}>

                            <div class="flex w-full justify-end mt-2 px-2">
                                {!! Form::button(trans('articles.forms.update'), array('class' => 'btn btn-success rounded-full','type' => 'submit' )) !!}
                            </div>
                            {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('footer_scripts')

<script src="{{{ config('settings.dropZoneJsCDN') }}}"></script>

<script type="text/javascript">
Dropzone.autoDiscover = false;

$(function() {

    Dropzone.options.articleForm = {
        url: '{{ route('articles.update',['id' => $article->id]) }}',
        paramName: 'file',
        autoProcessQueue: false,
        previewsContainer: '#articleDropzone',
        clickable: '.dz-message',
        maxFilesize: 60, // MB
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".png,.jpeg,.jpg",
        headers: {
            "Pragma": "no-cache"
        },
        accept: function(file, done) {
        // $("div#dp"+id)..css({'display':'flex'}).fadeIn();
        var reader = new FileReader();
        // Closure to capture the file information.
        var base64URL = "";
        reader.onload = function(e) {
            //get the base64 url
            var base64URL = e.target.result;
            //print to console

            var display = "";
            if(file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/gif' || file.type == 'image/bmp' || file.type == 'image/jpeg') {
              display = "<img src='"+base64URL+"' class=''>"
            }
            $(".dz-preview").removeClass('hidden');
            $(".dz-preview").html(display);
            $('#imageDeleted').val('imageDeleted');
            $('#close-icon').removeClass('hidden');
            $('#errors-edit-club').addClass('hidden');
            done();

        };
        // Read in the image file as a data URL.
        reader.readAsDataURL(file);

      },
        init: function() {
            this.on("maxfilesexceeded", function(file) {
                //
            });
            this.on("maxfilesreached", function(file) {
                //
            });
            this.on("uploadprogress", function(file) {
                $('.dz-message').text("@lang('forms.uploading-file')").show();
            });
            this.on("addedfile", function(file) {
                if (formDropzone.fileTracker) {
                    this.removeFile(formDropzone.fileTracker);
                }
                formDropzone.fileTracker = file;

                // this.removeFile(file);
            });
            this.on("success", function(file, response) {
                window.location.href = response.url + '?success=updated';
                // $("form").submit();
                
            });
            this.on("error", function(file, res) {
                $.each(formDropzone.files, function(i, file) {
                    file.status = Dropzone.QUEUED
                });
                $('.dz-message').text("@lang('forms.upload-file')").show();
                // window.location.href = res.url;
                let lista = JSON.parse(JSON.stringify(res.errors.errors[0]));
                let errors = '';
                lista.forEach(e => {
                    errors += `<li> ${e} </li>`;
                })
                $('#errors-edit-club').removeClass('hidden');
                $('#errors-edit-club ul').append(errors);
                
            });
        }
    };
    var formDropzone = new Dropzone("#articleForm");

    $("form").submit(function(event) {
        // console.log(formDropzone.files.length);
        event.preventDefault();    
        event.stopPropagation();
        $('#errors-edit-club ul').html('');
        if(formDropzone.files.length > 0) {
            formDropzone.processQueue(); //processes the queue
        } else {
            submitWithoutImage();
        }
        // if(formDropzone.files.length > 0) {
        //     event.preventDefault();    
        //     event.stopPropagation();
        //     if(!handleErrors()) formDropzone.processQueue();

        // }
    });

    $('#close-icon').on('click',function(event) {
        event.preventDefault();    
        event.stopPropagation();
        $('#imageDeleted').val('imageDeleted');
        formDropzone.removeAllFiles();
        // $(".dz-preview").addClass('hidden');
        $(".dz-preview").html("<img src='/img/article.png' class=''>"); 
    });

    function submitWithoutImage() {
        let isError = false;
        $('#errors-edit-club ul').html('');
        
        // let file = formDropzone.files.length > 0 ? base64URL : null;
        let title_en = $('#title_en').val();
        let resume_en = $('#resume_en').val();
        let body_en = $('#body_en').val();
        let title_es = $('#title_es').val();
        let resume_es = $('#resume_es').val();
        let body_es = $('#body_es').val();
        let imageDeleted = $('#imageDeleted').val();
        let articleId = $('#articleId').val();
        $.ajax({
          type:'PUT',
          url: `/articles/${articleId}`,
          data: { title_en, resume_en, body_en, title_es, resume_es, body_es, imageDeleted },
          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          success: function(response) {
              console.log(response);
              window.location.href = response.url + '?success=updated';
            //   return result;
          },
          error: function (response, status, error) {
            //   return error;
            let lista = JSON.parse(JSON.stringify(response.responseJSON.errors.errors[0]));
            let errors = '';
            lista.forEach(e => { errors += `<li> ${e} </li>`; })
            $('#errors-edit-club').removeClass('hidden');
            $('#errors-edit-club ul').append(errors);
            $("html, body").animate({ scrollTop: 0 }, "slow");
          }
        });

        return isError;
    }


    // Languages
    const buttonsLang = $('.item-l');
    buttonsLang.on('click',function() {
        const textLink = $(this).text();
        $(this).addClass('active').siblings().removeClass('active');
        if(textLink == 'ES') {
            $('.form-es').removeClass('hidden');
            $('.form-en').addClass('hidden');
        }
        if(textLink == 'EN') {
            $('.form-es').addClass('hidden');
            $('.form-en').removeClass('hidden');
        }
    });
    
});

</script>
@endsection