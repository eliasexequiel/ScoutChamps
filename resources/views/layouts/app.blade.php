<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="api-base-url" content="{{ route('welcome') }}">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('meta_tags')

        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
        <meta name="description" content="">
        <meta name="author" content="Mateo">
        <link rel="shortcut icon" type='image/x-icon' href="{{ asset('img/favicon.ico') }}">

        @if(App::environment('prod'))
            @include('partials.googletagmanager-head')
        @endif
        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        @yield('template_fastload_css')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href=" {{ asset('assets/air-datepicker/css/datepicker.css') }} ">
        <link rel="stylesheet" href=" {{ asset('assets/chosen/chosen.min.css') }} ">

        @yield('template_linked_css')

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!};
        </script>

        @yield('head')

    </head>
    <body>
        @if(App::environment('prod'))
            @include('partials.googletagmanager-body')
        @endif
        <div id="app">

            @if (App::environment('prod'))
                {{ \Debugbar::disable() }}
            @endif

            {{-- <div class="flex"> --}}
                    @include('partials.sidebar')

                    <div class="wrapper-content">
                        
                        @if(Route::currentRouteName() == 'home')
                        @role('athlete|scout')
                            {{-- Publicity --}}
                            <div class="w-full px-6 my-2">
                                @include('partials.publicities.publicity-above')
                            </div>
                        @endrole
                        @endif

                        @include('partials.nav')

                        <div class="w-full px-6 mt-2">
                            @include('partials.form-status')
                        </div>
            
                        <main class="my-2">

                            @yield('content')

                            @if(!Request::is('home'))
                            @role('athlete|scout')
                            
                                <div class="w-full px-6 mt-4">
                                    <p-updates></p-updates>  
                                </div>

                                {{-- Publicity --}}
                                <div class="w-full mt-4">
                                    @include('partials.publicities.publicity-above')
                                </div>
                                <div class="w-full mt-4">
                                    @include('partials.publicities.publicity-below')
                                </div>
                            @endrole
                            @endif
                        </main>

                    </div>
            {{-- </div> --}}


        </div>

        {{-- Scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>

        
        @if(Config::get('app.locale') == 'es')
            <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.es.js') }}"></script>
        @elseif(Config::get('app.locale') == 'en')
            <script src="{{ asset('assets/air-datepicker/js/i18n/datepicker.en.js') }}"></script>
        @endif

        <script src="{{ asset('assets/chosen/chosen.jquery.min.js') }}"></script>

        @include('partials.facebook-script')
        
        @yield('footer_scripts')

    </body>
</html>
