<script src="{{{ config('settings.dropZoneJsCDN') }}}"></script>
<script type="text/javascript">

Dropzone.autoDiscover = false;

$(function() {
   Dropzone.options.avatarDropzone = {
        paramName: 'file',
        maxFilesize: 50, // MB
        addRemoveLinks: true,
        maxFiles: 1,
        acceptedFiles: ".png,.jpeg,.jpg",
        renameFile: 'avatar.jpg',
        headers: {
            "Pragma": "no-cache"
        },
        init: function() {
            $('#avatar_container .dz-message').text("@lang('forms.upload-file-profile')")
            this.on("maxfilesexceeded", function(file) {
                //
            });
            this.on("maxfilesreached", function(file) {
                //
            });
            this.on("uploadprogress", function(file) {
                $('.photoProfileError').addClass('hidden');
                $('#avatar_container .dz-message').text("@lang('forms.uploading-file')").show();
            });
            this.on("maxfilesreached", function(file) {});
            this.on("complete", function(file) {
                this.removeFile(file);
            });
            this.on("success", function(file, response) {
                let html = '';
                $('#avatar_container .dz-message').text("@lang('forms.upload-file-profile')").show();
                // setTimeout(function() {
                //     $('.dz-message').text('Drop files here to upload').show();
                // }, 1000);
                $('#user_selected_avatar, .user-avatar-nav').attr('src', response.path);
                $('.sidebar .user-avatar-photo').attr('src', response.path);
            });
            this.on("error", function(file, res) {
                $('#avatar_container .dz-message').text("@lang('forms.upload-failed')").show();
                $('.photoProfileError').removeClass('hidden');
                $('.photoProfileError').html("<div> @lang('forms.upload-file-profile-error') </div>");
                setTimeout(function() {
                    $('#avatar_container .dz-message').text("@lang('forms.upload-file-profile')").show();
                }, 2000);
                console.log(res);
            });
        }
    };
    var avatarDropzone = new Dropzone("#avatarDropzone");

    // Remove avatar
    $('#remove-avatar').on('click',function() {
        $('#remove-avatar span').text("@lang('forms.removing-file')");
        $.ajax({
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "{{ route('avatar.delete') }}",
            data: {},
            success: function(response) {
                $('#user_selected_avatar, .user-avatar-nav').attr('src', response.path);
                $('.sidebar .user-avatar-photo').attr('src', response.path);
                // $(this).text({{ trans('forms.remove-file') }});
                $('#remove-avatar span').text("@lang('forms.remove-file')");
            },
            error: function (response, status, error) {
                // console.log(error);
            }
        });
    })

    // When click image call simulate click in button to upload photo.
    $('#user_selected_avatar').on('click',function() {
        $('#avatarDropzone .dz-default.dz-message').trigger('click');
    }) 
});

</script>
