@extends('layouts.app')

@section('template_title')
    {{ trans('posts.title') }}
@endsection

@section('template_fastload_css')
<style>
.text-prev {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  line-height: 1.3rem;
  max-height: 3.9rem;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
}
</style>
@endsection

@section('content')
<div class="w-full lg:w-5/6 mx-auto px-6 py-2">
    <div class="card-p">
            <div class="flex justify-between items-center pb-2 border-b mb-2">

                <div class="flex flex-wrap items-center">
                        <h4 class="font-bold"> {!! trans('posts.title') !!}</h4>

                        <div class="pl-4"> 
                            <span class="badge badge-primary rounded-full text-white">{{ trans('posts.postTotal') }}: {{ $posts->total() }} </span>
                        </div>
                </div>
                <div class="btn-group pull-right btn-group-xs">
                        <a class="btn btn-gray rounded-full" href="/posts/create">
                            <i class="fa fa-plus fa-fw" aria-hidden="true"></i>
                            {!! trans('posts.buttons.create-post') !!}
                        </a>
                </div>
            </div>

        <div class="w-full pt-4">
            @foreach ($posts as $p)
                <div class="border-b py-1">
                    <div class="flex flex-wrap justify-between items-center">
                        <div class="block w-auto sm:w-1/2 md:w-3/5"> 
                            <div>
                                <span class="text-prev"> {!! $p->body !!} </span>
                                <div class="text-xs text-gray-600">  
                                        {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$p->created_at)->format('d/m/Y H:i')  }} 
                                </div>
                            </div>
                            <div class="flex">
                                <div class="flex items-center text-sm">
                                        <img class="icon-like" src="/img/icons/like.png" alt="Like">
                                        <span class="pl-2 text-like text-xs text-gray-900">
                                            <strong> {{ count($p->likes) }} </strong> 
                                            {{ trans_choice('dashboard.posts.likes',count($p->likes)) }}
                                        </span>
                                </div>
                                <div class="ml-4 flex items-center text-sm">
                                        <img class="icon-comment" src="/img/icons/comentario.png" alt="Comment">
                                        <span class="pl-2 text-comment text-xs text-gray-900">
                                                <strong> {{ count($p->comments) }} </strong> 
                                                {{ trans_choice('dashboard.posts.comments',count($p->comments)) }}
                                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="w-auto">
                                <a class="btn btn-sm btn-outline-success rounded-full" href="{{ route('posts.show',[$p->id]) }}" data-toggle="tooltip" title="Show">
                                        {!! trans('posts.buttons.show') !!}
                                </a>
                                
                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ route('posts.edit',[$p->id]) }}" data-toggle="tooltip" title="Edit">
                                        {!! trans('posts.buttons.edit') !!}
                                </a>
                                {!! Form::open(array('url' => 'posts/' . $p->id, 'class' => 'form-inline', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button(trans('posts.buttons.delete'), array('class' => 'btn btn-outline-danger rounded-full btn-sm','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete Post', 'data-message' => 'Are you sure you want to delete this post?')) !!}
                                {!! Form::close() !!}
                                    
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        @if(config('usersmanagement.enablePagination'))
            <div class="w-full mt-4">
                    {{ $posts->links() }}
            </div>
        @endif
    </div>

</div>

@include('modals.modal-delete')

@endsection

@section('footer_scripts')
@include('scripts.delete-modal-script')
    @if(config('clubes.tooltipsEnabled'))
        @include('scripts.tooltips')
    @endif

@endsection