@extends('layouts.app-guest')

@section('template_title')
    {!! trans('profile.results') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 py-4 mb-2 w-full">
            <div class="w-full relative flex justify-center">
                    <div class="w-auto mx-auto flex items-center p-2">
                        <div class="w-auto items-center">
                            <img class="mx-auto h-32 object-contain" src="{{ $currentSport->getAvatarSelectedHome() }}" alt="{{ $currentSport->name }}">
                        </div>
                    </div>
            </div>

        </div>

        <!-- RESULTS ATHLETE-->
        <div id="ahletesClub" class="w-full px-2 md:w-11/12 mx-auto">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl">  {{ trans('titles.athletes') }} </div>
            </div>
            <div class="flex flex-wrap w-full">
            @if(count($usersAthlete) > 0)
                @foreach ($usersAthlete as $user)
                        <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/3 flex flex-wrap px-2 mb-4">
                            <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-2 border-primary border">
                                    <div class="pr-2 w-auto">
                                        @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                                    </div>
                                <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                                    <div class="w-auto">  
                                            @include('partials.user-card-information')
                                    </div>
                                </div>
                                <div class="pt-4 sm:pt-1 sm:pl-1">
                                    <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                        @if(request()->input('typeUser') != 'Athlete' && request()->input('typeUser') != 'Scout')
                                            @if($user->roles[0]->name == 'Scout')
                                                <div> <span class="badge text-sm badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                                            @endif
                                            @if($user->roles[0]->name == 'Athlete')
                                                <div> <span class="badge text-sm badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                                            @endif
                                        @endif
                                        <div class="pl-2 sm:pl-0 sm:mt-1">
                                            
                                            @php 
                                                if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                                                else $route = route('register');
                                            @endphp
                                            
                                            <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $route }}">
                                                <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                                {{ trans('profile.viewProfile') }} 
                                            </a>
                                        </div>
                                    </div>   
                                </div>
                            </div>  
                        </div>
                    

                    
                    @endforeach
                    <div class="flex justify-center w-full mt-3">
                            <div class="w-auto mb-2">
                                {{ $usersAthlete->links() }}
                            </div>
                    </div>
                @else
                <div class="w-full mb-8 flex text-center items-center justify-center">
                    <div class="text-xl"> {{ trans('profile.noResults') }} </div>
                </div>
                @endif
            </div>
        </div>

        <!-- RESULTS SCOUTS-->
        <div id="ahletesClub" class="w-full px-2 md:w-11/12 mx-auto mt-8">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl">  {{ trans('titles.scouts') }} </div>
            </div>
            <div class="flex flex-wrap w-full">
            @if(count($usersScout) > 0)
                @foreach ($usersScout as $user)
                        <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/3 flex flex-wrap px-2 mb-4">
                            <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-2 border-primary border">
                                    <div class="pr-2 w-auto">
                                        @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                                    </div>
                                <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                                    <div class="w-auto">  
                                            @include('partials.user-card-information')
                                    </div>
                                </div>
                                <div class="pt-4 sm:pt-1 sm:pl-1">
                                    <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                        @if(request()->input('typeUser') != 'Athlete' && request()->input('typeUser') != 'Scout')
                                            @if($user->roles[0]->name == 'Scout')
                                                <div> <span class="badge text-sm badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                                            @endif
                                            @if($user->roles[0]->name == 'Athlete')
                                                <div> <span class="badge text-sm badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                                            @endif
                                        @endif
                                        <div class="pl-2 sm:pl-0 sm:mt-1">
                                            
                                            @php 
                                                if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                                                else $route = route('register');
                                            @endphp
                                            
                                            <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $route }}">
                                                <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                                {{ trans('profile.viewProfile') }} 
                                            </a>
                                        </div>
                                    </div>   
                                </div>
                            </div>  
                        </div>
                    

                    
                    @endforeach
                    <div class="flex justify-center w-full mt-3">
                            <div class="w-auto mb-2">
                                {{ $usersScout->links() }}
                            </div>
                    </div>
                @else
                <div class="w-full mb-8 flex text-center items-center justify-center">
                    <div class="text-xl"> {{ trans('profile.noResults') }} </div>
                </div>
                @endif
            </div>
        </div>



        {{-- SPORTS --}}
        <section id="welcomeSports" class="w-full bg-white py-12 relative">
            <div class="w-11/12 sm:w-5/6 lg:w-3/4 mx-auto">
                <div class="mb-12 w-full text-center relative">
                    <div class="title-home text-2xl sm:text-3xl"> {{ trans('welcome.sports') }} </div>
                </div>
                <div class="relative px-6">
                    <div id="welcomeCarouselSports" class="swiper-container welcomeCarouselSports">
                        <div class="swiper-wrapper">
                            @foreach ($sports as $s)
                                <div class="swiper-slide">
                                    <div class="px-2">
                                        <a href="{{ route('guest.sport.users',['id' => $s->id]) }}" class="">
                                            <img src="{{ $s->getAvatarHome() }}" alt="{{ $s->name }}">
                                        </a>
                                    </div>
                                </div>
                            @endforeach
        
                        </div>
                    </div>
                    <div class="welcomeCarouselSports swiper-button-next">
                        <span class="arrow-right"></span>
                    </div>
                    <div class="welcomeCarouselSports swiper-button-prev">
                        <span class="arrow-left"></span>
                    </div>
                </div>
            </div>
        </section>
        

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>

@endsection

@section('footer_scripts')
<script src="{{ asset('assets/swiper/swiper-bundle.js') }}"></script>
<script src="{{ asset('assets/swiper/swiper-bundle.min.js') }}"></script>
<script type="text/javascript">
    (function() {

        new Swiper(`.swiper-container.welcomeCarouselSports`, {
            autoplay: true,
            // loop: true,
            slidesPerView: 6,
            spaceBetween: 0,
            breakpoints: {
                1350: {
                    slidesPerView: 6,
                    spaceBetween: 0
                },
                1023: {
                    slidesPerView: 6,
                    spaceBetween: 0
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 0
                },
                500: {
                    slidesPerView: 3,
                    spaceBetween: 0
                },
                400: {
                    slidesPerView: 3,
                    spaceBetween: 0
                },
                300: {
                    slidesPerView: 3,
                    spaceBetween: 0
                }
            },
            // pagination: {
            //   el: ".swiper-pagination",
            //   clickable: true
            // },
            navigation: {
                nextEl: `.welcomeCarouselSports.swiper-button-next`,
                prevEl: `.welcomeCarouselSports.swiper-button-prev`
            },
        });

    })();

</script>
@append