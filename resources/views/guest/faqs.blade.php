@extends('layouts.app-guest')

@section('template_title')
    {!! trans('welcome.faqs') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div id="faqsPage" class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 mb-2 mt-6 w-full">
            <div class="w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> 
                        {{ trans('welcome.faqs') }}
                </div>
            </div>
        </div>

        {{-- Faqs Athlete --}}
        <section class="w-11/12 sm:w-2/3 lg:w-3/5 py-6 mx-auto relative">
            <div class="ml-12 mb-6">
                <span class="border-b-4 uppercase font-semibold border-primary text-2xl"> {{ trans('titles.athlete') }} </span>
            </div>
            <div class="accordion" id="accordionAthlete">
                    @foreach ($faqsAthlete as $i => $f)
                        
                    <div class="card">
                        <div class="card-header" id="{{ $i }}">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse{{ $i }}" aria-expanded="false" aria-controls="collapse{{ $i }}">
                                 {{ $f->question }}
                            </button>
                            </h2>
                        </div>
                        <div id="collapse{{ $i }}" class="collapse" aria-labelledby="{{ $i }}" data-parent="#accordionAthlete">
                            <div class="card-body">
                                    {{ $f->answer }}
                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>
        </section>

        {{-- Faqs SCOUT --}}
        <section class="w-11/12 sm:w-2/3 lg:w-3/5 py-6 mx-auto relative">
            <div class="ml-12 mb-6">
                <span class="border-b-4 uppercase font-semibold border-primary text-2xl"> {{ trans('titles.scout') }} </span>
            </div>
            <div class="accordion" id="accordionScout">
                    @foreach ($faqsScout as $i => $f)
                        
                    <div class="card">
                        <div class="card-header" id="scout{{ $i }}">
                            <h2 class="mb-0">
                            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#scoutcollapse{{ $i }}" aria-expanded="false" aria-controls="scoutcollapse{{ $i }}">
                                 {{ $f->question }}
                            </button>
                            </h2>
                        </div>
                        <div id="scoutcollapse{{ $i }}" class="collapse" aria-labelledby="scout{{ $i }}" data-parent="#accordionScout">
                            <div class="card-body">
                                    {{ $f->answer }}
                            </div>
                        </div>
                    </div>

                    @endforeach
                </div>
        </section>


        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>
@endsection
