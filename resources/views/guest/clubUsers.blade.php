@extends('layouts.app-guest')

@section('template_title')
    {!! trans('profile.results') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 py-4 mb-2 w-full">
            <div class="w-full relative flex justify-center">
                    <div class="w-auto mx-auto flex items-center p-2">
                        <div class="w-auto pr-8 items-center">
                            <img class="mx-auto h-32 object-contain" src="{{ $currentClub->avatar }}" alt="{{ $currentClub->name }}">
                        </div>
                        <div>
                            <div class="text-2xl">
                                {{ $currentClub->name }}
                            </div>
                            @if($currentClub->type)
                            <div class="text-base text-gray-700"> {{ trans('clubes.types.'.$currentClub->type->name) }} </div>
                            @endif
                            @if($currentClub->country)
                            <div class="text-sm">  {{ $currentClub->country->name }}
                            </div>
                            @endif
                        </div>
                    </div>
            </div>

        </div>

        <!-- RESULTS ATHLETE-->
        <div id="ahletesClub" class="w-full px-2 md:w-11/12 mx-auto">
            <div class="title-clubusers mx-4 md:mx-8 text-2xl">  {{ trans('titles.athlete') }} </div>
            @if(count($usersAthlete) > 0)
            <div class="w-full relative px-6">
                <div id="carouselAthletes" class="flex swiper-container carouselAthletes">    
                    <div class="swiper-wrapper">
                    @foreach ($usersAthlete as $user)
                        <div class="swiper-slide">
                            <div class="px-2 mb-4">
                                <div class="p-2 flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-2 border-primary border">
                                        <div class="pr-2 w-auto">
                                            @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                                        </div>
                                    <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                                        <div class="w-auto">  
                                                @include('partials.user-card-information')
                                            {{-- @if($user->profile->nationality)
                                                <span class="text-xs">{{ trans('profile.athlete.nationality') }} </span> : 
                                                <span class="text-xs text-gray-700"> {{ $user->profile->nationality->name }} </span>
                                            @endif --}}
                                        </div>
                                    </div>
                                    <div class="pt-4 sm:pt-1 sm:pl-1">
                                        <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                            @if(request()->input('typeUser') != 'Athlete' && request()->input('typeUser') != 'Scout')
                                                <div> <span class="badge text-sm badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                                            @endif
                                            <div class="pl-2 sm:pl-0 sm:mt-1">
                                                
                                                @php 
                                                    if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                                                    else $route = route('register');
                                                @endphp
                                                
                                                <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $route }}">
                                                    <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                                    {{ trans('profile.viewProfile') }} 
                                                </a>
                                            </div>
                                        </div>   
                                    </div>
                                </div>  
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
                <div class="welcomeCarouselSports swiper-button-next">
                    <span class="arrow-right"></span>
                </div>
                <div class="welcomeCarouselSports swiper-button-prev">
                    <span class="arrow-left"></span>
                </div>
            </div>
            @else
                <div class="w-full mx-4 md:mx-8 mb-8">
                    <div class="text-lg"> {{ trans('profile.noResults') }} </div>
                </div>
            @endif
        </div>


        <!-- RESULTS SCOUT-->
        <div id="scoutsClub" class="mt-12 px-2 w-full md:w-11/12 mx-auto">
            <div class="title-clubusers mx-4 md:mx-8 text-2xl">  {{ trans('titles.scouts') }} </div>
            @if(count($usersScout) > 0)
            <div class="w-full relative px-6">
            <div id="carouselAthletes" class="swiper-container carouselScout">    
                    <div class="swiper-wrapper">
                    @foreach ($usersScout as $user)
                        <div class="swiper-slide">
                                <div class="px-2 mb-4">
                                    <div class="p-2 flex flex-wrap sm:flex-no-wrap items-center sm:justify-between border-primary border">
                                            <div class="pr-2 w-auto">
                                                @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                                            </div>
                                        <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                                            <div class="w-auto">  
                                                    @include('partials.user-card-information')
                                            </div>
                                        </div>
                                        <div class="pt-4 sm:pt-1 sm:pl-1">
                                            <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                                @if(request()->input('typeUser') != 'Athlete' && request()->input('typeUser') != 'Scout')
                                                    <div> <span class="badge text-sm badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                                                @endif
                                                <div class="pl-2 sm:pl-0 sm:mt-1">
                                                    
                                                    @php 
                                                        if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                                                        else $route = route('register');
                                                    @endphp
                                                    
                                                    <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $route }}">
                                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                                        {{ trans('profile.viewProfile') }} 
                                                    </a>
                                                </div>
                                            </div>   
                                        </div>
                                    </div>  
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
                <div class="welcomeCarouselSports swiper-button-next">
                    <span class="arrow-right"></span>
                </div>
                <div class="welcomeCarouselSports swiper-button-prev">
                    <span class="arrow-left"></span>
                </div>
            </div>
            @else
                <div class="w-full mx-4 md:mx-8 mb-8">
                    <div class="text-lg"> {{ trans('profile.noResults') }} </div>
                </div>
            @endif
        </div>


        {{-- CLUBES --}}
        <div class="px-6 py-4 mt-10 mb-2 w-full">
                <div class="mb-12 w-full text-center relative">
                    <div class="title-home text-2xl sm:text-3xl">  {{ trans('welcome.clubs') }} </div>
                </div>
    
                <!-- FILTERS -->
                {!! Form::open(array('route' => 'guest.search.club', 'method' => 'GET', 'class' => 'form w-full')) !!}
                <div class="w-full mx-auto md:w-2/3 lg:w-1/2 p-2 border border-primary rounded">
                        <div class="border-b pb-1 mb-2">
                            <span class="text-base font-normal"> {{ trans('titles.filters') }} </span>
                        </div>
                        <div class="flex flex-wrap">
                            {{-- Country --}}
                            <div class="w-full flex-1 px-2 border-r border-gray-400">
                                    <label class="text-gray-900 font-thin mb-0" for="nationality"> {{ trans('profile.filters.country') }} </label>
                                    {!! Form::select('country', $countries, request()->input('country'), ['class' => 'mt-2 form-control form-control-sm', 'placeholder' => trans('profile.filters.allCountries') ]) !!}
                            </div>
                                
                            <div class="flex items-end px-2 justify-end">
                                <div>
                                    <button class="mt-2 btn btn-sm btn-gray" type="submit"> {{ trans('profile.filters.search') }} </button>
                                </div>
                            </div>
                        </div>
                </div>
                {!! Form::close() !!}
        </div>

        <div class="flex flex-wrap w-full md:w-11/12 lg:w-4/5 mx-auto">
            @foreach ($clubs as $c)
                <div class="w-1/2 sm:w-1/3 md:w-1/3 lg:w-1/4 xl:w-1/4 flex flex-wrap px-2 mb-4">
                <a class="mx-auto" href="{{ route('guest.club.users',['id' => $c->id]) }}">
                    <div class="w-full items-center px-4 md:px-12 py-2">
                        <div class="w-full items-center">
                            <img class="mx-auto h-32 object-contain" src="{{ $c->avatar }}" alt="{{ $c->name }}">
                        </div>
                        <div class="text-center text-base">
                            {{ $c->name }}
                            @if($c->type)
                                <div class="font-normal text-base text-gray-700"> {{ trans('clubes.types.'.$c->type->name) }} </div>
                            @endif
                        </div>
                    </div>
                </a>  
                </div>
                
            @endforeach
                <div class="flex justify-center w-full mt-3">
                        <div class="w-auto mb-2">
                            {{ $clubs->appends(Request::only('country'))->links() }}
                        </div>
                </div>
        </div>
        

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>

@endsection

@section('footer_scripts')
<script src="{{ asset('assets/swiper/swiper-bundle.js') }}"></script>
<script src="{{ asset('assets/swiper/swiper-bundle.min.js') }}"></script>
<script type="text/javascript">
    (function() {

        let cantAthletes = "{{ count($usersAthlete) }}";
        // console.log(cantAthletes);
        
        new Swiper(`.swiper-container.carouselAthletes`, {
            autoplay: true,
            // loop: true,
            slidesPerView: 3,
            spaceBetween: 0,
            breakpoints: {
                1100: {
                    slidesPerView: 3,
                    spaceBetween: 0
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 0
                },
                650: {
                    slidesPerView: 1,
                    spaceBetween: 0
                },
                300: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            },
            navigation: {
                nextEl: `.welcomeCarouselSports.swiper-button-next`,
                prevEl: `.welcomeCarouselSports.swiper-button-prev`
            },
        });


        new Swiper(`.swiper-container.carouselScout`, {
            autoplay: true,
            // loop: true,
            slidesPerView: 3,
            spaceBetween: 0,
            breakpoints: {
                1100: {
                    slidesPerView: 3,
                    spaceBetween: 0
                },
                768: {
                    slidesPerView: 2,
                    spaceBetween: 0
                },
                650: {
                    slidesPerView: 1,
                    spaceBetween: 0
                },
                300: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            },
            navigation: {
                nextEl: `.welcomeCarouselSports.swiper-button-next`,
                prevEl: `.welcomeCarouselSports.swiper-button-prev`
            },
        });

    })();

</script>
@append