@extends('layouts.app-guest')

@section('template_title')
    {!! trans('profile.results') !!}
@endsection

@section('template_fastload_css')
@endsection

@section('content')
    <div class="w-full bg-white">
        @include('partials.publicities.publicity-above')

        <div class="px-6 py-4 mb-2 w-full">
            <div class="mb-12 w-full text-center relative">
                <div class="title-home text-2xl sm:text-3xl"> 
                        @if(request()->input('typeUser') == '3') 
                            {{ trans('titles.athletes') }} 
                        @elseif(request()->input('typeUser') == '2') 
                            {{ trans('titles.scouts') }} 
                        @else 
                        {{ trans('titles.athletes') }} / {{ trans('titles.scouts') }}
                        @endif
                </div>
            </div>

            <!-- FILTERS -->
            {!! Form::open(array('route' => 'guest.search', 'method' => 'GET', 'class' => 'form w-full')) !!}
            <div class="w-full mx-auto md:w-3/4 lg:w-2/3 p-2 border border-primary rounded">
                    <div class="border-b pb-1 mb-2">
                            <span class="text-base font-normal"> {{ trans('titles.filters') }} </span>
                    </div>
                    <div class="flex flex-wrap">
                            {{-- Sport --}}
                            <div class="w-full sm:w-2/5 px-2 sm:border-r sm:border-gray-400">
                                <label class="text-gray-900 font-thin mb-0" for="sport"> {{ trans('profile.filters.sport') }} </label>
                                <select class="form-control mt-2 form-control-sm" id="sportFilter" name="sport" value="{{ request()->input('sport') }}">
                                    <option value=""> {{ trans('profile.filters.sport') }} </option>
                                    @foreach($sports as $s)
                                        <option value="{{$s->id}}" {{(request()->input('sport') == $s->id) ? 'selected' : '' }} > {{ trans('sportfields.titles.'.$s->name) }} </option>
                                    @endforeach
                                </select>
                            </div>
                            {{-- Nationality --}}
                            <div class="flex-1 w-full sm:w-auto px-2 sm:border-r sm:border-gray-400">
                                <label class="text-gray-900 font-thin mb-0" for="nationality"> {{ trans('profile.filters.nationality') }} </label>
                                {!! Form::select('nationality', $countries, request()->input('nationality'), ['class' => 'mt-2 form-control form-control-sm', 'placeholder' => trans('profile.filters.allNationalities') ]) !!}
                            </div>
                        
                        {{-- @if(request()->input('typeUser') == 'Athlete') --}}
                        <input type="hidden" name="typeUser" value="{{ request()->input('typeUser') }}">
                        {{-- @elseif(request()->input('typeUser') == 'Scout') --}}
                            {{-- <input type="hidden" name="typeUser" value="Scout"> --}}
                        {{-- @endif --}}
                            
                        <div class="flex items-end px-2 justify-end">
                            <div>
                                <button class="mt-2 btn btn-sm btn-gray" type="submit"> {{ trans('profile.filters.search') }} </button>
                            </div>
                        </div>
                    </div>
            </div>
            {!! Form::close() !!}
        </div>

        <!-- RESULTS -->
        <div class="flex flex-wrap w-full md:w-11/12 mx-auto">
        @if(count($users) > 0)
        @foreach ($users as $user)
                <div class="w-full sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/3 flex flex-wrap px-2 mb-4">
                    <div class="w-full flex flex-wrap sm:flex-no-wrap items-center sm:justify-between px-1 py-2 border-primary border">
                            <div class="pr-2 w-auto">
                                @include('partials.avatar-user',['user' => $user, 'size' => 'h-16 w-16'])
                            </div>
                        <div class="w-3/5 sm:w-2/5 flex flex-wrap items-center">
                            <div class="w-auto">  
                                    @include('partials.user-card-information',['link' => true])
                            </div>
                        </div>
                        <div class="pt-4 sm:pt-1 sm:pl-1">
                            <div class="flex items-center flex-row sm:flex-col text-center flex-wrap">
                                @if(request()->input('typeUser') != '2' && request()->input('typeUser') != '3')
                                    @if($user->roles[0]->name == 'Scout')
                                        <div> <span class="badge text-sm badge-primary rounded-lg"> {{trans('titles.Scout') }} </span> </div>
                                    @endif
                                    @if($user->roles[0]->name == 'Athlete')
                                        <div> <span class="badge text-sm badge-success rounded-lg"> {{trans('titles.Athlete') }} </span> </div>
                                    @endif
                                @endif
                                <div class="pl-2 sm:pl-0 sm:mt-1">
                                    
                                    @php 
                                        if(Auth::check()) $route = route('public.profile.show',['uuid' => $user->uuid]);
                                        else $route = route('register');
                                    @endphp
                                    
                                    <a class="btn btn-sm btn-outline-info rounded-full" href="{{ $route }}">
                                        <i class="fa fa-eye fa-fw" aria-hidden="true"></i> 
                                        {{ trans('profile.viewProfile') }} 
                                    </a>
                                </div>
                            </div>   
                        </div>
                    </div>  
                </div>
            

            
            @endforeach
            <div class="flex justify-center w-full mt-3">
                    <div class="w-auto mb-2">
                        {{ $users->appends(Request::only('fullname','sport','typeUser','nationality'))->links() }}
                    </div>
            </div>
        @else
        <div class="w-full mb-8 flex text-center items-center justify-center">
            <div class="text-xl"> {{ trans('profile.noResults') }} </div>
        </div>
        @endif
        </div>

        {{-- Publicities --}}
        @include('partials.publicities.publicity-below')
        
        <!-- Contacto -->
        @include('welcome.contact')
    </div>
@endsection

@section('footer_scripts')
@append