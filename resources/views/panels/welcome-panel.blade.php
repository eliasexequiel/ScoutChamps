@php

    $levelAmount = 'level';

    if (Auth::User()->level() >= 2) {
        $levelAmount = 'levels';

    }

@endphp

<div class="card">
    <div class="card-body">
    <div class="mb-4 flex flex-wrap">

        <h6> Welcome {{ Auth::user()->fullName }} </h6>

        <div class="mt-2 w-full">
        @role('admin', true)
            <span class="rounded-lg badge badge-primary">
                Admin Access
            </span>
        @endrole
        @role('athlete', true)
            <span class="rounded-lg badge badge-warning">
                Athlete Access
            </span>
        @endrole
        @role('scout', true)
            <span class="rounded-lg badge badge-warning">
                Scout Access
            </span>
        @endrole
        </div>
    </div>
        
        <p>
            You have
                <strong>
                    @role('admin')
                       Admin
                    @endrole
                    @role('athlete')
                       Athlete
                    @endrole
                    @role('scout')
                       Scout
                    @endrole
                </strong>
            Access
        </p>
        @role('admin')

            <hr>

            <p>
                You have permissions:
                @permission('view.users')
                    <span class="badge badge-primary margin-half margin-left-0">
                        {{ trans('permsandroles.permissionView') }}
                    </span>
                @endpermission

                @permission('create.users')
                    <span class="badge badge-info margin-half margin-left-0">
                        {{ trans('permsandroles.permissionCreate') }}
                    </span>
                @endpermission

                @permission('edit.users')
                    <span class="badge badge-warning margin-half margin-left-0">
                        {{ trans('permsandroles.permissionEdit') }}
                    </span>
                @endpermission

                @permission('delete.users')
                    <span class="badge badge-danger margin-half margin-left-0">
                        {{ trans('permsandroles.permissionDelete') }}
                    </span>
                @endpermission

            </p>

        @endrole

    </div>
</div>
