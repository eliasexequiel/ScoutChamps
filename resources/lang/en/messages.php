<?php 

return [
    'title'            => 'Messages',
    'newMessage'       => 'New Message',
    'noResults'        => 'No results',
    'to'               => 'To',
    'message'          => 'Message',
    'send'             => 'Send',
    'conversations'    => 'Conversations',
    'noConversations'  => 'No Conversations',
    'inputPlaceholder' => 'Text here...',
    'conversationWith' => 'Conversation with',
    'conversationWithAdmin' => 'Conversation with Administrator',
    'archive'          => 'Archive Conversation', 
    'archiveConversation' => 'Are you sure you want to archive this conversation?',
    'moreResults'       => 'Load more',
    'defaultMessageToScout' => 'Hello, I invite you to take a look at <a href=":link"> my profile </a> . I believe I have what it takes to succeed in this sport. Regards.',
    'defaultMessages'  => [
        [ 'value' => 'hi', 'name' => 'Hi' ],
        [ 'value' => 'how are you?', 'name' => 'How are you?' ],
        [ 'value' => 'nice to meet you', 'name' => 'Nice to meet you' ]
    ]   
];