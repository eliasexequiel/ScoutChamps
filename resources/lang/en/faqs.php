<?php

return [
    'title' => 'Faqs',
    'newQuestion' => 'New Question',
    'editQuestion' => 'Edit Question',
    'form' => [
        'edit'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'delete'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'question' => 'Question',
        'answer'   => 'Answer',
        'question_es' => 'Question (es)',
        'answer_es'   => 'Answer (es)',
        'save'     => 'Save',
        'update'   => 'Update',
        'cancel'   => 'Cancel' 
    ]
];