<?php 

return [
    'title'           => 'Configuration',
    'save'            => 'Save',   
    'globalConfig'    => 'Global Config',
    'showClubsInHome' => 'Show Section Teams/Universities in Home',  
    'limitImagesAtleteFree' => 'Limit to Plan Athlete Free to upload Images',
    'limitImagesScoutFree'  => 'Limit to Scout to upload Images',
    'verifyScoutsClubs'     => 'Verify Scouts on Entities',
    'verifyUsers'     => 'Verify email users',
    'infoVerifyUsers' => 'Verify if Scouts are on entities.',
    'verifyClubs'     => 'Verify Entities',
    'infoVerifyClubs' => 'Verify entities registered by scouts and athletes.',
    'contactInfo'     => 'Contact Info',
    'contactPhone'    => 'Contact Phone',
    'contactEmail'    => 'Contact Email',

    // Flash Messages
    'updateSuccess'   => 'Successfully updated config! ',
    'updateTermsSuccess' => 'Successfully updated Terms and Conditions',
    'updatePrivacySuccess' => 'Successfully updated Privacy Policy',

    'publicityAbove'  => 'Publicity Above',
    'publicityBelow'  => 'Publicity Below',
    'terms'           => 'Terms and Conditions',
    'privacy'         => 'Privacy Policy',
    'back-to-config'  => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Config</span>',
    'back-config'     => 'Back to Config',
    'dropzoneLabel'   => 'Click or drop file here'
];