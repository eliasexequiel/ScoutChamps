<?php

return [
    'title' => 'Coupons',
    'create-new-coupon'     => 'Create New Coupon',
    'show-deleted-coupons'    => 'Show Deleted Coupon',
    'editing-coupon'          => 'Editing Coupon :name',
    'showing-coupon'          => 'Showing Coupon :name',
    'showing-coupon-title'    => ':name\'s Information',
    'back-coupons'            => 'Back to Coupons',

    // Flash Messages
    'createSuccess'   => 'Successfully created coupon! ',
    'updateSuccess'   => 'Successfully updated coupon! ',
    'deleteSuccess'   => 'Successfully deleted coupon! ',

    'couponTotal'    => 'Coupon Total',

    'coupons-table' => [
        'id'          => 'ID',
        'name'        => 'Name',
        'discount'    => 'Discount',
        'duration'    => 'Duration',
        'used'        => 'Used',
        'created'     => 'Created',
        'updated'     => 'Updated',
        'actions'     => 'Actions',
        'updated'     => 'Updated',
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Name',
        'type'      => 'Type',
        'type_percent_off'  => 'Discount percentage',
        'type_amount_off'  => 'Fixed percentage',
        'code'      => 'Code',
        'discount'  => 'Discount',
        'duration'  => 'Duration',
        'durationMonths'    => 'Months',
        'months'    => 'months',
        'create'    => 'Create Coupon',
        'update'    => 'Update Coupon',
    ],

    'buttons' => [
        'create-new'    => 'New Coupon',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-coupons' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Coupons</span>',
        'back-to-coupon'  => 'Back  <span class="hidden-xs">to Coupon</span>',
        'delete-coupon'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Coupon</span>',
        'edit-coupon'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Coupon</span>',
    ],
    
];