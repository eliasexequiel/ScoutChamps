<?php

return [
    'title'         => 'Notifications',
    'noResults'     => "There aren't notifications",
    'markAllAsRead' => 'Mark all notifications as read',
    'markAsRead'    => 'Mark as read',
    'read'          => 'Read',
    'unread'        => 'Unread',
    'Subscriptions' => 'Subscriptions',
    'NewUsers'      => 'New Users',
    'Clubs'         => 'Teams',
    'Showcase'      => 'Showcase',
    'Reports'       => 'Reports',
    'ContactsBetweenScoutsAndAthletes' => 'Contacts Scouts/Athletes',

    'messages'  => [
        'favoriteUser'          => ' add you as favorite.',
        'followUser'            => ' started following you.',
        'invitationRecruitment' => ' invited you to a new ',
        'requestInvitationRecruitment' => ' requested an invitation on a ',
        'recruitment'           => 'recruitment.',
        'clubCreated'           => ' created the entity ',
        'addedSusbcription'     => ' subscribed to plan ',
        'deletedSusbcription'   => ' removed his subscription on plan ',
        'registered'            => ' registered in ScoutChamps.',
        'showcase'              => ' added :typeEntity :nameEntity on his ',
        'showcaseLink'          => 'Showcase',
        'report'                => ' created a new ',
        'reportTitle'           => 'report.',
        'couponUsed'            => ' used coupon ',
        'reportedDeleted'       => 'A :type has been deleted by the Administrator.',
        'contactMessagePrivate' => ' contacted athlete ',
        'entityContactedByAdmin'=> '<strong>Scoutchamps</strong> contacted <strong> :nameEntity </strong> that you added in your '
    ]
];