<?php 

return [
    'title'         => 'Posts',
    'new-post'      => 'New Post',
    'editing-post'  => 'Editing Post',
    'showing-post'  => 'Showing Post',
    
    // Flash Messages
    'createSuccess'   => 'Successfully created post! ',
    'updateSuccess'   => 'Successfully updated post! ',
    'verifySuccess'   => 'Successfully verified post! ',
    'deleteSuccess'   => 'Successfully deleted post! ',

    'postTotal'     => 'Post Total',
    
    'deleteQuestion'  => 'Are you sure you want to delete this post?',
    
    'forms' => [
        'id'        => 'ID',
        'text'      => 'Text',
        'create'    => 'Create Post',
        'update'    => 'Update Post',
        'save'      => 'Save'
    ],

    'buttons' => [
        'create-post'    => 'New Post',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-posts' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Posts</span>',
        'back-to-post'  => 'Back  <span class="hidden-xs">to Post</span>',
        'delete-post'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Post</span>',
        'edit-post'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Post</span>',
    ],
];