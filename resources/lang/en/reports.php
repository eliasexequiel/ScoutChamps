<?php 

return [
    'title'   => 'Reports',
    'showing-report'    => 'Showing Report',
    'reportTotal'   => 'Reports Total',
    
    'reportSuccess'   => ':type reported.',
    'reportMessage'   => 'Are you sure you want to report this :type?',
    'reportClosedSuccess'   => 'Successfully Report closed.',
    'reportDeletedSuccess'  => 'Successfully :type deleted.',

    'conversationWith' => 'Conversation with',
    'table' => [
        'id'        => 'ID',
        'type'      => 'Type',
        'message'   => 'Message',
        'actions'   => 'Actions',
        'createdBy' => 'Created by user',
        'created'   => 'Date',
        'updated'   => 'Updated'
    ],

    'userOwner' => 'User',
    'solved'    => 'solved',
    'deleted'   => 'deleted',

    'types' => [
        'PostComment'   => 'Post Comment',
        'Post'          => 'Post',
        'Image'         => 'Image',
        'Video'         => 'Video',
        'MediaUrl'      => 'Video',
        'Message'       => 'Message'
    ],

    'buttons' => [
        'report'          => 'Report',
        'delete'          => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span>',
        'show'            => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span>',
        'edit'            => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span>',
        'back-to-reports' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Reports</span>',
        'close'           => 'Close Report',
        'remove'          => 'Remove'
    ],
];