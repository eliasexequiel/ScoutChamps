<?php 

return [
    'favorites' => [
        'noResults'          => 'No news',
        'newPhotos'          => 'upload a new photo.',
        'newVideos'          => 'upload a new video.',
        'updatePhotoProfile' => 'updated photo profile.',
        'updateSports'       => 'updated his sports.',
        'updateClubs'        => 'updated his clubs.',
        'newPost'            => ' published a new',
        'newPostShowing'     => ' published a new post',
        'newPostShowingOwn'  => ' published a new post'
    ],
    'followed' => [
        'title'         => 'Followed',
        'noResults'     => 'No news',
        'loadMore'      => 'Load more'
    ],
    'followed-favorite' => [
        'title'         => 'Followed - Favorites'
    ],
    'updates' => [
        'title'             => 'Updates',
        'followedUpdate'    => 'followed',
        'userRegistered'    => 'joined ',
        'noResults'         => 'Not new updates'
    ],

    'posts' => [
        'newPost'           => 'New Post',
        'myPosts'           => 'My Posts',
        'publish'           => 'Publish',
        'post'              => 'Post',
        'publishSuccess'    => 'Post published',
        'noComments'        => 'No comments',
        'noPosts'           => 'No posts',
        'comment'           => 'Comment',
        'comments'          => '{0} Comments|{1} Comment|{2,*} Comments',
        'like'              => 'Like',
        'likes'             => '{0} Likes|{1} Like|{2,*} Likes',
        'delete'            => 'Delete'
    ],

    'articles' => [
        'title'         => 'Latest Scoutchamps news',
        'readmore'      => 'Read More',
        'noResults'     => 'Not new articles',
        'moreResults'   => 'More Articles'
    ],

    'you'   => 'You',
    'publications' => [
        'title'         => 'Feed',
        'adminTitle'    => 'Activities Users'
    ],

    'recruitments'  => [
        'actives'        => '{0} recruitments actives|{1} recruitment active|{2,*} recruitments actives',
        'noRecruitments' => 'No recruitments'
    ],

    'popularAthletes' => 'Popular Athletes',
    'popularScouts'   => 'Popular Scouts',
    'popularUsers'    => 'Popular Users',
    'popularScoutsSuccess'   => 'Successfully updated scouts!',
    'popularAthletesSuccess' => 'Successfully updated athletes!'
];