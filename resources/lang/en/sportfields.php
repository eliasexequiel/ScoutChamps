<?php

return [
    'titles' => [
        'Track and field'       => 'Track and field',
        'Baseball'              => 'Baseball',
        'Basketball'            => 'Basketball',
        'Boxing'                => 'Boxing',
        'Cycling'               => 'Cycling',
        'Football'              => 'Football',
        'Golf'                  => 'Golf',
        'Tennis'                => 'Tennis',
        'Rugby'                 => 'Rugby',
        'Soccer'                => 'Soccer',
        'Swimming and diving'   => 'Swimming and diving',
        'Voleyball'             => 'Voleyball',
        'Triathlon'             => 'Triathlon',
        'Equestrian Jumping'    => 'Equestrian Jumping',
        'All Sports'            => 'All Sports'
    ],
    'position' => 'Primary Position',
    'positionSecondary' => 'Secondary Position',
    'Football' => [
        'fields' => [
            'Center(C)' => 'Center(C)',
            'Offensive Guard(OG)' => 'Offensive Guard(OG)',
            'Offensive Tackle(OT)' => 'Offensive Tackle(OT)',
            'Tight End(TE)' => 'Tight End(TE)',
            'Wide Receiver(WR)' => 'Wide Receiver(WR)',
            'Fullback(FB)' => 'Fullback(FB)',
            'Running Back(RB)' => 'Running Back(RB)',
            'Tailback(TB)' => 'Tailback(TB)',
            'Halfback(HB)' => 'Halfback(HB)',
            'Wingback(WB)' => 'Wingback(WB)',
            'Slotback(SB)' => 'Slotback(SB)',
            'Quarterback(QB)' => 'Quarterback(QB)',
            'Defensive End(DE)' => 'Defensive End(DE)',
            'Defensive Tackle(DT)' => 'Defensive Tackle(DT)',
            'Nose Guard(NG)' => 'Nose Guard(NG)',
            'Linebacker(LB)' => 'Linebacker(LB)',
            'Cornerback(CB)' => 'Cornerback(CB)',
            'Safety(FS or SS)' => 'Safety(FS or SS)',
            'Defensive Back(DB)' => 'Defensive Back(DB)',
            'Nickelback(NB)' => 'Nickelback(NB)'
        ]
    ],
    'Soccer' => [
        'fields' => [
            'Goalkeeper(GK)' => 'Goalkeeper(GK)',
            'Center Back(CB)' => 'Center Back(CB)',
            'Right Back(RB)' => 'Right Back(RB)',
            'Left Back(LB)' => 'Left Back(LB)',
            'Center Defensive Mid(CBM)' => 'Center Defensive Mid(CBM)',
            'Center Mid(CM)' => 'Center Mid(CM)',
            'Right Mid(RM)' => 'Right Mid(RM)',
            'Left Mid(LD)' => 'Left Mid(LD)',
            'Center Attacking Mid(CAM)' => 'Center Attacking Mid(CAM)',
            'Right Wing(RW)' => 'Right Wing(RW)',
            'Left Wing(LW)' => 'Left Wing(LW)',
            'Striker(ST)' => 'Striker(ST)'
        ]
    ]
];
