<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email' => 'Correo',
    'create_user_ph_email'    => 'Correo',
    'create_user_icon_email'  => 'fa-envelope',

    'create_user_label_username' => 'Username',
    'create_user_ph_username'    => 'Username',
    'create_user_icon_username'  => 'fa-user',

    'create_user_label_firstname' => 'Nombre',
    'create_user_ph_firstname'    => 'Nombre',
    'create_user_icon_firstname'  => 'fa-user',

    'create_user_label_lastname' => 'Apellido',
    'create_user_ph_lastname'    => 'Apellido',
    'create_user_icon_lastname'  => 'fa-user',

    'create_user_label_password' => 'Contraseña',
    'create_user_ph_password'    => 'Contraseña',
    'create_user_icon_password'  => 'fa-lock',

    'aboutme'   => 'Sobre Mi',

    'create_user_label_pw_confirmation' => 'Confirmar Contraseña',
    'create_user_ph_pw_confirmation'    => 'Confirmar Contraseña',
    'create_user_icon_pw_confirmation'  => 'fa-lock',

    'create_user_label_location' => 'User Location',
    'create_user_ph_location'    => 'User Location',
    'create_user_icon_location'  => 'fa-map-marker',

    'create_user_label_role' => 'User Role',
    'create_user_ph_role'    => 'Select User Role',
    'create_user_icon_role'  => 'fa fa-fw fa-shield',

    'create_user_button_text' => 'Create New User',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title' => 'Edit User Information',

    'label-username' => 'Username',
    'ph-username'    => 'Username',

    'label-useremail' => 'User Email',
    'ph-useremail'    => 'User Email',

    'label-userrole_id' => 'User Access Level',
    'option-label'      => 'Select a Level',
    'option-user'       => 'User',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the User!',

    'submit-btn-icon' => 'fa-save',
    'username-icon'   => 'fa-user',
    'useremail-icon'  => 'fa-envelope-o',

    'edit-clubs'     => 'Editar Clubes',
    'change-pw'      => 'Cambiar Contraseña',
    'cancel'         => 'Cancelar',
    'save-changes'   => '<i class="fa fa-fw fa-save" aria-hidden="true"></i> Guardar cambios',

    // Search Users Form
    'search-users-ph' => 'Buscar usuarios',
    'upload-file-profile' => 'Foto de Perfil',
    'upload-file'     => 'Cargue archivos aquí',
    'uploading-file'  => 'Subiendo...',   
    'upload-failed'   => 'Subida fallida',
    'upload-file-profile-error' => 'Tamaño máximo excedido.',
    'remove-file'     => 'Eliminar archivo',
    'removing-file'   => 'Eliminando...',

    'months' => [
        'January'   => 'Enero',
        'February'  => 'Febrero',
        'March'     => 'Marzo',
        'April'     => 'Abril',
        'May'       => 'Mayo',
        'June'      => 'Junio',
        'July'      => 'Julio',
        'August'    => 'Agosto',
        'September' => 'Septiembre',
        'October'   => 'Octubre',
        'November'  => 'Noviembre',
        'December'  => 'Diciembre'
    ]

];
