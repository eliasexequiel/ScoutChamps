<?php

return [
    'title'   => 'Denuncias',
    'showing-report'    => 'Ver Denuncia',
    'reportTotal'   => 'Total Denuncias',
    
    'reportSuccess'   => ':type denunciado.',
    'reportMessage'   => 'Estás seguro que quieres denunciar este :type?',
    'reportClosedSuccess'   => 'Denuncia cerrada correctamente.',
    'reportDeletedSuccess'  => ':type eliminado correctamente.',

    'conversationWith' => 'Conversación con',
    'table' => [
        'id'        => 'ID',
        'type'      => 'Tipo',
        'message'   => 'Mensaje',
        'actions'   => 'Acciones',
        'createdBy' => 'Creado por usuario',
        'created'   => 'Fecha',
        'updated'   => 'Actualizado'
    ],

    'userOwner' => 'Usuario',
    'solved'    => 'solucionado',
    'deleted'   => 'eliminado',

    'types' => [
        'PostComment'   => 'Comentario en Post',
        'Post'          => 'Post',
        'Image'         => 'Imagen',
        'Video'         => 'Video',
        'MediaUrl'      => 'Video',
        'Message'       => 'Mensaje',
    ],

    'buttons' => [
        'report'        => 'Denunciar',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-reports' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Denuncias</span>',
        'close'           => 'Cerrar Denuncia',
        'remove'          => 'Eliminar'
    ],
];
