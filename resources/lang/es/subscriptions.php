<?php
return [
    'title' => 'Suscripciones',

    'back-subscriptions'    => 'Volver a Suscripciones',
    'subscriptionsTotal'    => 'Total Suscripciones',
    'subscriptionsActiveTotal' => 'Total Suscripciones Activas',

    'subscriptions-table' => [
        'id'          => 'ID',
        'user'        => 'Usuario',
        'name'        => 'Nombre',
        'status'      => 'Estado',
        'hasPendingPayments' => 'Pagos Pendientes',
        'latestInvoice' => 'Última Factura',
        'created'     => 'Creado',
        'updated'     => 'Actualizado',
        'actions'     => 'Acciones'
    ],

    'status' => [
        'cancelled' => 'Cancelado',
        'active'    => 'Activo'
    ],

    'buttons'   => [
        'invoices'  => 'Mostrar facturas',
        'back-to-subscriptions' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Suscripciones</span>',
    ],

    'invoices' => [
        'title'           => 'Facturas',
        'invoicesTotal'   => 'Total Facturas',
        'table' => [
            'total'      => 'Total',
            'subtotal'   => 'Subtotal',
            'tax'        => 'Impuesto',
            'amountPaid' => 'Pagado',
            'created'    => 'Fecha',
            'status'     => 'Estado'
        ],
        'download' => 'Descargar',
        'status' => [
            'paid'      => 'Paga',
            'pending'   => 'Pendiente',
            'open'      => 'Abierta'
        ]
    ],
    'mysubscriptions' => [
        'title'             => 'My plan',
        'currentPlan'       => 'Plan actual',
        'modifyPlan'        => 'Modificar',
        'newPlan'           => 'Elige tu nuevo plan',
        'invoices'          => 'Facturas',
        'paymentsMethods'   => 'Métodos de Pago',
        'noPaymentsMethods' => "No se encontraron métodos de pago",
        'noSubscriptions'   => "No se encontraron suscripciones",
        'noInvoices'        => "No se encontraron facturas",
        'plans'             => 'Mostrar planes',
        'addPaymentMethod'  => 'Agregar método de pago',
        'saveCard'          => 'Guardar tarjeta',
        'storePaymentMethodSuccess' => 'Método de pago agregado exitosamente!',
        'defaultCardSuccess'        => 'Método de pago por defecto guardado exitosamente!',
        'deleteCardSuccess'         => 'Método de pago eliminado exitosamente!',
        'labelDefaultPaymentMethod' => 'Método por defecto',
        'fields' => [
            'cardholderName' => 'Nombre Titular',
        ],
        'buttons' => [
            'defaultCard'  => 'Usar en suscripción activa',
            'delete'       => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        ],
    ],

    'changeSubscription' => 'Cambiar Subscipción',
    'changeLabel'        => '¿Quieres cambiar tu plan actual por el plan :name?',
    'changeButtonAccept' => 'Aceptar',
    'changeButtonCancel' => 'Cancelar',
];
