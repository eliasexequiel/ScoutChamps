<?php
return [
    // Titles
    'showing-all-clubs'     => 'Entidades',
    'clubs-menu-alt'        => 'Entidades',
    'create-new-club'       => 'Crear Entidad',
    'editing-club'          => 'Editar Entidad :name',
    'showing-club'          => 'Ver Entidad :name',
    'showing-club-title'    => 'Ver :name',

    // Flash Messages
    'createSuccess'   => 'Entidad creado exitosamente! ',
    'updateSuccess'   => 'Entidad actualizado exitosamente! ',
    'deleteSuccess'   => 'Entidad eliminado exitosamente! ',

    'clubTotal'     => 'Total Entidades',

    'clubes-table' => [
        'id'        => 'ID',
        'name'      => 'Nombre',
        'website'   => 'Sitio Web',
        'type'      => 'Tipo',
        'email'     => 'Correo',
        'phone'     => 'Teléfono',
        'country'   => 'País',
        'created'   => 'Creado',
        'updated'   => 'Actualizado',
        'actions'   => 'Acciones',
        'verified'  => 'Verificado',
        'noVerified'  => 'No Verificado',
    ],

    'forms' => [
        'id'        => 'ID',
        'name'      => 'Nombre',
        'website'   => 'Sitio Web',
        'type'      => 'Tipo',
        'email'     => 'Correo',
        'phone'     => 'Teléfono',
        'country'   => 'País',
        'create'    => 'Crear Entidad',
        'update'    => 'Actualizar Entidad',
        'errorName' => 'Ya existe otra entidad con el mismo nombre'
    ],

    'buttons' => [
        'create-new'    => 'Nueva Entidad',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'verify'        => '<i class="fa fa-check fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Verificar</span>',
        'unverify'      => '<i class="fa fa-close fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Marcar como no verificado</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-clubs' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Entidades</span>',
        'back-to-club'  => 'Volver  <span class="hidden-xs">a Entidad</span>',
        'delete-club'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Entidad</span>',
        'edit-club'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Entidad</span>',
    ],

    'types' => [
        'Club'          => 'Academia/Equipo',
        'Institute'     => 'Academia',
        'University'    => 'Universidad',
        'Sponsor'       => 'Sponsor',
        'Company'       => 'Empresa',
        'clubs'         => 'Academias/Equipos',
        'institutes'    => 'Academias',
        'universities'  => 'Universidades',
        'sponsors'      => 'Sponsors',
        'companies'     => 'Empresas',
        'addClub'       => 'Agregar Academia/Equipo',
        'addInstitute'  => 'Agregar Academia',
        'addUniversity' => 'Agregar Universidad',
        'addSponsor'    => 'Agregar Sponsor',
        'addCompany'    => 'Agregar Empresa'
    ],
    'messageDeleteTitle' => 'Eliminar entidad',
    'messageDelete' => 'Seguro que quiere eliminar la entidad?'
];
