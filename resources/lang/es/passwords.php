<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Contraseñas deben tener al menos 6 caracteres.',
    'reset'    => 'Tu contraseña ha sido reestablecida!',
    'sent'     => 'Te hemos enviado un correo para reestablecer tu contraseña!',
    'token'    => 'El token para reestablecer la contraseña es inválido.',
    'user'     => "No pudimos encontrar un usuario con el correo ingresado.",

];
