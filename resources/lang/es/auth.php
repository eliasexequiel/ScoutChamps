<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'El usuario o la contraseña con incorrectos.',
    'throttle' => 'Demasiados intentos. Intente nuevamente en :seconds segundos.',

    // Activation items
    'sentEmail'        => 'Hemos enviado un correo a :email.',
    'clickInEmail'     => 'Por favor haga click aquí para activar su cuenta.',
    'anEmailWasSent'   => 'Un correo fue enviado a :email el :date.',
    'clickHereResend'  => 'Click aquí para reenviar el correo.',
    'successActivated' => 'Su cuenta fue activada correctamente.',
    'unsuccessful'     => 'Su cuenta no pudo ser activada; por favor intente nuevamente.',
    'blockedAccount'   => 'Su cuenta fue suspendida. Por favor contacte al administrador.',  
    'notCreated'       => 'Your account could not be created; please try again.',
    'tooManyEmails'    => 'Too many activation emails have been sent to :email. <br />Please try again in <span class="label label-danger">:hours hours</span>.',
    'regThanks'        => 'Gracias por registrarse, ',
    'invalidToken'     => 'Token de activación inválido. ',
    'activationSent'   => 'Correo de activación enviado. ',
    'alreadyActivated' => 'Cuenta ya activa. ',

    // Labels
    'whoops'          => 'Whoops! ',
    'someProblems'    => 'Hubo problemas en algunos campos.',
    'email'           => 'Correo',
    'password'        => 'Contraseña',
    'firstname'       => 'Nombre',
    'lastname'        => 'Apellido',  
    'phone'           => '(Código de país) Teléfono',
    'dob'             => 'Fecha de nacimiento', 
    'entity'          => 'Academia / Equipo Profesional / Sponsor / Universidad / Empresa que representa',
    'typeEntity'      => 'Tipo', 
    'otherClub'       => 'Otra Entidad',
    'otherEntity'     => 'Otro',
    'selectEntity'    => 'Seleccionar Entidad',
    'emailTutor'      => 'Correo Padres/Tutor Legal',
    'firstnameTutor'  => 'Nombre Padres/Tutor Legal',
    'lastnameTutor'   => 'Apellido Padres/Tutor Legal',
    'phoneTutor'      => 'Teléfono Padres/Tutor Legal',
    'rememberMe'      => 'Recordarme',
    'login'           => 'Ingreso',
    'forgot'          => 'Olvidaste tu contraseña?',
    'forgot_message'  => 'Password Troubles?',
    'name'            => 'Username',
    'first_name'      => 'Nombre',
    'last_name'       => 'Apellido',
    'confirmPassword' => 'Confirmar Contraseña',
    'register'        => 'Registro',
    'registerBtn'     => 'Registrarse',
    'loginBtn'        => 'Ingresar',

    // Placeholders
    'ph_name'          => 'Correo',
    'ph_email'         => 'Correo',
    'ph_firstname'     => 'Nombre',
    'ph_lastname'      => 'Apellido',
    'ph_password'      => 'Contraseña',
    'ph_password_conf' => 'Confirmar Contraseña',

    // User flash messages
    'sendResetLink' => 'Enviar link',
    'resetPassword' => 'Resetear Contraseña',
    'loggedIn'      => 'Ya estás logueado!',

    // email links
    'pleaseActivate'    => 'Por favor activa tu cuenta.',
    'clickHereReset'    => 'Haga click aquí para restablecer su contraseña: ',
    'clickHereActivate' => 'Haga click aquí para activar su cuenta: ',

    'adviceVerifyEmailTitle' => 'Verificar tu dirección de correo',
    'adviceVerifyEmail' => 'Antes de continuar, por favor verifique su casilla de correo para activar su cuenta.',
    'adviceVerifyEmailText2' => 'Si no recibiste un correo con el link de verificación',
    'adviceVerifyEmailLink'  => 'haga click aquí para solicitar uno nuevo',
    'adviceVerifyEmailLinkSent' => 'Un nuevo link de verificación ha sido enviado a su correo.',

    // Tooltips
    'tooltipEmail'      => 'Tu correo no será compartido con los demas usuarios de ScoutChamps, ni compartido con terceros.',
    'tooltipPhone'      => 'Tu teléfono no será compartido con los demas usuarios de ScoutChamps, ni compartido con terceros.',

    // Validators
    'userNameTaken'          => 'Nombre de Usuario ya fue usado',
    'userNameRequired'       => 'Usuario es requerido',
    'fNameRequired'          => 'Nombre es requerido',
    'lNameRequired'          => 'Apellido es requerido',
    'emailRequired'          => 'Correo es requerido',
    'emailInvalid'           => 'Correo es inválido',
    'passwordRequired'       => 'Contraseña es requerido',
    'PasswordMin'            => 'Contraseña debe tener al menos 6 caracteres',
    'PasswordMax'            => 'Contraseña debe tener como máximo 20 caracteres',
    'captchaRequire'         => 'Captcha es requerido',
    'roleRequired'           => 'Rol de usuario es requerido.',
    'dobDateFormat'          => 'Fecha de nacimiento debe tener el formato dd/mm/yyyy',
    'acceptTerms'      => 'Acepto los <a class="hover:underline" target="_blank" href="/conditions"> Términos y Condiciones</a> y 
                           las <a class="hover:underline" target="_blank" href="/privacy"> Politicas de Privacidad </a>',
    'tycRequired'      => 'Debe aceptar los Términos y Condiciones y las Politicas de Privacidad'

];
