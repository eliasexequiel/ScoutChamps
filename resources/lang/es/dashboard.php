<?php 

return [
    'favorites' => [
        'noResults'          => 'No hay noticias',
        'newPhotos'          => 'subió una nueva foto.',
        'newVideos'          => 'subió un nuevo video.',
        'updatePhotoProfile' => 'actualizó su foto de perfil.',
        'updateSports'       => 'actualizó sus deportes.',
        'updateClubs'        => 'actualizó sus clubes.',
        'newPost'            => ' realizó una nueva',
        'newPostShowing'     => ' realizó una nueva publicación',
        'newPostShowingOwn'  => ' realizaste una nueva publicación'
    ],
    'followed' => [
        'title'         => 'Seguidos',
        'noResults'     => 'No hay noticias',
        'loadMore'      => 'Cargar más'
    ],
    'followed-favorite' => [
        'title'         => 'Seguidos - Favoritos'
    ],
    'updates' => [
        'title'             => 'Actualziaciones',
        'followedUpdate'    => 'siguió a',
        'userRegistered'    => 'se unió a ',
        'noResults'         => 'No hay nuevas actualizaciones'
    ],

    'posts' => [
        'newPost'           => 'Nueva publicación',
        'myPosts'           => 'Mis publicaciones',
        'publish'           => 'Publicar',
        'post'              => 'Publicación',
        'publishSuccess'    => 'Publicación realizada',
        'noComments'        => 'No hay comentarios',
        'noPosts'           => 'No hay publicaciones',
        'comment'           => 'Comentario',
        'comments'          => '{0} Comentarios|{1} Comentario|{2,*} Comentarios',
        'like'              => 'Me gusta',
        'likes'             => '{0} Me gusta|{1,*} Me gusta',
        'delete'            => 'Eliminar'
    ],

    'articles' => [
        'title'         => 'Últimas Noticias',
        'readmore'      => 'Leer más',
        'noResults'     => 'No hay nuevos articulos',
        'moreResults'   => 'Más Articulos'
    ],

    'you'   => 'Tú',
    'publications' => [
        'title'         => 'Publicaciones',
        'adminTitle'    => 'Actividad Usuarios'
    ],

    'recruitments'  => [
        'actives'        => '{0} reclutamientos activos|{1} reclutamiento activo|{2,*} reclutamientos activos',
        'noRecruitments' => 'Sin reclutamientos'
    ],

    'popularAthletes' => 'Atletas Populares',
    'popularScouts'   => 'Scouts Populares',
    'popularUsers'    => 'Usuarios Populares',
    'popularScoutsSuccess'   => 'Scouts actualizados exitosamente!',
    'popularAthletesSuccess' => 'Atletas actualizados exitosamente!'
];