<?php

return [
    'title' => 'Preguntas frecuentes',
    'newQuestion' => 'Nueva pregunta',
    'editQuestion' => 'Editar pregunta',
    'form' => [
        'edit'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar </span>',
        'delete'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'question' => 'Pregunta',
        'answer'   => 'Respuesta',
        'question_es' => 'Pregunta (es)',
        'answer_es'   => 'Respuesta (es)',
        'save'     => 'Guardar',
        'update'   => 'Actualizar',
        'cancel'   => 'Cancelar' 
    ]
];