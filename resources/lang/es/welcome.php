<?php

return [

    'title-main'            => 'La Plataforma para Atletas de Alto Rendimiento',
    'title-1'               => 'Welcome to ScoutChamps',
    'text-1'                => 'A través de esta plataforma, los Scouts (Sponsors, Directores/Managers Deportivos y Entrenadores de equipos) pueden observar a innumerables deportistas de alto rendimiento para luego reclutarlos, y así incrementar la competitividad de sus equipos sin tener la necesidad de viajar a diferentes ciudades y países para conocerlos. ¡Prepárate para convertirte en la próxima estrella!',
    'title-2'               => 'Abaut ScoutChamps',
    'text-2'                => 'Nuestro trabajo incluye ayudar a universidades y equipos profesionales a contactarse con deportistas de alto rendimiento, que esperan una oportunidad para triunfar. Tenemos Scouts en cada deporte buscando a los mejores deportistas en nuestra plataforma, y así informar a sus equipos sobre ellos, incrementando las posibilidades de reclutamiento de los deportistas.',
    'dreams'                => 'Hacé tus sueños posible',
    'ourServices'           => 'Nuestros Servicios',
    'howWorks'              => [
        'title' => 'Como Funciona',
        'steps' => [
            '1' => 'Crea tu perfil: Scouts (Gratuito), Atletas (Gratuito o Premium). Completa tu perfil y muestra tus habilidades, sube fotos y videos. Podrás ver qué Scouts vieron tu perfil y quienes te han guardado como su atleta favorito.',
            '2' => 'Invita a Scouts (Sponsors, Directores/Managers Deportivos y Entrenadores de equipos) a ver tu perfil y buscarán a los mejores atletas para reforzar sus equipos (Profesionales o Universitarios).',
            '3' => 'Si eres el atleta ideal para reforzar un equipo, el Scout solicitará contactarse contigo y automáticamente le llegará tu información de contacto (correo electrónico y teléfono). El ponerse en contacto contigo no significa aún que te han contratado.',
            '4' => 'ScoutChamps no interfiere en la negociación y les desea éxitos a ambas partes.'
        ]
    ],
    'home'                  => 'Inicio',
    'aboutUs'               => [
        'title'       => 'Acerca de Nosotros',
        'text'        => 'Somos una empresa con alcance global enfocada en crear una red de contactos para atletas y así ayudar a jovenes talentosos del deporte a conseguir sus objetivos: hacerse conocer ante Scouts (Sponsors, Directores/Managers Deportivos y Entrenadores de equipos) para obtener becas deportivas en universidades, jugar profesionalmente o conseguir sponsor de alguna marca.',
        'misionTitle' => 'Misión',
        'mision'      => 'Ser una vitrina para que miles de deportistas de alto rendimiento puedan darse a conocer globalmente y ser reclutados por Scouts a nivel Profesional, Universitario o conseguir Sponsors de alguna marca.',
        'visionTitle' => 'Visión',
        'vision'      => 'Ser la empresa líder a nivel mundial que colabore a equipos amateurs/profesionales y universidades en el reclutamiento de deportistas elite.'
    ],
    'sports'                => 'Deportes',
    'clubs'                 => 'Equipos / Universidades',
    'faqs'                  => 'Faqs',
    'ultimasNoticias'       => 'Últimas noticias',
    'titleMap'              => 'Atletas y Scouts alrededor del mundo',
    'choosePlan'            => 'Seleccionar',
    'atlethesRegistered'    => 'Atletas registrados',
    'scoutsRegistered'      => 'Scouts registrados',
    'newConversations'      => 'Nuevas conversaciones',
    'contactBetweenScoutAndAthletes' => 'Contactos Scouts <i class="fa fa-arrow-right" aria-hidden="true"></i> Atletas',
    'countriesAthletesScouts'   => 'Países con Atletas/Scouts',
    'visits'                => 'Visitas',
    'popularAthletes'       => 'Atletas Reconocidos',
    'popularScouts'         => 'Scouts Reconocidos',
    'contact'               => [
        'title' => 'Contacto',
        'email' => 'Correo',
        'whatsapp' => 'WhatsApp'
    ],
    'searchText' => 'Buscar',
    'savePlan'   => 'Ahorrás'

];
