<?php 

return [
    'title'           => 'Configuración',
    'save'            => 'Guardar',   
    'globalConfig'    => 'Configuración Global',
    'showClubsInHome' => 'Mostrar Sección Equipos/Universidades en Home',
    'limitImagesAtleteFree' => 'Limite en Plan Atleta Free para subir imágenes',
    'limitImagesScoutFree'  => 'Limite de imágenes para Scouts',
    'verifyScoutsClubs'     => 'Verificar Scouts en Entidades',
    'verifyUsers'     => 'Verificar usuarios a través de correo electrónico',
    'infoVerifyUsers' => 'Verificar si Scouts están en las entidades con los que se registraron.',
    'verifyClubs'     => 'Verificar Entidades',
    'infoVerifyClubs' => 'Verificar entidades registrados por scouts y atletas.',
    'contactInfo'     => 'Información de Contacto',
    'contactPhone'    => 'Teléfono de Contacto',
    'contactEmail'    => 'Correo de Contacto',

    // Flash Messages
    'updateSuccess'        => 'Configuración actualizada correctamente! ',
    'updateTermsSuccess'   => 'Términos y Condiciones actualizados correctamente',
    'updatePrivacySuccess' => 'Politicas de Privacidad actualizados correctamente',

    'publicityAbove'  => 'Publicidad Superior',
    'publicityBelow'  => 'Publicidades Inferiores',
    'terms'           => 'Términos y Condiciones',
    'privacy'         => 'Politicas de Privacidad',
    'back-to-config'  => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Configuración</span>',
    'back-config'     => 'Volver a Configuración',
    'dropzoneLabel'   => 'Arrastre un archivo o haga click aquí'
];