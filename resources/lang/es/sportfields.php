<?php

return [
    'titles' => [
        'Track and field'       => 'Atletismo',
        'Baseball'              => 'Béisbol',
        'Basketball'            => 'Basketball',
        'Boxing'                => 'Boxeo',
        'Cycling'               => 'Ciclismo',
        'Football'              => 'Fútbol Americano',
        'Golf'                  => 'Golf',
        'Tennis'                => 'Tenis',
        'Rugby'                 => 'Rugby',
        'Soccer'                => 'Fútbol',
        'Swimming and diving'   => 'Natación y clavado',
        'Voleyball'             => 'Voleibol',
        'Triathlon'             => 'Triatlón', 
        'Equestrian Jumping'    => 'Salto Ecuestre',
        'All Sports'            => 'Todos los deportes'
    ],
    'position' => 'Posición Primaria',
    'positionSecondary' => 'Posición Secundaria',
    'Football' => [
        'fields' => [
            'Center(C)' => 'Centro',
            'Offensive Guard(OG)' => 'Guardia Ofensivo',
            'Offensive Tackle(OT)' => 'Tacle Ofensivo',
            'Tight End(TE)' => 'Ala Cerrada',
            'Wide Receiver(WR)' => 'Ala Abierta',
            'Fullback(FB)' => 'Corredor de Poder',
            'Running Back(RB)' => 'Corredor',
            'Tailback(TB)' => 'Corredor Profundo',
            'Halfback(HB)' => 'Corredor Medio',
            'Wingback(WB)' => 'Wingback',
            'Slotback(SB)' => 'Slotback',
            'Quarterback(QB)' => 'Mariscal de Campo',
            'Defensive End(DE)' => 'Ala Defensivo',
            'Defensive Tackle(DT)' => 'Tacle Defensivo',
            'Nose Guard(NG)' => 'Tacle o Guardia Nariz',
            'Linebacker(LB)' => 'Apoyador',
            'Cornerback(CB)' => 'Esquinero',
            'Safety(FS or SS)' => 'Profundo',
            'Defensive Back(DB)' => 'Back Defensivo',
            'Nickelback(NB)' => 'Defensa Nickel'
        ]
    ],
    'Soccer' => [
        
        'fields' => [
            'Goalkeeper(GK)' => 'Arquero',
            'Center Back(CB)' => 'Defensor Central',
            'Right Back(RB)' => 'Lateral Derecho',
            'Left Back(LB)' => 'Lateral Izquierdo',
            'Center Defensive Mid(CBM)' => 'Mediocampista Central Defensivo',
            'Center Mid(CM)' => 'Mediocampista Central',
            'Right Mid(RM)' => 'Mediocampista por derecha',
            'Left Mid(LD)' => 'Mediocampista por izquierda',
            'Center Attacking Mid(CAM)' => 'Delantero Centro Medio',
            'Right Wing(RW)' => 'Mediapunta derecho',
            'Left Wing(LW)' => 'Mediapunta izquierdo',
            'Striker(ST)' => 'Centro Delantero'
        ]
    ]


];