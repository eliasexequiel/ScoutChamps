<?php 

return [
    'title'         => 'Publicaciones',
    'new-post'      => 'Nueva Publicación',
    'editing-post'  => 'Editar Publicación',
    'showing-post'  => 'Ver Publicación',
    
    // Flash Messages
    'createSuccess'   => 'Publicación creada correctamente! ',
    'updateSuccess'   => 'Publicación actualizada correctamente! ',
    'deleteSuccess'   => 'Publicación eliminada correctamente! ',

    'postTotal'     => 'Total Publicaciones',
    
    'deleteQuestion'  => 'Seguro que quieres eliminar la publicación?',
    
    'forms' => [
        'id'        => 'ID',
        'text'      => 'Texto',
        'create'    => 'Crear Publicación',
        'update'    => 'Actualizar Publicación',
        'save'      => 'Guardar'
    ],

    'buttons' => [
        'create-post'    => 'Nueva Publicación',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'back-to-posts' => '<span class="hidden-sm hidden-xs">Volver </span><span class="hidden-xs">a las publicaciones</span>',
        'back-to-post'  => 'Volver  <span class="hidden-xs"> a la Publicación</span>',
        'delete-post'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Publicación</span>',
        'edit-post'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Publicación</span>',
    ],
];