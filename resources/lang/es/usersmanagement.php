<?php

return [

    // Titles
    'showing-all-users'     => 'Usuarios',
    'users-menu-alt'        => 'Usuarios',
    'create-new-user'       => 'Crear Nuevo Usuario',
    'show-blocked-users'    => 'Mostrar Usuarios Bloqueados',
    'editing-user'          => 'Editar Usuario :name',
    'editing-user-clubs'    => 'Editar Clubes - :name',
    'showing-user'          => 'Ver Usuario :name',
    'showing-user-title'    => 'Ver :name',

    // Flash Messages
    'createSuccess'    => 'Usuario creado exitosamente! ',
    'updateSuccess'    => 'Usuario actualizado exitosamente! ',
    'deleteSuccess'    => 'Usuario eliminado exitosamente! ',
    'deleteSelfError'  => 'No puede eliminarte a ti mismo! ',
    'verifiedSuccess'  => 'Usuario verificado exitosamente!' ,
    'blockedSuccess'   => 'Usuario bloqueado exitosamente!',
    'unblockedSuccess' => 'Usuario desbloqueado exitosamente!' ,

    // Show User Tab
    'viewProfile'            => 'Ver perfil',
    'editUser'               => 'Editar Usuario',
    'showPublicProfile'      => 'Mostrar perfil',
    'deleteUser'             => 'Eliminar Usuario',
    'usersBackBtn'           => 'Volver a Usuarios',
    'usersPanelTitle'        => 'Información de Usuario',
    'labelUserName'          => 'Usuario:',
    'labelEmail'             => 'Correo:',
    'labelPhone'             => 'Teléfono:',
    'labelFirstName'         => 'Nombre:',
    'labelLastName'          => 'Apellido:',
    'labelRole'              => 'Rol:',
    'labelStatus'            => 'Estado:',
    'labelAccessLevel'       => 'Accesso',
    'labelPermissions'       => 'Permisos:',
    'labelCreatedAt'         => 'Creado:',
    'labelUpdatedAt'         => 'Actualizado:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpEmail'           => 'Email Signup IP:',
    'labelIpConfirm'         => 'Confirmation IP:',
    'labelIpSocial'          => 'Socialite Signup IP:',
    'labelIpAdmin'           => 'Admin Signup IP:',
    'labelIpUpdate'          => 'Last Update IP:',
    'labelDeletedAt'         => 'Deleted on',
    'labelIpDeleted'         => 'Deleted IP:',
    'usersDeletedPanelTitle' => 'Información Usuario Bloqueado',
    'usersBackDelBtn'        => 'Volver a Usuarios bloqueados',

    // 'successRestore'    => 'User successfully restored.',
    // 'successDestroy'    => 'User record successfully destroyed.',
    'errorUserNotFound' => 'Usuario no encontrado.',

    'labelUserLevel'  => 'Nivel',
    'labelUserLevels' => 'Niveles',

    'no-records' => 'No hay usuarios',
    'showing-user-blocked' => 'Usuarios bloqueados',
    'userTotal'   => 'Total Usuarios',
    'users-table' => [
        'id'        => 'ID',
        'name'      => 'Nombre',
        'fname'     => 'Nombre',
        'lname'     => 'Apellido',
        'email'     => 'Correo',
        'role'      => 'Rol',
        'created'   => 'Creado',
        'lastLogin' => 'Última Conexión',
        'updated'   => 'Actualizado',
        'actions'   => 'Acciones',
        'deleted'   => 'Eliminado',
    ],

    'buttons' => [
        'create-new'    => 'Nuevo Usuario',
        'delete'        => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar</span>',
        'show'          => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Mostrar</span>',
        'edit'          => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Editar</span>',
        'verify'        => '<i class="fa fa-check fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Verificar</span>',
        'unverify'      => '<i class="fa fa-times fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Eliminar verificación</span>',
        'block'         => '<i class="fa fa-lock fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Bloquear</span>',
        'unblock'       => '<i class="fa fa-unlock fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Desbloquear</span>',
        'back-to-users' => '<span class="hidden-sm hidden-xs">Volver a </span><span class="hidden-xs">Usuarios</span>',
        'back-to-user'  => 'Volver <span class="hidden-xs">a Usuario</span>',
        'restoreUser'   => '<i class="fa fa-refresh" aria-hidden="true"></i> <span class="hidden-xs hidden-sm"> Restablecer User </span>',
        'destroyUser'   => '<i class="fa fa-times" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Eliminar Registro </span>',
        'delete-user'   => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Eliminar</span><span class="hidden-xs"> Usuario</span>',
        'edit-user'     => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Editar</span><span class="hidden-xs"> Usuario</span>',
    ],

    'tooltips' => [
        'delete'        => 'Eliminar',
        'show'          => 'Mostrar',
        'edit'          => 'Editar',
        'create-new'    => 'Crear Nuevo Usuario',
        'back-users'    => 'Volver a usuarios',
        'email-user'    => 'Correo :user',
        'submit-search' => 'Filtrar',
        'clear-search'  => 'Limpiar',
    ],

    'messages' => [
        'userNameTaken'          => 'Nombre de Usuario ya fue usado',
        'userNameRequired'       => 'Usuario es requerido',
        'fNameRequired'          => 'Nombre es requerido',
        'lNameRequired'          => 'Apellido es requerido',
        'emailRequired'          => 'Correo es requerido',
        'emailInvalid'           => 'Correo es inválido',
        'passwordRequired'       => 'Contraseña es requerido',
        'PasswordMin'            => 'Contraseña debe tener al menos 6 caracteres',
        'PasswordMax'            => 'Contraseña debe tener como máximo 20 caracteres',
        'captchaRequire'         => 'Captcha es requerido',
        'roleRequired'           => 'Rol de usuario es requerido.'
    ],

    'show-user' => [
        'id'                => 'User ID',
        'name'              => 'Nombre',
        'email'             => 'Correo',
        'role'              => 'Rol Usuario',
        'created'           => 'Creado',
        'updated'           => 'Actualizado',
        'labelRole'         => 'Rol Usuario',
        'labelAccessLevel'  => '<span class="hidden-xs">Usuario</span> Nivel de Accesso',
    ],

    'search'  => [
        'title'             => 'Mostrando resultados',
        // 'found-footer'      => ' Record(s) found',
        'no-results'        => 'No hay resultados',
        'search-users-ph'   => 'Buscar usuarios',
        'allTypeUsers'      => 'Todos los usuarios',
        'createdDate'       => 'Fecha de creación',
        'lastLoginDate'     => 'Fecha de última sesión',
        'typeUser'          => 'Tipo de usuario'
    ],

    'modals' => [
        'delete_user_message' => 'Seguro que quieres eliminar a :user?',
        'btnCancel' => '<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancelar',
        'btnDelete' => '<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Eliminar'
    ],
];
