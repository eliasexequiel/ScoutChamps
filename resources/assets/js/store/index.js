import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null,
        posts: [],
        updatesFollowedUsers: [],
        nextPageFollowedUsers: true,
        updatesFavoritesUsers: []
    },
    mutations: {
        SET_USER(state,payload) {
            state.user = payload;
        },
        SET_POSTS(state,payload) {
            state.posts = payload;
        },
        SET_UPDATES_FOLLOWED(state,payload) {
            state.updatesFollowedUsers = _.uniqBy([...state.updatesFollowedUsers,...payload],'id');
            state.nextPageFollowedUsers = payload.length > 0 ? true : false;
        },
        SET_UPDATES_FAVORITES(state,payload) {
            state.updatesFavoritesUsers = payload;
        }
    },
    actions: {
        set_user({ commit }) {
            return new Promise((resolve, reject) => {
                axios.get('/api/user').then(res => {
                    commit('SET_USER',res.data.user);
                    resolve(res);
                });
            });
        },
        set_my_posts({commit}) {
            return new Promise((resolve, reject) => {
                axios.get('/api/myposts').then(res => {
                    commit('SET_POSTS',res.data.posts);
                    resolve(res);
                });
            });
        },
        updates_from_followed_users({commit},page = 1) {
            return new Promise((resolve, reject) => {
                axios.get(`/api/updatesFollowedFavoritesUsers?page=${page}`).then(res => {
                    commit('SET_UPDATES_FOLLOWED',res.data.updates);
                    resolve(res);
                })
            });
        },
        updates_from_favorites_users({commit}) {
            return new Promise((resolve, reject) => {
                axios.get("/api/favoritesusers").then(res => {
                    commit('SET_UPDATES_FAVORITES',res.data.updates);
                    resolve(res);
                })
            });
        }
    }
});