export function findAndColorWord(html, word, color) {
  var indexWordStart = html.indexOf(word);
  var wordLength = word.length;
  var coloredWord = '<span style="color: ' + color + '">' + html + '</span>';

  var firstHtmlPart = html.substring(0, indexWordStart - 1);
  var secondHtmlPart = html.substring(indexWordStart + wordLength, html.length);

  return coloredWord;
};

export function diffHours(dt2, dt1) {

  var diff = (dt2.getTime() - dt1.getTime()) / 1000;
  diff /= (60 * 60);
  return Math.abs(Math.round(diff));

}

export const isToday = (someDate) => {
  const today = new Date()
  return someDate.getDate() == today.getDate() &&
    someDate.getMonth() == today.getMonth() &&
    someDate.getFullYear() == today.getFullYear()
}

export const cutText = (text, cant) => {
  if (text == null) return '';
  return text.length > cant ? `${text.substring(0, cant)}...` : text;
}

export const cutHtmlString = (text, cant) => {
  var openTag = 0, closeTag = 0, i = 0;
  for (i; i < cant; i++) {
    if (text[i] == "<")
      openTag++;
    if (text[i] == ">")
      closeTag++;
  }
  if (openTag > closeTag) {
    while (text[i] != ">")
      i++;
  }

  return text.length > cant ? `${text.substring(0, (i + 1))}...` : text;
}