const linkToShowUserMixin = {
    methods: {
        getLinkToUser(fullname,uuid) {
            return fullname.replace(/\s/g,'')+'_'+uuid;
        }
    }
}

export default linkToShowUserMixin;