let textActivityUser = {
    methods: {
        getTexto(update) {
            let texto = "";
            switch (update.type) {
                case "followed":
                    texto = this.$t("dashboard.updates.followedUpdate");
                    break;
                case "registered":
                    texto = this.$t("dashboard.updates.userRegistered");
                    break;
                case "updateSports":
                    texto = this.$t("dashboard.favorites.updateSports");
                    break;
                case "updateClubs":
                    texto = this.$t("dashboard.favorites.updateClubs");
                    break;
                case "updatePhotoProfile":
                    texto = this.$t("dashboard.favorites.updatePhotoProfile");
                    break;
                case "newPhotos":
                    texto = this.$t("dashboard.favorites.newPhotos");
                    break;
                case "newVideos":
                    texto = this.$t("dashboard.favorites.newVideos");
                    break;
                case "newPost":
                    // let body = JSON.parse(update.body);
                    texto = this.$t("dashboard.favorites.newPost");
                    // console.log(body.post_id);
                    break;
                default:
                    break;
            }
            return texto;
        }
    }
};

export default textActivityUser;