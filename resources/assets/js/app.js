
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('hideshowpassword');
// var Dropzone = require('dropzone');
var password = require('password-strength-meter');

window.Vue = require('vue');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// ADMIN
Vue.component('admin-publicidades', require('./components/admin/Publicidades.vue').default);
Vue.component('admin-updates', require('./components/admin/UpdatesAdmin.vue').default);
Vue.component('admin-popular-users', require('./components/admin/PopularUsers.vue').default);
Vue.component('admin-faqs', require('./components/admin/Faqs.vue').default);
Vue.component('admin-index-athlete-clubs', require('./components/admin/users/IndexAthleteClubs.vue').default);
Vue.component('admin-index-scout-clubs', require('./components/admin/users/scout/IndexScoutClubs.vue').default);


// USERS
Vue.component('user-images', require('./components/user/Images.vue').default);
Vue.component('user-videos', require('./components/user/Videos.vue').default);

// MESSAGES
Vue.component('user-messages', require('./components/messages/Index.vue').default);
Vue.component('contact-admin', require('./components/messages/ContactAdmin.vue').default);

// ATHLETE
Vue.component('athlete-sports', require('./components/user/Sports.vue').default);
Vue.component('athlete-clubs', require('./components/user/AthleteClubs.vue').default);
Vue.component('index-athlete-clubs', require('./components/user/IndexAthleteClubs.vue').default);
Vue.component('index-athlete-showcase', require('./components/user/AthleteShowcase.vue').default);

// SCOUT
Vue.component('scout-invitation-recruitment', require('./components/scout/InvitationRecruitment.vue').default);
Vue.component('scout-sport', require('./components/scout/Sport.vue').default);
Vue.component('scout-clubs', require('./components/scout/ScoutClubs.vue').default);
Vue.component('index-scout-clubs', require('./components/scout/IndexScoutClubs.vue').default);
Vue.component('scout-videos', require('./components/scout/ScoutVideos.vue').default);

// PUBLIC
Vue.component('p-user-images', require('./components/public/user/Images.vue').default);
Vue.component('p-user-videos', require('./components/public/user/Videos.vue').default);

Vue.component('p-updates', require('./components/public/Updates.vue').default);
Vue.component('p-articles', require('./components/public/articles/Articles.vue').default);
Vue.component('p-recruitments', require('./components/public/Recruitments.vue').default);
Vue.component('p-recruitments-scout', require('./components/public/RecruitmentsScout.vue').default);
Vue.component('p-view-recruitment', require('./components/public/ViewRecruitment.vue').default);

// MY POSTS - ATHLETES
Vue.component('p-my-posts', require('./components/public/MyPosts.vue').default);

// UPDATES ABOUT FAVORITE USERS
Vue.component('p-favorites-users', require('./components/public/FavoritesUsers.vue').default);

// UPDATES ABOUT FOLLOWED USERS
Vue.component('p-followed-users', require('./components/public/FollowedUsers.vue').default);

// NEW POST FROM DASHBOARD 
Vue.component('p-new-post', require('./components/public/posts/NewPost.vue').default);

// MODAL POST
Vue.component('p-modal-post', require('./components/public/posts/ModalPost.vue').default);

// EDIT POST && NEW POST 
Vue.component('edit-post', require('./components/public/posts/EditPost.vue').default);
// SHOW POST
Vue.component('show-post', require('./components/public/posts/ShowPost.vue').default);

// UTILS
Vue.component('user-photo', require('./utils/UserPhoto.vue').default);
Vue.component('user-card-information', require('./utils/UserCardInformation.vue').default);
Vue.component('modal', require('./utils/Modal.vue').default);
Vue.component('modal-media', require('./utils/ModalMedia.vue').default);
Vue.component('spinner', require('./utils/Spinner.vue').default);
Vue.component('date-picker', require('./utils/Datepicker.vue').default);
Vue.component('text-editor', require('./utils/RichEditor.vue').default);

Vue.component('mediamini-p', require('./components/public/utils/MediaMini.vue').default);


import store from './store';


import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';
Vue.use(VueInternationalization);
const lang = document.documentElement.lang.substr(0, 2);

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});


// Lazy Load Images
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
    loading: 'img/default-img.png',
    lazyComponent: true
});

// Vue Editor


// Vue Tabs
import { Tabs, Tab } from 'vue-slim-tabs'
Vue.component('app-tab', Tab);
Vue.component('app-tabs', Tabs);


import { mapState } from 'vuex';
const app = new Vue({
    el: '#app',
    store,
    i18n,
    computed: {
        ...mapState({ userV: state => state.user }),
    }
});

$.fn.extend({
    toggleText: function (a, b) {
        return this.text(this.text() == b ? a : b);
    },

    /**
     * Remove element classes with wildcard matching. Optionally add classes:
     *   $( '#foo' ).alterClass( 'foo-* bar-*', 'foobar' )
     *
     */
    alterClass: function (removals, additions) {
        var self = this;

        if (removals.indexOf('*') === -1) {
            // Use native jQuery methods if there is no wildcard matching
            self.removeClass(removals);
            return !additions ? self : self.addClass(additions);
        }

        var patt = new RegExp('\\s' +
            removals.
                replace(/\*/g, '[A-Za-z0-9-_]+').
                split(' ').
                join('\\s|\\s') +
            '\\s', 'g');

        self.each(function (i, it) {
            var cn = ' ' + it.className + ' ';
            while (patt.test(cn)) {
                cn = cn.replace(patt, ' ');
            }
            it.className = $.trim(cn);
        });

        return !additions ? self : self.addClass(additions);
    }
});
