<?php

// Registered, activated, and is admin routes.
Route::group(['middleware' => ['auth', 'activated','blocked', 'role:admin', 'activity', 'twostep']], function () {

    // Route::resource('/users/deleted', 'SoftDeletesController', ['only' => ['index', 'show', 'update', 'destroy']]);

    Route::resource('users', 'UsersManagementController', [
        'names' => ['index'   => 'users', 'destroy' => 'user.destroy']
    ]);

    Route::get('users/{id}/edit/clubs','UsersManagementController@editClubs')->name('user.edit.clubs');
    Route::get('users/{id}/data','UsersManagementController@getUser');
    
    Route::get('users/{id}/verify', 'UsersManagementController@verify')->name('users.verify');
    Route::get('users/{id}/unverify', 'UsersManagementController@unverify')->name('users.unverify');

    // BLOCK USERS
    Route::get('blockedusers', 'UsersManagementController@indexBlockedUsers')->name('users.blocked.index');
    Route::get('blockedusers/{id}/unblock', 'UsersManagementController@unblock')->name('users.unblock');
    Route::get('users/{id}/block', 'UsersManagementController@block')->name('users.block');

    Route::post('search-users', 'UsersManagementController@search')->name('search-users');

    // Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    // Route::get('routes', 'AdminDetailsController@listRoutes');

    Route::namespace('Admin')->group(function () {
        Route::resource('config', 'ConfigController',['only' => ['index','store']]);
        Route::get('config/tyc','ConfigController@showTerms')->name('terms.edit');
        Route::post('config/tyc','ConfigController@storeTerms')->name('terms.store');
        Route::get('config/privacy','ConfigController@showPrivacy')->name('privacy.edit');
        Route::post('config/privacy','ConfigController@storePrivacy')->name('privacy.store');

        Route::resource('clubes', 'ClubesController');
        Route::post('clubes/{id}/verify', 'ClubesController@verify')->name('clubes.verify');
        Route::resource('sports', 'SportsController',['only' => ['index','edit','update','show','destroy']]);
        Route::resource('articles', 'ArticleController');

        Route::resource('publicidad', 'PublicidadController',['only' => ['index']]);
        Route::get('/api/publicidad','PublicidadController@getPublicidades');
        Route::post('/api/publicidad','PublicidadController@savePublicidad');
        Route::delete('/api/publicidad/{id}','PublicidadController@deletePublicidad');
        Route::delete('/api/publicidad/{id}/image','PublicidadController@deleteImageFromPublicidad');

        Route::resource('coupons', 'Stripe\CouponController');
        Route::resource('subscriptions', 'Stripe\SubscriptionController');
        Route::get('subscriptions/{idUser}/invoices', 'Stripe\SubscriptionController@invoices')->name('subscriptions.invoices');

        Route::resource('popularUsers', 'PopularUsersController',['only' => ['index','store']]);

        Route::resource('faqs', 'FaqsController',['only' => ['index','update','store','destroy']]);
        Route::get('admin/faqsByType','FaqsController@getFaqsByType');

        // SHOWCASE
        Route::get('/showcase','ShowCaseController@index')->name('admin.showcase.index');
        Route::get('/showcase/user/{id}','ShowCaseController@show')->name('admin.showcase.show');
        Route::post('/showcase/user','ShowCaseController@store')->name('admin.showcase.store');

        // REPORTS
        Route::get('admin/report','ReportController@index')->name('admin.reports.index');
        Route::get('admin/report/{id}','ReportController@show')->name('admin.reports.show');
        Route::get('admin/report/{id}/close','ReportController@close')->name('admin.reports.close');
        Route::get('admin/report/{id}/delete','ReportController@deleteReported')->name('admin.reports.deleteReported');

        Route::prefix('admin')->name('admin.')->group(function () {
            Route::resource('planes', 'Stripe\PlanesController');
        });
    });
});