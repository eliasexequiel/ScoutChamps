<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

// Broadcast::channel('App.User.{id}', function ($user, $id) {
//     return (int) $user->id === (int) $id;
// });

Broadcast::channel('chart', function ($user) {
    return [
        'name' => $user->name,
    ];
});

Broadcast::channel('user.notifications.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('thread.{threadId}', function ($user, $threadId) {
    $thread = App\Models\Thread::find($threadId);
    return in_array($user->id,$thread->participantsUserIds());
    // return $user->id === Order::findOrNew($orderId)->user_id;
    // return true;
});

// Broadcast::channel('user.{id}.newthread', function ($user, $id) {
//     // $thread = \Cmgmyr\Messenger\Models\Thread::find($threadId);
//     // return in_array($user->id,$thread->participantsUserIds());
//     // return $user->id === Order::findOrNew($orderId)->user_id;
//     return true;
// });

Broadcast::channel('users.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
