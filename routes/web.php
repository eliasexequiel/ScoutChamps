<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
*/

// Authentication Routes
Auth::routes(['verify' => true]);

Route::post('/registerScout', 'Auth\RegisterController@registerScout')->name('register.scout');
Route::post('/registerAthlete', 'Auth\RegisterController@registerAthlete')->name('register.athlete');

// WEB HOOK STRIPE
Route::post('stripe/webhook','Admin\Stripe\WebhookController@handleWebhook');

Route::get('locale/{locale}', 'WelcomeController@setLocale');

Route::group(['middleware' => ['web','auth']], function () {

    Route::get('api/updates', 'UpdatesController@getUpdates');

});

// Public Routes
Route::group(['middleware' => ['web', 'activity']], function () {

    // Locale

    // Activation Routes
    Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

    Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
    Route::get('/activation', ['as' => 'authenticated.activation-resend', 'uses' => 'Auth\ActivateController@resend']);
    Route::get('/exceeded', ['as' => 'exceeded', 'uses' => 'Auth\ActivateController@exceeded']);

    // Socialite Register Routes
    Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
    Route::get('/social/handle/{provider}', ['as' => 'social.handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

    // Route to for user to reactivate their user deleted account.
    Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});


// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated','blocked', 'activity']], function () {
    
    Route::get('/planes/{id}/subscribe','PlanesController@show')->name('plan.show');
    Route::post('/planes/{id}/subscribe','PlanesController@saveSubscription')->name('plan.subscribe');
    Route::delete('/planes/{id}/subscribe','PlanesController@cancelSubscription')->name('plan.subscribe.cancel');
    Route::post('/api/planes/checkCoupon','PlanesController@checkPricePlanWithCoupon');

    // GET CURRENT SUBSCRIPTION
    Route::get('mySubscriptions','MySubcriptionsController@index')->name('mysubcriptions.index');
    Route::get('mySubscriptions/invoices','MySubcriptionsController@invoices')->name('mysubcriptions.invoices');
    Route::get('mySubscriptions/invoices/{invoice}','MySubcriptionsController@downloadInvoice');
    Route::get('mySubscriptions/paymentsMethods','MySubcriptionsController@paymentMethods')->name('mysubcriptions.paymentmethods');
    Route::get('mySubscriptions/paymentsMethods/create','MySubcriptionsController@createPaymentMethod')->name('mysubcriptions.paymentmethods.create');
    Route::post('mySubscriptions/paymentsMethods','MySubcriptionsController@storePaymentMethod')->name('mysubcriptions.paymentmethods.store');
    Route::get('mySubscriptions/paymentsMethods/{idCard}/default','MySubcriptionsController@setDefaultPaymentMethod')->name('mysubcriptions.paymentmethods.defaultcard');
    Route::delete('mySubscriptions/paymentsMethods/{id}','MySubcriptionsController@destroyPaymentMethod')->name('mysubcriptions.paymentmethods.destroy');
    
    // Activation Routes
    Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
    Route::get('/logout', ['uses' => 'Auth\LoginController@logout'])->name('logout');

    //  Homepage Route - Redirect based on user role is in controller.
    Route::get('/home', ['as' => 'home',   'uses' => 'UserController@index']);

    // Show users profile - viewable by other users.
    Route::get('profile/{username}', ['as'   => '{username}', 'uses' => 'ProfilesController@show']);

    Route::get('api/sports', 'Admin\SportsController@getSports');

    // Public Routes to see other profiles
    Route::get('p/profile/search', ['as'   => 'public.profile.search', 'uses' => 'PublicController@searchUser']);

    Route::get('p/profile/{uuid}', ['as'   => 'public.profile.show', 'uses' => 'PublicController@show']);

    Route::get('p/profile/{uuid}/follow', ['as'   => 'public.profile.follow', 'uses' => 'PublicController@follow']);
    Route::get('p/profile/{uuid}/unfollow', ['as'   => 'public.profile.unfollow', 'uses' => 'PublicController@unfollow']);

    Route::get('p/profile/{uuid}/addfavorite', ['as'   => 'public.profile.addFavorite', 'uses' => 'PublicController@addFavorite']);
    Route::get('p/profile/{uuid}/removefavorite', ['as'   => 'public.profile.removeFavorite', 'uses' => 'PublicController@removeFavorite']);

    
    // Route::get('api/favoritesusers','UpdatesController@getFavoritesUsers');
    Route::get('api/updatesFollowedFavoritesUsers','UpdatesController@getUpdatesFollowedFavoritesUsers');
    
    // Public View Recruitment
    Route::get('p/recruitment/{id}', ['as' => 'public.recruitment', 'uses' => 'UserController@showRecruitment']);
    
    Route::get('api/latestrecruitments', 'UserController@latestRecruitments');
    Route::get('api/recruitments/invitations','UserController@recruitmentsInvited');
    Route::post('api/recruitments/{id}/response','UserController@responseInvitationRecruitment');
    Route::post('api/recruitments/{id}/request','UserController@requestInvitationRecruitment');

    // POSTS
    Route::group(['middleware' => ['role:athlete']], function () {
        Route::resource('posts','PostController',['only' => ['index','edit','create','destroy']]);
        Route::post('api/post','PostController@savePost');
        Route::put('api/post/{id}','PostController@updatePost');
        Route::delete('api/post/{id}', 'PostController@destroyPost');
    });
    
    Route::get('posts/{id}','PostController@show')->name('posts.show');
    Route::get('api/myposts','PostController@getMyPosts');
    Route::get('api/post/{id}','PostController@getPost');
    Route::post('api/post/{id}/comment','PostController@saveComment');
    Route::post('api/post/{id}/like','PostController@saveLike');


    // REPORTS
    Route::post('api/report','ReportController@report');

    // NOTIFICATIONS
    Route::get('/notifications','NotificationsController@index')->name('notifications.index');
    Route::get('/notifications/{id}/read','NotificationsController@markAsRead')->name('notifications.markAsRead');
    Route::get('/notifications/read/all','NotificationsController@markAllAsRead')->name('notifications.markAllAsRead');
});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated','blocked', 'currentUser', 'activity', 'twostep']], function () {

    // User Profile and Account Routes
    Route::resource('profile', 'ProfilesController');

    Route::put('profile/{username}/updateUserAccount', ['as'   => '{username}', 'uses' => 'ProfilesController@updateUserAccount']);
    Route::put('profile/{username}/updateUserPassword', ['as'   => '{username}', 'uses' => 'ProfilesController@updateUserPassword']);
    Route::delete('profile/{username}/deleteUserAccount', ['as'   => '{username}', 'uses' => 'ProfilesController@deleteUserAccount']);

    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', ['uses' => 'ProfilesController@userProfileAvatar']);

    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);
    // Route to remove user avatar.
    Route::post('avatar/delete', ['as' => 'avatar.delete', 'uses' => 'ProfilesController@deleteAvatar']);

    Route::resource('favorites', 'FavoritesController');
    Route::resource('followed', 'FollowController');

    Route::get('messages','MessagesController@index')->name('messages.index');

    // SHOWCASE ATHLETE
    Route::group(['middleware' => ['role:athlete']], function () {
        Route::post('api/athlete/{id}/showcase', 'ShowCaseController@store');
        Route::get('api/athlete/entitiesAvailables','ShowCaseController@getEntitiesPreferedAvailables');
        Route::get('api/clubs/types', 'ShowCaseController@getTypesClubs');
    });


    Route::namespace('Api')->group(function () {

        // GET CURRENT USER
        Route::get('api/user', 'UserController@getUser');

        // GET USERS TO SEND MESSAGES
        Route::get('api/usersToMessages', 'UserController@getUsersToMessages');

        // GET USERS BY TYPE
        Route::get('api/users', 'UserController@getUsers');

        // Images
        Route::post('api/user/media/upload', 'UserController@uploadMedia')->middleware('upload.photos');
        // Videos
        Route::post('api/user/videos/upload', 'UserController@uploadVideo')->middleware('upload.videos');

        Route::get('api/user/media', 'UserController@getMediaUser');
        Route::delete('api/user/media/delete/{id}', 'UserController@deleteMedia');

        Route::get('api/athlete/{id}', 'AthleteController@getAthlete');
        Route::post('api/athlete/{id}/sports', 'AthleteController@saveSportsAthlete');
        Route::post('api/athlete/{id}/clubs', 'AthleteController@saveClubsAthlete');

        Route::post('api/scout/{id}/sports', 'ScoutController@saveSports');
        Route::post('api/scout/{id}/clubs', 'ScoutController@saveClubsScout');

        
    });
    // MESSAGES
    Route::get('api/threads', 'MessagesController@getThreads');
    Route::post('api/threads', 'MessagesController@storeThread');
    Route::delete('api/threads/{id}', 'MessagesController@deleteThread');
    Route::post('api/threads/{id}/messages', 'MessagesController@sendMessage');
    Route::post('api/threads/{id}/messages/markAsRead', 'MessagesController@markAsReadToCurrentUser');

    // Messages with Admin
    Route::get('/contactAdmin', 'MessagesController@indexMessagesAdmin')->name('messages.admin');

});
