module.exports = {
  theme: {
    extend: {
      colors: {
        'primary': '#be1f2e',
        'success': '#009f60',
        'info': '#2587E9'
      },
      height: {
        '44': '11rem',
        '96': '24rem',
        '128': '32rem'
      }
    },
    screens: {
      'sm': '640px',
      'md': '800px',
      'lg': '1024px',
      'xl': '1280px',
    }
  },
  variants: {
    borderWidth: ['responsive', 'last', 'hover', 'focus'],
  },
  plugins: [
      function({ addBase, config }) {
        addBase({
          'h1': { fontSize: config('theme.fontSize.4xl') },
          'h2': { fontSize: config('theme.fontSize.3xl') },
          'h3': { fontSize: config('theme.fontSize.2xl') },
          'h4': { fontSize: config('theme.fontSize.xl') },
          'h5': { fontSize: config('theme.fontSize.lg') },
          'h6': { fontSize: config('theme.fontSize.base') },
        })
      }
    ]
}
