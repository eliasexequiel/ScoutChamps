<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SubscriptionServiceProvider extends ServiceProvider
{


    public function register()
    {
        $this->app->bind(
            'App\Repositories\Interfaces\SubscriptionRepositoryInterface',
            'App\Repositories\SubscriptionRepository');
    }
}