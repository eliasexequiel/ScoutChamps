<?php

namespace App\Observers;

use App\Models\Post;

class PostObserver
{
    /**
     * Handle the post "created" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function created(Post $post)
    {
        //
    }

    /**
     * Handle the post "updated" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * Handle the post "deleted" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        $idPost = $post->id;
        $update = \DB::table('updates')
                       ->where('type','newPost')
                       ->where('body', 'like', '%"post_id":'.$idPost.'%')
                       ->delete();
        // \Log::info($update->id);
        // if(isset($update))
        //     $update->delete();
        \DB::table('reports')
            ->where('reported_type', 'App\Models\Post')
            ->where('reported_id', $idPost)
            ->delete();
    }

    /**
     * Handle the post "restored" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the post "force deleted" event.
     *
     * @param  \App\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
