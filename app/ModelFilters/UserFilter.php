<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use Carbon\Carbon;

class UserFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];
    // public $relations = [
    //     'athlete' => ['nationality', 'residenceountry'],
    // ];

    public function fullname($name)
    {
        return $this->where(function ($q) use ($name) {
            $names = explode(" ", $name);
            return $q->where('firstname', 'LIKE', "%$name%")
                ->orWhere('lastname', 'LIKE', "%$name%")
                ->orWhereIn('lastname', $names)
                ->whereIn('firstname', $names)
                ->orWhere('email', 'LIKE', "%$name%");
        });
    }

    public function sport($sport)
    {
        return $this->where(function ($query) use ($sport) {
            return $query->whereHas('athlete', function ($q) use ($sport) {
                return $q->where('sport_id', $sport)->orWhere('secondary_sport_id', $sport);
            })->orWhereHas('scout', function ($q) use ($sport) {
                return $q->where('sport_id', $sport)->orWhere('sport_id',13);
            });
        });
    }

    public function typeUser($rol)
    {
        // if ($rol == null) return $query;
        return $this->where(function ($query) use ($rol) {
            return $query->whereHas('rolesUser', function ($query) use ($rol) {
                return $query->where('role_id', $rol);
            });
        });
    }

    public function gender($value)
    {
        return $this->whereHas('athlete', function ($q) use ($value) {
            return $q->where('gender', $value);
        });
    }

    public function age($value)
    {

        $arrayNumbers = explode('-', $value);

        $ageFrom = (int) $arrayNumbers[0];
        $ageTo = (int) $arrayNumbers[1];

        $from = Carbon::now()->subYears((int) ($ageTo + 1));
        $from = $from->format('Y-m-d');
        $to = Carbon::now()->subYears($ageFrom)->format('Y-m-d');
        // \Log::info($from);
        // \Log::info($to);
        // if ($this->input('typeUser') == '2') return true;
        // if ($this->input('typeUser') == 'Scout' || $this->input('typeUser') == '') return true;
        return $this->where(function ($query) use ($from, $to) {
            return $query->whereHas('athlete', function ($q) use ($from, $to) {
                return $q->where('date_of_birth', '>', $from)->where('date_of_birth', '<=', $to);
            })->orWhereHas('scout');
        });
    }

    public function status($value)
    {
        return $this->whereHas('athlete', function ($q) use ($value) {
            return $q->where('status', $value);
        });
    }

    public function scoutSpeciality($value)
    {
        return $this->where(function ($query) use ($value) {
            return $query->whereHas('scout', function ($q) use ($value) {
                return $q->where('scout_position_id', $value)
                         ->orWhere('scout_position_secondary_id',$value);
            });
        });
    }

    public function sportingGoal($value)
    {
        return $this->whereHas('athlete', function ($q) use ($value) {
            return $q->where('sporting_goal', $value);
        });
    }

    public function residenceCountry($value)
    {
        return $this->where(function ($query) use ($value) {
            return $query->whereHas('athlete', function ($q) use ($value) {
                return $q->where('residence_country_id', $value);
            })->orWhereHas('scout', function ($q) use ($value) {
                return $q->where('residence_country_id', $value);
            });
        });
    }

    public function nationality($value)
    {
        return $this->where(function ($query) use ($value) {
            return $query->whereHas('athlete', function ($q) use ($value) {
                return $q->where('nationality_id', $value);
            })->orWhereHas('scout', function ($q) use ($value) {
                return $q->where('nationality_id', $value);
            });
        });
    }

    public function entity($entity)
    {
        return $this->where(function ($query) use ($entity) {
            return $query->whereHas('athlete.entities', function ($q) use ($entity) {
                return $q->where('entity_id', $entity);
            })->orWhereHas('scout.entities', function ($q) use ($entity) {
                return $q->where('entity_id', $entity);
            });
        });
    }

    public function lastSession($value)
    {
        $valueF = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        return $this->where(function ($query) use ($valueF) {
            return $query->whereHas('sesions', function ($q) use ($valueF) {
                return $q->whereRaw("DATE(created_at) = '" . $valueF . "'");
            });
        });
    }

    public function created($value)
    {
        $valueF = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        return $this->where(function ($query) use ($valueF) {
            return $query->whereRaw("DATE(created_at) = '" . $valueF . "'");
        });
    }
}
