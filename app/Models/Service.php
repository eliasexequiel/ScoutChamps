<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
    
    protected $fillable = ['name','stripe_plan', 'type','price'];

    public function items()
    {
        return $this->hasMany('App\Models\ServiceItemsValues','service_id');
    }

    public function getImageAttribute()
    {
        $img = null;
        switch ($this->name) {
            case 'Free':
                $img = asset('img/home/plans/Free.png');
                break;
            case'Month':
                $img = asset('img/home/plans/Premium3.png');
                break;
            case 'Semester':
                $img = asset('img/home/plans/Premium2.png');
                break;
            case 'Anual':
                $img = asset('img/home/plans/Premium1.png');
                break;
            default:
                break;
        }
        return $img; 
    }
}
