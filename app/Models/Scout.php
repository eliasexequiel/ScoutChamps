<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scout extends Model
{
    protected $table = 'scouts';

    protected $fillable = [
        'nationality_id',
        'residence_country_id',
        'user_id',
        'sport_id',
        'phone',
        'scout_position_id',
        'scout_position_secondary_id'
    ];

    protected $with = [
        'currentEntity',
        'nationality',
        'residenceCountry',
        'speciality',
        'specialitySecondary'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function sport()
    {
        return $this->belongsTo('App\Models\Sport')->withTrashed();
    }

    public function entities()
    {
        return $this->hasMany('App\Models\ScoutEntity', 'scout_id')->orderBy('start');
    }

    public function entitiesReverse()
    {
        return $this->hasMany('App\Models\ScoutEntity', 'scout_id')
            ->with('entity')
            ->select(['scout_entities.*', \DB::raw('IF(`end` IS NOT NULL, `end`, 9999-12-01) `end`'),
                \DB::raw('CASE WHEN entities_types.name IS NULL THEN 1
                       WHEN entities_types.name="Club" THEN 1
                       WHEN entities_types.name="University" THEN 2
                       WHEN entities_types.name="Company" THEN 3
                       WHEN entities_types.name="Sponsor" THEN 4 
                       END AS `typeEntity`')
            ])
            ->join('entities', 'scout_entities.entity_id', '=', 'entities.id')
            ->join('entities_types', 'entities.entity_type_id', '=', 'entities_types.id')
            ->orderBy('end', 'desc')
            ->orderBy('typeEntity', 'asc');
    }

    public function clubes()
    {
        return $this->entities()->whereHas('entity.type', function ($q) {
            return $q->where('name', 'Club');
        })->get();
    }

    public function institutes()
    {
        return $this->entities()->whereHas('entity.type', function ($q) {
            return $q->where('name', 'Institute');
        })->get();
    }

    public function universities()
    {
        return $this->entities()->whereHas('entity.type', function ($q) {
            return $q->where('name', 'University');
        })->get();
    }

    public function sponsors()
    {
        return $this->entities()->whereHas('entity.type', function ($q) {
            return $q->where('name', 'Sponsor');
        })->get();
    }

    public function currentEntity()
    {
        return $this->hasMany('App\Models\ScoutEntity', 'scout_id')->where('isCurrent', true);
    }

    public function nationality()
    {
        return $this->belongsTo('App\Models\Country', 'nationality_id');
    }

    public function residenceCountry()
    {
        return $this->belongsTo('App\Models\Country', 'residence_country_id');
    }

    public function speciality()
    {
        return $this->belongsTo('App\Models\ScoutPosition', 'scout_position_id');
    }

    public function specialitySecondary()
    {
        return $this->belongsTo('App\Models\ScoutPosition', 'scout_position_secondary_id');
    }
}
