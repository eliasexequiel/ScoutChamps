<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestInvitation extends Model
{
    protected $table = 'request_invitation_recruitment';

    protected $fillable = [
        'recruitment_id', 
        'user_id',
        'closed'
    ];

    public function recruitment()
    {
        return $this->belongsTo('App\Models\Recruitment');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
