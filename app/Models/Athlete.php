<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\DatesFormat;
use EloquentFilter\Filterable;

class Athlete extends Model
{
    use DatesFormat;
    use Filterable;
    
    protected $table = 'athletes';

    protected $fillable = [
        'nationality_id',
        'second_nationality_id',
        'residence_country_id',
        'residence_city',
        'date_of_birth',
        'phone',
        'sport_id',
        'secondary_sport_id',
        'sporting_goal',
        'professional_start_date',
        'professional_end_date',
        'height',
        'height_unit',
        'gender',
        'status',
        'emailTutor',
        'firstnameTutor',
        'lastnameTutor',
        'phoneTutor',
        'user_id'
    ];

    protected $with = [
        'sport',
        'sportSecondary',
        'currentEntity',
        'nationality',
        'secondNationality',
        'residenceCountry'
    ];

    public function age()
    {
        if(!$this->attributes['date_of_birth']) return null;
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function sport()
    {
        return $this->belongsTo('App\Models\Sport')->withTrashed();
    }

    public function sportSecondary()
    {
        return $this->belongsTo('App\Models\Sport','secondary_sport_id')->withTrashed();
    }
    
    public function nationality()
    {
        return $this->belongsTo('App\Models\Country','nationality_id');
    }

    public function secondNationality()
    {
        return $this->belongsTo('App\Models\Country','second_nationality_id');
    }

    public function residenceCountry()
    {
        return $this->belongsTo('App\Models\Country','residence_country_id');
    }


    public function fieldsSports()
    {
        return $this->hasMany('App\Models\SportFieldsAthlete','athlete_id');
    }

    public function entities()
    {
        return $this->hasMany('App\Models\AthleteEntity','athlete_id')->orderBy('start');
    }

    public function entitiesReverse()
    {
        return $this->hasMany('App\Models\AthleteEntity','athlete_id')
        ->with('entity')
        ->select(['athlete_entities.*', \DB::raw('IF(`end` IS NOT NULL, `end`, 9999-12-01) `end`'),
        \DB::raw('CASE WHEN entities_types.name IS NULL THEN 1
                       WHEN entities_types.name="Club" THEN 1
                       WHEN entities_types.name="University" THEN 2
                       WHEN entities_types.name="Company" THEN 3
                       WHEN entities_types.name="Sponsor" THEN 4 
                       END AS `typeEntity`')])
        ->join('entities','athlete_entities.entity_id','=','entities.id')
        ->join('entities_types','entities.entity_type_id','=','entities_types.id')
        ->orderBy('end','desc')
        ->orderBy('typeEntity','asc');
    }

    public function entitiesPrefered()
    {
        return $this->hasMany('App\Models\AthleteEntityPreference','athlete_id');
    }

    public function clubes()
    {
        return $this->entities()->whereHas('entity.type',function($q) {
            return $q->where('name','Club');
        })->get();
    }

    public function institutes()
    {
        return $this->entities()->whereHas('entity.type',function($q) {
            return $q->where('name','Institute');
        })->get();
    }

    public function universities()
    {
        return $this->entities()->whereHas('entity.type',function($q) {
            return $q->where('name','University');
        })->get();
    }

    public function sponsors()
    {
        return $this->entities()->whereHas('entity.type',function($q) {
            return $q->where('name','Sponsor');
        })->get();
    }

    public function currentEntity()
    {
        return $this->hasMany('App\Models\AthleteEntity','athlete_id')->where('isCurrent',true);
    }

    public function setDateOfBirthAttribute($value)
    {
        if(app()->getLocale() == "en")
            $this->attributes['date_of_birth'] = Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d');
        else
            $this->attributes['date_of_birth'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }

    public function getDateOfBirthAttribute($value)
    {
            return $this->getDate($value);
    }

    public function getProfessionalStartDateAttribute($value)
    {
            return $this->getDate($value);
    }
    public function setProfessionalStartDateAttribute($value)
    {
        $this->attributes['professional_start_date'] = $this->setDate($value);
    }

    public function getProfessionalEndDateAttribute($value)
    {
            return $this->getDate($value);
    }
    public function setProfessionalEndDateAttribute($value)
    {
        $this->attributes['professional_end_date'] = $this->setDate($value);
    }
}
