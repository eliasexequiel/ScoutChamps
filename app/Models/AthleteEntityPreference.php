<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AthleteEntityPreference extends Model
{
    protected $table = 'athlete_entities_preferences';

    protected $fillable = ['athlete_id','entity_id','contacted'];

    protected $with = [
        'entity'
    ];

    public function athlete()
    {
        return $this->belongsTo('App\Models\Athlete');
    }

    public function entity()
    {
        return $this->belongsTo('App\Models\Entity');
    }
}
