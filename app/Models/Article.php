<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Translatable\HasTranslations;

class Article extends Model implements HasMedia
{

    use HasMediaTrait, HasTranslations;

    protected $table = 'articles';

    protected $fillable = ['title', 'resume', 'body', 'user_id'];

    public $translatable = ['title', 'resume', 'body'];

    protected $appends = [
        'image',
        'timeAgo',
        'hasImage'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getImageAttribute()
    {
        $img = $this->getMedia('article');
        if (count($img) == 0) {
            $img = asset('img/article.png');
        } else {
            $img = $img->last()->getFullUrl();
        }
        return $img;
    }

    public function getHasImageAttribute()
    {
        $img = $this->getMedia('article');
        return count($img) > 0;
    }

    public function getTimeAgoAttribute()
    {
        if ($this->updated_at == null) return null;
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->diffForHumans();
    }
}
