<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $table = 'followers';

    protected $fillable = ['following_user_id','user_id',];

    public function following()
    {
        return $this->belongsTo('App\Models\User','following_user_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
