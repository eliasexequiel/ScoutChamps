<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SportFieldsAthlete extends Model
{
    // use HasTranslations;

    protected $table = 'sport_fields_athletes';
    
    protected $fillable = ['sport_field_id', 'athlete_id','sport_id','value','is_secondary'];
    // public $translatable = ['value'];

    protected $with = ['sportfield'];
    
    // Overwrite Trait HasTranslations.
    // public function setTranslation(string $key, string $locale, $value)
    // {
    //     $this->attributes[$key] = $value;
    // }

    public function sportfield()
    {
        return $this->belongsTo('App\Models\SportFields','sport_field_id');
    }

    public function athlete()
    {
        return $this->belongsTo('App\Models\Athlete','athlete_id');
    }

    public function sport()
    {
        return $this->belongsTo('App\Models\Sport','sport_id');
    }
}
