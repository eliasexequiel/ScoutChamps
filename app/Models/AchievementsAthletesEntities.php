<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AchievementsAthletesEntities extends Model
{
    protected $table = 'achievements_athletes_entities';

    protected $fillable = ['athlete_entity_id', 'name','month','year'];
}
