<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = ['message', 'user_id', 'reported_blocked', 'solved'];

    protected $appends = [
        'tipo',
        'ownerResource'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function reported()
    {
        return $this->morphTo()->withTrashed();
        // $morphTo = $this->morphTo();
        // try {
        //     return $morphTo->withTrashed();
        // } catch (\Throwable $e) {
        //     return $morphTo;
        // }
    }

    public function getTipoAttribute()
    {
        $type = explode("\\", $this->reported_type)[2];
        $type = $this->reported->collection_name == 'images' || $this->reported->collection_name == 'profile' ? 'Image' : $type;
        $type = $this->reported->collection_name == 'videos' ? 'Video' : $type;
        return $type;
    }

    public function getOwnerResourceAttribute()
    {
        $type = explode("\\", $this->reported_type)[2];
        $type = $this->reported->collection_name == 'images' || $this->reported->collection_name == 'profile' ? 'Image' : $type;
        $type = $this->reported->collection_name == 'videos' ? 'Video' : $type;
        $userTypes = ['Post','PostComment','MediaUrl','Message'];
        if(in_array($type,$userTypes)) {
            return $this->reported->user;
        }
        if($type == 'Image' || $type == 'Video') {
            return $this->reported->model;
        }
        return null;
    }
}
