<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostComment extends Model
{
    use SoftDeletes;
    
    protected $table = 'post_comments';

    protected $dates = ['deleted_at'];

    protected $fillable = ['post_id','user_id', 'body'];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function report()
    {
        return $this->morphOne('App\Models\Report', 'reported');
    }
    
}
