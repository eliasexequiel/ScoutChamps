<?php

namespace App\Models;

use App\Traits\Shareable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use EloquentFilter\Filterable;
use Cmgmyr\Messenger\Traits\Messagable;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasRoleAndPermission;
    use Notifiable;
    use SoftDeletes;
    use HasMediaTrait;
    use Filterable;
    use Messagable;
    use Billable;
    use Shareable;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'uuid',
        'password',
        'activated',
        'email_verified_at',
        'blocked',
        'token',
        'about'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'activated'
    ];

    protected $dates = [
        'deleted_at',
    ];

    protected $appends = [
        'fullName',
        'publicLink',
        'profile',
        'profileText',
        'urlProfile',
        'avatar',
        'avatarSport',
        'messagesUnread',
        'sportName',
        'isSubscribed',
        'imagesToUpload',
        'videosToUpload'
    ];

    protected $with = [
        'athlete',
        'scout'
    ];

    protected $shareOptions = [
        'columns' => [
            'title' => 'textShareSocialMedia'
        ],
        'url' => 'urlProfile'
    ];

    // Stripe Cashier Tax Percentaje.
    public function taxPercentage()
    {
        return 7;
    }

    public function scopeNotBlocked($query)
    {
        return $query->where('blocked', false);
    }

    public function scopeOnlyBlocked($query)
    {
        return $query->where('blocked', true);
    }

    public function getFullNameAttribute()
    {
        return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
    }

    public function getPublicLinkAttribute()
    {
        $fullname = ucfirst($this->firstname) . '' . ucfirst($this->lastname);
        return route('public.profile.show',[$fullname.'_'.$this->uuid]);
    }

    public function getTextShareSocialMediaAttribute()
    {
        return trans('sidebar.textShareProfile');
    }

    public function getUrlProfileAttribute()
    {
        $fullname = ucfirst($this->firstname) . '' . ucfirst($this->lastname);
        return route('public.profile.show',[$fullname.'_'.$this->uuid]);
    }

    public function scopeUuid($query, $uuid)
    {
        return $query->where('uuid', '=', $uuid);
    }

    /**
     ** User Relationships.
     **/
    public function social()
    {
        return $this->hasMany('App\Models\Social');
    }

    public function athlete()
    {
        return $this->hasOne('App\Models\Athlete');
    }

    public function scout()
    {
        return $this->hasOne('App\Models\Scout');
    }

    public function getProfileAttribute()
    {
        if ($this->scout) return $this->scout;
        if ($this->athlete) return $this->athlete;
        return null;
    }

    public function getProfileTextAttribute()
    {
        if ($this->scout) return 'scout';
        if ($this->athlete) return 'athlete';
        return 'administrator';
    }

    public function getMessagesUnreadAttribute()
    {
        return $this->newThreadsCount();
    }

    public function getAvatarAttribute()
    {
        $img = $this->getMedia('profile');
        if (count($img) == 0) {
            $img = asset('img/profile-d.png');
        } else {
            $img = $img->last()->getFullUrl();
        }
        return $img;
    }

    public function getAvatarSportAttribute()
    {
        if ($this->athlete && $this->athlete->sport) {
            return $this->athlete->sport->getAvatar();
        }
        // Scout always show same icon
        if ($this->scout) {
            return asset('img/sports/Scout.png');
        }
        return null;
    }

    public function getSportNameAttribute()
    {
        if ($this->athlete && $this->athlete->sport) {
            return $this->athlete->sport->name;
            // return "das";
        }
        if ($this->scout && $this->scout->sport) {
            return $this->scout->sport->name;
        }
        return null;
    }

    public function mediaurls()
    {
        return $this->hasMany('App\Models\MediaUrl');
    }

    public function followed()
    {
        return $this->hasMany('App\Models\Follower', 'user_id')->has('following');
    }

    public function followed_users()
    {
        $users = $this->hasMany('App\Models\Follower', 'user_id')->has('following')->get()
            ->map(function ($u) {
                return $u->following;
            })
            ->unique('id');
        return $users;
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\Favorite', 'user_id')->has('favorite');
    }

    public function followers()
    {
        return $this->hasMany('App\Models\Follower', 'following_user_id')->has('following');
    }

    public function followers_users()
    {
        $users = $this->hasMany('App\Models\Follower', 'following_user_id')->has('user')->get()
            ->map(function ($u) {
                return $u->user;
            })
            ->unique('id');
        return $users;
    }

    public function visited_count()
    {
        return $this->hasMany('App\Models\UserVisited', 'user_id')->has('user')->count();
    }

    public function visited_scout_count()
    {
        $usersCount = $this->hasMany('App\Models\UserVisited', 'user_id')->with('visitor')->get()
            ->filter(function ($u) {
                return $u->visitor->hasRole('scout');
            })->count();
        return $usersCount;
    }

    public function visited_scout()
    {
        $users = $this->hasMany('App\Models\UserVisited', 'user_id')->with('visitor')->get()
            ->filter(function ($u) {
                return $u->visitor->hasRole('scout');
            })
            ->map(function ($u) {
                return $u->visitor;
            })
            ->unique('id');
        return $users;
    }

    public function visits()
    {
        $users = $this->hasMany('App\Models\UserVisited', 'user_id')->with('visitor')->get()
            ->map(function ($u) {
                return $u->visitor;
            })
            ->unique('id');
        return $users;
    }

    public function modelFilter()
    {
        return $this->provideFilter(\App\ModelFilters\UserFilter::class);
    }

    public function rolesUser()
    {
        return $this->hasMany('App\Models\RoleUser', 'user_id');
    }

    public function sesions()
    {
        return $this->hasMany('App\Models\UserSesion', 'user_id');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post', 'user_id')->orderBy('created_at', 'desc');
    }

    public function scopeNotAdmin($query)
    {
        return $query->whereHas('rolesUser', function ($query) {

            return $query->where('role_id', '!=', 1);
        });
    }

    public function scopeTypeUser($query, $rol)
    {
        if ($rol == null) return $query;
        return $query->where(function ($query) use ($rol) {
            return $query->whereHas('rolesUser', function ($query) use ($rol) {

                return $query->where('role_id', $rol->id);
            });
        });
    }

    public function scopePremiun($query, $idRoles)
    {
        return $query->whereHas('rolesUser', function ($query) use ($idRoles) {

            return $query->whereIn('role_id', $idRoles);
        });
    }


    public function scopeSports($query, $sports)
    {
        return $this->where(function ($query) use ($sports) {
            return $query->whereHas('athlete', function ($query) use ($sports) {
                // \Log::info(json_encode($query));
                if (!array_filter(array_values($sports))) return true;
                foreach ($sports as $k => $v) {
                    $query->whereHas('fieldsSports', function ($query) use ($k,$v) {
                        // \Log::info($v);
                        // $names = array_keys($sports);
                        // return $query->whereIn('value',array_values($sports));
                        // $field = explode('_',$k);
                        if ($v != '') {
                            $idField = substr($k, strrpos($k, '_') + 1);;
                            $query->where('value->en', $v)->where('sport_field_id',$idField);
                        }
                        // return $query->where(,)->orwhereIn('value', array_values($sports));
                    });
                }
                return $query;
            })->orWhereHas('scout', function ($query) {
                return true;
            });
        });
    }

    public function recruitmentsInvited()
    {
        return $this->hasMany('App\Models\RecruitmentAthlete', 'user_id');
    }

    public function recruitmentsRequested()
    {
        return $this->hasMany('App\Models\RequestInvitation', 'user_id');
    }

    public function recruitments()
    {
        return $this->hasMany('App\Models\Recruitment', 'user_id')->orderBy('day_start', 'asc');
    }

    public function getUsersToMessagesAttribute()
    {
        if ($this->scout) {
            $followed = $this->followed->map(function ($u) {
                return $u->following;
            });
            $favorites = $this->favorites->map(function ($u) {
                return $u->favorite;
            });
            return $followed->merge($favorites)->unique('id');
        }
        if ($this->athlete) {
            /* El atleta solo puede enviar mensajes a sus seguidos y si ese seguido es un SCOUT,
               el mismo tambien debe seguirlo para que pueda escribirle y el atleta debe ser Premiun. */
            $filtered =  $this->followed->filter(function ($u) {
                $idsUsersFollowed = $u->following->followed->map(function ($uf) {
                    return $uf->following->id;
                })->toArray();
                return  $u->following->hasRole('athlete') || ($u->following->hasRole('scout') && $this->isPremiun() && in_array(\Auth::id(), $idsUsersFollowed));
            });
            $mapped = $filtered->map(function ($u) {
                return $u->following;
            });
            return $mapped->values();
        }
        return [];
    }

    public function isPremiun()
    {
        // return true;
        return $this->hasRole('athlete.premiun')
            || $this->hasRole('athlete.premiun.semester')
            || $this->hasRole('athlete.premiun.anual');
    }

    public function entitiesPreferedTotal()
    {
        if ($this->hasRole('athlete.premiun')) {
            return 10;
        }
        if ($this->hasRole('athlete.premiun.semester')) {
            return 20;
        }
        if ($this->hasRole('athlete.premiun.anual')) {
            return 30;
        }
        return 0;
    }

    public function imagesAvailables()
    {
        if ($this->hasRole('athlete.premiun')) {
            return 10;
        }
        if ($this->hasRole('athlete.premiun.semester')) {
            return 20;
        }
        if ($this->hasRole('athlete.premiun.anual')) {
            return 25;
        }
        return 0;
    }

    public function getImagesToUploadAttribute()
    {
        if (
            $this->hasRole('athlete.premiun.semester') ||
            $this->hasRole('athlete.premiun.anual') ||
            $this->hasRole('athlete.premiun')
        ) {
            return 'Unlimited';
        }
        if ($this->hasRole('scout')) {
            $imagesScoutFree = \DB::table('config_site')->where('name', 'limitImagesScoutFree')->value('value');
            return $imagesScoutFree;
        }
        $imagesAthleteFree = \DB::table('config_site')->where('name', 'limitImagesAthleteFree')->value('value');
        return $imagesAthleteFree;
    }

    public function getVideosToUploadAttribute()
    {
        if($this->hasRole('scout')) {
            return 2;
        }
        if ($this->hasRole('athlete.premiun.semester') || $this->hasRole('athlete.premiun.anual') || $this->hasRole('athlete.premiun')) {
            return 'Unlimited';
        }
        return 1;
    }

    public function entitiesPreferedAvailables()
    {
        $entitiesPrefered = $this->athlete->entitiesPrefered->count();
        return $this->entitiesPreferedTotal() - $entitiesPrefered;
    }

    /**
     * Scope a query to only get active subscribers
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSubscribers($query)
    {
        return $query->whereHas('subscriptions', function ($q) {
            $q->where('name', 'main');
        });
    }

    public function getIsSubscribedAttribute()
    {
        // return true;
        return $this->subscribed('main');
    }

}
