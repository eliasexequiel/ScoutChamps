<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecruitmentAthlete extends Model
{
    protected $table = 'recruitments_athletes';

    protected $fillable = [
        'recruitment_id', 
        'user_id',
        'assist',
        'viewed',
        'message'
    ];

    public function recruitment()
    {
        return $this->belongsTo('App\Models\Recruitment');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
