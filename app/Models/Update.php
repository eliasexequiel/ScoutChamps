<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    protected $table = 'updates';
    
    protected $fillable = ['body','type','user_id'];

    protected $with = [
        'user'
    ];

    protected $appends = [
        'timeAgo'
    ];

    // protected $casts = [
    //     'body' => 'array'
    // ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function scopeUserNotDeleted($query) 
    {
        return $query->whereHas('user');
    }

    public function getTimeAgoAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$this->updated_at)->diffForHumans();
    }

    public function getBodyAttribute($value)
    {
        return json_decode($value,true);
    }
}
