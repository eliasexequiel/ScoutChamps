<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostMedia extends Model
{
    protected $table = 'post_likes';

    protected $fillable = ['post_id','media_id'];

    public function post()
    {
        return $this->belongsTo('App\Models\Post');
    }

    public function media()
    {
        return $this->belongsTo('App\Models\Media');
    }
}
