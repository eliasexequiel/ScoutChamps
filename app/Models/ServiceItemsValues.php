<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceItemsValues extends Model
{
    protected $table = 'service_items_values';
    
    protected $fillable = ['service_id', 'service_item_id','value'];

    public function service()
    {
        return $this->belongsTo('App\Models\Service','service_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\ServiceItems','service_item_id');
    }
}
