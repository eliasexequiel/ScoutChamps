<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = 'searches';
    
    protected $fillable = ['text_searched', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}
