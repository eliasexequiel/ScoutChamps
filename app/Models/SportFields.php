<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class SportFields extends Model
{
    use HasTranslations;

    protected $table = 'sport_fields';
    
    protected $fillable = ['sport_id','name', 'fieldtype'];
    public $translatable = ['name'];

    protected $with = ['options_select'];

    public function sport()
    {
        return $this->belongsTo('App\Models\Sport');
    }

    public function options_select()
    {
        return $this->hasMany('App\Models\SportFieldsOptionsSelect','sport_field_id');
    }

    public function sport_fields_athletes()
    {
        return $this->hasMany('App\Models\SportFieldsAthlete','sport_field_id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($sportfield) { // before delete() method call this
            $sportfield->options_select()->delete();
            $sportfield->sport_fields_athletes()->delete();
        });
    }
}
