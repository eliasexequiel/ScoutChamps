<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Favorite;
use App\Models\User;

class FavoritesController extends Controller
{
    public function index()
    {
        $paginationSize = 6;
        $favorites = Favorite::where('user_id',\Auth::user()->id)->pluck('favorite_user_id');
        $users = User::notBlocked()->whereIn('id',$favorites)->paginate($paginationSize);
        return view('favorites.index')->with(['users' => $users]);

    }
}
