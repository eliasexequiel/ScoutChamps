<?php

namespace App\Http\Controllers;

use App\Models\Athlete;
use App\Models\AthleteEntityPreference;
use App\Models\Entity;
use App\Models\EntityType;
use App\Models\User;
use App\Notifications\ClubCreated;
use App\Notifications\UserAddClubToShowCase;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use jeremykenedy\LaravelRoles\Models\Role;

class ShowCaseController extends Controller
{
    public function getTypesClubs()
    {
        $typesEntities = ['Club','University','Sponsor'];
        $types = EntityType::orderBy('name')->whereIn('name',$typesEntities)->get();
        // $types = EntityType::all();
        return response()->json(['typesEntities' => $types]);
    }

    public function getEntitiesPreferedAvailables()
    {
        return response()->json(['entitiesAvailables' => \Auth::user()->entitiesPreferedAvailables()]);
    }

    public function store(Request $request, $id)
    {
        try {
            if ($this->validateEntities($request->clubs)) throw new Exception(trans('clubes.forms.errorName'));

            // Borramos los CLUBES que no están mas presentes.
            $athlete = Athlete::find($id);
            $currentIds = $athlete->entitiesPrefered->pluck('id')->toArray() ?? array();
            $ids = array_filter($request->clubs, function ($c) {
                return isset($c['id']);
            });
            $ids = array_map(function ($c) {
                return $c['id'];
            }, $ids);
            $diffToDelete = array_diff($currentIds, $ids);
            AthleteEntityPreference::destroy($diffToDelete);

            foreach ($request->clubs as $c) {
                if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                    $type = EntityType::where('id', $c['type_entity'])->first();
                    $e = Entity::create([
                        'name' => $c['other_entity'],
                        'entity_type_id' => $type->id,
                        'verified' => false
                    ]);

                    // Notifiy to ADMIN Users.
                    $adminRol = Role::where('slug', 'admin')->first();
                    $usersAdmin = User::typeUser($adminRol)->get();
                    Notification::send($usersAdmin, new ClubCreated(\Auth::user(), $e));

                    $athleteEntity = AthleteEntityPreference::create([
                        'entity_id' => $e->id,
                        'athlete_id' => $id
                    ]);
                    $entity = $e->fresh();
                    $this->notifyToAdminUserAddClubToShowcase(\Auth::user(), $entity);
                } else {
                    if (!isset($c['id'])) {
                        $athleteEntity = AthleteEntityPreference::firstOrCreate([
                            'entity_id' => $c['entity_id'],
                            'athlete_id' => $id
                        ]);

                        $entity = Entity::find($c['entity_id']);
                        $this->notifyToAdminUserAddClubToShowcase(\Auth::user(), $entity);
                    }
                }
            }

            return response()->json(['msg' => trans('showcase.updateSuccess')]);
        } catch (Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], 500);
        }
    }

    protected function notifyToAdminUserAddClubToShowcase(User $user, Entity $entity)
    {
        $adminRol = Role::where('slug', 'admin')->first();
        $usersAdmin = User::typeUser($adminRol)->get();
        Notification::send($usersAdmin, new UserAddClubToShowCase($user, $entity));
    }

    private function validateEntities($entities)
    {
        $exist = false;
        foreach ($entities as $c) {
            if ($c['entity_id'] == 'other' && $c['other_entity'] != '') {
                if(Entity::where('name',$c['other_entity'])->exists()) {
                    $exist = true;
                    break;
                }
            }
        }
        return $exist;
    }
}
