<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\PostLikes;
use Validator;
use App\Events\NewUpdate as NewUpdateEvent;

class PostController extends Controller
{
    public function index()
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $posts = Post::where('user_id', \Auth::user()->id)
                ->orderBy('created_at','desc')
                ->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $posts = Post::where('user_id', \Auth::user()->id)
                         ->orderBy('created_at','desc')
                         ->get();
        }
        return view('posts.index', compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $post->load('comments.user','likes.user','user');
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $post->load('comments.user','likes.user','user');
        // dd($post);
        return view('posts.edit',compact('post'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        // foreach ($post->comments as $c) {
        //     $c->delete();
        // }
        // foreach ($post->likes as $l) {
        //     $l->delete();
        // }
        $post->delete();
        return redirect()->route('posts.index')->with('success', trans('posts.deleteSuccess'));
    }



    // API CALLS
    public function getMyPosts()
    {
        $userCurrent = \Auth::user();
        $usersFollowed = $userCurrent->followed->pluck('following_user_id')->all();
        $usersFavorites = $userCurrent->favorites->pluck('favorite_user_id')->all();
        $usersId = array_merge([$userCurrent->id],$usersFollowed,$usersFavorites);
        $posts = Post::whereIn('user_id',$usersId)->latest()->get();
        $posts->load('comments.user','likes.user','user');
        return response()->json(['posts' => $posts]);
    }

    public function getPost($id)
    {
        $post = Post::findOrFail($id);
        $post->load('comments.user','likes.user','user');
        return response()->json(['post' => $post]);
    }

    public function updatePost(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ['text' => 'required|max:10000']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 402);
        }

        Post::findOrFail($id)->update(['body' => $request->text]);

        $request->session()->flash('success', trans('posts.updateSuccess'));
        $route = route('posts.index');
        return response()->json(['url' => $route], 200);
    }


    public function savePost(Request $request)
    {
        $validator = Validator::make($request->all(), ['text' => 'required|max:10000']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 402);
        }

        $post = Post::create([
            'body' => $request->text,
            'user_id' => \Auth::user()->id
        ]);

        $body = json_encode(['post_id' => $post->id]);
        event(new NewUpdateEvent(\Auth::user(),$body, 'newPost'));
        $route = route('posts.index');
        return response()->json(['url' => $route],200);
    }

    public function destroyPost($id)
    {
        $post = Post::findOrFail($id);
        foreach ($post->comments as $c) {
            $c->delete();
        }
        foreach ($post->likes as $l) {
            $l->delete();
        }
        $post->delete();

        return response()->json(['msg' => trans('posts.deleteSuccess')]);   
    }

    public function saveComment(Request $request,$id)
    {
        $validator = Validator::make($request->all(), ['body' => 'required|max:10000']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 402);
        }

        $comment = PostComment::create([
            'body' => $request->body,
            'post_id' => $id,
            'user_id' => \Auth::user()->id
        ]);

        return response()->json(['comment' => $comment]);
    }

    public function saveLike(Request $request,$id)
    {
        $like = PostLikes::where('post_id',$id)->where('user_id',\Auth::user()->id)->first();

        if($like) {
            $like->delete();
        } else {
            $like = PostLikes::create([
                'post_id' => $id,
                'user_id' => \Auth::user()->id
            ]);
        }
        return response()->json(['like' => $like]);
    }
}
