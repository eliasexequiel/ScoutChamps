<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\ActivationTrait;
use App\Traits\CaptchaTrait;
use App\Traits\CaptureIpTrait;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Validation\Rule;
use jeremykenedy\Uuid\Uuid;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\CreateAthleteRequest;
use App\Http\Requests\CreateScoutRequest;
use App\Mail\TermsAndConditions;
use App\Models\Athlete;
use App\Models\Entity;
use App\Models\EntityType;
use App\Models\Scout;
use App\Models\ScoutEntity;
use App\Notifications\ClubCreated;
use App\Notifications\UserRegistered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use ActivationTrait;
    use CaptchaTrait;
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/activate';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => 'logout',
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        $typesEntities = ['Club','University','Sponsor','Company'];
        $entitiestype = EntityType::orderBy('name')->whereIn('name',$typesEntities)->get()->toArray();
        // $entitiesAux = EntityType::with('entities')->get()->toArray();
        // $entitiesAux = $entitiesAux + array('otherClub' => trans('auth.otherClub'));
        // $entities = [];
        // foreach ($entitiesAux as $e) {
        //     array_push($entities,array($e['name']));
        // }
        // dd($entities);
        if ($request->query('tab')) $request->session()->flash('currentTab', '#scout');
        return view('auth.register', compact('entitiestype'));
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['captcha'] = $this->captchaCheck();

        if (!config('settings.reCaptchStatus')) {
            $data['captcha'] = true;
        }

        return Validator::make(
            $data,
            [
                'first_name'            => '',
                'last_name'             => '',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:30|confirmed',
                'password_confirmation' => 'required|same:password',
                'date_of_birth'         => 'required',
                'emailTutor'            => Rule::requiredIf($data['age'] < 18),
                'firstnameTutor'            => Rule::requiredIf($data['age'] < 18),
                'lastnameTutor'            => Rule::requiredIf($data['age'] < 18),
                'phoneTutor'            => Rule::requiredIf($data['age'] < 18),
            ],
            [
                'name.unique'                   => trans('auth.userNameTaken'),
                'name.required'                 => trans('auth.userNameRequired'),
                'first_name.required'           => trans('auth.fNameRequired'),
                'last_name.required'            => trans('auth.lNameRequired'),
                'email.required'                => trans('auth.emailRequired'),
                'email.email'                   => trans('auth.emailInvalid')
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        // $ipAddress = new CaptureIpTrait();

        $user = User::create([
            'email'             => $data['email'],
            'password'          => Hash::make($data['password']),
            'phone'             => $data['phone'],
            'token'             => str_random(64),
            'activated'         => !config('settings.activation'),
        ]);

        $user->attachRole($role);
        $this->initiateEmailActivation($user);

        return $user;
    }

    public function registerAthlete(CreateAthleteRequest $request)
    {
        try {
            $role = Role::where('slug', '=', 'athlete')->first();

            $user = User::create([
                'firstname'         => $request->firstname,
                'lastname'          => $request->lastname,
                'email'             => $request->email,
                'password'          => Hash::make($request->password),
                'uuid'              => Uuid::generate(4),
                'token'             => str_random(64),
                'activated'         => !config('settings.activation'),
                // 'verified'          => true
            ]);

            $athlete = Athlete::create([
                'phone'         => $request->phone,
                'date_of_birth' => $request->date_of_birth,
                'user_id'       => $user->id
            ]);

            if ($request->age < 18) {
                $athlete->emailTutor = $request->emailTutor;
                $athlete->firstnameTutor = $request->firstnameTutor;
                $athlete->lastnameTutor = $request->lastnameTutor;
                $athlete->phoneTutor = $request->phoneTutor;
                $athlete->save();
            }

            $user->attachRole($role);
            // $this->initiateEmailActivation($user);
            event(new Registered($user));
            $this->notifyAdminUsersRegistered($user);
            \DB::table('user_sesions')->updateOrInsert(['user_id' => $user->id],['created_at' => Carbon::now()->toDateTimeString()]);
            
            // $user->sendEmailVerificationNotification();
            $this->sendMailTermsAndPolitics($user);

            $send_registered = [
                'event_name'     => 'Registro',
                'event_category' => 'Nuevo Usuario',
                'event_label'    => 'Atleta' 
            ];
            request()->session()->flash('send_registered',$send_registered);

            $this->guard()->login($user);
            return $this->registered($request, $user) ?: redirect()->route('mysubcriptions.index');

            // return $this->registered($request, $user) ?: redirect($this->redirectPath());
        } catch (\Exception $e) {
            $this->guard()->login($user);
            return $this->registered($request, $user) ?: redirect()->route('mysubcriptions.index');
        }
    }

    public function registerScout(CreateScoutRequest $request)
    {
        try {
            // if ($request->entity_id == 'otherClub' && $request->otherClub != '') {
            //     if(Entity::where('name',$request->otherClub)->first()) {
            //         return back()->withErrors([trans('clubes.forms.errorName')]);
            //     }
            // }

            // dd($request->all());
            $role = Role::where('slug', '=', 'scout')->first();

            $user = User::create([
                'firstname'         => $request->firstname,
                'lastname'          => $request->lastname,
                'email'             => $request->email,
                'password'          => Hash::make($request->password),
                'uuid'              => Uuid::generate(4),
                'token'             => str_random(64),
                'activated'         => !config('settings.activation'),
            ]);

            $scout = Scout::create([
                'phone'         => $request->phone,
                'user_id'       => $user->id
            ]);

            if ($request->entity_id != '') {
                if ($request->entity_id == 'otherClub' && $request->otherClub != '') {
                    $entityExist = Entity::where('name',$request->otherClub)->first();
                    if($entityExist != null) {
                        $request->entity_id = $entityExist->id;
                    } else {
                        // Si agregó un nuevo CLUB lo creamos.
                        $newEntity = Entity::create(['name' => $request->otherClub, 'entity_type_id' => $request->entity_type_id, 'verified' => false]);
                        $request->entity_id = $newEntity->id;

                        // Notifiy to ADMIN Users.
                        $adminRol = Role::where('slug', 'admin')->first();
                        $usersAdmin = User::typeUser($adminRol)->get();
                        Notification::send($usersAdmin, new ClubCreated($user, $newEntity));
                    }
                }
                ScoutEntity::create([
                    'scout_id' => $scout->id,
                    'entity_id'     => $request->entity_id
                ]);
            }

            $user->attachRole($role);
            // $this->initiateEmailActivation($user);
            event(new Registered($user));
            $this->notifyAdminUsersRegistered($user);
            \DB::table('user_sesions')->updateOrInsert(['user_id' => $user->id],['created_at' => Carbon::now()->toDateTimeString()]);

            // $user->sendEmailVerificationNotification();
            $this->sendMailTermsAndPolitics($user);

            $send_registered = [
                'event_name'     => 'Registro',
                'event_category' => 'Nuevo Usuario',
                'event_label'    => 'Scout' 
            ];
            request()->session()->flash('send_registered',$send_registered);

            $this->guard()->login($user);
            return $this->registered($request, $user) ?: redirect()->route('profile.index');

            // return $this->registered($request, $user) ?: redirect($this->redirectPath());
        } catch (\Exception $e) {
            $this->guard()->login($user);
            return $this->registered($request, $user) ?: redirect()->intended($this->redirectPath());
        }
    }

    protected function notifyAdminUsersRegistered(User $user)
    {
        $adminRol = Role::where('slug', 'admin')->first();
        $usersAdmin = User::typeUser($adminRol)->get();
        Notification::send($usersAdmin, new UserRegistered($user));
        return true;
    }

    public function sendMailTermsAndPolitics($user)
    {
        // Send Terms and Conditions to new USER.
        try {
            if (app()->getLocale() == 'en') {
                $terms = 'terms';
                $privacy = 'privacy';
            } else {
                $terms = 'terms_es';
                $privacy = 'privacy_es';
            }
            \Log::info("EMAIL WITH POLITICS SENDED TO: " . $user->fullName);
            $termsHtml = \DB::table('config_site')->where('name', $terms)->value('value');
            $privacyHtml = \DB::table('config_site')->where('name', $privacy)->value('value');
            Mail::to($user)->send(new TermsAndConditions($termsHtml, $privacyHtml));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
