<?php

namespace App\Http\Controllers;

use App\Mail\TermsAndConditions;
use Illuminate\Http\Request;
use App\Models\Recruitment;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use App\Repositories\Interfaces\RecruitmentRepositoryInterface;
use App\Notifications\FavoriteUser;
use App\Notifications\NewRequestInvitationRecruitmentToScout;
use Illuminate\Support\Facades\Mail;

// use Spatie\GoogleCalendar\Event;

class UserController extends Controller
{
    protected $recruitmentRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RecruitmentRepositoryInterface $recruitmentRepository)
    {
        $this->middleware('auth');
        $this->recruitmentRepository = $recruitmentRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        // $subscription = $user->subscription('main')->asStripeSubscription();
        // dd($subscription);
        // $user->subscription('main')->syncTaxPercentage();
        if ($user->isAdmin()) {
            return view('pages.admin.home');
        }
        return view('pages.user.home');
    }

    public function latestRecruitments()
    {
        $userCurrent = \Auth::user();
        $usersFollowed = $userCurrent->followed->pluck('following_user_id')->all();
        $recruitments = $this->recruitmentRepository->latestByUsers(null,$usersFollowed,5);
        return response()->json(['recruitments' => $recruitments],200);
    }

    public function recruitmentsInvited()
    {
        $user = \Auth::user();
        $recruitments = $this->recruitmentRepository->getInvitedByUser($user,Carbon::now()->format('Y-m-d'));
        return response()->json(['recruitments' => $recruitments],200); 
    }

    public function responseInvitationRecruitment(Request $request,$id)
    {
        $recruitment = Recruitment::find($id);
        $user = \Auth::user();
        $this->recruitmentRepository->responseInvitation($recruitment,$user,$request->assist);
        return response()->json(['msg' => trans('recruitments.invitation.responseSuccess')],200);
    }

    public function requestInvitationRecruitment(Request $request,$id) 
    {
        $recruitment = Recruitment::find($id);
        $user = \Auth::user();
        $this->recruitmentRepository->requestInvitation($recruitment,$user);

        $scoutRecruitment = User::find($recruitment->user_id); 
        $scoutRecruitment->notify(new NewRequestInvitationRecruitmentToScout($user, $recruitment));

        return response()->json(['msg' => trans('recruitments.request.responseSuccess')],200);
    }

    public function showRecruitment($id)
    {
        $recruitment = Recruitment::findOrFail($id);
        $recruitment->load('user');
        $invitation = $this->recruitmentRepository->getInvitation(\Auth::user(),$recruitment);
        return view('public.recruitments.show',compact('invitation','recruitment'));
    }
}
