<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Faq;
use Validator;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.faqs.index');
    }

    public function getFaqsByType(Request $request) 
    {
        $faqs = Faq::where('type',$request->query('type'))->get();
        return response()->json(['faqs' => $faqs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'pregunta_en' => 'required|max:2000',
                'pregunta_es' => 'required|max:2000',
                'respuesta_en'  => 'required|max:4000',
                'respuesta_es'  => 'required|max:4000'
            ]
        );

        if ($validator->fails()) {
            // return back()->withErrors($validator)->withInput();
            return response()->json(['errors' => $validator->errors()->all()], 422);
        }

        $faq = Faq::create([
            'question' => [ 'en' => $request->pregunta_en, 'es' => $request->pregunta_es],
            'answer' => [ 'en' => $request->respuesta_en, 'es' => $request->respuesta_es],
            'type' => $request->type
        ]);
        
        return response()->json(['msg' => 'ok'], 200);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $validator = Validator::make(
            $request->all(),
            [
                'pregunta_en' => 'required|max:2000',
                'pregunta_es' => 'required|max:2000',
                'respuesta_en'  => 'required|max:4000',
                'respuesta_es'  => 'required|max:4000'
            ]
        );
        if ($validator->fails()) {
            // return back()->withErrors($validator)->withInput();
            return response()->json(['errors' => $validator->errors()->all()], 422);
        }

        $faq = Faq::find($id)->update([
            'question' => [ 'en' => $request->pregunta_en, 'es' => $request->pregunta_es],
            'answer' => [ 'en' => $request->respuesta_en, 'es' => $request->respuesta_es]
        ]);
        return response()->json(['msg' => 'ok'], 200);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq= Faq::findOrFail($id);
        $faq->delete();
        return response()->json(['msg' => 'Faq deleted'], 200);  
    }
}
