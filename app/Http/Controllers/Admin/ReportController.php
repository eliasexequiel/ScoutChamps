<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\MediaUrl;
use App\Models\Message;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\Report;
use App\Notifications\ReportClosedWithReportedDeleted;
use Illuminate\Http\Request;
use Exception;
use Validator;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $reports = Report::orderByDesc('created_at')->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $reports = Report::orderByDesc('created_at')->get();
        }
        return view('admin.reports.index', compact('reports'));
    }

    public function show($id)
    {
        $report = Report::findOrFail($id);
        return view('admin.reports.show', compact('report'));
    }

    public function close($id)
    {
        $report = Report::findOrFail($id);
        $report->solved = true;
        $report->save();
        return redirect()->route('admin.reports.index')->with('success', trans('reports.reportClosedSuccess'));
    }

    public function deleteReported($id)
    {
        try {
            $report = Report::findOrFail($id);
            switch ($report->tipo) {
                case 'Post':
                    $post = Post::find($report->reported_id);
                    $post->delete();
                    break;
                case 'PostComment':
                    $commentpost = PostComment::find($report->reported_id);
                    $commentpost->delete();
                    break;
                case 'Image':
                    $image = Media::find($report->reported_id);
                    $image->delete();
                    break;
                case 'Video':
                    $video = Media::find($report->reported_id);
                    $video->delete();
                    break;
                case 'MediaUrl':
                    $video = MediaUrl::find($report->reported_id);
                    $video->delete();
                    break;
                case 'Message':
                        $message = Message::find($report->reported_id);
                        $message->delete();
                        break;
                default:
                    break;
            }

            $report->solved = true;
            $report->reported_blocked = true;
            $report->save();

            // Notification to user owner of resource.
            $report->ownerResource->notify(new ReportClosedWithReportedDeleted($report));

            $tipo = $report->tipo == 'MediaUrl' ? 'Video' : $report->tipo; 
            return redirect()->route('admin.reports.index')->with('success', trans('reports.reportDeletedSuccess', ['type' => trans('reports.types.'.$tipo)]));
        } catch (Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }
}
