<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ServiceItemsValues;
use Validator;

class ConfigController extends Controller
{
    public function index()
    {
        $verifyUsers = \DB::table('config_site')->where('name', 'verifyUsers')->first();
        $verifyScoutsClubs = \DB::table('config_site')->where('name', 'verifyScoutsClubs')->first();
        $verifyClubs = \DB::table('config_site')->where('name', 'verifyClubs')->first();
        $sectionClubs = \DB::table('config_site')->where('name', 'sectionClubsInHome')->first();
        $contactEmail = \DB::table('config_site')->where('name', 'contactEmail')->value('value');
        $contactPhone = \DB::table('config_site')->where('name', 'contactPhone')->value('value');
        $limitImagesAthleteFree = \DB::table('config_site')->where('name', 'limitImagesAthleteFree')->value('value');
        $limitImagesScoutFree = \DB::table('config_site')->where('name', 'limitImagesScoutFree')->value('value');
        $data = [
            'verifyUsers'        => $verifyUsers,
            'verifyScoutsClubs'  => $verifyScoutsClubs,
            'verifyClubs'        => $verifyClubs,
            'sectionClubs'       => $sectionClubs,
            'email'              => $contactEmail,
            'phone'              => $contactPhone,
            'limitImages'        => $limitImagesAthleteFree,
            'limitImagesScout'   => $limitImagesScoutFree
        ];
        return view('admin.config.index')->with($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), ['limitImages' => 'required|integer|between:1,200']);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $users = $request->has('verifyUsers') ? true : false;
        $scoutClubs = $request->has('verifyScoutsClubs') ? true : false;
        $clubs = $request->has('verifyClubs') ? true : false;
        $sectionClubs = $request->has('sectionClubs') ? true : false;
        \DB::table('config_site')->where('name', 'verifyUsers')->update(['value' => $users]);
        \DB::table('config_site')->where('name', 'verifyScoutsClubs')->update(['value' => $scoutClubs]);
        \DB::table('config_site')->where('name', 'verifyClubs')->update(['value' => $clubs]);
        \DB::table('config_site')->updateOrInsert(['name' => 'sectionClubsInHome'], ['value' => $sectionClubs]);

        \DB::table('config_site')->updateOrInsert(['name' => 'limitImagesScoutFree'], ['value' => $request->limitImagesScout]);
        // Update limit images on config and on service_items_value to show table correctly. 
        \DB::table('config_site')->updateOrInsert(['name' => 'limitImagesAthleteFree'], ['value' => $request->limitImages]);


        ServiceItemsValues::whereHas('service', function ($q) { $q->where('name', 'Free'); })
                          ->whereHas('item', function ($q) { $q->where('name', 'LIKE', '%"en":"Number of pictures"%'); })
                          ->update(['value' => $request->limitImages]);

        \DB::table('config_site')->updateOrInsert(
            ['name' => 'contactEmail'],
            ['value' => $request->email]
        );
        \DB::table('config_site')->updateOrInsert(
            ['name' => 'contactPhone'],
            ['value' => $request->phone]
        );

        return redirect()->route('config.index')->with('success', trans('config.updateSuccess'));
    }

    public function showTerms()
    {
        $terms = \DB::table('config_site')->where('name', 'terms')->value('value');
        $terms_es = \DB::table('config_site')->where('name', 'terms_es')->value('value');
        return view('admin.config.terms')->with(['terms' => $terms, 'terms_es' => $terms_es]);
    }

    public function storeTerms(Request $request)
    {
        $validator = Validator::make($request->all(), ['terms' => 'max:50000', 'terms_es' => 'max:50000']);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        \DB::table('config_site')->updateOrInsert(['name' => 'terms'], ['value' => $request->terms]);
        \DB::table('config_site')->updateOrInsert(['name' => 'terms_es'], ['value' => $request->terms_es]);

        return redirect()->route('terms.edit')->with('success', trans('config.updateTermsSuccess'));
    }

    public function showPrivacy()
    {
        $privacy = \DB::table('config_site')->where('name', 'privacy')->value('value');
        $privacy_es = \DB::table('config_site')->where('name', 'privacy_es')->value('value');
        return view('admin.config.privacy')->with(['privacy' => $privacy, 'privacy_es' => $privacy_es]);
    }

    public function storePrivacy(Request $request)
    {
        $validator = Validator::make($request->all(), ['privacy' => 'max:50000', 'privacy_es' => 'max:50000']);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        \DB::table('config_site')->updateOrInsert(['name' => 'privacy'], ['value' => $request->privacy]);
        \DB::table('config_site')->updateOrInsert(['name' => 'privacy_es'], ['value' => $request->privacy_es]);

        return redirect()->route('privacy.edit')->with('success', trans('config.updatePrivacySuccess'));
    }
}
