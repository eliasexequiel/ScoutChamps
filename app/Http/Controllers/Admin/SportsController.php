<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Sport;
use App\Models\SportFields;
use App\Models\SportFieldsOptionsSelect;
use Validator;

class SportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $sports = Sport::where('name','!=','All Sports')->orderBy('name')->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $sports = Sport::where('name','!=','All Sports')->orderBy('name')->get();
        }

        return view('admin.sports.index', compact('sports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     $types = trans('fields.typesFieldsSports');
    //     $data = [ 'types' => $types];
    //     return view('admin.sports.create')->with($data);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [ 'name' => 'required|unique:sports' ]);

    //     if ($validator->fails()) {
    //         return back()->withErrors($validator)->withInput();
    //     }
    //     $data = $request->only('name','description');
    //     $sport = Sport::create($data);

    //     return redirect()->route('sports.index')->with('success', trans('sports.createSuccess'));
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sport = Sport::find($id);
        $sport->load('fields.options_select');
        return view('admin.sports.show', compact('sport'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sport = Sport::find($id);
        $types = trans('fields.typesFieldsSports');
        $data = ['types' => $types, 'sport' => $sport];
        return view('admin.sports.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());

        $validator = Validator::make($request->all(), [
            'description'           => 'max: 5000',
            'fields.*.name_en'      => 'required',
            'fields.*.name_es'      => 'required',
            'fields.*.options.*.en' => 'required',
            'fields.*.options.*.es' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $data = $request->only('description');
        $sport = Sport::findOrFail($id)->update($data);
        $sport = Sport::findOrFail($id);

        $ids = is_array($request->fields) ? array_map(function ($field) {
            return $field['id'];
        }, $request->fields) : array();
        $currentIds = $sport->fields->pluck('id')->toArray();
        $diffToDelete = array_diff($currentIds, $ids);

        SportFields::destroy($diffToDelete);
        // var_dump($diffToDelete);
        // exit;

        if (isset($request->fields)) {
            foreach ($request->fields as $field) {

                $name_en = $field['name_en'];
                $name_es = $field['name_es'];
                $type = $field['type'];
                if ($name_en != null && $name_es != null) {
                    if ($field['id'] != '') {
                        $sportField =  SportFields::find($field['id'])->update([
                            'name' => [
                                'en' => $name_en,
                                'es' => $name_es
                            ],
                            'fieldtype' => $type
                        ]);
                        $sportField = SportFields::find($field['id']);
                        if ($type == 'select' && isset($field['options']) && is_array($field['options'])) {
                            $idsOptionsField = array_map(function ($option) {
                                return $option['id'];
                            }, $field['options']);
                            $currentIdsOptions = $sportField->options_select->pluck('id')->toArray();
                            $diffToDeleteOptions = array_diff($currentIdsOptions, $idsOptionsField);
                            SportFieldsOptionsSelect::destroy($diffToDeleteOptions);

                            foreach ($field['options'] as $option) {
                                \Log::info($option);
                                if ($option['id'] != null && $option['es'] != null && $option['en'] != null) {
                                    SportFieldsOptionsSelect::find($option['id'])->update([
                                        'name' => [
                                            'en' => $option['en'],
                                            'es' => $option['es']
                                        ]
                                    ]);
                                } elseif ($option['es'] != null && $option['en'] != null) {
                                    SportFieldsOptionsSelect::create([
                                        'name' => [
                                            'en' => $option['en'],
                                            'es' => $option['es']
                                        ],
                                        'sport_field_id' => $field['id']

                                    ]);
                                }
                            }
                        } else {
                            $currentIdsOptions = $sportField->options_select->pluck('id')->toArray();
                            SportFieldsOptionsSelect::destroy($currentIdsOptions);
                        }
                    } else {
                        $sportField =  SportFields::create([
                            'name' => [
                                'en' => $name_en,
                                'es' => $name_es
                            ],
                            'fieldtype' => $type,
                            'sport_id' => $sport->id
                        ]);

                        if ($type == 'select' && isset($field['options']) && is_array($field['options'])) {
                            foreach ($field['options'] as $option) {
                                if ($option['es'] != null && $option['en'] != null) {
                                    SportFieldsOptionsSelect::create([
                                        'name' => [
                                            'en' => $option['en'],
                                            'es' => $option['es']
                                        ],
                                        'sport_field_id' => $sportField->id
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        // dd($request->all());

        return redirect()->route('sports.index')->with('success', trans('sports.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sport = Sport::findOrFail($id);
        $sport->delete();
        return redirect()->route('sports.index')->with('success', trans('sports.deleteSuccess'));
    }

    public function getSports()
    {
        $user = \Auth::user();
        if ($user->hasRole('athlete')) {
            $sports = Sport::where('name','!=','All Sports')->with('fields.options_select')->get();
        } else {
            $sports = Sport::with('fields.options_select')->get();
        }
        $sports = $sports->map(function($item) {
            if($item->name == 'All Sports') $item['nameTranslated'] = 'AAAA';
            else $item['nameTranslated'] = trans('sportfields.titles.'. $item->name);
            return $item;
        })->sortBy('nameTranslated');
        $sports = array_values($sports->toArray());
        return response()->json(['sports' => $sports]);
    }
}
