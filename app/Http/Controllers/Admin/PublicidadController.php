<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MediaRequest;
use App\Models\Media;
use App\Models\Publicity;
use Illuminate\Support\Facades\Input;

class PublicidadController extends Controller
{
    public function index()
    {
        return view('admin.publicidad.index');
    }

    public function getPublicidades()
    {
        $mediaAbove = Publicity::where('position', 'above')->first();
        $mediaBelow = Publicity::where('position', 'below')->get();
        return response()->json(['imageAbove' => $mediaAbove,'imagesBelow' => $mediaBelow]);
    }

    public function savePublicidad(MediaRequest $request)
    {
        try {
            if($request->id) {
                $publicity = Publicity::find($request->id);
            } else {
                $publicity = Publicity::create(['position' => $request->query('position')]);
            } 
            if($request->link) {
                $publicity->link = $request->link;
                $publicity->save();
            }

            if (Input::hasFile('file')) {
                $file = Input::file('file');
                $publicity->addMedia($file)->toMediaCollection('publicities');
            }
            return response()->json(['msg' => 'Publicity added'], 200);
        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 404);
            // return response()->json(['msg' => 'Error uploading media'], 404);
        }
    }

    public function deleteImageFromPublicidad($id)
    {
        $publicity = Publicity::findOrFail($id);
        $media = $publicity->getFirstMedia('publicities');
        if(isset($media)) $media->forceDelete();
        return response()->json(['msg' => 'Publicity Media file deleted']);
    }

    public function deletePublicidad($id)
    {
        $publicity = Publicity::findOrFail($id);
        $media = $publicity->getFirstMedia('publicities');
        if(isset($media)) $media->forceDelete();
        $publicity->delete();
        return response()->json(['msg' => 'Publicity deleted']);
    }
}
