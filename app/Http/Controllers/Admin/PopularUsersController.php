<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Input;

class PopularUsersController extends Controller
{
    public function index()
    {
        $idUsersScout = \DB::table('statistics')->where('name', 'popularScouts')->value('value');
        $usersScout = User::whereIn('id', explode(',', $idUsersScout))->get();

        $idUsersAthlete = \DB::table('statistics')->where('name', 'popularAthletes')->value('value');
        $usersAthlete = User::whereIn('id', explode(',', $idUsersAthlete))->get();

        // dd($usersAthlete);
        return view('admin.popularusers.index', compact('usersScout', 'usersAthlete'));
    }

    public function store(Request $request)
    {
        if ($request->typeUser == 'Scout') {
            \DB::table('statistics')->where('name', 'popularScouts')
                ->updateOrInsert(
                    ['name' => 'popularScouts'],
                    ['value' => $request->usersId]
                );
            $msg = trans('dashboard.popularScoutsSuccess');
        }
        if ($request->typeUser == 'Athlete') {
            \DB::table('statistics')->where('name', 'popularAthletes')
                ->updateOrInsert(
                    ['name' => 'popularAthletes'],
                    ['value' => $request->usersId]
                );
            $msg = trans('dashboard.popularAthletesSuccess');
        }
        return response()->json(['msg' => $msg]);
    }
}
