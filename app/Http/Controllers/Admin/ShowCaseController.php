<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Athlete;
use App\Models\AthleteEntityPreference;
use App\Models\Entity;
use App\Models\EntityType;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Stripe\Stripe;
use App\Notifications\AdminContactedClubInShowcase as NotificationAdminContactedClubInShowcase;
use Illuminate\Support\Facades\Notification;

class ShowCaseController extends Controller
{

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        $rolesPremiun = ['athlete.premiun', 'athlete.premiun.anual', 'athlete.premiun.semester'];
        $roles = Role::whereIn('slug', $rolesPremiun)->pluck('id');
        $users = User::premiun($roles)->paginate(10);
        return view('admin.showcase.index', compact('users'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('admin.showcase.show', compact('user'));
    }

    public function store(Request $request)
    {
        $alreadyContacted = AthleteEntityPreference::where('athlete_id', $request->athlete_id)->whereNotNull('contacted')->pluck('id')->toArray();
        $showcase = $request->has('showcase') ? array_keys($request->showcase) : array();
        $newsEntitiesPreferencesContacted = array_values(array_diff($showcase,$alreadyContacted));
        
        $this->notifyShowcaseContactedToAthlete($request->athlete_id,$newsEntitiesPreferencesContacted);
        // dd($showcase);
        AthleteEntityPreference::where('athlete_id', $request->athlete_id)
            ->whereIn('id', $showcase)
            ->update(['contacted' => date("Y-m-d H:i:s")]);

        AthleteEntityPreference::where('athlete_id', $request->athlete_id)
            ->whereNotIn('id', $showcase)
            ->update(['contacted' => null]);

        return redirect()->route('admin.showcase.index')->with('success', trans('showcase.admin.storeSuccess'));
    }

    private function notifyShowcaseContactedToAthlete($idAthlete,array $idsNewEntitiesPreferencesContacted)
    {
        $userToNotificate = Athlete::findOrFail($idAthlete)->user; 
        foreach ($idsNewEntitiesPreferencesContacted as $entityPreferenceId) {
            $entityPreference = AthleteEntityPreference::find($entityPreferenceId);
            $entity = Entity::find($entityPreference->entity_id);
            if(isset($entity)) {
                Notification::send($userToNotificate, new NotificationAdminContactedClubInShowcase($entity));
            }
        }
        return true;
    }
}
