<?php

namespace App\Http\Controllers\Admin\Stripe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Stripe;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use Validator;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role as JeremykenedyRole;
use Laravel\Cashier\Subscription;
use Carbon\Carbon;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        try {
            $perPage = 5;
            $users = User::subscribers()->paginate($perPage);
            $susbscriptionsActivesTotal = Subscription::where(function ($q) {
                        $q->where('ends_at', '>', Carbon::now());
                        $q->orWhereNull('ends_at');
                    })
                    ->orWhere(function ($q) {
                        $q->whereNotNull('trial_ends_at');
                        $q->where('trial_ends_at', '>', Carbon::today());
                    })->count();
            // dd($users);
        } catch (\Exception $e) {
            return view('admin.subscriptions.index')->with(['message' => $e->getMessage(), 'status' => 'danger']);
        }
        return view('admin.subscriptions.index', compact('users','susbscriptionsActivesTotal'));
    }

    public function invoices($idUser)
    {
        try {
            $user = User::find($idUser);
            $invoices = $user->invoicesIncludingPending();
        } catch (\Exception $e) {
            return view('admin.subscriptions.index')->with(['message' => $e->getMessage(), 'status' => 'danger']);
        }

        // dd($invoices);
        return view('admin.subscriptions.invoices', compact('user','invoices'));
    }
}
