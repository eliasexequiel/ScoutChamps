<?php

namespace App\Http\Controllers\Admin\Stripe;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Illuminate\Pagination\LengthAwarePaginator;
use Auth;
use Validator;

class CouponController extends Controller
{
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function index()
    {
        try {
            $perPage = 10;
            $couponsAux = \Stripe\Coupon::all(['limit' => $perPage, 'starting_after' => request()->query('after')]);
            // dd($couponsAux);
            $has_more = $couponsAux->has_more; 
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            $coupons = new LengthAwarePaginator($couponsAux->data, count($couponsAux->data), $perPage, $currentPage);

            // dd($coupons);
            return view('admin.coupons.index', compact('coupons','has_more'));
        } catch (\Exception $e) {
            return view('admin.coupons.index')->with(['message' => $e->getMessage(), 'status' => 'danger']);
        }
        // dd("COUPONS");
    }

    public function create()
    {
        $duration = trans('fields.durationDiscounts');
        return view('admin.coupons.create',compact('duration'));
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'code' => 'required',
                'name' => 'required',
                'duration' => 'required',
                'type' => 'required',
                'amount_discount' => 'required'
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            if ($request->type == 'percent_off') {
                $coupon = \Stripe\Coupon::create([
                    'id'          => $request->code,
                    'name'        => $request->name,
                    'percent_off' => $request->amount_discount,
                    'duration'    => $request->duration,
                    'duration_in_months' =>  $request->has('duration_in_months') ? $request->duration_in_months : null
                ]);
            } else if ($request->type == 'amount_off') {
                $coupon = \Stripe\Coupon::create([
                    'id'          => $request->code,
                    'name'        => $request->name,
                    'amount_off'  => $request->amount_discount * 100,
                    'currency'    => config('services.currency'),
                    'duration'    => $request->duration,
                    'duration_in_months' =>  $request->has('duration_in_months') ? $request->duration_in_months : null
                ]);
            }

            return redirect()->route('coupons.index')->with('success', trans('coupons.createSuccess'));
        } catch (\Exception $e) {
            return back()->withErrors(['message' => $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try {
            $coupon = \Stripe\Coupon::retrieve($id);
            $coupon->delete();
            return redirect()->route('coupons.index')->with('success', trans('coupons.deleteSuccess'));
        } catch (\Exception $e) {
            return back()->withErrors(['message' => $e->getMessage()]);
        }
    }
}
