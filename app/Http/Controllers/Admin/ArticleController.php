<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;
use App\Models\Article;
use Validator;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        if ($pagintaionEnabled) {
            $articles = Article::orderByDesc('created_at')->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $articles = Article::orderByDesc('created_at')->get();
        }
        // dd($articles);

        if (request()->query('success') == 'created') return redirect()->route('articles.index')->with('success', trans('articles.createSuccess'));
        if (request()->query('success') == 'updated') return redirect()->route('articles.index')->with('success', trans('articles.updateSuccess'));
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        // $data = $request->only('title', 'resume', 'body');
        $userId = \Auth::user()->id;

        $article = Article::create([
            'title' => [ 'en' => $request->title_en, 'es' => $request->title_es],
            'resume' => [ 'en' => $request->resume_en, 'es' => $request->resume_es],
            'body' => [ 'en' => $request->body_en, 'es' => $request->body_es],
            'user_id' => $userId
        ]);

        // dd($request->all());
        if ($request->has('file')) {
            $image = $request->file;
            // dd($avatar);
            $article->addMedia($image)->toMediaCollection('article');
        }

        if ($request->ajax()) {
            $route = route('articles.index');
            return response()->json(['url' => $route], 200);
        } else {
            return redirect()->route('articles.index')->with('success', trans('articles.createSuccess'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('admin.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);
        // dd($article->getTranslations('resume'));
        // $article->load('translations');
        $data = ['article' => $article];
        return view('admin.articles.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = Article::findOrFail($id)->update([
            'title' => [ 'en' => $request->title_en, 'es' => $request->title_es],
            'resume' => [ 'en' => $request->resume_en, 'es' => $request->resume_es],
            'body' => [ 'en' => $request->body_en, 'es' => $request->body_es]
        ]);
        $article = Article::findOrFail($id);
        // dd($request->all());

        // Si tenia una imagen y la eliminó, eliminamos la imagen
        if ($request->imageDeleted == 'imageDeleted') {
            $article->clearMediaCollection('article');
        }

        if ($request->has('file')) {
            $avatar = $request->file;
            // dd($avatar);
            $article->addMedia($avatar)->toMediaCollection('article');
        }

        if ($request->ajax()) {
            $route = route('articles.index');
            return response()->json(['url' => $route], 200);
        } else {
            return redirect()->route('articles.index')->with('success', trans('articles.updateSuccess'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article= Article::findOrFail($id);
        $article->delete();
        return redirect()->route('articles.index')->with('success', trans('articles.deleteSuccess'));
    }

    public function getArticles()
    {
        if(request()->query('size')) {
            $articles = Article::latest()->paginate(request()->query('size'));
        } else {
            $articles = Article::all();
        }
        return response()->json(['articles' => $articles]);
    }
}
