<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Auth;
use App\Models\Media;
use App\Models\User;
use App\Models\MediaUrl;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Auth\Access\AuthorizationException;
use App\Http\Requests\MediaRequest;
use App\Http\Requests\VideoRequest;
use App\Events\NewUpdate as NewUpdateEvent;

class UserController extends Controller
{
    public function getUser()
    {
        // if(Auth::guest()) return response()->json(['error' => 'Unauthenticated'],401);
        $user = User::with('athlete.entities.achievements','athlete.entitiesPrefered','scout.entities','followed.following','favorites.favorite','recruitmentsRequested')
                    ->find(Auth::id());
        return response()->json(['user' => $user]);
    }

    public function getUsers()
    {
        $rol = Role::where('name', request()->typeUser)->first();
        $users = User::typeUser($rol)
                       ->with('scout.nationality','scout.residenceCountry','athlete.nationality','athlete.residenceCountry','athlete.secondNationality','athlete.sport','athlete.sportSecondary')
                       ->where('id', '!=', Auth::user()->id)
                       ->get();
        return response()->json(['users' => $users]);
    }

    public function getUsersToMessages()
    {
        if(\Auth::user()->hasRole('admin')) {
            $users = User::where('id', '!=', Auth::user()->id)->get();
        }
        else {
            $rol = Role::where('name', 'admin')->first();
            // $usersAdmin  = User::typeUser($rol)->get();
            $user = User::find(Auth::id())->append('usersToMessages');
            $users = $user->usersToMessages;
            // dd($user->usersToMessages);
        }
        return response()->json(['users' => $users]);  
    }

    public function getMediaUser(Request $request)
    {
        $media = Auth::user()->getMedia($request->query('type'));
        $urls = MediaUrl::where('user_id',Auth::id())->where('type',$request->query('type'))->get();
        return response()->json(['images' => $media,'urls' => $urls]);
    }

    public function uploadMedia(MediaRequest $request)
    {
        try {

            $currentUser = \Auth::user();
            if (Input::hasFile('file')) {
                $file = Input::file('file');
                // $filename = 'avatar.' . $file->getClientOriginalExtension();
                $imageCreated = $currentUser->addMedia($file)->toMediaCollection('images');
            } 
            if($request->has('url')) {
                $url = $request->url;
                $imageCreated = $currentUser->addMediaFromUrl($url)->toMediaCollection('images');
            }
            // $currentUser = \Auth::user();
            // $imageCreated = $currentUser->getMedia('images')->last();
            // \Log::info($imageCreated);
            $bodyUpdate = json_encode(['id_image' => $imageCreated->id,'url_image' => $imageCreated->getFullUrl()]);

            event(new NewUpdateEvent($currentUser,$bodyUpdate, 'newPhotos'));

            return response()->json(['msg' => 'Image added'], 200);

        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 404);
            // return response()->json(['msg' => 'Error uploading media'], 404);
        }
    }

    public function uploadVideo(VideoRequest $request)
    {
        try {

            $currentUser = \Auth::user();
            if (Input::hasFile('file')) {
                $file = Input::file('file');
                // $filename = 'avatar.' . $file->getClientOriginalExtension();
                $media = $currentUser->addMedia($file)->toMediaCollection('videos');
                // $videoCreated = $currentUser->getMedia('videos')->last();
                $bodyUpdate = json_encode(['url' => false,'id_video' => $media->id,'url_video' => $media->getFullUrl(),'mime_type' => $media->mime_type]); 
            }

            if($request->has('url')) {
                $media = MediaUrl::create(['user_id' => $currentUser->id,'type' => 'videos', 'url' => $request->url]);
                $media->fresh();
                $bodyUpdate = json_encode(['url' => true,'id_video' => $media->id,'url_video' => $media->url]);
            }

            event(new NewUpdateEvent($currentUser,$bodyUpdate, 'newVideos'));

            return response()->json(['msg' => 'Video added'], 200);
            
        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 404);
        }
    }

    public function deleteMedia($id)
    {
        if(request()->query('isUrl') == "url") {
            $media = MediaUrl::findOrFail($id);
            if(\Auth::user()->id != $media->user_id) new AuthorizationException("You are not owner this url media");
            $media->delete();

        } else {
            $media = Media::findOrFail($id);
            $media->forceDelete();
        }
        return response()->json(['msg' => 'Media file deleted']);
    }

}
