<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Models\User;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use App\Models\Participant;
use App\Models\Thread;
use App\Events\MessagePushed;
use App\Events\NewThread;
use App\Notifications\NewThread as NotificationsNewThread;
use Illuminate\Support\Facades\Notification;
use Validator;

class MessagesController extends Controller
{
    public function index()
    {
        return view('messages.index');
    }

    public function getThreads()
    {
        $threads = Thread::forUser(Auth::id())
            ->with('messages.user', 'participants.user')
            ->orderByDesc('updated_at')
            ->get();
        $threads->map(function ($t) {
            $t['cant'] = $t->userUnreadMessagesCount(Auth::id());
            // $t['participant'] = $t->participants()->with('user.athlete','user.scout')->where('user_id','!=', Auth::id())->firstOrFail();
            return $t;
        });
        $threads = $threads->filter(function($t)  {
            $usersBlockedOrDeleted = $t->users->filter(function($u) { return $u->deleted_at != null || $u->blocked; });
            return count($usersBlockedOrDeleted) == 0;
        });
        // $threads->load('userUnreadMessagesCount');
        return response()->json(['threads' => $threads]);
    }

    public function storeThread(Request $request)
    {
        $validator = Validator::make($request->all(), ['to' => 'required', 'text' => 'required']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 402);
        }

        $userTo = User::findOrFail($request->to);
        $this->updateStatistics(\Auth::user(), $userTo);
        $this->notificateToAdmin(\Auth::user(), $userTo);

        $subject = 'Conversation user: ' . Auth::user()->fullName . ' and ' . $userTo->fullName;
        if(Auth::user()->hasRole('admin')) {
            $subject = 'Conversation between ADMIN and user: ' . $userTo->id;
        }
        $thread = Thread::create([
            'subject' => $subject
        ]);
        // Message
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'body' => $request->text,
        ]);

        // Sender
        Participant::create([
            'thread_id' => $thread->id,
            'user_id' => Auth::id(),
            'last_read' => new Carbon,
        ]);

        // Recipients
        $thread->addParticipant($userTo->id);

        $thread = $thread->fresh();
        $thread->load('messages.user', 'participants.user');
        broadcast(new NewThread($thread))->toOthers();
        return response()->json(['thread' => $thread], 200);
    }

    public function sendMessage(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), ['text'  => 'required']);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 402);
            }
            $thread = Thread::findOrFail($id);
            $thread->activateAllParticipants();
            // Message
            Message::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
                'body' => $request->text
            ]);
            // Add replier as a participant
            $participant = Participant::firstOrCreate([
                'thread_id' => $thread->id,
                'user_id' => Auth::id(),
            ]);
            $participant->last_read = new Carbon;
            $participant->save();

            $thread = $thread->fresh();
            $thread->load('messages.user', 'participants.user');
            broadcast(new MessagePushed($thread))->toOthers();
            return response()->json(['thread' => $thread], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error_message' => 'The thread with ID: ' . $id . ' was not found.'], 402);
        }
    }

    public function deleteThread($id)
    {
        $thread = Thread::findOrFail($id);
        $thread->delete();

        return response()->json(['msg' => 'Conversation Archived']);
    }

    public function markAsReadToCurrentUser($id)
    {
        $thread = Thread::findOrFail($id);
        $thread->markAsRead(Auth::id());
        return response()->json(['msg' => 'All messages read to current user.']);
    }

    protected function updateStatistics($userFrom, $userTo)
    {
        if ($userFrom->hasRole('scout') && $userTo->hasRole('athlete')) {
            $fieldToUpdate = 'conversationsScoutsAthletes';
        }
        if ($userFrom->hasRole('athlete') && $userTo->hasRole('scout')) {
            $fieldToUpdate = 'conversationsAthletesScouts';
        }
        if ($userFrom->hasRole('athlete') && $userTo->hasRole('athlete')) {
            $fieldToUpdate = 'conversationsAthletes';
        }
        
        if(isset($fieldToUpdate)) {
            $contacts = \DB::table('statistics')->updateOrInsert(['name' => $fieldToUpdate],[]);
            $contacts = \DB::table('statistics')->where('name', $fieldToUpdate)->first();
            $newValue = $contacts->value ? ($contacts->value + 1) : 1;
            \DB::table('statistics')->where('name', $fieldToUpdate)->update(['value' => $newValue]);
        }
        return true;
    }

    public function notificateToAdmin($userFrom,$userTo)
    {
        if ($userFrom->hasRole('scout') && $userTo->hasRole('athlete')) {
            $adminRol = Role::where('slug', 'admin')->first();
            $usersAdmin = User::typeUser($adminRol)->get();
            Notification::send($usersAdmin, new NotificationsNewThread($userFrom, $userTo));
        }
        return true;
    }

    public function indexMessagesAdmin()
    {
        $subject = 'Conversation between ADMIN and user: ' . Auth::user()->id;

        $thread = Thread::with('messages.user', 'participants.user')->where('subject', $subject)->first();
        // Si todavia no se creo la conversación con el Admin, la creamos para despues poder enviar mensajes.
        if (!isset($thread)) {
            $thread = Thread::create([
                'subject' => $subject
            ]);

            // Sender
            Participant::create([
                'thread_id' => $thread->id,
                'user_id' => Auth::user()->id,
                'last_read' => new Carbon,
            ]);

            $this->addUsersAdminToThread($thread->id);

            $thread = $thread->fresh();
            $thread->load('messages.user', 'participants.user');
            broadcast(new NewThread($thread))->toOthers();
        }

        return view('messages.contactAdmin', compact('thread'));
    }

    public function addUsersAdminToThread($id)
    {
        $adminRol = Role::where('slug', 'admin')->first();
        $usersAdmin = User::typeUser($adminRol)->get();
        foreach ($usersAdmin as $user) {
            $participant = Participant::firstOrCreate([
                'thread_id' => $id,
                'user_id' => $user->id,
            ]);
        }
        return true;
    }
}
