<?php

namespace App\Http\Controllers;

use App\Events\NewNotification;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\User;
use App\Models\Sport;
use App\Models\UserVisited;
use App\Models\Follower;
use App\Models\Favorite;
use App\Events\UserVisited as UserVisitedEvent;
use App\Events\NewUpdate as NewUpdateEvent;
use App\Events\NewSearch as NewSearchEvent;
use jeremykenedy\LaravelRoles\Models\Role;
use Carbon\Carbon;
use App\Repositories\Interfaces\RecruitmentRepositoryInterface;
use Illuminate\Support\Facades\Notification;
use App\Notifications\FavoriteUser;
use App\Notifications\FollowUser;

class PublicController extends Controller
{

    protected $recruitmentRepository;

    public function __construct(RecruitmentRepositoryInterface $recruitmentRepository)
    {
        $this->recruitmentRepository = $recruitmentRepository;
    }

    public function showTermsAndConditions()
    {
        if (app()->getLocale() == 'en') $terms = \DB::table('config_site')->where('name', 'terms')->value('value');
        else $terms = \DB::table('config_site')->where('name', 'terms_es')->value('value');
        return view('public.conditions')->with(['terms' => $terms]);
    }

    public function showPrivacyPolicy()
    {
        if (app()->getLocale() == 'en') $privacy = \DB::table('config_site')->where('name', 'privacy')->value('value');
        else $privacy = \DB::table('config_site')->where('name', 'privacy_es')->value('value');
        return view('public.privacy')->with(['privacy' => $privacy]);
    }

    public function show($uuid)
    {
        // try {
            $params = explode('_', $uuid);
            if(count($params) == 2) $uuidParsed = $params[1];
            else $uuidParsed = $params[0];
            $user = User::uuid($uuidParsed)->firstOrFail();
            $user->load('athlete.fieldsSports.sportfield', 'scout.entities', 'posts', 'athlete.entities.achievements', 'mediaurls', 'media');
            
            if($user->hasRole('admin')) return redirect()->route('home');

            $userCurrent = \Auth::user();

            // Si ya tiene una conversación iniciada con el usuario lo dejamos enviarle mensajes.
            $hasConversationInited = $userCurrent->threads->filter(function($thread) use($user) { return $thread->hasParticipant($user->id); })->count() > 0;
            
            $usersFollowed = $userCurrent->followed->pluck('following_user_id')->all();
            $followed = in_array($user->id, $usersFollowed);

            $usersFavorites = $userCurrent->favorites->pluck('favorite_user_id')->all();
            $isFavorite = in_array($user->id, $usersFavorites);

            $recruitments = $this->recruitmentRepository->getByDates(Carbon::now()->format('Y-m-d'));
            $userRecruitments = $user->recruitmentsInvited->pluck('recruitment_id')->toArray();

            $recruitments = $recruitments->filter(function ($r) use ($userRecruitments) {
                return !in_array($r->id, $userRecruitments);
            });

            $followersUsers = $user->followers_users();
            $followingUsers = $user->followed_users();
            $visitedScout = $user->visited_scout();
            $visits = $user->visits();

            if (!$userCurrent->hasRole('admin') && $userCurrent->id != $user->id) {
                event(new UserVisitedEvent($userCurrent, $user));
            }
            return view('public.users.show', compact('user', 'followed','followersUsers','followingUsers','visitedScout','visits', 'isFavorite', 'recruitments','hasConversationInited'));
        // } catch (ModelNotFoundException $exception) {
            // return view('pages.status')->with('error', trans('profile.notYourProfile'));
        // }
    }

    public function follow($uuid)
    {
        // dd($request->all());
        $user = \Auth::user();
        $userFollowed = User::uuid($uuid)->firstOrFail();
        Follower::firstOrCreate(['user_id' => $user->id, 'following_user_id' => $userFollowed->id]);

        // Si un SCOUT sigue a un ATLETA lo añadimos a la estadistica
        if ($user->hasRole('scout') && $userFollowed->hasRole('athlete')) {
            $contacts = \DB::table('statistics')->updateOrInsert(['name' => 'conversationsScoutsAthletes'], []);
            $contacts = \DB::table('statistics')->where('name', 'conversationsScoutsAthletes')->first();
            $newValue = $contacts->value ? ($contacts->value + 1) : 1;
            \DB::table('statistics')->where('name', 'conversationsScoutsAthletes')->update(['value' => $newValue]);
        }

        $body = json_encode(['uuid_user_followed' => $userFollowed->uuid, 'username_user_followed' => $userFollowed->fullName]);
        event(new NewUpdateEvent($user, $body, 'followed'));

        $userFollowed->notify(new FollowUser($user,$userFollowed));
        $this->notificateToAdminNewFollow($user,$userFollowed);
        event(new NewNotification($userFollowed));

        return redirect()->back()->with('openModalRecruitment', true);
    }

    public function notificateToAdminNewFollow($user,$userFollowed)
    {
        if ($user->hasRole('scout') && $userFollowed->hasRole('athlete')) {
            $adminRol = Role::where('slug', 'admin')->first();
            $usersAdmin = User::typeUser($adminRol)->get();
            Notification::send($usersAdmin, new FollowUser($user,$userFollowed));
        }
        return true;
    }

    public function unfollow($uuid)
    {
        $user = \Auth::user();
        $userFollowed = User::uuid($uuid)->firstOrFail();
        $f = Follower::where([['user_id', $user->id], ['following_user_id', $userFollowed->id]])->first();
        // dd($user->id . $userFollowed->id);
        if(isset($f)) $f->delete();

        return redirect()->back();
        // return redirect()->route('public.profile.show', ['uuid' => $uuid]);
    }

    public function addFavorite($uuid)
    {
        $user = \Auth::user();
        $userFav = User::uuid($uuid)->firstOrFail();
        Favorite::firstOrCreate(['user_id' => $user->id, 'favorite_user_id' => $userFav->id]);

        $userFav->notify(new FavoriteUser($user));
        event(new NewNotification($userFav));

        return redirect()->back()->with('openModalRecruitment', true);;
        // return redirect()->route('public.profile.show', ['uuid' => $uuid]);
    }

    public function removeFavorite($uuid)
    {
        $user = \Auth::user();
        $userFav = User::uuid($uuid)->firstOrFail();
        $f = Favorite::where([['user_id', $user->id], ['favorite_user_id', $userFav->id]])->first();
        if(isset($f)) $f->delete();

        return redirect()->back();
        // return redirect()->route('public.profile.show', ['uuid' => $uuid]);
    }

    public function searchUser(Request $request)
    {
        // dd($request->all());
        $pagesize = 10;
        $filtersSport = $request->only(preg_grep('/sport_/', array_keys($request->query())));
        $filtersSport = array_filter($filtersSport, function ($e) {
            return isset($e);
        });
        // $users = User::notAdmin()->sports($filtersSport)->get();
        // dd($users);
        $idRolesPremiun = Role::whereIn('slug', ['athlete.premiun', 'athlete.premiun.semester', 'athlete.premiun.anual'])->pluck('id');
        // dd($idRolesPremiun);

        $currentuser = \Auth::user();

        // $rol = Role::where('name', $request->typeUser)->first();
        // dd($rol);
        // Premiun Users son mostrados primero
        $usersPremiun = User::notAdmin()
            ->notBlocked()
            // ->typeUser($rol)
            ->sports($filtersSport)
            // ->where('id', '!=', $currentuser->id)
            ->filter($request->all())
            ->premiun($idRolesPremiun)
            ->get();

        $users = User::notAdmin()
            ->notBlocked()
            // ->typeUser($rol)
            ->sports($filtersSport)
            // ->where('id', '!=', $currentuser->id)
            ->filter($request->all())
            ->get();

        $mergeUsers = $usersPremiun->merge($users);

        $paginateUsers = $mergeUsers->paginate($pagesize);

        // dd($users);
        $data = ['users' => $paginateUsers];

        event(new NewSearchEvent($currentuser, $request->all()));

        return view('public.results')->with($data);
    }
}
