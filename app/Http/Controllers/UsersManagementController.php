<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\CaptureIpTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use jeremykenedy\LaravelRoles\Models\Role;
use Validator;
use jeremykenedy\Uuid\Uuid;
use Illuminate\Support\Facades\Hash;
use App\Models\Scout;
use App\Models\Athlete;
use Carbon\Carbon;

class UsersManagementController extends Controller
{
    protected $idMultiKey = '618423'; //int
    protected $seperationKey = '****';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentuser = \Auth::user();
        
        $pagintaionEnabled = config('usersmanagement.enablePagination');
        $rol = Role::where('name', $request->typeUser)->first();
        if ($pagintaionEnabled) {
            $users = User::with('roles')
                ->where('id', '!=', $currentuser->id)
                ->notBlocked()
                ->typeUser($rol)
                ->filter(['fullname' => $request->fullnameFilter, 'lastSession' => $request->lastSession,'created' => $request->createdAt])
                ->paginate(config('usersmanagement.paginateListSize'));
        } else {
            $users = User::with('roles')
                ->where('id', '!=', $currentuser->id)
                ->notBlocked()
                ->typeUser($rol)
                ->filter(['fullname' => $request->fullnameFilter, 'lastSession' => $request->lastSession])
                ->get();
        }
        $perfiles = ['Admin', 'Scout', 'Athlete'];
        $roles = Role::whereIn('name', $perfiles)->get();

        return View('usersmanagement.show-users', compact('users', 'roles'));
    }

    public function indexBlockedUsers()
    {
        $users = User::onlyBlocked()->get();
        $roles = Role::all();

        return View('usersmanagement.show-blocked-users', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perfiles = ['Admin', 'Scout', 'Athlete'];
        $roles = Role::whereIn('name', $perfiles)->get();

        $data = [
            'roles' => $roles,
        ];

        return view('usersmanagement.create-user')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make(
            $request->all(),
            [
                'firstname'            => 'required',
                'lastname'             => 'required',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'role'                  => 'required',
            ],
            [
                'firstname.required' => trans('auth.fNameRequired'),
                'lastname.required'  => trans('auth.lNameRequired'),
                'email.required'      => trans('auth.emailRequired'),
                'email.email'         => trans('auth.emailInvalid'),
                'password.required'   => trans('auth.passwordRequired'),
                'password.min'        => trans('auth.PasswordMin'),
                'password.max'        => trans('auth.PasswordMax'),
                'role.required'       => trans('auth.roleRequired'),
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user = User::create([
            'firstname'         => $request->input('firstname'),
            'lastname'          => $request->input('lastname'),
            'email'             => $request->input('email'),
            'password'          => Hash::make($request->password),
            'uuid'              => Uuid::generate(4),
            'token'             => str_random(64),
            'activated'         => true,
            'verified'          => true,
            'email_verified_at' => new Carbon

        ]);

        $user->attachRole($request->role);
        $user->save();

        $role = Role::find($request->role);
        if ($role->slug == 'scout')   Scout::create(['user_id' => $user->id]);
        if ($role->slug == 'athlete') Athlete::create(['user_id' => $user->id]);


        return redirect('users')->with('success', trans('usersmanagement.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('usersmanagement.show-user')->withUser($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $perfiles = ['Admin', 'Scout', 'Athlete'];
        $roles = Role::whereIn('name', $perfiles)->get();

        // foreach ($user->roles as $user_role) {
        //     $currentRole = $user_role;
        // }
        $data = ['user' => $user, 'roles' => $roles];
        return view('usersmanagement.edit-user')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $currentUser = Auth::user();
        $user = User::find($id);
        $emailCheck = ($request->input('email') != '') && ($request->input('email') != $user->email);
        // $ipAddress = new CaptureIpTrait();


        if ($emailCheck) {
            $validator = Validator::make($request->all(), [
                'firstname'            => 'required',
                'lastname'             => 'required',
                'email'                => 'email|max:255|unique:users',
                'password'             => 'present|nullable|confirmed|min:6',
            ]);
        } else  {
            $validator = Validator::make($request->all(), [
                'firstname'            => 'required',
                'lastname'             => 'required',
                'password'             => 'present|nullable|confirmed|min:6'
            ]);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');

        if ($emailCheck) {
            $user->email = $request->input('email');
        }

        if ($request->input('password') != null) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();

        return back()->with('success', trans('usersmanagement.updateSuccess'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::user();
        $user = User::findOrFail($id);
        $ipAddress = new CaptureIpTrait();

        if ($user->id != $currentUser->id) {
            // $user->deleted_ip_address = $ipAddress->getClientIp();
            // Cambiamos el mail antes de eliminarlo para que se pueda registrar nuevamente con el MISMO MAIL.
            $user->email = $user->email . '_deleted' . time();
            $user->save();
            $user->delete();


            return redirect('blockedusers')->with('success', trans('usersmanagement.deleteSuccess'));
        }

        return back()->with('error', trans('usersmanagement.deleteSelfError'));
    }

    public function verify($id)
    {
        $user = User::findOrFail($id);
        $user->verified = true;
        $user->save();

        return redirect('users')->with('success', trans('usersmanagement.verifiedSuccess'));
    }

    public function unverify($id)
    {
        $user = User::findOrFail($id);
        $user->verified = false;
        $user->save();

        return redirect('users')->with('success', trans('usersmanagement.unverifiedSuccess'));
    }

    public function block($id)
    {
        $currentUser = Auth::user();
        $user = User::findOrFail($id);
        if ($user->id != $currentUser->id) {
            $user->blocked = true;
            $user->save();
        }

        return redirect('users')->with('success', trans('usersmanagement.blockedSuccess'));
    }

    public function unblock($id)
    {
        $user = User::findOrFail($id);
        $user->blocked = false;
        $user->save();

        return redirect('users')->with('success', trans('usersmanagement.unblockedSuccess'));
    }

    /**
     * Get User Restore ID Multiplication Key.
     *
     * @return string
     */
    public function getIdMultiKey()
    {
        return $this->idMultiKey;
    }

    /**
     * Get User Restore Seperation Key.
     *
     * @return string
     */
    public function getSeperationKey()
    {
        return $this->seperationKey;
    }


    /**
     * Get data User from Ajax.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser($id)
    {
        // if(Auth::guest()) return response()->json(['error' => 'Unauthenticated'],401);
        $user = User::with('athlete.entities.achievements','athlete.entitiesPrefered','scout.entities')->find($id);
        return response()->json(['user' => $user]);
    }

    /**
     * Show the form for editing clubs of Athlete/Scout.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function editClubs($id)
    {
        $user = User::findOrFail($id);
        $data = ['user' => $user];
        return view('usersmanagement.edit-user-clubs')->with($data);
    }
}
