<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Report;
use App\Models\Post;
use App\Models\PostComment;
use App\Models\Media;
use App\Models\MediaUrl;
use App\Models\Message;
use jeremykenedy\LaravelRoles\Models\Role;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewReportCreated;
use Validator;
use Auth;

class ReportController extends Controller
{
    public function report(Request $request)
    {
        $userId = Auth::user()->id;
        $message = isset($request->message) ? $request->message : '';

        $report = null;

        switch ($request->type) {
            case 'post':
                $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\Post', 'reported_id' => $request->id], ['message' => $message]);
                $post = Post::find($request->id);
                $post->report()->save($report);
                break;
            case 'commentPost':
                $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\PostComment', 'reported_id' => $request->id], ['message' => $message]);
                $comment = PostComment::find($request->id);
                $comment->report()->save($report);
                break;
            case 'image':
                $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\Media', 'reported_id' => $request->id], ['message' => $message]);
                $image = Media::find($request->id);
                $image->report()->save($report);
                break;
            case 'video':
                $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\Media', 'reported_id' => $request->id], ['message' => $message]);
                $video = Media::find($request->id);
                $video->report()->save($report);
                break;
            case 'videoUrl':
                $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\MediaUrl', 'reported_id' => $request->id], ['message' => $message]);
                $video = MediaUrl::find($request->id);
                $video->report()->save($report);
                break;
            case 'messageChat':
                    $report = Report::firstOrCreate(['user_id' => $userId, 'reported_type' => 'App\Models\Message', 'reported_id' => $request->id], ['message' => $message]);
                    $message = Message::find($request->id);
                    $message->report()->save($report);
                    break;
            default:
                break;
        }

        $this->notifyAdminNewReport(Auth::user(), $report);

        return response()->json(['msg' => trans('reports.reportSuccess', ['type' => trans('reports.types.' . $report->tipo)])]);
    }

    protected function notifyAdminNewReport(User $user, Report $report)
    {
        $adminRol = Role::where('slug', 'admin')->first();
        $usersAdmin = User::typeUser($adminRol)->get();
        Notification::send($usersAdmin, new NewReportCreated($user, $report));
        return true;
    }
}
