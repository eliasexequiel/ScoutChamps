<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPasswordRequest;
use App\Http\Requests\UpdateUserProfile;
use App\Models\User;
use App\Models\Athlete;
use App\Models\Scout;
use App\Models\Sport;
use App\Models\Country;
use App\Models\SportFieldsAthlete;
use App\Models\ScoutPosition;
use App\Notifications\SendGoodbyeEmail;
use App\Traits\CaptureIpTrait;
use File;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Spatie\MediaLibrary\Models\Media;
use Image;
use jeremykenedy\Uuid\Uuid;
use Validator;
use View;
use App\Events\NewUpdate as NewUpdateEvent;

class ProfilesController extends Controller
{
    protected $idMultiKey = '618423'; //int
    protected $seperationKey = '****';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        try {
            // dd(request()->query('changeTabProfile'));
            $user = \Auth::user();
            $countries = Country::get()->sortBy('name')->pluck('name','id');
            $data = [
                'user' => $user,
                'sports' => Sport::pluck('name', 'id'),
                'countries' => $countries,
                'status' => trans('fields.status'),
                'sportingGoal' => trans('fields.sportingGoal'),
                'genders' => trans('fields.genders')
            ];

            if(request()->query('changeTabProfile')) request()->session()->flash('changeTabProfile',request()->query('changeTabProfile'));
            // dd($data['status']);
            // dd($data);
            if ($user->hasRole('admin')) {
                return view('profiles.admin')->with($data);
            } else if ($user->hasRole('athlete')) {
                return view('profiles.athlete')->with($data);
            } else if ($user->hasRole('scout')) {
                $data['cargosScout'] = ScoutPosition::pluck('name','id');
                return view('profiles.scout')->with($data);
            }

            throw new ModelNotFoundException();
        } catch (ModelNotFoundException $exception) {
            return view('pages.status')
                ->with('error', trans('profile.notYourProfile'))
                ->with('error_title', trans('profile.notYourProfileTitle'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param string $username
     *
     * @return Response
     */
    public function show($uuid)
    {
        try {
            $user = User::uuid($uuid)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }

        $data = ['user' => $user];

        return view('profiles.show')->with($data);
    }

    /**
     * /profiles/username/edit.
     *
     * @param $username
     *
     * @return mixed
     */
    public function edit($uuid)
    {
        try {
            $user = User::uuid($uuid)->firstOrFail();
            $data = ['user' => $user];
            // dd($data);
            if ($user->hasRole('athlete')) {
                return view('profiles.athlete')->with($data);
            } else if ($user->hasRole('scout')) {
                return view('profiles.scout')->with($data);
            }
            throw new ModelNotFoundException();
        } catch (ModelNotFoundException $exception) {
            return view('pages.status')
                ->with('error', trans('profile.notYourProfile'))
                ->with('error_title', trans('profile.notYourProfileTitle'));
        }
    }

    /**
     * Update a user's profile.
     *
     * @param \App\Http\Requests\UpdateUserProfile $request
     * @param $username
     *
     * @throws Laracasts\Validation\FormValidationException
     *
     * @return mixed
     */
    public function update(UpdateUserProfile $request, $id)
    {
        // dd($request->all());
        $user = \Auth::user();

        if ($user->hasRole('athlete')) {
            $input = Input::only('nationality_id', 'second_nationality_id', 'residence_country_id', 'residence_city', 'phone','date_of_birth', 'gender', 'sporting_goal','height_unit', 'height', 'status');
            if($request->has('firstnameTutor') && $request->has('lastnameTutor') && $request->has('emailTutor') && $request->has('phoneTutor')) {
                $input = array_merge($input,Input::only('firstnameTutor','lastnameTutor','emailTutor','phoneTutor'));    
            }
            Athlete::findOrFail($user->profile->id)->update($input);
        }

        if ($user->hasRole('scout')) {
            $input = Input::only('nationality_id', 'residence_country_id','phone','scout_position_id','scout_position_secondary_id');
            Scout::findOrFail($user->profile->id)->update($input);
        }

        $emailCheck = ($request->email != '') && ($request->email != $user->email);

        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->about = $request->about;

        if ($emailCheck) {
            $user->email = $request->email;
        }
        
        $user->save();

        return redirect('profile')->with(['success' => trans('profile.updateSuccess'),'changeTabProfile' => '1']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateUserPasswordRequest $request
     * @param int                                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserPassword(UpdateUserPasswordRequest $request, $id)
    {
        $user = \Auth::user();
        // $user = User::findOrFail($id);
        // $ipAddress = new CaptureIpTrait();

        if ($request->input('password') != null) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->save();
        // $user->updated_ip_address = $ipAddress->getClientIp();
        return redirect('profile')->with(['success' => trans('profile.updatePWSuccess'),'changeTabProfile' => '2']);
    }

    /**
     * Upload and Update user avatar.
     *
     * @param $file
     *
     * @return mixed
     */
    public function upload()
    {
        if (Input::hasFile('file')) {
            $currentUser = \Auth::user();
            $avatar = Input::file('file');
            $filename = 'avatar.' . $avatar->getClientOriginalExtension();
            // $save_path = storage_path() . '/users/id/' . $currentUser->id . '/uploads/images/avatar/';
            // $path = $save_path . $filename;
            // $public_path = '/profile/' . $currentUser->id . '/avatar/' . $filename;

            // Make the user a folder and set permissions
            // File::makeDirectory($save_path, $mode = 0755, true, true);

            // Save the file to the server
            // Image::make($avatar)->resize(300, 300)->save($save_path . $filename);

            // Si ya tiene una imagen de perfil la eliminamos.
            $imageProfile = $currentUser->getFirstMedia('profile');

            $currentUser->addMedia($avatar)->toMediaCollection('profile');

            if (isset($imageProfile)) {
                $imageProfile->forceDelete();
            }

            event(new NewUpdateEvent($currentUser,'', 'updatePhotoProfile'));

            $currentUser->refresh();

            return response()->json(['path' => $currentUser->avatar], 200);
        } else {
            return response()->json(false, 200);
        }
    }

    // Remove User Avatar
    public function deleteAvatar(Request $request) 
    {
        $currentUser = \Auth::user();
        $imageProfile = $currentUser->getFirstMedia('profile');
        if (isset($imageProfile)) {
            $imageProfile->forceDelete();
        }
        return response()->json(['path' => asset('img/profile-d.png')], 200);
    }
    /**
     * Show user avatar.
     *
     * @param $id
     * @param $image
     *
     * @return string
     */
    public function userProfileAvatar($id, $image)
    {
        return Image::make(storage_path() . '/users/id/' . $id . '/uploads/images/avatar/' . $image)->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\DeleteUserAccount $request
     * @param int                                  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteUserAccount(Request $request, $id)
    {
        $currentUser = \Auth::user();
        $user = User::findOrFail($id);
        // $ipAddress = new CaptureIpTrait();

        if ($user->id != $currentUser->id) {
            return redirect('/profile')->with('error', trans('profile.errorDeleteNotYour'));
        }

        $user->email = $user->email . '_deleted' . time();

        // Create and encrypt user account restore token
        $sepKey = $this->getSeperationKey();
        $userIdKey = $this->getIdMultiKey();
        $restoreKey = config('settings.restoreKey');
        $encrypter = config('settings.restoreUserEncType');
        $level1 = $user->id * $userIdKey;
        $level2 = urlencode(Uuid::generate(4) . $sepKey . $level1);
        $level3 = base64_encode($level2);
        $level4 = openssl_encrypt($level3, $encrypter, $restoreKey);
        $level5 = base64_encode($level4);

        // Save Restore Token and Ip Address
        $user->token = $level5;
        // $user->deleted_ip_address = $ipAddress->getClientIp();
        $user->save();

        // Send Goodbye email notification
        // $this->sendGoodbyEmail($user, $user->token);

        // Soft Delete User
        $user->delete();

        // Clear out the session
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/login')->with('success', trans('profile.successUserAccountDeleted'));
    }

    /**
     * Send GoodBye Email Function via Notify.
     *
     * @param array  $user
     * @param string $token
     *
     * @return void
     */
    public static function sendGoodbyEmail(User $user, $token)
    {
        $user->notify(new SendGoodbyeEmail($token));
    }

    /**
     * Get User Restore ID Multiplication Key.
     *
     * @return string
     */
    public function getIdMultiKey()
    {
        return $this->idMultiKey;
    }

    /**
     * Get User Restore Seperation Key.
     *
     * @return string
     */
    public function getSeperationKey()
    {
        return $this->seperationKey;
    }
}
