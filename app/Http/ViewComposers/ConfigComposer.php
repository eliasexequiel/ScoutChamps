<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class ConfigComposer
{

    // protected $usersport;

    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        // $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $verifyScoutsClubs = \DB::table('config_site')->where('name', 'verifyScoutsClubs')->first();
        $verifyClubs = \DB::table('config_site')->where('name', 'verifyClubs')->first();
        $contactEmail = \DB::table('config_site')->where('name', 'contactEmail')->value('value');
        $contactPhone = \DB::table('config_site')->where('name', 'contactPhone')->value('value');
        $data = [
            'verifyScoutsClubs'  => $verifyScoutsClubs,
            'verifyClubs'  => $verifyClubs,
            'contactEmail' => $contactEmail,
            'contactPhone' => $contactPhone
        ];
        $view->with($data);
    }
}
