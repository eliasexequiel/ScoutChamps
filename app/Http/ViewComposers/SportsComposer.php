<?php

namespace App\Http\ViewComposers;

use App\Models\Athlete;
use Illuminate\View\View;
use App\Models\Sport;
use App\Models\Country;
use App\Models\Entity;
use App\Models\Scout;
use App\Models\ScoutPosition;
use Illuminate\Support\Facades\Cache;

class SportsComposer
{

    // protected $usersport;

    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        // $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $sports = Sport::with('fields.options_select')->orderBy('name')->get();
        $sports = $sports->map(function($item) {
            if($item->name == 'All Sports') $item['nameTranslated'] = 'AAAA';
            else $item['nameTranslated'] = trans('sportfields.titles.'. $item->name);
            return $item;
        })->sortBy('nameTranslated');
        // $sports = array_values($sports->toArray());
        
        $countries = Country::get()->sortBy('name')->pluck('name','id')->toArray();
        
        $athletesCount = Athlete::has('user')->count();
        $scoutsCount = Scout::has('user')->count();
        $conversationsScoutsAthletes = \DB::table('statistics')->where('name', 'conversationsScoutsAthletes')->value('value');
        
        $entities = Entity::pluck('name','id')->toArray();
        // $countries = Cache::get('countries');

        $send_registered = null;
        if (request()->session()->has('send_registered')) { 
            $send_registered = request()->session()->get('send_registered');
        };

        $data = [
            'specialitiesScouts'          => ScoutPosition::get()->sortBy('name')->pluck('name','id'),
            'entitiesAll'                 => $entities,
            'sportsAll'                   => $sports,
            'countriesAll'                => $countries,
            'sportingGoal'                => trans('fields.sportingGoal'),
            'genders'                     => trans('fields.genders'),
            'statusAthlete'               => trans('fields.status'),
            'athletesCount'               => $athletesCount,
            'scoutsCount'                 => $scoutsCount,
            'conversationsScoutsAthletes' => $conversationsScoutsAthletes,
            'send_registered'             => $send_registered
        ];
        $view->with($data);
    }
}