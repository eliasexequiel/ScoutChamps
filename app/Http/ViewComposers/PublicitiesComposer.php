<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Media;
use App\Models\Publicity;

class PublicitiesComposer
{

    // protected $usersport;

    public function __construct()
    {
        // Dependencies automatically resolved by service container...
        // $this->users = $users;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $publicityAbove = Publicity::where('position', 'above')->has('media')->first();
        $publicitiesBelow = Publicity::where('position', 'below')->has('media')->get();
        $data = [
            'publicityAbove' => $publicityAbove,
            'publicitiesBelow' => $publicitiesBelow
        ];
        $view->with($data);
    }
}
