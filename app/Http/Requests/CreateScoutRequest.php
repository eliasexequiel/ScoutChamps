<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateScoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Expression to validate corporative mail
        // 'regex:/^((?!' . config('mail.banned_email_providers') . ').)*$/'
        $rules =  [

            'firstname'             => 'required|max:255',
            'lastname'              => 'required|max:255',
            'email'                 => ['required','email','max:255','unique:users'],
            // 'entity_id'             => 'required',
            'phone'                 => ['required','regex:/^[+]?\(?[0-9]*\)?[0-9]*$/'],
            'otherClub'             => 'required_if:entity_id,otherClub',
            'entity_type_id'        => 'required_if:entity_id,otherClub',
            'password'              => 'required|min:6|max:30|confirmed',
            'password_confirmation' => 'required|same:password',
            'tyc'                   => 'required'
        ];
        return $rules;
    }

    public function messages() {
        return [
            'otherClub.required_if' => trans('validation.nameClub'),
            'entity_type_id.required_if' => trans('validation.entityTypeId'),
            // 'email.regex'           => trans('validation.mailCorporative'),
            'tyc.required'              => trans('auth.tycRequired'),
            'phone.regex'               => trans('validation.athlete.phoneRegex')
        ];
    }
}
