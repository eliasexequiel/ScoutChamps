<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AthleteEntityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clubs.*.entity_id' => 'bail|required',
            'clubs.*.start' => 'bail|nullable|date_format:d/m/Y',
            'clubs.*.end' => 'bail|nullable|date_format:d/m/Y|after:clubs.*.start',
            'clubs.*.info_aditional' => 'nullable|max:12000', 
            'clubs.*.achievements.*.name' => 'bail|required'
        ];
    }

    public function messages() {
        return [
            'clubs.*.entity_id.required' => trans('profile.errors.clubRequired'),
            'clubs.*.achievements.*.name.required' => trans('profile.errors.achievementNameRequired'),
            'clubs.*.start.date_format'  => trans('profile.errors.clubStartDateFormat'),
            'clubs.*.end.date_format'    => trans('profile.errors.clubEndDateFormat'),
            'clubs.*.end.after'          => trans('profile.errors.clubEndGreaterThanStartDate'),
            'clubs.*.info_aditional.max' => trans('profile.errors.clubInfoAditionalMaxLength',['max' => 12000])
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $errors = array();

            if ($validator->errors()->has('clubs.*.entity_id')) {
                array_push($errors, $validator->errors()->first('clubs.*.entity_id')); 
            }

            if ($validator->errors()->has('clubs.*.achievements.*.name')) {
                array_push($errors, $validator->errors()->first('clubs.*.achievements.*.name')); 
            }

            if ($validator->errors()->has('clubs.*.start')) {
                array_push($errors, $validator->errors()->first('clubs.*.start')); 
            }

            if ($validator->errors()->has('clubs.*.end')) {
                array_push($errors, $validator->errors()->first('clubs.*.end')); 
            }

            if ($validator->errors()->has('clubs.*.info_aditional')) {
                array_push($errors, $validator->errors()->first('clubs.*.info_aditional')); 
            }

            if(count($errors) > 0) {
                $validator->errors()->add('errors',$errors);
                // $validator->errors = $errors;
            }
            return $errors;
        });

    }

    public function wantsJson()
    {
        return true;
    }
}
