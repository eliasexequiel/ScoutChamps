<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'firstname'             => 'required|string|max:255',
            'lastname'              => 'required|string|max:255',
            'nationality_id'        => 'sometimes|required',
            // 'residence_country_id'  => 'sometimes|required',
            'residence_city'        => 'sometimes|required',
            'phone'                 => ['sometimes','nullable','regex:/^[+]?\(?[0-9]*\)?[0-9]*$/'],
            'date_of_birth'         => 'sometimes|required|date_format:d/m/Y',
            'about'                 => 'max:13000',
            'emailTutor'            => 'sometimes|nullable|email',
            // 'firstnameTutor'        => 'sometimes|required',
            // 'lastnameTutor'         => 'sometimes|required',
            'phoneTutor'            => ['sometimes','nullable','regex:/^[+]?\(?[0-9]*\)?[0-9]*$/']
        ];

        if($this->height != '')
        {
            $rules['height_unit'] = 'required';
            if($this->height_unit == 'cm') {
                $rules['height'] = ['required','regex:/^[0-9]{2,3}?[,]?[0-9]*$/'];
            }
            if($this->height != '' && $this->height_unit == 'feets')
            {
                $rules['height'] = ["required","regex:/^[0-9]{1}?[']?[0-9]*$/"];
            }
        }

        // if($this->professional_start_date != '')
        // {
        //     $rules['professional_start_date'] = 'date_format:d/m/Y';
        // }
        
        // if($this->professional_end_date != '')
        // {
        //     $rules['professional_end_date'] = 'date_format:d/m/Y|after:professional_start_date';
        // }

        $user = \Auth::user();
        $emailCheck = ($this->email != '') && ($this->email != $user->email);
        
        if ($emailCheck) {
            $rules['email'] = 'email|max:255|unique:users';
        } else {
            $rules['email'] ='email|max:255';
        }
        if($emailCheck && $user->hasRole('scout')) {
            $rules['email'] = ['required','email','max:255','unique:users'];
            // $rules['email'] = ['regex:/^((?!' . config('mail.banned_email_providers') . ').)*$/','email','max:255','unique:users'];
        }

        return $rules;
    }

    public function messages() {
        return [
            'email.regex'  => trans('validation.mailCorporative'),
            'height.regex' => trans('validation.athlete.heightRegex'),
            'phone.regex' => trans('validation.athlete.phoneRegex'),
            'nationality_id.required' => trans('validation.athlete.nacionalidadRequired'),
            'residence_city.required' => trans('validation.athlete.residenceCityRequired')
        ];
    }
}
