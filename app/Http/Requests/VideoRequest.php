<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\VideoRule;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url' => ["sometimes","url",new VideoRule],
            'file' => 'sometimes|mimetypes:video/avi,video/x-flv,video/mp4,video/3gpp,video/x-msvideo,video/x-ms-wmv|max:300000',
        ];
    }

    public function messages() {
        return [
            'url.url' => trans('validation.videos.url'),
            'file.max' => trans('validation.videos.sizeBig',['size' => '300MB'])
        ];
    }
}
