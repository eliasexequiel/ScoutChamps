<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_en' => 'required|max:255',
            'body_en'  => 'required|max:10000',
            'title_es' => 'required|max:255',
            'body_es'  => 'required|max:10000'
        ];
    }

    public function messages() {
        return [
            'title_en.required' => trans('validation.articles.titleRequired'),
            'title_es.required' => trans('validation.articles.titleRequired'),
            'body_en.required' => trans('validation.articles.bodyRequired'),
            'body_es.required' => trans('validation.articles.bodyRequired')
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $errors = array();

            if ($validator->errors()->has('title_en') || $validator->errors()->has('title_es')) {
                $error = $validator->errors()->has('title_en') ? $validator->errors()->first('title_en') : $validator->errors()->first('title_es');  
                array_push($errors, $error); 
            }

            if ($validator->errors()->has('body_en') || $validator->errors()->has('body_es')) {
                $error = $validator->errors()->has('body_en') ? $validator->errors()->first('body_en') : $validator->errors()->first('body_es');  
                array_push($errors, $error); 
            }

            if(count($errors) > 0) {
                $validator->errors()->add('errors',$errors);
                // $validator->errors = $errors;
            }
            return $errors;
        });

    }

    public function wantsJson()
    {
        return true;
    }
}
