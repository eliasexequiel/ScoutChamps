<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\DeleteExpiredActivations::class,
        Commands\ClearLogs::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('activations:clean')->daily();
        $schedule->command('log:clear --keep-last')->daily();
        $schedule->command('telescope:prune --hours=48')->daily();
        $schedule->command('backup:clean --disable-notifications')->weeklyOn(1,'01:00');
        $schedule->command('backup:run --disable-notifications')->weeklyOn(1,'02:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
