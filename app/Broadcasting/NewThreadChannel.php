<?php

namespace App\Broadcasting;

use App\Models\User;
use Cmgmyr\Messenger\Models\Thread;

class NewThreadChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param  \App\Models\User  $user
     * @return array|bool
     */
    public function join(User $user,Thread $thread)
    {
        $thread = Thread::find($thread->id);
        return in_array($user->id,$thread->participantsUserIds());
    }
}
