<?php

namespace App\Repositories\Interfaces;

use App\Models\User;
use App\Models\Service;

interface SubscriptionRepositoryInterface
{
    public function all();

    public function getById($planId) : Service;

    public function createSubscription(User $user,$namePlan,$idPlan,$coupon,$token,$cardholder);

    public function swapSubscription(User $user, $idPlan);

    public function cancelSubscription(User $user, $idPlan);

    public function updateRolUser(User $user,$idPlan);
}
