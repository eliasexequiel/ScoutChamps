<?php

namespace App\Repositories;

use App\Models\User;
use Carbon\Carbon;
use App\Models\Service;
use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use jeremykenedy\LaravelRoles\Models\Role;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    public function all()
    {
        return Service::all();
    }

    public function getById($planId): Service
    {
        return Service::findOrFail($planId);
    }

    public function swapSubscription(User $user, $idPlan)
    {
        // if($coupon != '') {
        $user->subscription('main')
            ->skipTrial()
            ->swap($idPlan);
        // } else {
        // $user->newSubscription('main', $idPlan)->create($tokenStripe);
        // }
        // Attach role premiun to User
        $this->updateRolUser($user, $idPlan);

        return true;
    }

    public function createSubscription(User $user, $namePlan, $idPlan, $coupon, $tokenStripe, $cardholder)
    {
        $anchor = Carbon::parse('first day of next month');
        if ($namePlan == 'Monthly') {
            // El plan comienza a principio de mes solo cuando es Mensual
            if ($coupon != '') {
                $user->newSubscription('main', $idPlan)
                    ->withCoupon($coupon)
                    ->anchorBillingCycleOn($anchor->startOfDay())
                    ->withMetadata(['email' => $user->email, 'name' => $cardholder])
                    ->create($tokenStripe);
            } else {
                $user->newSubscription('main', $idPlan)
                    ->anchorBillingCycleOn($anchor->startOfDay())
                    ->withMetadata(['email' => $user->email, 'name' => $cardholder])
                    ->create($tokenStripe);
            }
        } else {
            if ($coupon != '') {
                $user->newSubscription('main', $idPlan)
                    ->withCoupon($coupon)
                    ->withMetadata(['email' => $user->email, 'name' => $cardholder])
                    ->create($tokenStripe);
            } else {
                $user->newSubscription('main', $idPlan)
                    ->withMetadata(['email' => $user->email, 'name' => $cardholder])
                    ->create($tokenStripe);
            }
        }

        // Attach role premiun to User
        $this->updateRolUser($user, $idPlan);

        return true;
    }

    public function cancelSubscription(User $user, $idPlan)
    {
        if ($user->subscribedToPlan($idPlan, 'main')) {
            $user->subscription('main')->cancelNow();
            \Log::info("SUBSCRIPTION CANCELLED: " . $user->fullName);
            // Detach role premiun to User
            // $role = Role::where('name','Athlete Premiun')->first();
            if (count($user->roles) > 1) {
                $user->detachRole($user->roles->last());
                $user->save();
            }
        }
        return true;
    }

    public function updateRolUser(User $user, $idPlan)
    {
        $user->detachAllRoles();
        $role = Role::where('name', 'Athlete')->first();
        $user->attachRole($role);
        $user->save();

        $service = Service::where('stripe_plan', $idPlan)->first();
        if ($service) {
            switch ($service->name) {
                case 'Semester':
                    $role = Role::where('name', 'Athlete Premiun Semester')->first();
                    break;
                case 'Anual':
                    $role = Role::where('name', 'Athlete Premiun Anual')->first();
                    break;
                default:
                    $role = Role::where('name', 'Athlete Premiun')->first();
                    break;
            }
            $user->attachRole($role);
            $user->save();
        }
    }
}
