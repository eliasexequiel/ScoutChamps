let mix = require('laravel-mix');
let tailwindcss = require('tailwindcss');
require('laravel-mix-alias');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .options({
      processCssUrls: false,
      postCss: [ tailwindcss('./tailwind.config.js'),require('autoprefixer') ]
    })
   .version();

   mix.alias({
      '@': '/resources/assets/js',
      '~': '/resources/assets/sass',
      '@components': '/resources/assets/js/components',
  });

  if (mix.inProduction()) {
   mix.options({
       terser: {
           terserOptions: {
               compress: {
                  drop_console: true
               }
           }
       }
   });
}