<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Entity;
use Faker\Generator as Faker;

$factory->define(Entity::class, function (Faker $faker) {
    return [
        'name' => $faker->city . ' F.C.',
        'entity_type_id' => $faker->numberBetween(1,4), 
        'verified' => true
    ];
});
