<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'firstname'                      => $faker->firstName,
        'lastname'                       => $faker->lastName,
        'uuid'                           => $faker->uuid,
        'email'                          => $faker->email,
        'password'                       => Hash::make('password'),
        'token'                          => str_random(64),
        'activated'                      => true,
        'verified'                       => true
    ];
});
