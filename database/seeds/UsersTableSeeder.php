<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $adminRole = Role::whereName('Admin')->first();
        $userRole = Role::whereName('Athlete')->first();
        $scoutRole = Role::whereName('Scout')->first();

        $fillWithUsers = false;

        // Seed test admin
        $seededAdminEmail = 'scoutchamps@gmail.com';
        $user = User::where('email', '=', $seededAdminEmail)->first();
        if ($user === null) {
            $user = User::create([
                'firstname'                      => 'Admin',
                'lastname'                       => 'ScoutChamps',
                'uuid'                           => $faker->uuid,
                'email'                          => $seededAdminEmail,
                'password'                       => Hash::make('bolivia!entrenadores2019'),
                'token'                          => str_random(64),
                'activated'                      => true,
                'email_verified_at'              => \Carbon\Carbon::now(),
                'verified'                       => true
            ]);
            $user->attachRole($adminRole);
            $user->save();
        }

        if ($fillWithUsers) {
            // Seed test user
            $user = User::where('email', '=', 'roman@mail.com')->first();
            if ($user === null) {
                $user = User::create([
                    'firstname'                      => 'roman',
                    'lastname'                       => 'riquelme',
                    'uuid'                           => $faker->uuid,
                    'email'                          => 'roman@mail.com',
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'email_verified_at'              => \Carbon\Carbon::now(),
                    'verified'                       => true
                ]);

                factory(App\Models\Athlete::class)->create(['user_id' => $user->id]);
                $user->attachRole($userRole);
                $user->save();
            }

            // Seed SCOUT user
            $scoutEmail = 'scout@mail.com';
            $user = User::where('email', '=', $scoutEmail)->first();
            if ($user === null) {
                $user = User::create([
                    'firstname'                      => 'Guillermo',
                    'lastname'                       => 'Mariago',
                    'uuid'                           => $faker->uuid,
                    'email'                          => $scoutEmail,
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'email_verified_at'              => \Carbon\Carbon::now(),
                    'verified'                       => true
                ]);

                factory(App\Models\Scout::class)->create(['user_id' => $user->id]);
                $user->attachRole($scoutRole);
                $user->save();
            }

            for ($i = 0; $i < 50; $i++) {
                $user = User::create([
                    'firstname'                      => $faker->firstName,
                    'lastname'                       => $faker->lastName,
                    'uuid'                           => $faker->uuid,
                    'email'                          => $faker->email,
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'email_verified_at'              => \Carbon\Carbon::now(),
                    'verified'                       => true
                ]);

                factory(App\Models\Athlete::class)->create(['user_id' => $user->id]);
                $user->attachRole($userRole);
                $user->save();
            }

            // SCOUTS
            for ($i = 0; $i < 20; $i++) {
                $user = User::create([
                    'firstname'                      => $faker->firstName,
                    'lastname'                       => $faker->lastName,
                    'uuid'                           => $faker->uuid,
                    'email'                          => $faker->email,
                    'password'                       => Hash::make('password'),
                    'token'                          => str_random(64),
                    'activated'                      => true,
                    'email_verified_at'              => \Carbon\Carbon::now(),
                    'verified'                       => true
                ]);

                factory(App\Models\Scout::class)->create(['user_id' => $user->id]);
                $user->attachRole($scoutRole);
                $user->save();
            }
        }
    }
}
