<?php

use Illuminate\Database\Seeder;

class EntityTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('entities_types')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $entities = [
            ['name' => 'Club'],
            ['name' => 'Institute'],
            ['name' => 'University'],
            ['name' => 'Sponsor'],
            ['name' => 'Company']
        ];
        \DB::table('entities_types')->insert($entities);
    }
}
