<?php

use Illuminate\Database\Seeder;

class SportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('sports')->truncate();
        \DB::table('sport_fields')->truncate();
        \DB::table('sport_fields_options_select')->truncate();
        \DB::table('sport_fields_athletes')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $sports = [
            [
                'name' => 'Basketball',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Primary Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Point Guard(PG)', "es" => "Base"])],
                            ['name' => json_encode(["en" => 'Shooting Guard(SG)', "es" => "Escolta"])],
                            ['name' => json_encode(["en" => 'Small Forward(SF)', "es" => "Alero"])],
                            ['name' => json_encode(["en" => 'Power Forward(PF)', "es" => "Ala Pivot"])],
                            ['name' => json_encode(["en" => 'Center (C)', "es" => "Pivot"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Point Guard(PG)', "es" => "Base"])],
                            ['name' => json_encode(["en" => 'Shooting Guard(SG)', "es" => "Escolta"])],
                            ['name' => json_encode(["en" => 'Small Forward(SF)', "es" => "Alero"])],
                            ['name' => json_encode(["en" => 'Power Forward(PF)', "es" => "Ala Pivot"])],
                            ['name' => json_encode(["en" => 'Center (C)', "es" => "Pivot"])]
                        ]
                    ]
                ]
            ],
            [
                'name' => 'Baseball',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Primary Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Pitcher', "es" => "Lanzador"])],
                            ['name' => json_encode(["en" => 'Catcher', "es" => "Receptor"])],
                            ['name' => json_encode(["en" => 'First Baseman', "es" => "Primera Base"])],
                            ['name' => json_encode(["en" => 'Second Baseman', "es" => "Segunda Base"])],
                            ['name' => json_encode(["en" => 'Thirds Baseman', "es" => "Tercera Base"])],
                            ['name' => json_encode(["en" => 'Short Stop', "es" => "Campocorto / Parador en Corto"])],
                            ['name' => json_encode(["en" => 'Left Fielder', "es" => "Jardinero Izquierdo"])],
                            ['name' => json_encode(["en" => 'Center Fielder', "es" => "Jardinero Central"])],
                            ['name' => json_encode(["en" => 'Hitter', "es" => "Bateador"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Pitcher', "es" => "Lanzador"])],
                            ['name' => json_encode(["en" => 'Catcher', "es" => "Receptor"])],
                            ['name' => json_encode(["en" => 'First Baseman', "es" => "Primera Base"])],
                            ['name' => json_encode(["en" => 'Second Baseman', "es" => "Segunda Base"])],
                            ['name' => json_encode(["en" => 'Thirds Baseman', "es" => "Tercera Base"])],
                            ['name' => json_encode(["en" => 'Short Stop', "es" => "Campocorto / Parador en Corto"])],
                            ['name' => json_encode(["en" => 'Left Fielder', "es" => "Jardinero Izquierdo"])],
                            ['name' => json_encode(["en" => 'Center Fielder', "es" => "Jardinero Central"])],
                            ['name' => json_encode(["en" => 'Hitter', "es" => "Bateador"])]
                        ]
                    ]
                ]
            ],
            // [
            //     'name' => 'Boxing',
            //     'fields' => []
            // ],
            // [
            //     'name' => 'Cycling',
            //     'fields' => []
            // ],
            [
                'name' => 'Swimming and diving',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Sprints', "es" => "Corta Distancia"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '50m', "es" => "50m"])],
                            ['name' => json_encode(["en" => '100m', "es" => "100m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Middle Distance', "es" => "Media Distancia"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '200m', "es" => "200m"])],
                            ['name' => json_encode(["en" => '400m', "es" => "400m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Long Distance', "es" => "Larga Distancia"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '800m', "es" => "800m"])],
                            ['name' => json_encode(["en" => '1500m', "es" => "1500m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Relay', "es" => "Relevo"]),
                        'fieldtype'   => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '4 x 100m (Relay)', "es" => "4 x 100m (Relevo)"])],
                            ['name' => json_encode(["en" => '4 x 200m (Relay)', "es" => "4 x 200m (Relevo)"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Styles', "es" => "Estilos"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Butterfly', "es" => "Estilo Mariposa"])],
                            ['name' => json_encode(["en" => 'Backstroke', "es" => "Estilo Espalda"])],
                            ['name' => json_encode(["en" => 'Breststroke', "es" => "Estilo Pecho"])],
                            ['name' => json_encode(["en" => 'Freestyle / Front Crawl', "es" => "Estilo Libre / Crol"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Diving', "es" => "Clavados"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Forward Diving', "es" => "Hacia Delante"])],
                            ['name' => json_encode(["en" => 'Backward Diving', "es" => "Hacia Atrás"])],
                            ['name' => json_encode(["en" => 'Reverse Diving', "es" => "Inverso"])],
                            ['name' => json_encode(["en" => 'Inward Diving', "es" => "Hacia adentro"])],
                            ['name' => json_encode(["en" => 'Twisting Diving', "es" => "Con giro"])],
                            ['name' => json_encode(["en" => 'Armstand Diving', "es" => "Equilibrio de mano"])]
                        ]
                    ],
                ]
            ],
            [
                'name' => 'Football',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Primary Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Center(C)', "es" => "Centro"])],
                            ['name' => json_encode(["en" => 'Offensive Guard(OG)', "es" => "Guardia Ofensivo"])],
                            ['name' => json_encode(["en" => 'Offensive Tackle(OT)', "es" => "Tacle Ofensivo"])],
                            ['name' => json_encode(["en" => 'Tight End(TE)', "es" => "Ala Cerrada"])],
                            ['name' => json_encode(["en" => 'Wide Receiver(WR)', "es" => "Ala Abierta"])],
                            ['name' => json_encode(["en" => 'Fullback(FB)', "es" => "Corredor de Poder"])],
                            ['name' => json_encode(["en" => 'Running Back(RB)', "es" => "Corredor"])],
                            ['name' => json_encode(["en" => 'Tailback(TB)', "es" => "Corredor Profundo"])],
                            ['name' => json_encode(["en" => 'Halfback(HB)', "es" => "Corredor Medio"])],
                            ['name' => json_encode(["en" => 'Wingback(WB)', "es" => "Wingback"])],
                            ['name' => json_encode(["en" => 'Slotback(SB)', "es" => "Slotback"])],
                            ['name' => json_encode(["en" => 'Quarterback(QB)', "es" => "Mariscal de Campo"])],
                            ['name' => json_encode(["en" => 'Defensive End(DE)', "es" => "Ala Defensivo"])],
                            ['name' => json_encode(["en" => 'Defensive Tackle(DT)', "es" => "Tacle Defensivo"])],
                            ['name' => json_encode(["en" => 'Nose Guard(NG)', "es" => "Tacle o Guardia Nariz"])],
                            ['name' => json_encode(["en" => 'Linebacker(LB)', "es" => "Apoyador"])],
                            ['name' => json_encode(["en" => 'Cornerback(CB)', "es" => "Esquinero"])],
                            ['name' => json_encode(["en" => 'Safety(FS or SS)', "es" => "Profundo"])],
                            ['name' => json_encode(["en" => 'Defensive Back(DB)', "es" => "Back Defensivo"])],
                            ['name' => json_encode(["en" => 'Nickelback(NB)', "es" => "Defensa Nickel"])],
                            ['name' => json_encode(["en" => 'Dimeback', "es" => "Defensa Dime"])],
                            ['name' => json_encode(["en" => 'Kicker(K)', "es" => "Pateador"])],
                            ['name' => json_encode(["en" => 'Punter(P)', "es" => "Pateador de Despeje"])],
                            ['name' => json_encode(["en" => 'Holder(H)', "es" => "Sujetador"])],
                            ['name' => json_encode(["en" => 'Long Snapper(LP)', "es" => "Centro Largo"])],
                            ['name' => json_encode(["en" => 'Kickoff Returner(KR)', "es" => "Regresador de Patada"])],
                            ['name' => json_encode(["en" => 'Upback(UB)', "es" => "Upback"])],
                            ['name' => json_encode(["en" => 'Punt Return(PR)', "es" => "Regresador de Despeje"])],
                            ['name' => json_encode(["en" => 'Gunner(G)', "es" => "Gunner"])],
                            ['name' => json_encode(["en" => 'Wedge Buster(WB)', "es" => "Wedge Buster"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Center(C)', "es" => "Centro"])],
                            ['name' => json_encode(["en" => 'Offensive Guard(OG)', "es" => "Guardia Ofensivo"])],
                            ['name' => json_encode(["en" => 'Offensive Tackle(OT)', "es" => "Tacle Ofensivo"])],
                            ['name' => json_encode(["en" => 'Tight End(TE)', "es" => "Ala Cerrada"])],
                            ['name' => json_encode(["en" => 'Wide Receiver(WR)', "es" => "Ala Abierta"])],
                            ['name' => json_encode(["en" => 'Fullback(FB)', "es" => "Corredor de Poder"])],
                            ['name' => json_encode(["en" => 'Running Back(RB)', "es" => "Corredor"])],
                            ['name' => json_encode(["en" => 'Tailback(TB)', "es" => "Corredor Profundo"])],
                            ['name' => json_encode(["en" => 'Halfback(HB)', "es" => "Corredor Medio"])],
                            ['name' => json_encode(["en" => 'Wingback(WB)', "es" => "Wingback"])],
                            ['name' => json_encode(["en" => 'Slotback(SB)', "es" => "Slotback"])],
                            ['name' => json_encode(["en" => 'Quarterback(QB)', "es" => "Mariscal de Campo"])],
                            ['name' => json_encode(["en" => 'Defensive End(DE)', "es" => "Ala Defensivo"])],
                            ['name' => json_encode(["en" => 'Defensive Tackle(DT)', "es" => "Tacle Defensivo"])],
                            ['name' => json_encode(["en" => 'Nose Guard(NG)', "es" => "Tacle o Guardia Nariz"])],
                            ['name' => json_encode(["en" => 'Linebacker(LB)', "es" => "Apoyador"])],
                            ['name' => json_encode(["en" => 'Cornerback(CB)', "es" => "Esquinero"])],
                            ['name' => json_encode(["en" => 'Safety(FS or SS)', "es" => "Profundo"])],
                            ['name' => json_encode(["en" => 'Defensive Back(DB)', "es" => "Back Defensivo"])],
                            ['name' => json_encode(["en" => 'Nickelback(NB)', "es" => "Defensa Nickel"])],
                            ['name' => json_encode(["en" => 'Dimeback', "es" => "Defensa Dime"])],
                            ['name' => json_encode(["en" => 'Kicker(K)', "es" => "Pateador"])],
                            ['name' => json_encode(["en" => 'Punter(P)', "es" => "Pateador de Despeje"])],
                            ['name' => json_encode(["en" => 'Holder(H)', "es" => "Sujetador"])],
                            ['name' => json_encode(["en" => 'Long Snapper(LP)', "es" => "Centro Largo"])],
                            ['name' => json_encode(["en" => 'Kickoff Returner(KR)', "es" => "Regresador de Patada"])],
                            ['name' => json_encode(["en" => 'Upback(UB)', "es" => "Upback"])],
                            ['name' => json_encode(["en" => 'Punt Return(PR)', "es" => "Regresador de Despeje"])],
                            ['name' => json_encode(["en" => 'Gunner(G)', "es" => "Gunner"])],
                            ['name' => json_encode(["en" => 'Wedge Buster(WB)', "es" => "Wedge Buster"])]
                        ]
                    ],
                ]
            ],
            [
                'name' => 'Soccer',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Primary Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Goalkeeper(GK)', "es" => "Arquero"])],
                            ['name' => json_encode(["en" => 'Center Back(CB)', "es" => "Defensor Central"])],
                            ['name' => json_encode(["en" => 'Right Back(RB)', "es" => "Lateral Derecho"])],
                            ['name' => json_encode(["en" => 'Left Back(LB)', "es" => "Lateral Izquierdo"])],
                            ['name' => json_encode(["en" => 'Center Defensive Mid(CBM)', "es" => "Mediocampista Central Defensivo"])],
                            ['name' => json_encode(["en" => 'Center Mid(CM)', "es" => "Mediocampista Central"])],
                            ['name' => json_encode(["en" => 'Right Mid(RM)', "es" => "Mediocampista por derecha"])],
                            ['name' => json_encode(["en" => 'Left Mid(LD)', "es" => "Mediocampista por izquierda"])],
                            ['name' => json_encode(["en" => 'Center Attacking Mid(CAM)', "es" => "Delantero Centro Medio"])],
                            ['name' => json_encode(["en" => 'Right Wing(RW)', "es" => "Mediapunta derecho"])],
                            ['name' => json_encode(["en" => 'Left Wing(LW)', "es" => "Mediapunta izquierdo"])],
                            ['name' => json_encode(["en" => 'Striker(ST)', "es" => "Centro Delantero"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Goalkeeper(GK)', "es" => "Arquero"])],
                            ['name' => json_encode(["en" => 'Center Back(CB)', "es" => "Defensor Central"])],
                            ['name' => json_encode(["en" => 'Right Back(RB)', "es" => "Lateral Derecho"])],
                            ['name' => json_encode(["en" => 'Left Back(LB)', "es" => "Lateral Izquierdo"])],
                            ['name' => json_encode(["en" => 'Center Defensive Mid(CBM)', "es" => "Mediocampista Central Defensivo"])],
                            ['name' => json_encode(["en" => 'Center Mid(CM)', "es" => "Mediocampista Central"])],
                            ['name' => json_encode(["en" => 'Right Mid(RM)', "es" => "Mediocampista por derecha"])],
                            ['name' => json_encode(["en" => 'Left Mid(LD)', "es" => "Mediocampista por izquierda"])],
                            ['name' => json_encode(["en" => 'Center Attacking Mid(CAM)', "es" => "Delantero Centro Medio"])],
                            ['name' => json_encode(["en" => 'Right Wing(RW)', "es" => "Mediapunta derecho"])],
                            ['name' => json_encode(["en" => 'Left Wing(LW)', "es" => "Mediapunta izquierdo"])],
                            ['name' => json_encode(["en" => 'Striker(ST)', "es" => "Centro Delantero"])]
                        ]
                    ]
                ]
            ],
            [
                'name' => "Tennis",
                'fields' => []
            ],
            [
                'name' => "Rugby",
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Primary Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Loose-head prop', "es" => "Pilier Izquierdo"])],
                            ['name' => json_encode(["en" => 'Hooker', "es" => "Talonador"])],
                            ['name' => json_encode(["en" => 'Tight-head prop', "es" => "Pilier Derecho"])],
                            ['name' => json_encode(["en" => 'Second row', "es" => "Segunda Línea Izquierdo"])],
                            ['name' => json_encode(["en" => 'Second row', "es" => "Segunda Línea Derecho"])],
                            ['name' => json_encode(["en" => 'Blind-side flanker', "es" => "Tercera Izquierdo"])],
                            ['name' => json_encode(["en" => 'Open-side flanker', "es" => "Tercera Derecho"])],
                            ['name' => json_encode(["en" => 'Number 8', "es" => "Tercera Centro"])],
                            ['name' => json_encode(["en" => 'Scrum-half', "es" => "Medio Melé"])],
                            ['name' => json_encode(["en" => 'Fly-half', "es" => "Apertura"])],
                            ['name' => json_encode(["en" => 'Left Wing', "es" => "Ala Izquierdo"])],
                            ['name' => json_encode(["en" => 'Inside Center', "es" => "Primer Centro"])],
                            ['name' => json_encode(["en" => 'Outside Center', "es" => "Segundo Centro"])],
                            ['name' => json_encode(["en" => 'Right Wing', "es" => "Ala Derecho"])],
                            ['name' => json_encode(["en" => 'Full-back', "es" => "Zaguero"])],

                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Loose-head prop', "es" => "Pilier Izquierdo"])],
                            ['name' => json_encode(["en" => 'Hooker', "es" => "Talonador"])],
                            ['name' => json_encode(["en" => 'Tight-head prop', "es" => "Pilier Derecho"])],
                            ['name' => json_encode(["en" => 'Second row', "es" => "Segunda Línea Izquierdo"])],
                            ['name' => json_encode(["en" => 'Second row', "es" => "Segunda Línea Derecho"])],
                            ['name' => json_encode(["en" => 'Blind-side flanker', "es" => "Tercera Izquierdo"])],
                            ['name' => json_encode(["en" => 'Open-side flanker', "es" => "Tercera Derecho"])],
                            ['name' => json_encode(["en" => 'Number 8', "es" => "Tercera Centro"])],
                            ['name' => json_encode(["en" => 'Scrum-half', "es" => "Medio Melé"])],
                            ['name' => json_encode(["en" => 'Fly-half', "es" => "Apertura"])],
                            ['name' => json_encode(["en" => 'Left Wing', "es" => "Ala Izquierdo"])],
                            ['name' => json_encode(["en" => 'Inside Center', "es" => "Primer Centro"])],
                            ['name' => json_encode(["en" => 'Outside Center', "es" => "Segundo Centro"])],
                            ['name' => json_encode(["en" => 'Right Wing', "es" => "Ala Derecho"])],
                            ['name' => json_encode(["en" => 'Full-back', "es" => "Zaguero"])],

                        ]
                    ]
                ]
            ],
            [
                'name' => "Golf",
                'fields' => []
            ],
            [
                'name' => 'Track and field',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Sprints', "es" => "Carreras de Velocidad"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '60m', "es" => "60m"])],
                            ['name' => json_encode(["en" => '100m', "es" => "100m"])],
                            ['name' => json_encode(["en" => '200m', "es" => "200m"])],
                            ['name' => json_encode(["en" => '400m', "es" => "400m"])],
                            ['name' => json_encode(["en" => '4 x 100m Relay', "es" => "4 x 100m Relevo"])],
                            ['name' => json_encode(["en" => '4 x 400m Relay', "es" => "4 x 400m Relevo"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Middle Distance', "es" => "Media Distancia"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '500m', "es" => "500m"])],
                            ['name' => json_encode(["en" => '600m', "es" => "600m"])],
                            ['name' => json_encode(["en" => '800m', "es" => "800m"])],
                            ['name' => json_encode(["en" => '1000m', "es" => "1000m"])],
                            ['name' => json_encode(["en" => '1200m', "es" => "1200m"])],
                            ['name' => json_encode(["en" => '1500m', "es" => "1500m"])],
                            ['name' => json_encode(["en" => '1600m (1 mile)', "es" => "1600m (1 milla)"])],
                            ['name' => json_encode(["en" => '2000m', "es" => "2000m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Long Distance', "es" => "Larga Distancia"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '3000m', "es" => "3000m"])],
                            ['name' => json_encode(["en" => '3200m', "es" => "3200m"])],
                            ['name' => json_encode(["en" => '5000m', "es" => "5000m"])],
                            ['name' => json_encode(["en" => '10000m', "es" => "10000m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Cross Country', "es" => "Carrera a Campo"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => '1.0-1.2 mi', "es" => "1.7-2.0 Km"])],
                            ['name' => json_encode(["en" => '2.5-3.7 mi', "es" => "4-6 Km"])],
                            ['name' => json_encode(["en" => '3.7-5.0 mi', "es" => "6-8 Km"])],
                            ['name' => json_encode(["en" => '5.0-6.2 mi', "es" => "8-10 Km"])],
                            ['name' => json_encode(["en" => '6.2-7.5 mi', "es" => "10-12 Km"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Hurdling', "es" => "Salto de Vallas"]),
                        'fieldtype'   => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Race Walking', "es" => "Carrera Caminando"])],
                            ['name' => json_encode(["en" => '50m', "es" => "50m"])],
                            ['name' => json_encode(["en" => '100m', "es" => "100m"])],
                            ['name' => json_encode(["en" => '110m', "es" => "110m"])],
                            ['name' => json_encode(["en" => '400m', "es" => "400m"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Jumps', "es" => "Saltos"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Long Jump', "es" => "Salto Largo"])],
                            ['name' => json_encode(["en" => 'Triple Jump', "es" => "Salto Triple"])],
                            ['name' => json_encode(["en" => 'High Jump', "es" => "Salto Alto"])],
                            ['name' => json_encode(["en" => 'Pole Vault', "es" => "Salto con Pértiga"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Throwing', "es" => "Lanzamientos"]),
                        'fieldtype'      => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Shot Put', "es" => "Lanzamiento de Peso"])],
                            ['name' => json_encode(["en" => 'Javelin Throw', "es" => "Lanzamiento de Jabalina"])],
                            ['name' => json_encode(["en" => 'Discus Throw', "es" => "Lanzamiento de Disco"])],
                            ['name' => json_encode(["en" => 'Hammer Throw', "es" => "Lanzamiento de Martillo"])]
                        ]
                    ],
                ]
            ],
            [
                'name' => 'Voleyball',
                'fields' => [
                    [
                        'name' => json_encode(["en" => 'Position', "es" => "Posición primaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Left Side Hitter / Wing Spiker / Left Side', "es" => "Remachador Exterior / Banda Izquierda"])],
                            ['name' => json_encode(["en" => 'Right Side Hitter / Wing Spiker / Right Side', "es" => "Remachador Derecho / Banda Derecha"])],
                            ['name' => json_encode(["en" => 'Opposite Hitter / Attacker', "es" => "Remachador Opuesto / Atacante"])],
                            ['name' => json_encode(["en" => 'Setter', "es" => "Armador / Levantador"])],
                            ['name' => json_encode(["en" => 'Middle Blocker / Center / Middle Hitter', "es" => "Bloque Centro / Central"])],
                            ['name' => json_encode(["en" => 'Libero', "es" => "Libero"])],
                            ['name' => json_encode(["en" => 'Defensive Specialist', "es" => "Especialista Defensivo"])]
                        ]
                    ],
                    [
                        'name' => json_encode(["en" => 'Secondary Position', "es" => "Posición secundaria"]),
                        'fieldtype' => 'select',
                        'options_select' => [
                            ['name' => json_encode(["en" => 'Left Side Hitter / Wing Spiker / Left Side', "es" => "Remachador Exterior / Banda Izquierda"])],
                            ['name' => json_encode(["en" => 'Right Side Hitter / Wing Spiker / Right Side', "es" => "Remachador Derecho / Banda Derecha"])],
                            ['name' => json_encode(["en" => 'Opposite Hitter / Attacker', "es" => "Remachador Opuesto / Atacante"])],
                            ['name' => json_encode(["en" => 'Setter', "es" => "Armador / Levantador"])],
                            ['name' => json_encode(["en" => 'Middle Blocker / Center / Middle Hitter', "es" => "Bloque Centro / Central"])],
                            ['name' => json_encode(["en" => 'Libero', "es" => "Libero"])],
                            ['name' => json_encode(["en" => 'Defensive Specialist', "es" => "Especialista Defensivo"])]
                        ]
                    ]
                ]
            ],
        ];
        foreach ($sports as $i => $s) {
            \DB::table('sports')->insert(['name' => $s['name']]);

            foreach ($s['fields'] as $j => $f) {
                $idSportField = \DB::table('sport_fields')->insertGetId([
                    'name'           => $f['name'],
                    'fieldtype'      => $f['fieldtype'],
                    'sport_id'        => $i + 1
                ]);
                foreach ($f['options_select'] as $option) {
                    \DB::table('sport_fields_options_select')->insert([
                        'name' => $option['name'],
                        'sport_field_id' => $idSportField
                    ]);
                }
            }
        }
    }
}
