<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("message")->nullable();
            $table->integer('reported_id')->nullable();
            $table->string('reported_type')->nullable();
            $table->boolean('reported_blocked')->default(false); // True si decidimos bloquear el objeto denunciado
            $table->boolean('solved')->default(false); // True si se resolvió eliminar o no el objeto

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
