<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToScoutEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scout_entities', function (Blueprint $table) {
            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->boolean('isCurrent')->default(0);

            $table->string('info_aditional',15000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scout_entities', function (Blueprint $table) {
            //
        });
    }
}
