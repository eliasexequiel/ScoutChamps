<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAthletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athletes', function (Blueprint $table) {
            $table->bigIncrements('id');

            // $table->string('nationality')->nullable();
            $table->unsignedBigInteger('nationality_id')->nullable();
            $table->foreign('nationality_id')->references('id')->on('countries');
            
            $table->unsignedBigInteger('second_nationality_id')->nullable();
            $table->foreign('second_nationality_id')->references('id')->on('countries');

            $table->unsignedBigInteger('residence_country_id')->nullable();
            $table->foreign('residence_country_id')->references('id')->on('countries');

            $table->string('residence_city')->nullable();

            $table->date('date_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('gender')->nullable();

            $table->unsignedBigInteger('sport_id')->nullable();
            $table->foreign('sport_id')->references('id')->on('sports');

            $table->string('sporting_goal')->nullable();
            $table->date('professional_start_date')->nullable();
            $table->date('professional_end_date')->nullable();
            $table->string('height')->nullable();
            $table->string('height_unit')->nullable();
            $table->string('status')->nullable(); // Youth Level - Professional

            // TUTOR INFO
            $table->string('emailTutor')->nullable();
            $table->string('firstnameTutor')->nullable();
            $table->string('lastnameTutor')->nullable();
            $table->string('phoneTutor')->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athletes');
    }
}
