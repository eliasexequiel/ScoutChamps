<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportFieldsAthletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sport_fields_athletes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('sport_field_id');
            $table->foreign('sport_field_id')->references('id')->on('sport_fields')->onDelete('cascade');

            $table->unsignedBigInteger('sport_id');
            $table->foreign('sport_id')->references('id')->on('sports');

            $table->unsignedBigInteger('athlete_id');
            $table->foreign('athlete_id')->references('id')->on('athletes');

            $table->string('value')->nullable();
            
            $table->boolean('is_secondary')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sport_fields_athletes');
    }
}
