<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_site', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('value',15000)->nullable();
            $table->timestamps();
        });

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('config_site')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $config = [
            ['name' => 'verifyUsers', 'value' => true],
            ['name' => 'verifyScoutsClubs', 'value' => true],
            ['name' => 'verifyClubs', 'value' => true],
            ['name' => 'contactEmail', 'value' => 'contact@scoutchamps.com'],
            ['name' => 'contactPhone', 'value' => '+3123123'],
            ['name' => 'sectionClubsInHome', 'value' => false],
            ['name' => 'terms', 'value' => null],
            ['name' => 'terms_es', 'value' => null],
            ['name' => 'privacy', 'value' => null],
            ['name' => 'privacy_es', 'value' => null],
            ['name' => 'limitImagesAthleteFree', 'value' => 30],
            ['name' => 'limitImagesScoutFree', 'value' => 30]
        ];
        \DB::table('config_site')->insert($config);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_site');
    }
}
