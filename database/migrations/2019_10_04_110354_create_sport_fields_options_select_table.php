<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportFieldsOptionsSelectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sport_fields_options_select', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('name');
            
            $table->unsignedBigInteger('sport_field_id');
            $table->foreign('sport_field_id')->references('id')->on('sport_fields');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sport_fields_options_select');
    }
}
